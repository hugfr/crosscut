library(RPostgreSQL)

#GLOBAL PARAMETER
drv = dbDriver("PostgreSQL")
host = "10.38.192.30"
user = "postgres"
pwd = "lceslt"
db = "db"

alphafill = 50
alphaborder = 70

no_more_no_less = function (x, lim){
  if (x > lim[2]){
    x = lim[2]
  } else if (x < lim[1]){
    x = lim[1]
  }
  return(x)
}

plot_wbt = function (dst_dir,envid, name, alt, campdate, period){
  
  #####GRAPH PARAMETERS
  scenarios = c("historical", "rcp26", "rcp45", "rcp85")
  cols = c("#878787","#80cdc1", "#08519c", "#cb181d")
  types = c("poly", "lines")
  delta = (period - 1)/2
  
  #XLIM and YLIM
  query = paste0("
  select min(season) + ",delta," , max(season) - ",delta,"
  from crocus.wbt
  ")
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  xlim <- dbGetQuery(con, query)
  dbDisconnect(con)
  xlim = as.numeric(xlim)
  
  prettyx = pretty(xlim)
  
  #GRAPH OUTPUT
  png(width = 800, height = 1200, paste0(dst_dir,"wbt_",envid,"_",alt,"_",campdate,"_",xlim[1],"_",xlim[2],".png"), res = 150)
  layout(matrix(c(1,2,3),3,1))
  
  wbts = c('-1-4', '-4-6', '-6')
  for (wbt in wbts){
      par(mar=c(3,3,2,0))
      
      query = paste0("
        with a as (
            select a.envid, c.* from crosscut.loc_weight_env a
            join crosscut.envelopes_3035 b on a.envid = b.envid
            join crocus.loc_zonage_safran_france c on gid = loc
            where a.envid = ",envid," and alt = ",alt,"
        ), b as (
            select distinct b.* from crocus.loc_zonage_safran_france b
            join a on a.id_mf = b.id_mf and a.alt = b.alt and b.slope = 0
        )
        
        select distinct 0, ceiling(max(nbh)/20) * 20 nbh
        from crocus.wbt
        join b on gid = loc
        where wbt = '",wbt,"'
      ")
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      ylim <- dbGetQuery(con, query)
      dbDisconnect(con)
      ylim = as.numeric(ylim)
      
      ######PLOTTING
      #SETUP DEVICE
      if (campdate == '0111-2012'){
        camp = "Avant saison"
      } else if (campdate == '2112-3101'){
        camp = "Confortement saison"
      } else if (campdate == '0102-3103'){
        camp = "Fin de saison"
      }
        
      if (wbt == '-1-4'){
        wbtr = "(température humide entre -1°C et -4°C)"
      } else if (wbt == '-4-6'){
        wbtr = "(température humide entre -4°C et -6°C)"
      } else if (wbt == '-6'){
        wbtr = "(température humide moins de -6°C)"
      }
      main = paste0(camp, " ", wbtr)
      plot(0,0, type = 'n', main = main, cex.main = .8, xlim = xlim, ylim = ylim, , axes = F, xlab = NA, ylab =NA)
      
      #PLOT BACKGROUND
      for (i in 1:(length(prettyx) - 1)){
        if (i / 2 == round(i / 2)){
          col = "#F0F0F0"
        } else {
          col = "#FFFFFF"
        }
        polygon(c(prettyx[i],prettyx[i],prettyx[i+1],prettyx[i+1]), c(ylim,rev(ylim)), col = col, border = NA)
      }
      for (j in pretty(ylim)[2:length(pretty(ylim))]){
        lines(xlim, c(j,j), col = 'lightgrey', lty = 2, lwd = 1)
      }
      
      #PLOT EXPERIMENT TIME SERIES
      for (t in types){
        i = 0
        print(t)
        for (scenario in scenarios){
          print(scenario)
          i = i + 1
          
          
        query = paste0("  
        with hdata as(
            with a as (
                select a.envid, c.* from crosscut.loc_weight_env a
                join crosscut.envelopes_3035 b on a.envid = b.envid
                join crocus.loc_zonage_safran_france c on gid = loc
                where a.envid = ",envid," and alt = ",alt,"
            ), b as (
                select distinct b.* from crocus.loc_zonage_safran_france b
                join a on a.id_mf = b.id_mf and a.alt = b.alt and b.slope = 0
            )
            
            select distinct alt, season, model, scenario, period, wbt, avg(nbh) nbh
            from crocus.wbt
            join b on gid = loc
            where period = '",campdate,"' and wbt = '",wbt,"' and scenario = '",scenario,"'
            group by alt, season, model, scenario, period, wbt
        ), a as (
            select a.season, a.model,
            min(min(a.season)) over() smin, max(max(a.season)) over() smax,
            avg(b.nbh) mean
            from hdata a
            join hdata b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
            and a.alt = b.alt and a.model = b.model and a.scenario = b.scenario and a.period = b.period and a.wbt = b.wbt
            group by a.season, a.model
            )
                         
        select season, avg(mean) mean, stddev(mean) stddev
        from a 
        where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
        group by season
        order by season"
          )
          con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
          data <- dbGetQuery(con, query)
          dbDisconnect(con)
          
          if (scenario == "historical"){
            query = paste0("
                            with hdata as(
                                with a as (
                                    select a.envid, c.* from crosscut.loc_weight_env a
                                    join crosscut.envelopes_3035 b on a.envid = b.envid
                                    join crocus.loc_zonage_safran_france c on gid = loc
                                    where a.envid = ",envid," and alt = ",alt,"
                                ), b as (
                                    select distinct b.* from crocus.loc_zonage_safran_france b
                                    join a on a.id_mf = b.id_mf and a.alt = b.alt and b.slope = 0
                                )
                                
                                select distinct alt, season, model, scenario, period, wbt, avg(nbh) nbh
                                from crocus.wbt
                                join b on gid = loc
                                where period = '",campdate,"' and wbt = '",wbt,"'
                                group by alt, season, model, scenario, period, wbt
                            ), a as (
                           select max(season) - ",delta," hmax from hdata
                           where scenario = '",scenario,"'
                           ), b as (
                           select min(season) + ",delta," emin from hdata
                           where scenario != '",scenario,"' and scenario != 'reanalysis'
                           ), c as (
                           select c.season, c.model, avg(d.nbh) mean
                           from hdata c
                           join hdata d on  d.season >= c.season - ",delta," and d.season <= c.season +",delta,"
                           and d.alt = c.alt and d.model = c.model and d.scenario = c.scenario and d.period = c.period and d.wbt = c.wbt
                           and c.season > (select hmax from a) and c.season < (select emin from b)
                           group by c.season, c.model
                           )
                           select c.season, avg(mean) mean, stddev(mean) stddev
                           from c
                           group by season
                           order by season
                           "
            )
            con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
            hist <- dbGetQuery(con, query)
            dbDisconnect(con)
            data = rbind(data,hist)
          }
          
          # Create csv file
          write.csv(data, paste0(dst_dir,"wbt",wbt,"_",envid,"_",alt,"_",campdate,"_",xlim[1],"_",xlim[2],"_",scenario,".csv"), row.names = FALSE)
          
          #MODIF A REFLECHIR: LANCER BOUCLE TYPE ICI (UTILISATION DU MEME DATAFRAME
          if (t == "poly"){
            polygon(c(data[,1], rev(data[,1])), c(sapply(data[,2] - data[,3],no_more_no_less,ylim), rev(sapply(data[,2] + data[,3],no_more_no_less,ylim))),
                    border = paste0(cols[i],alphaborder), col = paste0(cols[i], alphafill))
          } else {
            par(new = T)
            plot(data[,1], data[,2], type = "l", col = cols[i], lwd = 2,
                 xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
            )
          }
        }
      }
      
      #ADD SAFRAN REANALYSIS ROLLING MEAN
      query = paste0(" with hdata as(
                        with a as (
                            select a.envid, c.* from crosscut.loc_weight_env a
                            join crosscut.envelopes_3035 b on a.envid = b.envid
                            join crocus.loc_zonage_safran_france c on gid = loc
                            where a.envid = ",envid," and alt = ",alt,"
                        ), b as (
                            select distinct b.* from crocus.loc_zonage_safran_france b
                            join a on a.id_mf = b.id_mf and a.alt = b.alt and b.slope = 0
                        )
                        
                        select distinct alt, season, model, scenario, period, wbt, avg(nbh) nbh
                        from crocus.wbt
                        join b on gid = loc
                        where period = '",campdate,"' and wbt = '",wbt,"'
                        and scenario = 'reanalysis' and model = 'safran'
                        group by alt, season, model, scenario, period, wbt
                    ), a as (
                     select a.season, min(min(a.season)) over() smin, max(max(a.season)) over() smax,
                     avg(b.nbh) mean, stddev(b.nbh) stddev
                     from hdata a
                     join hdata b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
                     and a.alt = b.alt and a.model = b.model and a.scenario = b.scenario and a.period = b.period and a.wbt = b.wbt
                     group by a.season
                     )
                     
                     
                     select season, mean, stddev
                     from a 
                     where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
                     order by season"
      )
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      data <- dbGetQuery(con, query)
      dbDisconnect(con)
          
      # Create csv file
      write.csv(data, paste0(dst_dir,"wbt",wbt,"_",envid,"_",alt,"_",campdate,"_",xlim[1],"_",xlim[2],"_safran_mean.csv"), row.names = FALSE)
          
      par(new = T)
      plot(data[,1], data[,2], type = "l", col = "black", lwd = 2,
           xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
      )
      
      #ADD SAFRAN REANALYSIS ANNUAL VALUES
      query = paste0(
      "with a as (
            select a.envid, c.* from crosscut.loc_weight_env a
            join crosscut.envelopes_3035 b on a.envid = b.envid
            join crocus.loc_zonage_safran_france c on gid = loc
            where a.envid = ",envid," and alt = ",alt,"
        ), b as (
            select distinct b.* from crocus.loc_zonage_safran_france b
            join a on a.id_mf = b.id_mf and a.alt = b.alt and b.slope = 0
        )
        
        select distinct season, nbh
        from crocus.wbt
        join b on gid = loc
        where period = '",campdate,"' and wbt = '",wbt,"'
        and scenario = 'reanalysis' and model = 'safran'
        order by season"
      )
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      data <- dbGetQuery(con, query)
      dbDisconnect(con)
          
      # Create csv file
      write.csv(data, paste0(dst_dir,"wbt",wbt,"_",envid,"_",alt,"_",campdate,"_",xlim[1],"_",xlim[2],"_safran_val.csv"), row.names = FALSE)
          
      par(new = T)
      plot(data[,1], data[,2], type = "l", col = "#878787", lwd = 1, lty = 2, pch = 1,
           xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
      )
      
      par(new = T)
      plot(data[,1], data[,2], col = "#878787", pch = 1, cex = .6,
           xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
      )
      # text(data[,1], data[,2], round(data[,2],2), cex= .4)
      
      #ADD AXIS AND AXIS LABELS
      axis(side = 1, line = -.5, tck = -.01, labels = NA)
      axis(side = 1, line = -.5, at=xlim, labels=c("",""), lwd.ticks=0)
      axis(side=1, line = -1.2, lwd = 0, cex.axis = 0.8, font.lab=2, las = 0)
      mtext(side = 1, line = 1.5, "Saisons", cex = .8)
      
      axis(side = 2, line = -.8, tck = -.01, labels = NA)
      axis(side =2, line = -.8, at=ylim, labels=c("",""), lwd.ticks=0)
      axis(side=2, line = -1.4, lwd = 0, cex.axis = .7, font.lab=2, las = 2)
      mtext(side = 2, line = 1, "Nombre d'heures", cex = .8) 
      
      # lines(xlim, c(q20, q20), col = 'black', lty = 2, lwd = 1)
      # axis(side=2, line = -1.6, at = q20, as.character(round(q20,1)), lwd = 0, cex.axis = .7, font.lab=2, las = 2, font = 2)
    }
    
    dev.off()
}

plot_wbt_quantiles = function (dst_dir,envid, name, alt, campdate, period){
  
  #####GRAPH PARAMETERS
  scenarios = c("historical", "rcp26", "rcp45", "rcp85")
  cols = c("#878787","#80cdc1", "#08519c", "#cb181d")
  types = c("poly", "lines")
  delta = (period - 1)/2
  
  #XLIM
  query = paste0("
  select min(season) + ",delta," , max(season) - ",delta,"
  from crocus.wbt
  ")
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  xlim <- dbGetQuery(con, query)
  dbDisconnect(con)
  xlim = as.numeric(xlim)
  
  prettyx = pretty(xlim)
  
  #GRAPH OUTPUT
  png(width = 800, height = 1200, paste0(dst_dir,"wbt_quantiles_",envid,"_",alt,"_",campdate,"_",xlim[1],"_",xlim[2],".png"), res = 150)
  layout(matrix(c(1,2,3),3,1))
  
  wbts = c('-1-4', '-4-6', '-6')
  for (wbt in wbts){
      par(mar=c(3,3,2,0))
      
      #YLIM FOR WBT (rather than campdate)
      query = paste0("
        with a as (
            select a.envid, c.* from crosscut.loc_weight_env a
            join crosscut.envelopes_3035 b on a.envid = b.envid
            join crocus.loc_zonage_safran_france c on gid = loc
            where a.envid = ",envid," and alt = ",alt,"
        ), b as (
            select distinct b.* from crocus.loc_zonage_safran_france b
            join a on a.id_mf = b.id_mf and a.alt = b.alt and b.slope = 0
        )
        
        select distinct 0, ceiling(max(nbh)/20) * 20 nbh
        from crocus.wbt
        join b on gid = loc
        where wbt = '",wbt,"'
      ")
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      ylim <- dbGetQuery(con, query)
      dbDisconnect(con)
      ylim = as.numeric(ylim)
      
      
      ######PLOTTING
      #SETUP DEVICE
      if (campdate == '0111-2012'){
        camp = "Avant saison"
      } else if (campdate == '2112-3101'){
        camp = "Confortement saison"
      } else if (campdate == '0102-3103'){
        camp = "Fin de saison"
      }
        
      if (wbt == '-1-4'){
        wbtr = "(entre -1°C et -4°C)"
      } else if (wbt == '-4-6'){
        wbtr = "(entre -4°C et -6°C)"
      } else if (wbt == '-6'){
        wbtr = "(moins de -6°C)"
      }
      main = paste0(camp, " ", wbtr)
      plot(0,0, type = 'n', main = main, cex.main = .8, xlim = xlim, ylim = ylim, , axes = F, xlab = NA, ylab =NA)
      
      #PLOT BACKGROUND
      for (i in 1:(length(prettyx) - 1)){
        if (i / 2 == round(i / 2)){
          col = "#F0F0F0"
        } else {
          col = "#FFFFFF"
        }
        polygon(c(prettyx[i],prettyx[i],prettyx[i+1],prettyx[i+1]), c(ylim,rev(ylim)), col = col, border = NA)
      }
      for (j in pretty(ylim)[2:length(pretty(ylim))]){
        lines(xlim, c(j,j), col = 'lightgrey', lty = 2, lwd = 1)
      }
      
      #PLOT EXPERIMENT TIME SERIES
      for (t in types){
        i = 0
        print(t)
        for (scenario in scenarios){
          print(scenario)
          i = i + 1
          
          
        query = paste0("  
        with hdata as(
            with a as (
                select a.envid, c.* from crosscut.loc_weight_env a
                join crosscut.envelopes_3035 b on a.envid = b.envid
                join crocus.loc_zonage_safran_france c on gid = loc
                where a.envid = ",envid," and alt = ",alt,"
            ), b as (
                select distinct b.* from crocus.loc_zonage_safran_france b
                join a on a.id_mf = b.id_mf and a.alt = b.alt and b.slope = 0
            )
            
            select distinct alt, season, model, scenario, period, wbt, avg(nbh) nbh
            from crocus.wbt
            join b on gid = loc
            where period = '",campdate,"' and wbt = '",wbt,"' and scenario = '",scenario,"'
            group by alt, season, model, scenario, period, wbt
        ), a as (
            select a.season,
            min(min(a.season)) over() smin, max(max(a.season)) over() smax,
            percentile_cont(0.2) within group (order by b.nbh) q20,
            percentile_cont(0.5) within group (order by b.nbh) q50,
            percentile_cont(0.8) within group (order by b.nbh) q80
            from hdata a
            join hdata b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
            and a.alt = b.alt and a.model = b.model and a.scenario = b.scenario and a.period = b.period and a.wbt = b.wbt
            group by a.season
            )
                         
        select season, q20, q50, q80
        from a 
        where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
        order by season"
          )
          con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
          data <- dbGetQuery(con, query)
          dbDisconnect(con)
          
          if (scenario == "historical"){
            query = paste0("
                            with hdata as(
                                with a as (
                                    select a.envid, c.* from crosscut.loc_weight_env a
                                    join crosscut.envelopes_3035 b on a.envid = b.envid
                                    join crocus.loc_zonage_safran_france c on gid = loc
                                    where a.envid = ",envid," and alt = ",alt,"
                                ), b as (
                                    select distinct b.* from crocus.loc_zonage_safran_france b
                                    join a on a.id_mf = b.id_mf and a.alt = b.alt and b.slope = 0
                                )
                                
                                select distinct alt, season, model, scenario, period, wbt, avg(nbh) nbh
                                from crocus.wbt
                                join b on gid = loc
                                where period = '",campdate,"' and wbt = '",wbt,"'
                                group by alt, season, model, scenario, period, wbt
                            ), a as (
                           select max(season) - ",delta," hmax from hdata
                           where scenario = '",scenario,"'
                           ), b as (
                           select min(season) + ",delta," emin from hdata
                           where scenario != '",scenario,"' and scenario != 'reanalysis'
                           )
                           
                           select c.season,
                           percentile_cont(0.2) within group (order by d.nbh) q20,
                           percentile_cont(0.5) within group (order by d.nbh) q50,
                           percentile_cont(0.8) within group (order by d.nbh) q80
                           from hdata c
                           join hdata d on  d.season >= c.season - ",delta," and d.season <= c.season +",delta,"
                           and d.alt = c.alt and d.model = c.model and d.scenario = c.scenario and d.period = c.period and d.wbt = c.wbt
                           and c.season > (select hmax from a) and c.season < (select emin from b)
                           group by c.season
                           order by c.season
                           "
            )
            con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
            hist <- dbGetQuery(con, query)
            dbDisconnect(con)
            data = rbind(data,hist)
          }
          
          # Create csv file
          write.csv(data, paste0(dst_dir,"wbt",wbt,"_quantiles_",envid,"_",alt,"_",campdate,"_",xlim[1],"_",xlim[2],"_",scenario,".csv"), row.names = FALSE)
          
          #MODIF A REFLECHIR: LANCER BOUCLE TYPE ICI (UTILISATION DU MEME DATAFRAME
          if (t == "poly"){
            polygon(c(data[,1], rev(data[,1])), c(sapply(data[,2],no_more_no_less,ylim), rev(sapply(data[,4],no_more_no_less,ylim))),
                    border = paste0(cols[i],alphaborder), col = paste0(cols[i], alphafill))
          } else {
            par(new = T)
            plot(data[,1], data[,3], type = "l", col = cols[i], lwd = 2,
                 xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
            )
          }
        }
      }
      
      #ADD SAFRAN REANALYSIS ROLLING MEAN
      query = paste0(" with hdata as(
                        with a as (
                            select a.envid, c.* from crosscut.loc_weight_env a
                            join crosscut.envelopes_3035 b on a.envid = b.envid
                            join crocus.loc_zonage_safran_france c on gid = loc
                            where a.envid = ",envid," and alt = ",alt,"
                        ), b as (
                            select distinct b.* from crocus.loc_zonage_safran_france b
                            join a on a.id_mf = b.id_mf and a.alt = b.alt and b.slope = 0
                        )
                        
                        select distinct alt, season, model, scenario, period, wbt, avg(nbh) nbh
                        from crocus.wbt
                        join b on gid = loc
                        where period = '",campdate,"' and wbt = '",wbt,"'
                        and scenario = 'reanalysis' and model = 'safran'
                        group by alt, season, model, scenario, period, wbt
                    ), a as (
                     select a.season, min(min(a.season)) over() smin, max(max(a.season)) over() smax,
                     percentile_cont(0.2) within group (order by b.nbh) q20,
                     percentile_cont(0.5) within group (order by b.nbh) q50,
                     percentile_cont(0.8) within group (order by b.nbh) q80
                     from hdata a
                     join hdata b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
                     and a.alt = b.alt and a.model = b.model and a.scenario = b.scenario and a.period = b.period and a.wbt = b.wbt
                     group by a.season
                     )
                     
                     
                     select season, q20, q50, q80
                     from a 
                     where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
                     order by season"
      )
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      data <- dbGetQuery(con, query)
      dbDisconnect(con)
          
      # Create csv file
      write.csv(data, paste0(dst_dir,"wbt",wbt,"_quantiles_",envid,"_",alt,"_",campdate,"_",xlim[1],"_",xlim[2],"_safran_mean.csv"), row.names = FALSE)
          
      par(new = T)
      plot(data[,1], data[,3], type = "l", col = "black", lwd = 2,
           xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
      )
      
      #ADD SAFRAN REANALYSIS ANNUAL VALUES
      query = paste0(
      "with a as (
            select a.envid, c.* from crosscut.loc_weight_env a
            join crosscut.envelopes_3035 b on a.envid = b.envid
            join crocus.loc_zonage_safran_france c on gid = loc
            where a.envid = ",envid," and alt = ",alt,"
        ), b as (
            select distinct b.* from crocus.loc_zonage_safran_france b
            join a on a.id_mf = b.id_mf and a.alt = b.alt and b.slope = 0
        )
        
        select distinct season, nbh
        from crocus.wbt
        join b on gid = loc
        where period = '",campdate,"' and wbt = '",wbt,"'
        and scenario = 'reanalysis' and model = 'safran'
        order by season"
      )
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      data <- dbGetQuery(con, query)
      dbDisconnect(con)
          
      # Create csv file
      write.csv(data, paste0(dst_dir,"wbt",wbt,"_",envid,"_",alt,"_",campdate,"_",xlim[1],"_",xlim[2],"_safran_val.csv"), row.names = FALSE)
          
      par(new = T)
      plot(data[,1], data[,2], type = "l", col = "#878787", lwd = 1, lty = 2, pch = 1,
           xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
      )
      
      par(new = T)
      plot(data[,1], data[,2], col = "#878787", pch = 1, cex = .6,
           xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
      )
      # text(data[,1], data[,2], round(data[,2],2), cex= .4)
      
      #ADD AXIS AND AXIS LABELS
      axis(side = 1, line = -.5, tck = -.01, labels = NA)
      axis(side = 1, line = -.5, at=xlim, labels=c("",""), lwd.ticks=0)
      axis(side=1, line = -1.2, lwd = 0, cex.axis = 0.8, font.lab=2, las = 0)
      mtext(side = 1, line = 1.5, "Saisons", cex = .8)
      
      axis(side = 2, line = -.8, tck = -.01, labels = NA)
      axis(side =2, line = -.8, at=ylim, labels=c("",""), lwd.ticks=0)
      axis(side=2, line = -1.4, lwd = 0, cex.axis = .7, font.lab=2, las = 2)
      mtext(side = 2, line = 1, "Nombre d'heures", cex = .8) 
      
      # lines(xlim, c(q20, q20), col = 'black', lty = 2, lwd = 1)
      # axis(side=2, line = -1.6, at = q20, as.character(round(q20,1)), lwd = 0, cex.axis = .7, font.lab=2, las = 2, font = 2)
    }
    
    dev.off()
}

###############PLOT WBT FOR ALL ENVELOPES IN IND, ALT, PERIOD
# inds = c('0508', '0516','0518','0523A','0523B','0531','0536','0640','2600A','2600B')
# inds = c('0508','0516','0518','0523A','0523B','0531','0536','0537','0640','0402','0403','0405','0406A','0406B','0502','0503','0507','0511A','0511B','0513','0519','0521B','0601A','0602A','0602B','0607','8401','8402')
inds = c('6501A', '6501B', '6506A', '6506B', '6508A', '6508B', '6603')
inds = c('0902A', '0902B', '6402', '0101A', '0101B')
inds = c('6402B','6402C', '6402D')
inds = c('7419A','7419B')
inds = c('7302', '7306', '7307A', '7307B', '7320')
inds = c('7303A', '7303B', '7303C', '7311', '7330', '7338', '7328A', '7328B', '7304')
inds = c('7325A', '7325B', '7313A', '7313B')
inds = c('1502B')
inds = c('7425A', '7425B', '7402A', '7402B', '7407A', '7407B', '7407C', '7313C', '7313D', '7408')
inds = c('7407B')
inds = c('7415A', '7415B')
inds = c('CH01')
inds = c('IT01A', 'IT01B', 'IT02A', 'IT02B')
period = 15

for (i in 1:length(inds)){
    ind = inds[i]
    query = paste0("select * from crosscut.resorts where ind = '",ind,"';")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    rname <- dbGetQuery(con, query)
    dbDisconnect(con)
    rname = rname[2]
    dst_dir_base = paste0("/home/francois/data/",rname,"/results/")

    query = paste0("with a as (
        select a.envid, name, c.* from crosscut.loc_weight_env a
        join crosscut.envelopes_3035 b on a.envid = b.envid
        join crocus.loc_zonage_safran_france c on gid = loc
        where ind = '",ind,"'
    ), b as (
        select distinct envid, name, b.* from crocus.loc_zonage_safran_france b
        join a on a.id_mf = b.id_mf and a.alt = b.alt and b.slope = 0
    )

    select distinct envid, name, alt, period
    from crocus.wbt
    join b on gid = loc
    order by 1,2,3")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    listenv <- dbGetQuery(con, query)
    dbDisconnect(con)

    for (env in 1:nrow(listenv)){
      envid = listenv[env,1]
      name = listenv[env,2]
      alt = listenv[env,3]
      campdate = listenv[env,4]
      dst_dir = paste0(dst_dir_base,envid,"/plots/")
      dir.create(dst_dir)
      print(paste0("envid: ",envid,", alt: ",alt,", period: ",campdate))
      # plot_wbt(dst_dir,envid, name, alt, campdate, period)
      plot_wbt_quantiles(dst_dir,envid, name, alt, campdate, period)
    }
}