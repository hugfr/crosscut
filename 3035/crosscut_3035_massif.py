from osgeo import gdal
from osgeo import ogr
from osgeo import osr
import psycopg2
import numpy as np
import os
from subprocess import call
from shutil import copyfile
import conn_param

gdal.UseExceptions()


# For each ski area envelope
    # Compute alt, slope, and aspect properties and polygonize alt
    # Compute and create mp_rast
    # Compute reanalysis viability index

def create_geotiff (dst_file, xsize, ysize, src_trans, src_proj):
    format = "GTiff"
    driver = gdal.GetDriverByName(format)
    if os.path.isfile(dst_file):
        os.remove(dst_file)
    dst_ds = driver.Create(dst_file, xsize, ysize, 1, gdal.GDT_Byte, [ 'TILED=YES', 'COMPRESS=LZW' ])
    dst_ds.SetGeoTransform(src_trans)
    dst_ds.SetProjection(src_proj)
    return dst_ds

def cut2extent(envid, src_file, dst_file, sql, ndv):
    # PSQL connection
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    # Define new extent
    rast = gdal.Open(src_file)
    src_trans = rast.GetGeoTransform()
    
    query = "select st_xmin(geom), st_ymin(geom), st_xmax(geom), st_ymax(geom) from crosscut.envelopes_3035 where envid = %s"
    cur.execute(query,(envid,))
    sta = cur.fetchone()
    x0 = src_trans[0] + (round((sta[0] - src_trans[0])/src_trans[1])*src_trans[1])
    x1 = (src_trans[0] + rast.RasterXSize*src_trans[1])-(round(((src_trans[0] + rast.RasterXSize*src_trans[1])-sta[2])/src_trans[1])*src_trans[1])
    y1 = src_trans[3] - (round((src_trans[3] - sta[3])/src_trans[5])*src_trans[5])
    y0 = (src_trans[3] - rast.RasterYSize*src_trans[5]) + (round((sta[1] - (src_trans[3] - rast.RasterYSize*src_trans[5]))/src_trans[5])*src_trans[5])
    te_str = f'{x0} {y0} {x1} {y1}'
    
    # Cutting to sql cutline with gdalwarp
    connString = f"PG: host = {conn_param.host} dbname = {conn_param.dbname} user={conn_param.user} password={conn_param.password}"
    sql = sql.replace("\n", " ").replace("\r", " ")
    
    if os.path.isfile(dst_file):
        os.remove(dst_file)
    # call(f"gdalwarp  -co \"COMPRESS=LZW\" -co \"TILED=YES\" -cutline \"{connString}\" -csql \"{sql}\" -te {te_str} -dstnodata {ndv} \"{src_file}\" \"{dst_file}\"", shell=True)
    try:
        gdal.Warp(dst_file, rast,
            outputBounds = [x0, y0, x1, y1],
            format = "GTiff",
            warpOptions = ['COMPRESS=LZW', 'TILED=YES'],
            cutlineDSName = connString,
            cutlineSQL = sql,
            dstNodata = ndv
        )
    except RuntimeError: #create a no data raster of the same size (case when no intersection with any snowmaking area)
        gdal.Warp(dst_file, rast,
            outputBounds = [x0, y0, x1, y1],
            format = "GTiff",
            warpOptions = ['COMPRESS=LZW', 'TILED=YES'],
            dstNodata = ndv
        )
        
        rast = gdal.Open(dst_file, gdal.GA_Update)
        arr = rast.GetRasterBand(1).ReadAsArray()
        arr[arr != ndv] = ndv
        rast.GetRasterBand(1).WriteArray(arr)
        rast = None
        rast = gdal.Open(dst_file)
        arr = rast.GetRasterBand(1).ReadAsArray()
        print(np.unique(arr))
        input()
    
def loc2rast(envid, src_loc, src_dir, dst_dir, dst_loc, sql, ndv):
    #psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    # create loc raster
    cut2extent(envid, src_loc, dst_loc, sql, ndv)
            
    # Create alt, slope and aspect rasters
    rtypes =["altitude", "slope", "aspect"]
    for type in rtypes:
        rast_type = f"crocus_{type}"
        src_rast = os.path.join(src_dir, f"{rast_type}.tif") # not working in this context src rasters created in properties function
        dst_file = os.path.join(dst_dir, f"{rast_type}.tif")
        if os.path.isfile(dst_file) == False:
            #vector data from postgis for cutting
            sql = f"""select geom
                from crosscut.envelopes_3035
                where envid = '{envid}'"""
                
            cut2extent(envid, src_rast, dst_file, sql, ndv)
    
    # HACK FOR LOC OUTSIDE GEO_ZONE_SAFRAN_FRANCE OR FULLY WITHIN AREAS WHERE SIM ARE NOT AVAILABLE
    print("loc hack in progress")
    locrast = gdal.Open(dst_loc, gdal.GA_Update)
    locarr = locrast.GetRasterBand(1).ReadAsArray()
    
    query = """with a as (
        select a.gid, name, mountain, type_zone, st_makevalid(a.the_geom) the_geom
        from stations.geo_zonage_safran_france a
        join stations.geo_zones_sympo b on a.id_mf = b.id
        join stations.zonage_safran_france_noms c on b.gid = c.gid
        where a.type_zone = 'sympo' and a.gid not in (595,602)

        union

        select a.gid, title, mountain, type_zone, st_makevalid(a.the_geom) the_geom
        from stations.geo_zonage_safran_france a
        join stations.geo_massifs_meteo b on a.id_mf = b.code
        where a.type_zone = 'massif' and b.mountain ilike 'a%%' or b.mountain ilike 'pyrénées'
    )

    select  a.*
    from a, crosscut.envelopes_3035 b
    where envid = %s and st_intersects(geom, the_geom)
    """
    cur.execute(query,(envid,))
    nbzones = cur.rowcount
    
    if 0 in locarr or nbzones == 0:
        print("first if ok")
        if nbzones == 0: # case when there's no available sim => choose the closer area
            query = """with a as (
                select a.gid, name, mountain, type_zone, id_mf, st_makevalid(a.the_geom) the_geom
                from stations.geo_zonage_safran_france a
                join stations.geo_zones_sympo b on a.id_mf = b.id
                join stations.zonage_safran_france_noms c on b.gid = c.gid
                where a.type_zone = 'sympo' and a.gid not in (595,602)

                union

                select a.gid, title, mountain, type_zone, id_mf, st_makevalid(a.the_geom) the_geom
                from stations.geo_zonage_safran_france a
                join stations.geo_massifs_meteo b on a.id_mf = b.code
                where a.type_zone = 'massif' and b.mountain ilike 'a%%' or b.mountain ilike 'pyrénées'
            )

            select  envid, id_mf, type_zone, st_distance(geom, the_geom) dist
            from a, crosscut.envelopes_3035 b
            where envid = %s and st_distance(geom, the_geom) < 10000
            order by 4
            limit 1
            """
            cur.execute(query,(envid,))
            zone = cur.fetchone()
            tz = zone[2]
            idmf = zone[1]
            
            #attributes loc values corresponding to this area
            altfile = os.path.join(dst_dir,'crocus_altitude.tif')
            altrast = gdal.Open(altfile)
            altarr = altrast.GetRasterBand(1).ReadAsArray()    
            slopefile = os.path.join(dst_dir,'crocus_slope.tif')
            sloperast = gdal.Open(slopefile)
            slopearr = sloperast.GetRasterBand(1).ReadAsArray()
            aspectfile = os.path.join(dst_dir,'crocus_aspect.tif')
            aspectrast = gdal.Open(aspectfile)
            aspectarr = aspectrast.GetRasterBand(1).ReadAsArray()
            
            query = """
                select distinct gid, alt, slope, aspect
                from crocus.loc_zonage_safran_france
                where id_mf = %s and type_zone = %s
            """
            cur.execute(query,(idmf,tz,))
            
            
            for loc in cur:
                # When slope 0 apply the same loc whatever the aspect
                if loc[2] == 0:
                    locarr[np.logical_and(locarr != ndv, np.logical_and(altarr == loc[1], slopearr == loc[2]))] = loc[0]
                else:
                    locarr[np.logical_and(locarr != ndv, np.logical_and(altarr == loc[1], np.logical_and(slopearr == loc[2],aspectarr == loc[3])))] = loc[0]

        else: # case when part of the envelope is not covered by any loc
            uloc,locount = np.unique(locarr[np.logical_and(locarr != 0, locarr != ndv)], return_counts=True)
            uu = []
            for u in uloc:
                uu.append(u.item())
            uu = tuple(uu)
                
            #Retrieve the main loc safran
            query = """select gid, id_mf, type_zone, count(*)
            from crocus.loc_zonage_safran_france
            where gid in %s
            group by 1,2,3
            """
            cur.execute(query,(uu,))
            mfcount = []
            idmf = []
            tz = []
            for loc in cur:
                idmf.append(loc[1])
                tz.append(loc[2])
                mfcount.append(locount[uloc == loc[0]])
            idcount= []
            ids = []
            ts = []
            for id in np.unique(idmf):
                for z in np.unique(tz):
                    ids.append(id)
                    ts.append(z)
                    # idcount.append(sum(mfcount[idmf == id]))
                    idcount.append(sum(mfcount * np.logical_and(idmf == id, tz == z)))
            idmf = ids[idcount.index(max(idcount))]
            tz = ts[idcount.index(max(idcount))]
        
            #attributes loc values corresponding to this zonage where loc = 0
            altfile = os.path.join(dst_dir,'crocus_altitude.tif')
            altrast = gdal.Open(altfile)
            altarr = altrast.GetRasterBand(1).ReadAsArray()    
            slopefile = os.path.join(dst_dir,'crocus_slope.tif')
            sloperast = gdal.Open(slopefile)
            slopearr = sloperast.GetRasterBand(1).ReadAsArray()
            aspectfile = os.path.join(dst_dir,'crocus_aspect.tif')
            aspectrast = gdal.Open(aspectfile)
            aspectarr = aspectrast.GetRasterBand(1).ReadAsArray()
            
            print(idmf, tz)
            input()
            
            query = """
                select distinct gid, alt, slope, aspect
                from crocus.loc_zonage_safran_france
                where id_mf = %s and type_zone = %s
            """
            cur.execute(query,(idmf,tz,))
            
            
            for loc in cur:
                # When slope 0 apply the same loc whatever the aspect
                if loc[2] == 0:
                    locarr[np.logical_and(locarr == 0, np.logical_and(altarr == loc[1], slopearr == loc[2]))] = loc[0]
                else:
                    locarr[np.logical_and(locarr == 0, np.logical_and(altarr == loc[1], np.logical_and(slopearr == loc[2],aspectarr == loc[3])))] = loc[0]
            if locarr[locarr == 0].shape[0] > 0:
                print(f"{envid} WARNING")
                input()
    
    # Deal with slope 50 location and raster altitude (actually raster altitude not achieved)
    uloc = np.unique(locarr[locarr != ndv])
    uu = []
    for u in uloc:
        uu.append(u.item())
    uloc = tuple(uu)
    
    if len(uloc) > 0: # len = 0 may happen when there's no intersection with any snowmaking area
        query = """
            select distinct gid
            from crocus.loc_zonage_safran_france
            where gid in %s and slope = 50
            """
        cur.execute(query,(uloc,))
            
        for loc in cur:
            loc = loc[0]
            locarr[locarr == loc] = ndv
        
        locrast.GetRasterBand(1).WriteArray(locarr)
        locrast = None  

def mp2rast(src_loc, src_dir, dst_dir, envid, ndv):
    #psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    #### Polygonize altitudes
    #create tmp shp
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(3035)
    #create tmp shapefile for polygonization
    driver = ogr.GetDriverByName('ESRI Shapefile')
    if os.path.exists('tmp.shp'):
        driver.DeleteDataSource('tmp.shp')
    tmpshp = driver.CreateDataSource('tmp.shp')
    tmplayer = tmpshp.CreateLayer('tmp', srs)
    tmplayer.CreateField(ogr.FieldDefn("alt", ogr.OFTInteger))
    
    # Open raster and get alt
    dst_file = os.path.join(dst_dir, "crocus_altitude.tif")
    rast = gdal.Open(dst_file)
    band = rast.GetRasterBand(1)
    gdal.Polygonize(band, None, tmplayer, 0, [], None)
    arr = rast.GetRasterBand(1).ReadAsArray()
    ualt = np.unique(arr[arr != ndv])
    
    query ="delete from crosscut.env_mp_alt_3035 where envid = %s"
    cur.execute(query,(envid,))
    myconn.commit()
    for alt in ualt:
        print(alt)
        tmplayer.SetAttributeFilter(f'alt = {alt}')
        j = 0
        for feat in tmplayer:
            geom = feat.GetGeometryRef()
            if j == 0:
                geom_wkb = geom.ExportToWkb()
                geom = ogr.CreateGeometryFromWkb(geom_wkb)
                geomfinal = geom
                j = 1
            elif j == 1:
                geom_wkb = geom.ExportToWkb()
                geom = ogr.CreateGeometryFromWkb(geom_wkb)
                geomfinal = geomfinal.Union(geom)
        geom_wkt = geomfinal.ExportToWkt()
        query = """
            insert into crosscut.env_mp_alt_3035(envid, alt, geom)
            select %s, %s, st_makevalid(st_multi(ST_GeomFromText(%s,3035)))
            """
        cur.execute(query,(envid,alt.tolist(),geom_wkt,))
        myconn.commit()
        
    # Compute mp values
    query = """
    update crosscut.env_mp_alt_3035 foo set
        mp = bar.mp,
        mp_tot = bar.mp_tot
    from (
        with a as(
            select envid, alt, st_length(st_intersection(a.geom, b.geom)) / st_length(a.geom) * a.mp mp_part
            from crosscut.resort_lifts_3035 a, crosscut.env_mp_alt_3035 b
            where st_intersects(a.geom, b.geom)
            and envid = %s and ind = (
                select ind from crosscut.envelopes_3035 where envid = %s
            )
        )

        select envid, alt, coalesce(sum(mp_part), 0) mp, coalesce(sum(sum(mp_part)) over(),0) mp_tot
        from a
        group by 1,2
    ) bar
    where foo.envid = bar.envid and foo.alt = bar.alt;
    
    --add this in case there's no intersection between lifts and alt part of the envelope
    update crosscut.env_mp_alt_3035 foo set
        mp = 0,
        mp_tot = 0
    where mp is null;
    """
    cur.execute(query,(envid,envid,))
    myconn.commit()
    
    # Create mp raster
    trans = rast.GetGeoTransform()
    proj = rast.GetProjection()
    xsize = band.XSize
    ysize = band.YSize
    
    format = "GTiff"
    driver = gdal.GetDriverByName(format)
    dst_file = os.path.join(dst_dir, "mp_part.tif")
    if os.path.isfile(dst_file):
        os.remove(dst_file)
    dst_ds = driver.Create(dst_file, xsize, ysize, 1, gdal.GDT_Float32)
    dst_ds.SetGeoTransform(trans)
    dst_ds.SetProjection(proj)
    dst_ds.GetRasterBand(1).SetNoDataValue(ndv)
    mp_data = np.zeros((ysize, xsize), np.float32)
    mp_data[mp_data == 0] = ndv
    
    # mask alt arr with loc to get the right number of pixel (remove slopes >= 45)
    dst_loc = os.path.join(dst_dir, "loc.tif")
    locrast = gdal.Open(dst_loc)
    locarr = locrast.GetRasterBand(1).ReadAsArray()
    arr[locarr == ndv] = ndv
    
    cur = myconn.cursor()
    query = """
    select alt, mp, mp_tot from crosscut.env_mp_alt_3035
    where envid = %s and mp != 0;"""
    cur.execute(query,(envid,))
    
    for altmp in cur:
         nb_pix = (arr == altmp[0]).sum()
         if nb_pix != 0:
             pix_val = (float(altmp[1]) / nb_pix / altmp[2]) * 100
             mp_data[arr == altmp[0]] = (pix_val)
                
    dst_ds.GetRasterBand(1).WriteArray(mp_data)
    dst_ds = None
    print("mp_part done")
        
def properties(src_dir, dst_dir, ind, envid, envname, massif_id, ndv):
    #psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    #destination tables
    query = """
    create table if not exists crosscut.env_prop(
        envid int, rast_type varchar, val float8, surf float8, mp_part float8
        )
    """
    cur.execute(query)
    myconn.commit()
    
    query = """
    create table if not exists crosscut.env_mp_alt_3035(
        envid int, alt integer, mp float8, mp_tot float8, geom geometry(Multipolygon, 3035)
        )
    """
    cur.execute(query)
    myconn.commit()
    
    # test if rm are present
    query = "select ind from crosscut.resort_lifts_3035 where ind = %s"
    cur.execute(query, (ind,))
    nbrm = cur.rowcount
    
    ##### Create alt, slope and aspect rasters
    rtypes =["altitude", "slope", "aspect"]
    for type in rtypes:   
        # create base rasters
        if type == "altitude":
            src_alt = os.path.join(src_dir, 'eudem3035', 'eudem3035.vrt')
            dst_file = os.path.join(dst_dir, f"{type}.tif")
            # cutline
            sql = f"""select st_envelope(st_buffer(geom, 500))::geometry
                from crosscut.envelopes_3035
                where envid = '{envid}'"""
            
            cut2extent(envid, src_alt, dst_file, sql, ndv)  
        else:
            src_alt = os.path.join(dst_dir, "altitude.tif")
            dst_file = os.path.join(dst_dir, f"{type}.tif")
            gdal.DEMProcessing(dst_file, src_alt, type)
            
        #
        rast_type = f"crocus_{type}"
        src_rast = dst_file
        dst_file = os.path.join(dst_dir, f"{rast_type}.tif")
        if os.path.isfile(dst_file) == False:
            #vector data from postgis for cutting
            sql = f"""select geom
                from crosscut.envelopes_3035
                where envid = '{envid}'"""
                
            cut2extent(envid, src_rast, dst_file, sql, ndv)
            
        rast = gdal.Open(dst_file, gdal.GA_Update)
        arr = rast.GetRasterBand(1).ReadAsArray()
        
        #update raster
        if type == "altitude":
            valori = list(range(150, 5100, 300))
            valori.insert(0,0)
            valdst = list(range(0, 5100, 300))
            for i in range(len(valori) - 1):
                arr[np.logical_and(np.logical_and(arr >= valori[i], arr < valori[i + 1]), arr != ndv)] = valdst[i]
            rast.GetRasterBand(1).WriteArray(arr)
        if type == "slope":
            arr[arr >= 45] = ndv
            valori = list(range(5, 50, 10))
            valori.insert(0,0)
            valdst = list(range(0, 50, 10))
            for i in range(len(valori) - 1):
                arr[np.logical_and(np.logical_and(arr >= valori[i], arr < valori[i + 1]), arr != ndv)] = valdst[i]
            rast.GetRasterBand(1).WriteArray(arr)
        if type == "aspect":
            valori = np.arange(22.5, 405, 45).tolist()
            valori.insert(0,0)
            valdst = list(range(0, 405, 45))
            valdst[len(valdst) - 1] = 0
            for i in range(len(valori) - 1):
                arr[np.logical_and(np.logical_and(arr >= valori[i], arr < valori[i + 1]), arr != ndv)] = valdst[i]
            #set aspect to -1 where slope = 0
            slope_file = os.path.join(dst_dir, "crocus_slope.tif")
            slope_rast = gdal.Open(slope_file)
            slope_val = slope_rast.GetRasterBand(1).ReadAsArray()
            arr[slope_val == 0] = -1
            rast.GetRasterBand(1).WriteArray(arr)
        rast = None
        
    # Create loc rast for the given massif id
    altfile = os.path.join(dst_dir, "crocus_altitude.tif")
    slopefile = os.path.join(dst_dir, "crocus_slope.tif")
    aspectfile = os.path.join(dst_dir, "crocus_aspect.tif")
    altrast = gdal.Open(altfile)
    sloperast = gdal.Open(slopefile)
    aspectrast = gdal.Open(aspectfile)
    altband = altrast.GetRasterBand(1)
    altarr = altband.ReadAsArray()
    slopearr = sloperast.GetRasterBand(1).ReadAsArray()
    aspectarr = aspectrast.GetRasterBand(1).ReadAsArray()
    locarr = np.copy(altarr)
    locarr[locarr != ndv] = ndv
    query = """
        select gid, alt, slope, aspect from crocus.loc_zonage_safran_france
        where type_zone = 'massif' and  id_mf = %s
    """
    cur.execute(query,(massif_id,))
    for loc in cur:
        locarr[np.logical_and(np.logical_and(altarr == loc[1], slopearr == loc[2]), aspectarr == loc[3])] = loc[0]
    
    # create geotiff
    trans = altrast.GetGeoTransform()
    proj = altrast.GetProjection()
    xsize = altband.XSize
    ysize = altband.YSize
    
    format = "GTiff"
    driver = gdal.GetDriverByName(format)
    dst_file = os.path.join(dst_dir, "loc.tif")
    if os.path.isfile(dst_file):
        os.remove(dst_file)
    dst_ds = driver.Create(dst_file, xsize, ysize, 1, gdal.GDT_Int16)
    dst_ds.SetGeoTransform(trans)
    dst_ds.SetProjection(proj)
    dst_ds.GetRasterBand(1).SetNoDataValue(ndv)
    dst_ds.GetRasterBand(1).WriteArray(locarr)
    dst_ds = None
    src_loc = dst_file
            
    # Create mp raster if not exists
    if nbrm > 0:
        if os.path.isfile(os.path.join(dst_dir, "mp_part.tif")) == False:
            mp2rast(src_loc, src_dir, dst_dir, envid, ndv)
    
    # loop on rtype to fill the prop table
    for type in rtypes:
        rast_type = f"crocus_{type}"
        dst_file = os.path.join(dst_dir, f"{rast_type}.tif")
        # SQL cleaning
        query = "delete from crosscut.env_prop where rast_type = %s and envid = %s"
        cur.execute(query,(type,envid,))
        myconn.commit()
        
        # Retrieve crocus raster data
        rast = gdal.Open(dst_file)
        trans = rast.GetGeoTransform()
        band = rast.GetRasterBand(1)
        arr = band.ReadAsArray()
        
        # ndv = band.GetNoDataValue()
        # mp = gdal.Open(mp_rast)
        if nbrm > 0:
            mp_file = os.path.join(dst_dir, "mp_part.tif")
            mp_rast = gdal.Open(mp_file)
            mp_arr = mp_rast.GetRasterBand(1).ReadAsArray()
        
        values = np.unique(arr).tolist()
        for val in values:
            if val != ndv:
                surf = (arr == val).sum() * trans[1] * abs(trans[5])
                surf = float(surf)
                if nbrm > 0:
                    mp_part = np.sum(mp_arr[np.logical_and(arr == val, mp_arr != ndv)])
                    mp_part = float(mp_part)
                else:
                    mp_part = None
                
                query = "insert into crosscut.env_prop values(%s, %s, %s, %s, %s);"
                cur.execute(query,(envid, type, val, surf, mp_part,))
                myconn.commit()
                
        print(f"{rast_type} for {envid} done") 

def weight_computing(loc_snow, envid, snowtype, mp):
    # PSQL connection
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    #location data
    loc_rast = gdal.Open(loc_snow)
    loc_band = loc_rast.GetRasterBand(1)
    data_loc = loc_band.ReadAsArray()
    unique_loc = np.unique(data_loc[data_loc != ndv])
    
    # test if rm are present
    query = "select ind from crosscut.resort_lifts_3035 where ind = %s"
    cur.execute(query, (ind,))
    nbrm = cur.rowcount  

    for loc in unique_loc:
        nb_pix = data_loc[data_loc == loc].shape[0]
        if nbrm > 0:
            mp_data_part = mp[data_loc == loc]
            mp_part = mp_data_part.sum()
            mp_part = mp_part.tolist()
            if mp_part >= 0: #added in case no rm intersect env altitudinal slice => mp[loc] = ndv (-9999)
                query = """
                    insert into crosscut.loc_weight_env(envid,loc, snowtype, mp_part, nb_pix)
                    VALUES(%s, %s, %s, %s, %s)"""
                cur.execute(query,(envid,loc.tolist(), snowtype, mp_part, nb_pix,))
                myconn.commit() 
        else:
            query = """
                insert into crosscut.loc_weight_env(envid,loc, snowtype, nb_pix)
                VALUES(%s, %s, %s, %s)"""
            cur.execute(query,(envid,loc.tolist(), snowtype, nb_pix,))
            myconn.commit() 
                 
def loc_weight(src_loc, src_dir, dst_dir, ind, envid, ndv):
    # PSQL connection
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    cur.execute("""create table if not exists crosscut.loc_weight_env(
        envid int,
        loc int4,
        snowtype varchar(50),
        mp_part float8,
        surf_part float8,
        nb_pix integer)""")
    myconn.commit()
    
    query = """
        delete from crosscut.loc_weight_env
        where envid = %s"""
    cur.execute(query,(envid,))
    myconn.commit()
    
    # test if rm are present
    query = "select ind from crosscut.resort_lifts_3035 where ind = %s"
    cur.execute(query, (ind,))
    nbrm = cur.rowcount    
    
    if nbrm > 0:
        mp_rast_file = os.path.join(dst_dir, "mp_part.tif")
        mp_rast = gdal.Open(mp_rast_file)
        mp = mp_rast.GetRasterBand(1).ReadAsArray()
    else:
        mp = None

    #####################NATURAL AND GROOMED SNOW
    
    #Cut to linestring
    dst_loc_snow = os.path.join(dst_dir,"loc.tif")
    if os.path.isfile(dst_loc_snow) == False:
        sql = f"""select envid, geom
        from crosscut.envelopes_3035
        where envid = '{envid}'"""
       
        loc2rast(envid, src_loc, src_dir, dst_dir, dst_loc_snow, sql, ndv) # Not used => loc.tif created in properties function
        
    # Open location raster
    if os.path.isfile(dst_loc_snow):
        snowtype = 'ns&gro'
        weight_computing(dst_loc_snow, envid, snowtype, mp)
        
        cur.execute("""update crosscut.loc_weight_env foo
        set surf_part = sp from (
            select envid, loc, nb_pix::numeric/sum(nb_pix) over(partition by envid) sp
            from crosscut.loc_weight_env
            where snowtype = 'ns&gro'
            ) bar
        where foo.envid = bar.envid and foo.loc = bar.loc and snowtype = 'ns&gro'
        and surf_part is null""")
        myconn.commit()
    
        print(f"{snowtype} done")
                                
        #####################MANAGED SNOW
        query = """
            select distinct status from crosscut.sm_resort_3035
            where ind = %s"""
        cursm = myconn.cursor()
        cursm.execute(query,(ind,))
        nbsm = cursm.rowcount
        
        if nbsm > 0: #Case when there is no snowmaking area
            for status in cursm:
                status = status[0]
                
                ##### With snowmaking pixels
                snowtype = f'{status}_sm'
                dst_loc_snow = os.path.join(dst_dir, f'{snowtype}.tif')
            
                if os.path.isfile(dst_loc_snow) == False:
                    sql = f"""select envid, st_buffer(st_collectionextract(st_intersection(a.geom, b.geom),3),0.001) geom
                    from crosscut.envelopes_3035 a, crosscut.sm_resort_3035 b
                    where envid = {envid} and a.ind = '{ind}' and status = '{status}'
                    and a.ind = b.ind and st_intersects(a.geom, b.geom)"""
                   
                    # loc2rast(envid, src_loc, src_dir, dst_dir, dst_loc_snow, sql, ndv)
                    cut2extent(envid, src_loc, dst_loc_snow, sql, ndv)
                            
                if os.path.isfile(dst_loc_snow):
                    weight_computing(dst_loc_snow, envid, snowtype, mp)
                    print(f"{snowtype} done")
            
                ##### pixels without snowmaking
                snowtype = f'{status}_nosm'
                dst_loc_snow = os.path.join(dst_dir, f'{snowtype}.tif')
                
                if os.path.isfile(dst_loc_snow) == False:
                    sql = f"""select envid, st_buffer(st_collectionextract(st_difference(a.geom, b.geom),3),0.00001) geom
                    from crosscut.envelopes_3035 a, crosscut.sm_resort_3035 b
                    where envid = {envid} and a.ind = '{ind}' and status = '{status}'
                    and a.ind = b.ind and st_intersects(a.geom, b.geom)"""
                    
                    query = f"""with a as ({sql})
                                select st_isempty(geom) from a
                            """
                    curtest = myconn.cursor()
                    curtest.execute(query)
                    if curtest.rowcount == 0: #case when there's no intersection with snowmaking area, sql returns nothing
                        emptest = False
                    else:
                        emptest = curtest.fetchone()[0]
                    
                    if emptest: #create a ndv raster for nosm when the envelope is fully covered by snowmaking (i.e. st_difference returns an empty geometry => st_isempty returns True)
                        src_loc_snow = os.path.join(dst_dir,"loc.tif")
                        copyfile(src_loc_snow, dst_loc_snow)
                        rastest = gdal.Open(dst_loc_snow, gdal.GA_Update)
                        arrtest = rastest.GetRasterBand(1).ReadAsArray()
                        row, col = np.shape(arrtest)
                        arrnew = np.zeros((row, col), np.float32)
                        arrnew[arrnew == 0] = ndv
                        rastest.GetRasterBand(1).WriteArray(arrnew)
                        rastest = None
                        arrtest = None
                        arrnew = None
                        curtest.close()
                        print("nosm created")
                    else:
                        # loc2rast(envid, src_loc, src_dir, dst_dir, dst_loc_snow, sql, ndv)
                        cut2extent(envid, src_loc, dst_loc_snow, sql, ndv)
                        
                        # test if no cutline => only ndv in the "nosm" raster => replace by ns&gro raster
                        rastest = gdal.Open(dst_loc_snow)
                        arrtest = rastest.GetRasterBand(1).ReadAsArray()
                        utest = np.unique(arrtest)
                        if len(utest) == 1 and utest[0] == ndv:
                            print("Hey ! you should replace this file")
                            src_loc_snow = os.path.join(dst_dir,"loc.tif")
                            copyfile(src_loc_snow, dst_loc_snow)
                        rastest = None
                        arrtest = None
                        utest = None
                            
                if os.path.isfile(dst_loc_snow):
                    weight_computing(dst_loc_snow, envid, snowtype, mp)
                    print(f"{snowtype} done")
                
                query = """update crosscut.loc_weight_env foo
                set surf_part = sp from (
                    with a as (
                        select * from crosscut.loc_weight_env
                        where substr(snowtype,1, length(snowtype) - position('_' in reverse(snowtype))) = %s
                        and envid = %s
                        )
                    select envid, loc, snowtype, nb_pix::float8/sum(nb_pix) over(partition by envid) sp
                    from a
                    ) bar
                where foo.envid = bar.envid and foo.loc = bar.loc and foo.snowtype = bar.snowtype"""
                cur.execute(query,(status,envid,))
                myconn.commit()
                print(f"{status} done")
                
def envloop(src_dir, dst_dir, ind, massif_id, ndv):
    # PSQL connection
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    # Get all sector for the given (ind) resort
    query = "select * from crosscut.resorts where ind = %s"
    cur.execute(query,(ind,))
    rname = cur.fetchone()[1]
    # Create dst directory
    dst_dir = os.path.join(dst_dir, rname)
    if os.path.isdir(dst_dir) == False:
        os.mkdir(dst_dir)
    dst_dir = os.path.join(dst_dir, "results")
    if os.path.isdir(dst_dir) == False:
        os.mkdir(dst_dir)
    
    query = "select * from crosscut.envelopes_3035 where ind = %s"
    cur.execute(query,(ind,))
    if cur.rowcount == 0:
        query = "select num from crosscut.resort_sectors where ind = %s"
        cur.execute(query,(ind,))
        
        # Compute all envelopes
        for sector in cur:
            scur = myconn.cursor()
            num = sector[0]
            query = "select name from crosscut.resort_sectors where ind = %s and num = %s"
            scur.execute(query,(ind,num,))
            sname = scur.fetchone()[0]
            envname = f'{rname} (sans {sname})'
            
            # Create the required geometry
            query = """
                insert into crosscut.envelopes_3035(ind, name, geom)
                select %s, %s, st_multi(st_union(geom))
                from crosscut.resort_sectors_3035
                where ind = %s and num != %s
            """
            scur.execute(query,(ind, envname, ind, num,))
            myconn.commit()
            
        # Add full envelope
        query = """
            insert into crosscut.envelopes_3035(ind, name, geom)
            select %s, %s, st_multi(st_union(geom))
            from crosscut.resort_sectors_3035
            where ind = %s
        """
        scur.execute(query,(ind, rname, ind,))
        myconn.commit()
    
    # Loop over envelopes
    scur = myconn.cursor()
    query = "select * from crosscut.envelopes_3035 where ind = %s"
    scur.execute(query,(ind,))
    
    for env in scur:
        envid = env[0]
        envname = env[2]
        env_dir = os.path.join(dst_dir, str(envid))
        if os.path.isdir(env_dir) == False:
            os.mkdir(env_dir)
        # Compute alt, slope, and aspect properties and polygonize alt
        properties(src_dir, env_dir, ind, envid, envname, massif_id, ndv)
        src_loc = os.path.join(env_dir, "loc.tif")
        loc_weight(src_loc, src_dir, env_dir, ind, envid, ndv)
        
src_dir = '/home/francois/data/source_rasters/'
dst_dir = '/home/francois/data/'
# src_loc = os.path.join(src_dir, "crocus_location_fra.tif")
src_loc = os.path.join(src_dir, "crocus_location_fra_available_sim_only.tif")
ndv = -9999
inds = ['IT01A', 'IT01B', 'IT02A', 'IT02B']
massif_id = 17
for ind in inds:
    envloop(src_dir, dst_dir, ind, massif_id, ndv)
# mp2rast(src_loc, src_dir, dst_dir, 6, ndv)