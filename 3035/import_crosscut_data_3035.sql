insert into crosscut.resorts values ('IND', 'Nom station')
/*
INSERT INTO crosscut.resort_sectors
SELECT 'IND', ogc_fid, nom, ST_MULTI(ST_TRANSFORM(wkb_geometry,2154))
FROM crosscut.import_NAME_ski
*/
INSERT INTO crosscut.envelopes_3035(ind, name, geom)
SELECT 'IND', nom, ST_MULTI(ST_TRANSFORM(wkb_geometry,3035))
FROM crosscut.import_NAME_ski

INSERT INTO crosscut.resort_lifts_3035
--warning: SLP value should be checked carefuly to know wether we have to apply the above computation or not
SELECT 'IND', ogc_fid, nom, ROUND(moment_de_), ST_MULTI(ST_FORCE2D(ST_TRANSFORM(wkb_geometry,3035)))
from crosscut.import_NAME_rm

INSERT INTO crosscut.sm_resort_3035
--"nom" have to be checked to know wether it has to be updated and the single geometries merged to multi or not
SELECT 'IND', lower(exist_proj), ST_MULTI(ST_TRANSFORM(ST_UNION(wkb_geometry),3035))
FROM crosscut.import_NAME_nc
GROUP BY 1,2

UPDATE crosscut.sm_resort_3035 foo
SET geom = ST_MULTI(bar.geom)
    FROM
    (SELECT ind, status, ST_Collectionextract(ST_Intersection(a.geom, b.geom),3) geom
    FROM crosscut.sm_resort_3035 a,
        (SELECT ST_Union(geom) geom FROM crosscut.envelopes_3035
        WHERE ind = 'IND') b
    WHERE ind = 'IND' AND ST_Intersects(a.geom,b.geom)
    ) bar
WHERE foo.ind = bar.ind AND foo.status = bar.status
/*
INSERT INTO crosscut.sm_track(ind, status, geom)
SELECT 'IND', lower(exist_proj), ST_MULTI(ST_TRANSFORM(ST_UNION(ST_COLLECTIONEXTRACT(ST_MAKEVALID(wkb_geometry),3)),2154))
FROM crosscut.import_NAME_nc_pistes
GROUP BY 1,2
*/
INSERT INTO crosscut.tracks_3035(ind,name, col,geom) 
select 'IND', nom, couleur, ST_FORCE2D(ST_MULTI(ST_TRANSFORM(ST_COLLECTIONEXTRACT(ST_MAKEVALID(wkb_geometry),3),3035)))
from crosscut.import_NAME_pistes;

UPDATE crosscut.tracks_3035
SET col = CASE
    WHEN col ilike '%bleu%' or col ilike 'blue' THEN 'blue'
    WHEN col ilike '%vert%' or col ilike 'green' THEN 'green'
    WHEN col ilike '%rouge%' or col ilike 'red' THEN 'red'
    WHEN col ilike '%noir%' or col ilike 'black' THEN 'black'
	WHEN col ilike '%violet%' or col ilike 'purple' THEN 'purple'
	WHEN col ilike '%orange%' or col ilike 'orange' THEN 'orange'
    --WHEN col IS NULL THEN NULL
    ELSE 'pink' END
WHERE ind = 'IND';

INSERT INTO crosscut.sm_points_3035(ind, status, geom)
SELECT 'IND', lower(exist_proj), ST_TRANSFORM(wkb_geometry,3035)
from crosscut.import_NAME_points_neige

INSERT INTO crosscut.reservoirs_3035(ind, geom)
SELECT 'IND', ST_MULTI(ST_TRANSFORM(wkb_geometry,3035))
from crosscut.import_NAME_retenues

SELECT * FROM crosscut.envelopes_3035 WHERE ind = 'IND'

INSERT INTO crosscut.actual_wc
SELECT ENVID, season::integer, REPLACE(actual_wc, ' ', '')::float8
FROM crosscut.import_NAME_wc
where actual_wc != ''

