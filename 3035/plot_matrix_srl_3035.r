library(RPostgreSQL)

shadowtext <- function(x, y=NULL, labels, col='white', bg='black', 
                       theta= seq(0, 2*pi, length.out=50), r=0.1, ... ) {
  
  xy <- xy.coords(x,y)
  xo <- r*strwidth('A')
  yo <- r*strheight('A')
  
  # draw background text with small shift in x and y in background colour
  for (i in theta) {
    text( xy$x + cos(i)*xo, xy$y + sin(i)*yo, labels, col=bg, ... )
  }
  # draw actual text in exact xy position in foreground colour
  text(xy$x, xy$y, labels, col=col, ... )
}
list_alt = function(envid, src_mnt, ndv, custom_alts, period){
    query = paste0("select * from crosscut.resort_lift_stats where envid = ", envid,";")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    data = dbGetQuery(con, query)
    dbDisconnect(con)
    
    if (nrow(data) == 0){#Actually does not work: I need to know how to pass arguments when executing python script
        print("lauching python script calculate_env_stats.py")
        input()
        system("source /home/francois/python37/bin/activate")
        system("python /home/francois/scripts/calculate_env_stats.py")
    
        query = paste0("select * from crosscut.resort_lift_stats where envid = ", envid,";")
        con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
        data = dbGetQuery(con, query)
        dbDisconnect(con)
    }
    
    df = data.frame(alt = unlist(data[1,2:4]), alt_name = c("Altitude minimum", "Altitude maximum", "Altitude moyenne"))
    df = rbind(df, custom_alts)
    
    for (i in 1:nrow(df)){
        alt = df[i,1]
        alt_name = df[i,2]
        print(paste(envid, ":", alt, ":", alt_name))
        query = paste0("select * from crosscut.srl_quantiles where envid = ", envid, " and alt = ", alt, " and alt_name = '", alt_name, "';")
        con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
        data = dbGetQuery(con, query)
        dbDisconnect(con)
        
        if (nrow(data) == 0){
            print(paste(envid, ":", alt, ":", alt_name, " computation"))
            delta = (period - 1) / 2
            
            query = paste0("
                insert into crosscut.srl_quantiles
                with a as(
                    select distinct ", envid," envid, ", alt, " alt_ref, floor(", alt, "/300) * 300 minalt
                ), btot as (
                    select a.envid, id_mf, type_zone, sum(surf_part) surf_part from crosscut.loc_weight_env
                    join a on loc_weight_env.envid = a.envid
                    join crocus.loc_zonage_safran_france on loc = gid
                    --where alt = (select minalt from a)
                    group by 1, 2, 3
                ), bmax as (
                    select envid, max(surf_part) part_max from btot
                    group by 1
                ), b as (
                    select btot.* from btot
                    join bmax on btot.envid = bmax.envid and surf_part = part_max
                ), c as (
                    select distinct a.envid, gid, alt, alt_ref, minalt
                    from crocus.loc_zonage_safran_france ls
                    join b on ls.id_mf = b.id_mf and ls.type_zone = b.type_zone
                    join a on a.envid = b.envid
                    where slope = 0
                    and (alt = minalt or alt = minalt + 300)
                ), nbdays as (
                    select foo.envid, foo.alt_ref, foo.season, foo.snowtype, foo.model, foo.scenario,
                    (foo.alt_ref - foo.alt) / 300 * (bar.nbdays - foo.nbdays) + foo.nbdays as nbdays
                    from (select * from viability.nbdays
                         join c on gid = loc
                         where alt = minalt
                        and model != 'safran' and scenario != 'historical'
                         ) foo
                    join (select * from viability.nbdays
                         join c on gid = loc
                         where alt = minalt + 300
                        and model != 'safran' and scenario != 'historical'
                         ) bar on
                        foo.envid = bar.envid and foo.season = bar.season and foo.snowtype = bar.snowtype and foo.model = bar.model and foo.scenario = bar.scenario
                    )

                select a.envid, a.alt_ref, '", alt_name, "' alt_name, a.snowtype, a.season, a.scenario,
                round(percentile_cont(0.2) within group (order by b.nbdays)) nbdays_q20,
                round(percentile_cont(0.5) within group (order by b.nbdays)) nbdays_q50,
                round(percentile_cont(0.8) within group (order by b.nbdays)) nbdays_q80,
                round(sum(case when b.nbdays > 100 then 1 else 0 end)::numeric / count(b.season) * 100, 2) prob100
                from nbdays a
                join nbdays b
                    on a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario
                    and b.season >= a.season - ", delta, " and b.season <= a.season + ", delta, "
                where a.season in (2035,2050, 2090)
                group by a.envid, a.alt_ref, 3, a.season, a.snowtype, a.scenario;
            "
            )
            con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
            dbSendQuery(con, query)
            dbDisconnect(con)
            # print(query)
            # readline(prompt="Press [enter] to continue")
            
            
            print(paste(envid, ":", alt, ":", alt_name, " computation done"))
        }
        
    }

}
plot_idx_matrix = function(dst_dir, envid, period){
    delta = (period - 1) / 2
    query = paste0("select distinct envid, alt, alt_name from crosscut.srl_quantiles where envid = ", envid, ";")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    srl = dbGetQuery(con, query)
    dbDisconnect(con)
    
    for (r in 1:nrow(srl)){
        alt = srl[r, 2]
        alt_name = srl[r,3]
        print(paste0("plotting ", alt_name))
        #Get data
        cbyear = 1986
        ceyear = 2015
        query = paste0("
        with a as (
            with a as(
                select distinct ", envid, " envid,", alt, " alt_ref, floor(", alt, "/300) * 300 minalt
            ), btot as (
                select a.envid, id_mf, type_zone, sum(surf_part) surf_part from crosscut.loc_weight_env
                join a on loc_weight_env.envid = a.envid
                join crocus.loc_zonage_safran_france on loc = gid
                --where alt = (select minalt from a)
                group by 1, 2, 3
            ), bmax as (
                select envid, max(surf_part) part_max from btot
                group by 1
            ), b as (
                select btot.* from btot
                join bmax on btot.envid = bmax.envid and surf_part = part_max
            ), c as (
                select distinct a.envid, gid, alt, alt_ref, minalt
                from crocus.loc_zonage_safran_france ls
                join b on ls.id_mf = b.id_mf and ls.type_zone = b.type_zone 
                join a on a.envid = b.envid
                where slope = 0
                and (alt = minalt or alt = minalt + 300)
            ), nbdays as (
                select foo.envid, foo.season, foo.snowtype,
                (foo.alt_ref - foo.alt) / 300 * (bar.nbdays - foo.nbdays) + foo.nbdays as nbdays
                from (
                    select envid, season, snowtype, alt, alt_ref, nbdays
                    from viability.nbdays_safran_reanalysis nsra
                    join c on loc = gid
                    where alt = minalt and season >= ",cbyear," and season <= ",ceyear,"
                ) foo
                join (
                    select envid, season, snowtype, alt, alt_ref, nbdays
                    from viability.nbdays_safran_reanalysis nsra
                    join c on loc = gid
                    where alt = minalt + 300 and season >= ",cbyear," and season <= ",ceyear,"
                ) bar on foo.envid = bar.envid and foo.season = bar.season and foo.snowtype = bar.snowtype
            )

            select snowtype, season, scenario, nbdays_q20, nbdays_q50, nbdays_q80, prob100
            from crosscut.srl_quantiles
            where envid = ",envid," and alt = ",alt," and alt_name = '",alt_name,"'

            union

            select snowtype, ", ndv, ", 'reanalysis',
            round(percentile_cont(0.2) within group (order by nbdays)) nbdays_q20,
            round(percentile_cont(0.5) within group (order by nbdays)) nbdays_q50,
            round(percentile_cont(0.8) within group (order by nbdays)) nbdays_q80,
            round(sum(case when nbdays > 100 then 1 else 0 end)::numeric / count(season) * 100, 2) prob100
            from nbdays
            group by snowtype
        )

        select * from a
        order by
        case when snowtype = 'nn' then 1
         when snowtype = 'gro' then 2
         when snowtype like 'lance' then 3 else 4 end,
        season, scenario"
        )
        con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
        data = dbGetQuery(con, query)
        dbDisconnect(con)
        
        snowtypes = unique(data[,1])
        scenarios = unique(data[data[,2] != ndv,3])
        seasons = unique(data[data[,2] != ndv,2])

        #layout and plot parameters  
        nbcol = length(seasons) * length(scenarios) + 1
        xlim = c(0,nbcol)
        ymax = length(snowtypes)+3
        y = ymax - 3
        ylim = c(0,ymax)

        file = paste0(dst_dir, "matrix_srl_", alt,".png")
        png(width = 1000, height = ymax * 80, file, res = 150, bg = "transparent")
        layout(matrix(c(2,2,2,1,1,1,1,1,1,3,3),1,11))

        par(mar=c(0,0,0,0))
        plot(0,0,type = 'n', xlim = xlim, ylim = ylim,
           axes = F, xlab = NA, ylab = NA)
        mycol = colorRampPalette(c('#ffffd9','#41b6c4','#081d58'))(200)

        # plot matrix in layout1
        sc = 0
        ymar = .1
        # Add ref column
        for (i in 1:length(snowtypes)){
            stval = data[data[,1]==snowtypes[i] & data[,2]==ndv & data[,3] == 'reanalysis',4:7]
            
            polygon(c(sc, sc + 1, sc + 1, sc), c(y - i + 1, y - i + 1, y - i, y - i),
                    col = "white", border = "black")
            xtxt = sc + .5
            ytxt = y - i + .5
            tcol = "black"
            # text(xtxt, ytxt, labels = round(stval[1,1]), adj = c(.5,.5))
            text(xtxt, y - i + ymar, labels = round(stval[1,1]), adj = c(.5,0), col = tcol, cex = .95)
            text(xtxt, ytxt, labels = round(stval[1,2]), adj = c(.5,.5), col = tcol, font = 2)
            text(xtxt, y - i + 1 - ymar, labels = round(stval[1,3]), adj = c(.5,1), col = tcol, cex = .95)
        }

        xtxt = .5 + sc
        ytxt = ymax - 2.95
        text(xtxt, ytxt, labels = paste0('Référence\n', cbyear, "-", ceyear), adj = c(0, .5), font = 2, srt = 90)
        
        #add title
        xtxt = xlim[2] / 2
        ytxt = ymax - .5
        name = paste0(alt_name, " (", round(alt), "m)")
        text(xtxt, ytxt, labels = name, adj = c(.5, .5), font = 2, cex = 1.2)
        
        #Loop over season and SCENARIO
        s = 0
        sc = 1
        for (season in seasons){
        nc = (nbcol - 1) / length(seasons)
        xtxt = 1 + nc/2 + s * nc
        ytxt = ymax - 1.5
        text(xtxt, ytxt, labels = paste0(season), adj = c(.5, 0), cex= 1.4, font = 2)
        text(xtxt, ytxt - .1, labels = paste0("(", season - delta, "-", season + delta,")"), adj = c(.5, 1), cex= .8, font = 2, col = "darkgrey")

        s = s + 1
        for (scenario in scenarios){
          for (i in 1:length(snowtypes)){
            stval = data[data[,1]==snowtypes[i] & data[,2]==season & data[,3] == scenario,4:7]
            # print(stval)
            if (floor(stval[1,4] * 2) == 0){
                col = mycol[1]
            } else {
                col = mycol[floor(stval[1,4] * 2)]
            }
            if (sum(col2rgb(col) * c(299, 587,114))/1000 < 125){
                tcol = "white"
            } else {
                tcol = "black"
            }
            polygon(c(sc, sc + 1, sc + 1, sc), c(y - i + 1, y - i + 1, y - i, y - i),
                    col = col, border = "black")
            xtxt = sc + .5
            ytxt = y - i + .5
            text(xtxt, y - i + ymar, labels = round(stval[1,1]), adj = c(.5,0), col = tcol, cex = .95)
            text(xtxt, ytxt, labels = round(stval[1,2]), adj = c(.5,.5), col = tcol, font = 2)
            text(xtxt, y - i + 1 - ymar, labels = round(stval[1,3]), adj = c(.5,1), col = tcol, cex = .95)
            # text(xtxt, ytxt, labels = paste0(round(stval[1,2]),"%"), adj = c(.5,.5), col = tcol, font = 2)
          }
          
          xtxt = .5 + sc
          ytxt = ymax - 2.95
          scenario = toupper(paste0(substr(scenario,1,4),".",substr(scenario,5,5)))
          text(xtxt, ytxt, labels = scenario, adj = c(0, .5), font = 2, srt = 90)
          sc = sc + 1
        }
        }

        #Add row names in layout2
        par(mar=c(0,0,0,0))
        plot(0,0,type = 'n', xlim = xlim, ylim = ylim,
           axes = F, xlab = NA, ylab = NA)
        for (i in 1:length(snowtypes)){
        snowtype = snowtypes[i]
        if (snowtype == 'nn'){
            name = 'Neige naturelle'
        } else if (snowtype == 'gro'){
            name = 'Neige naturelle damée'
        } else {
            if (snowtype == 'lance') {
                nc = 'perches'
            } else {
                nc = "ventilateurs"
            }

        name = paste0("Neige de culture damée\n(", nc,")")
        }

        xtxt = .5
        ytxt = ymax - 2.5 - i
        text(xtxt, ytxt, labels = name, adj = c(0, .5), font = 2)
        sc = sc + 1
        }


        #Add colorbar scale in layout3
        xlim = c(0,3.5)
        ymax = length(mycol)/length(snowtypes)*(length(snowtypes) + 3)
        ylim = c(0,ymax)
        par(mar=c(0,0,0,0), xpd = T)
        plot(0,0,type = 'n', xlim = xlim, ylim = ylim,
           axes = F, xlab = NA, ylab = NA)
        # mycol = rev(mycol)
        for (i in 1:length(mycol)){
        polygon(c(1.3, 2, 2, 1.3), c(i - 1, i-1, i, i),
                col = mycol[i], border = NA)
        }
        y = ymax - length(mycol)/length(snowtypes)*3
        text(0, (y)/2,
           labels ="Probabilité que le\nnombre jours skiables soit\nsupérieur ou égal à 100 jours",
           adj = c(.5,1), srt = 90, font = 2, cex = 1)
        x = 2 + .1
        text(c(x,x), c(y,0), labels = c("100%", "0%"), adj = c(0,.5), cex = 1.2, font = 2)

        dev.off()
    }
}

################################FUNCTIONS END###################################

#GLOBAL PARAMETER
drv = dbDriver("PostgreSQL")
host = "10.38.192.30"
user = "postgres"
pwd = "lceslt"
db = "db"

query = "
    --drop table if exists crosscut.srl_quantiles;
    create table if not exists crosscut.srl_quantiles(
        envid integer,
        alt float8,
        alt_name varchar(255),
        snowtype varchar(50),
        season integer,
        scenario varchar(50),
        nbdays_q20 float8,
        nbdays_q50 float8,
        nbdays_q80 float8,
        prob100 float8
    )"
    
con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
dbSendQuery(con, query)
dbDisconnect(con)


#Retrieve inds
period = 15
ndv = -9999
src_mnt = "/home/francois/data/source_rasters/eudem3035/eudem3035.vrt"
dst_data = "/home/francois/data/"

inds = c('6501A', '6501B', '6506A', '6506B', '6508A', '6508B', '6603')
inds = c('6402B','6402C', '6402D')
inds = c('7419A','7419B')
inds = c('7302', '7306', '7307A', '7307B', '7320')
inds = c('7303A', '7303B', '7303C', '7311', '7330', '7338', '7328A', '7328B', '7304')
inds = c('6505')
inds = c('7302')
inds = c('7303A', '7303C')
inds = c('7325A', '7325B', '7313A', '7313B')
inds = c('1502A')
inds = c('7425A', '7425B', '7402A', '7402B', '7407A', '7407B', '7407C', '7313C', '7313D', '7408')
inds = c('7407B')
inds = c('7415A', '7415B')
inds = c('CH01')
inds = c('IT01A', 'IT01B', 'IT02A', 'IT02B')
# custom_alts = data.frame(alt = c(1400, 1550, 1700, 1800, 2000, 2300), alt_name = c("Ax 1400", "Ax 1550", "Ax 1700", "Ax 1800", "Ax 2000", "Ax 2300"))
custom_alts = data.frame(alt = c(), alt_name = c())

for (i in 1:length(inds)){
  ind = inds[i]
  print(ind)
  query = paste0("select name from crosscut.resorts where ind = '", ind, "';")
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  rname = dbGetQuery(con, query)
  rname = rname[1,1]
  dst_dir = paste0(dst_data, rname, "/")
  dir.create(dst_dir)
  dst_base = paste0(dst_dir, "results/")
  dir.create(dst_dir)
  
  query = paste0("select envid from crosscut.envelopes_3035 where ind = '", ind, "';")
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  envlist = dbGetQuery(con, query)
  dbDisconnect(con)
  
  for (j in 1:nrow(envlist)){
    envid = envlist[j,1]
    dst_dir = paste0(dst_base, envid, "/")
    dir.create(dst_dir)
    dst_dir = paste0(dst_dir, "srl_matrix/")
    dir.create(dst_dir)
    
    list_alt(envid, src_mnt, ndv, custom_alts, period)
    plot_idx_matrix(dst_dir, envid, period)
    }
}

