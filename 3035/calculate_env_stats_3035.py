from osgeo import gdal
from osgeo import ogr
from osgeo import osr
import psycopg2
import numpy as np
import os
from subprocess import call
from shutil import copyfile
import conn_param

gdal.UseExceptions()


def cut2extent(envid, src_file, dst_file, sql, ndv):
    # PSQL connection
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    # Define new extent
    rast = gdal.Open(src_file)
    src_trans = rast.GetGeoTransform()
    
    query = "select st_xmin(geom), st_ymin(geom), st_xmax(geom), st_ymax(geom) from crosscut.envelopes_3035 where envid = %s"
    cur.execute(query,(envid,))
    sta = cur.fetchone()
        
    x0 = src_trans[0] + (round((sta[0] - src_trans[0])/src_trans[1])*src_trans[1])
    x1 = (src_trans[0] + rast.RasterXSize*src_trans[1])-(round(((src_trans[0] + rast.RasterXSize*src_trans[1])-sta[2])/src_trans[1])*src_trans[1])
    y1 = src_trans[3] - (round((src_trans[3] - sta[3])/src_trans[5])*src_trans[5])
    y0 = (src_trans[3] - rast.RasterYSize*src_trans[5]) + (round((sta[1] - (src_trans[3] - rast.RasterYSize*src_trans[5]))/src_trans[5])*src_trans[5])
    te_str = f'{x0} {y0} {x1} {y1}'
    
    # Cutting to sql cutline with gdalwarp
    connString = f"PG: host = {conn_param.host} dbname = {conn_param.dbname} user={conn_param.user} password={conn_param.password}"
    sql = sql.replace("\n", " ").replace("\r", " ")
    
    if os.path.isfile(dst_file):
        os.remove(dst_file)
    # call(f"gdalwarp  -co \"COMPRESS=LZW\" -co \"TILED=YES\" -cutline \"{connString}\" -csql \"{sql}\" -te {te_str} -dstnodata {ndv} \"{src_file}\" \"{dst_file}\"", shell=True)
    try:
        gdal.Warp(dst_file, rast,
            outputBounds = [x0, y0, x1, y1],
            format = "GTiff",
            warpOptions = ['COMPRESS=LZW', 'TILED=YES'],
            cutlineDSName = connString,
            cutlineSQL = sql,
            dstNodata = ndv
        )
    except RuntimeError: #create a no data raster of the same size (case when no intersection with any snowmaking area)
        gdal.Warp(dst_file, rast,
            outputBounds = [x0, y0, x1, y1],
            format = "GTiff",
            warpOptions = ['COMPRESS=LZW', 'TILED=YES'],
            dstNodata = ndv
        )
        
        rast = gdal.Open(dst_file, gdal.GA_Update)
        arr = rast.GetRasterBand(1).ReadAsArray()
        arr[arr != ndv] = ndv
        rast.GetRasterBand(1).WriteArray(arr)
        rast = None
        rast = gdal.Open(dst_file)
        arr = rast.GetRasterBand(1).ReadAsArray()
        print(np.unique(arr))
        input()

def alt_stat(envid, src_mnt, ndv):
    # PSQL connection
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    # Create table
    query = """
        create table if not exists crosscut.resort_lift_stats(
            envid integer,
            altmin float8,
            altmax float8,
            altmoy float8
        );
    """
    cur.execute(query)
    myconn.commit()
    
    # Get all sector for the given (ind) resort
    query = "select * from crosscut.resorts where ind = (select ind from crosscut.envelopes_3035 where envid = %s)"
    cur.execute(query,(envid,))
    rname = cur.fetchone()[1]
    # Create dst directory
    dst_dir = '/home/francois/data/'
    dst_dir = os.path.join(dst_dir, rname)
    if os.path.isdir(dst_dir) == False:
        os.mkdir(dst_dir)
    dst_dir = os.path.join(dst_dir, "results")
    if os.path.isdir(dst_dir) == False:
        os.mkdir(dst_dir)
    dst_dir = os.path.join(dst_dir, f"{envid}")
    if os.path.isdir(dst_dir) == False:
        os.mkdir(dst_dir)
        
    #Open DEM
    dst_file = os.path.join(dst_dir, "mnt.tif")
    
    
    
    #Retrieve points for loop
    query = """
    with raw as (
        select a.ind, gid, st_length(a.geom) len, mp, a.geom
        from crosscut.resort_lifts_3035 a, crosscut.envelopes_3035 b
        where a.ind = (select ind from crosscut.envelopes_3035 where envid = %s)
        and st_intersects(a.geom, b.geom)
        and envid = %s
    ), a as (
        select distinct gid,
        st_endpoint((st_dump(geom)).geom) sp, st_endpoint((st_dump(geom)).geom) ep,
        st_length((st_dump(geom)).geom) / len * mp mp
        from raw
    )

    select st_x(sp), st_y(sp), st_x(ep), st_y(ep), mp
    from a
    -- added for 6505 with "private" lifts and 7313A where several "magic carpet" have null power
    where mp is not null
    """
    cur.execute(query,(envid,envid,))
    
    if cur.rowcount == 0:
        print("no lift")
        sql = f"select geom from crosscut.envelopes_3035 where envid = {envid}"
        cut2extent(envid, src_mnt, dst_file, sql, ndv)
        
        mnt_rast = gdal.Open(dst_file)
        mnt_gt = mnt_rast.GetGeoTransform()
        mnt = mnt_rast.GetRasterBand(1).ReadAsArray()
        
        altmin = np.amin(mnt[mnt != ndv])
        altmax = np.amax(mnt[mnt != ndv])
        altmean = np.mean(mnt[mnt != ndv]).tolist()
    else:
        sql = f"select st_buffer(st_extent(geom)::geometry, 50) from crosscut.resort_lifts_3035 where ind = (select ind from crosscut.envelopes_3035 where envid = {envid})" 
        cut2extent(envid, src_mnt, dst_file, sql, ndv)
        
        mnt_rast = gdal.Open(dst_file)
        mnt_gt = mnt_rast.GetGeoTransform()
        mnt = mnt_rast.GetRasterBand(1).ReadAsArray()
        h, w = mnt.shape
        
        xmin = mnt_gt[0]
        xmax = xmin + mnt_gt[1] * w
        ymax = mnt_gt[3]
        ymin = ymax * mnt_gt[5] * h
            
        
        ptb = []
        pth = []
        mp = []
        
        for pt in cur:
            # print(pt)
            sprow = None
            spcol = None
            eprow = None
            epcol = None
            spalt = None
            epalt = None
            if pt[0] >= xmin and pt[0] <= xmax and pt[1] >= ymin and pt[1] <= ymax and pt[2] >= xmin and pt[2] <= xmax and pt[3] >= ymin and pt[3] <= ymax:
                sprow = int(round((ymax - pt[1]) / -mnt_gt[5]))
                spcol = int(round((pt[0] - xmin) / mnt_gt[1]))
                eprow = int(round((ymax - pt[3]) / -mnt_gt[5]))
                epcol = int(round((pt[2] - xmin) / mnt_gt[1]))
                if sprow < h and spcol < w:
                    spalt = mnt[sprow, spcol]
                if eprow < h and epcol < w:
                    epalt = mnt[eprow, epcol]
                if spalt is not None and epalt is not None:
                    if spalt < epalt:
                        ptb.append(spalt)
                        pth.append(epalt)
                    else:
                        ptb.append(epalt)
                        pth.append(spalt)
                    mp.append(pt[4])
                else:
                    if spalt is None and epalt is not None:
                        ptb.append(epalt)
                        pth.append(epalt)
                    if spalt is not None and epalt is None:
                        ptb.append(spalt)
                        pth.append(spalt)
                        
                
        altmin = min(ptb)
        altmax = max(pth)
        # print(mp)
        # input()
        
        num = 0
        # print(mp)
        # input()
        for i in range(len(mp)):
            print(i)
            print(pth[i], ptb[i], mp[i])
            num = num + (pth[i] + ptb[i])/2 * float(mp[i])
        altmean = num / float(sum(mp))
    
    query = """
        delete from crosscut.resort_lift_stats
        where envid = %s;
        
        insert into crosscut.resort_lift_stats values(%s, %s, %s, %s)
    """
    cur.execute(query, (envid, envid, altmin.tolist(), altmax.tolist(), altmean))
    myconn.commit()

################FUNCTIONS END
myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()
   
inds = ['6501A', '6501B', '6506A', '6506B', '6508A', '6508B', '6603']
inds = ['0902A', '0902B', '6402', '0101A', '0101B']
inds = ['6402B','6402C', '6402D']
inds = ['7419A','7419B']
inds = ['7302', '7306', '7307A', '7307B', '7320']
inds = ['7303A', '7303B', '7303C', '7311', '7330', '7338', '7328A', '7328B', '7304']
inds = ['6505']
inds = ['7325A', '7325B', '7313A', '7313B']
inds = ['1502A', '1502B']
inds = ['7425A', '7425B', '7402A', '7402B', '7407A', '7407B', '7407C', '7313C', '7313D', '7408']
inds = ['7415A', '7415B']
inds = ['CH01']
inds = ['IT01A', 'IT01B', 'IT02A', 'IT02B']
src_mnt = "/home/francois/data/source_rasters/eudem3035/eudem3035.vrt"
ndv = -9999
for ind in inds:
    query = "select distinct envid from crosscut.envelopes_3035 where ind = %s"
    cur.execute(query,(ind,))
    for envid in cur:
        envid = envid[0]
        print(f"{ind}: {envid}")
        alt_stat(envid, src_mnt, ndv)
        