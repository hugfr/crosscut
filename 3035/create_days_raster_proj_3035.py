from osgeo import gdal
import psycopg2
import numpy as np
import os
from subprocess import call
import conn_param
import multiprocessing

def create_geotiff (dst_file, xsize, ysize, src_trans, src_proj, ndv):
    format = "GTiff"
    driver = gdal.GetDriverByName(format)
    if os.path.isfile(dst_file):
        os.remove(dst_file)
    dst_ds = driver.Create(dst_file, xsize, ysize, 1, gdal.GDT_Int16, [ 'TILED=YES', 'COMPRESS=LZW' ])
    dst_ds.SetGeoTransform(src_trans)
    dst_ds.SetProjection(src_proj)
    dst_ds.GetRasterBand(1).SetNoDataValue(ndv)
    return dst_ds
    
def fill_arrays(locarr,meanarr, sdarr, q20arr, season, snowtype, scenario, period, ndv):
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    srctab = f"nbdays_mv{period}_{scenario}"
    uloc = np.unique(locarr[locarr != ndv])
    uu = []
    for u in uloc:
        uu.append(u.tolist())
    uloc = tuple(uu)
    
    if len(uloc) > 0: #uloc could be of length 0 when there is no snowmaking intersecting the concerned envelope
        query = f"""
            select loc, mean, stddev, q20
            from viability.{srctab}
            where season = %s and snowtype = %s
            and loc in %s
        """
        cur.execute(query,(season,snowtype,uloc,))
        
        # loop on loc to fill arrays
        for loc in cur:
            meanarr[locarr == loc[0]] = loc[1]
            sdarr[locarr == loc[0]] = loc[2]
            q20arr[locarr == loc[0]] = loc[3]
    return meanarr, sdarr, q20arr
        
def season_nbdays_raster_launcher(srcdir, envid, period):
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()    
    
    # count number of snowmaking areas
    query = """
        select distinct status from crosscut.sm_resort_3035
        where ind = %s"""
    cur.execute(query,(ind,))
    nbsm = cur.rowcount
    
    if nbsm > 0:
        query = f"""
            select distinct scenario, snowtype, season
            from viability.nbdays_mv{period}_available_simulations
        """
    else:
        query = f"""
            select distinct scenario, snowtype, season
            from viability.nbdays_mv{period}_available_simulations
            where snowtype = 'nn' or snowtype = 'gro'
        """
    cur.execute(query)
    
    args = []
    for sim in cur:
        scenario = sim[0]
        snowtype = sim[1]
        season = sim[2]
        
        # create the required directories
        dstdir = os.path.join(srcdir,str(envid),"seasonal_rasters")
        if os.path.isdir(dstdir) == False:
            os.mkdir(dstdir)
        dstdir = os.path.join(dstdir,snowtype)
        if os.path.isdir(dstdir) == False:
            os.mkdir(dstdir)
        dstdir = os.path.join(dstdir,scenario)
        if os.path.isdir(dstdir) == False:
            os.mkdir(dstdir)
        if os.path.isdir(os.path.join(dstdir,'mean')) == False:
            os.mkdir(os.path.join(dstdir,'mean'))
        if os.path.isdir(os.path.join(dstdir,'stddev')) == False:
            os.mkdir(os.path.join(dstdir,'stddev'))
        if os.path.isdir(os.path.join(dstdir,'q20')) == False:
            os.mkdir(os.path.join(dstdir,'q20'))
            
        args.append((srcdir, envid, scenario, snowtype, season, period))
    print("args done")
    pool = multiprocessing.Pool(processes = 10)
    pool.map(season_nbdays_raster, args)
    pool.close()
    
def season_nbdays_raster(args):
    srcdir, envid, scenario, snowtype, season, period = args
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    locfile = os.path.join(srcdir,str(envid),"loc.tif")
    locrast = gdal.Open(locfile)
    loctrans = locrast.GetGeoTransform()
    locproj = locrast.GetProjection()
    locband = locrast.GetRasterBand(1)
    xsize = locband.XSize
    ysize = locband.YSize
    ndv = locband.GetNoDataValue()
    locarr = locband.ReadAsArray()
    
    # Create output arrays
    dstdir = os.path.join(srcdir,str(envid),"seasonal_rasters",snowtype, scenario)
    
    meanarr = np.zeros((ysize, xsize), np.float32)
    meanarr[meanarr == 0] = ndv
    
    sdarr = np.zeros((ysize, xsize), np.float32)
    sdarr[sdarr == 0] = ndv

    q20arr = np.zeros((ysize, xsize), np.float32)
    q20arr[q20arr == 0] = ndv     

    if snowtype == 'gro' or snowtype == 'nn':
        # retrieve nbdays for the snowtype, season, and period
        meanarr, sdarr, q20arr = fill_arrays(locarr,meanarr, sdarr, q20arr, season, snowtype, scenario, period, ndv)

        sdfile = os.path.join(os.path.join(dstdir,'stddev'), f"len_sd_{season}.tif")
        sdrast = create_geotiff (sdfile, xsize, ysize, loctrans, locproj, ndv)
        meanfile = os.path.join(os.path.join(dstdir,'mean'), f"len_mean_{season}.tif")
        meanrast = create_geotiff (meanfile, xsize, ysize, loctrans, locproj, ndv)
        q20file = os.path.join(os.path.join(dstdir,'q20'), f"len_q20_{season}.tif")
        q20rast = create_geotiff (q20file, xsize, ysize, loctrans, locproj, ndv)
        
        meanrast.GetRasterBand(1).WriteArray(meanarr)
        meanrast = None
        sdrast.GetRasterBand(1).WriteArray(sdarr)
        sdrast = None
        q20rast.GetRasterBand(1).WriteArray(q20arr)
        q20rast = None
    else:
        #Loop on sm status
        query = """
            select distinct status from crosscut.sm_resort_3035
            where ind = %s"""
        cursm = myconn.cursor()
        cursm.execute(query,(ind,))

        for status in cursm:
            status = status[0]
            
            # With snowmaking pixels
            smlocfile = os.path.join(srcdir, str(envid),f'{status}_sm.tif')
            smrast = gdal.Open(smlocfile)
            smarr = smrast.GetRasterBand(1).ReadAsArray()
            meanarr, sdarr, q20arr = fill_arrays(smarr,meanarr, sdarr, q20arr, season, snowtype, scenario, period, ndv)
            
            # Without snowmaking pixels
            nosmlocfile = os.path.join(srcdir, str(envid),f'{status}_nosm.tif')
            nosmrast = gdal.Open(nosmlocfile)
            nosmarr = nosmrast.GetRasterBand(1).ReadAsArray()
            meanarr, sdarr, q20arr = fill_arrays(nosmarr,meanarr, sdarr, q20arr, season, 'gro', scenario, period, ndv)
            
            sdfile = os.path.join(os.path.join(dstdir,'stddev'), f"{status}_len_sd_{season}.tif")
            sdrast = create_geotiff (sdfile, xsize, ysize, loctrans, locproj, ndv)
            meanfile = os.path.join(os.path.join(dstdir,'mean'), f"{status}_len_mean_{season}.tif")
            meanrast = create_geotiff (meanfile, xsize, ysize, loctrans, locproj, ndv)
            q20file = os.path.join(os.path.join(dstdir,'q20'), f"{status}_len_q20_{season}.tif")
            q20rast = create_geotiff (q20file, xsize, ysize, loctrans, locproj, ndv)
            
            meanrast.GetRasterBand(1).WriteArray(meanarr)
            meanrast = None
            sdrast.GetRasterBand(1).WriteArray(sdarr)
            sdrast = None
            q20rast.GetRasterBand(1).WriteArray(q20arr)
            q20rast = None
            
    print(f"rast season {season} for {envid} ({scenario}, {snowtype}) done")

# psycopg2 connection to DB
myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()

# inds = ['0508','0516','0518','0523A','0523B','0531','0536','0640','2600A','2600B']
# inds = ['6509A', '6509B', '6509C']
# inds = ["0402","0403","0405","0406A","0406B","0502","0503","0507","0511A","0511B","0513","0519","0521B","0601A","0602A","0602B","0607","8401","8402"]
# inds = ['0902A', '0902B', '6402', '0101A', '0101B']
# inds = ['6402B','6402C', '6402D']
# inds = ['7419A','7419B']
# inds = ['7302', '7306', '7307A', '7307B', '7320']
# inds = ['7303A', '7303B', '7303C', '7311', '7330', '7338', '7328A', '7328B', '7304']
# inds = ['7325A', '7325B', '7313A', '7313B']
# inds = ['1502B']
inds = ['7425A', '7425B', '7402A', '7402B', '7407A', '7407B', '7407C', '7313C', '7313D', '7408']
inds = ['7407B']
inds = ['7415A', '7415B']
inds = ['CH01']
inds = ['IT01A', 'IT01B', 'IT02A', 'IT02B']

period = 15

for ind in inds:
    query = """
        select name from crosscut.resorts
        where ind = %s
    """
    cur.execute(query,(ind,))
    name = cur.fetchone()[0]
    srcdir = os.path.join("/home/francois/data/", name, "results/")

    query = """
        select distinct envid from crosscut.envelopes_3035
        where ind = %s
    """
    cur.execute(query,(ind,))

    for envid in cur:
        envid = envid[0]
        season_nbdays_raster_launcher(srcdir, envid, period)

