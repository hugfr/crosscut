
import numpy as np
import conn_param
import multiprocessing
import psycopg2


def compute_idx_launcher(ind):
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    # count number of snowmaking areas
    query = """
        select distinct status from crosscut.sm_resort
        where ind = %s"""
    cur.execute(query,(ind,))
    nbsm = cur.rowcount

    # Retrieve zone type for the ski resort
    query = """select distinct c.zonetype from crosscut.loc_weight_env a
    join crosscut.envelopes b on a.envid = b.envid
    join crocus.loc_zone c on a.loc = c.loc
    where ind = %s"""
    cur.execute(query,(ind,))
    
    zones = []
    for zt in cur:
        zones.append(zt[0])
    zones = tuple(zones)
    
    if nbsm > 0:
        if "sym" in zones: # add an exception in order to limit season <= 2014 and season => 1961 in case of sym zone mixed with other zones
            query = """
                select distinct model, scenario, snowtype, season
                from crocus.available_simulations
                where typezone in %s and model != 'safran'
                
                union
                
                select distinct model, scenario, snowtype, season
                from crocus.available_simulations
                where typezone in %s and model = 'safran'
                and season::integer >= 1961 and season::integer <= 2014
                """
            cur.execute(query,(zones,zones,))
        else:
            # Retrieve available simulations for these zones
            query = """select distinct model, scenario, snowtype, season from crocus.available_simulations
            where typezone in %s"""
            cur.execute(query,(zones,))
    else: # Case when there is no snowmaking area => limit sim to nn and gro
        if "sym" in zones: # add an exception in order to limit season <= 2014 and season => 1961 in case of sym zone mixed with other zones 
            query = """
                select distinct model, scenario, snowtype, season
                from crocus.available_simulations
                where typezone in %s and model != 'safran' and (snowtype = 'nn' or snowtype = 'gro')
                
                union
                
                select distinct model, scenario, snowtype, season
                from crocus.available_simulations
                where typezone in %s and model = 'safran' and (snowtype = 'nn' or snowtype = 'gro')
                and season::integer >= 1961 and season::integer <= 2014 
                """
            cur.execute(query,(zones,zones,))
        else:
            # Retrieve available simulations for these zones
            query = """select distinct model, scenario, snowtype, season from crocus.available_simulations
            where typezone in %s and (snowtype = 'nn' or snowtype = 'gro')"""
            cur.execute(query,(zones,))
        
    args = []
    # Create the required temp table looping on available sims
    for sim in cur:
        model = sim[0]
        scenario = sim[1]
        snowtype = sim[2]
        season = sim[3]
        args.append((ind, zones, model, scenario, snowtype, season))
    print("args done")
    # args.append((os.path.join(src_dir, flist[0]), zone, model, scenario, snowtype))
    pool = multiprocessing.Pool(processes = 10)
    pool.map(compute_idx, args)
    pool.close()
        
def compute_idx(args):
    ind, zones, model, scenario, snowtype, season = args
    print(f'launching idx computing for {ind}: {season}, {model}, {scenario}, {snowtype}')
    
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur2 = myconn.cursor()
    if snowtype == 'gro' or snowtype == 'nn':
        # Dealing with ns&gro envelopes snowtype
        i = 0
        for typezone in zones:
            # typezone = typezone[0]
            srctab = f'snow_{typezone}_{model}_{scenario}_{snowtype}_{season}'
            subq = f"""
            select * from crocus.{srctab}
            where loc in (
                select distinct loc from crosscut.loc_weight_env a
                join crosscut.envelopes b on a.envid = b.envid
                where ind = '{ind}'
                )"""
            if i == 0:
                query = subq
            else:
                query = f"""
                    {query}
                    
                    union
                    
                    {subq}
                """
            i = i + 1
        
        tmpsnow = f'tmp_{model}_{scenario}_{snowtype}_{season}'
        query = f"""
        drop table if exists crocus.{tmpsnow};
        create table crocus.{tmpsnow} as
        {query};"""
        cur2.execute(query)
        myconn.commit()
        print(f'{tmpsnow} done')
        
        # test if rm are present
        query = "select ind from crosscut.resort_lifts where ind = %s"
        curm = myconn.cursor()
        curm.execute(query, (ind,))
        nbrm = curm.rowcount 
        if nbrm > 0:
            varagg = "mp_part"
        else:
            varagg = "surf_part * 100"
    
        tmpidx = f'tmp_idx_{model}_{scenario}_{snowtype}_{season}'
        query = f"""
        drop table if exists crocus.{tmpidx};
        create table crocus.{tmpidx} as 
        select envid, ddate, season, a.snowtype||'_'||%s snowtype, model, scenario, sum(case when swe >= 100 then {varagg} else 0 end) didx
        from crosscut.loc_weight_env a
        join crocus.{tmpsnow} b on a.loc = b.loc
        where a.snowtype = 'ns&gro'
        and (ddate between (season||'-12-20')::date and ((season + 1)||'-01-05')::date
            or ddate between ((season + 1)||'-02-05')::date and ((season + 1)||'-03-05')::date
        )
        --and a.ind = '{ind}'
        group by envid, ddate, season, a.snowtype, model, scenario;"""
        cur2.execute(query,(snowtype,))
        myconn.commit()
        print(f'{tmpidx} done')
        
        table = f"idx_{model}_{scenario}"
        query = f"""
        create table if not exists viability.{table}(
            CONSTRAINT {table}_check
                CHECK (model = '{model}' and scenario = '{scenario}')
        )
        inherits(viability.idx);
        create index if not exists
            idx_{table}_envid on viability.{table}(envid);
        create index if not exists
            idx_{table}_season on viability.{table}(season);
        create index if not exists
            idx_{table}_snowtype on viability.{table}(snowtype);
        """
        cur2.execute(query)
        myconn.commit()
        
        query = f"select distinct snowtype from crocus.{tmpidx}"
        cur2.execute(query)
        
        st = []
        for s in cur2:
            st.append(s[0])
        st = tuple(st)
        
        query = f"""
        delete from viability.{table} where envid in (
            select envid from crosscut.envelopes where ind = %s
        )
        and season = %s and snowtype in %s;
        insert into viability.{table}
        with a as (
                select envid, season, snowtype, model, scenario, avg(didx) idx
                from crocus.{tmpidx}
                where ddate between (season||'-12-20')::date and ((season + 1)||'-01-05')::date
                group by envid, season, snowtype, model, scenario
            ), b as (
                select envid, season, snowtype, model, scenario, avg(didx) idx
                from crocus.{tmpidx}
                where ddate between ((season + 1)||'-02-05')::date and ((season + 1)||'-03-05')::date
                group by envid, season, snowtype, model, scenario
            )
        
        select a.envid, a.season, a.snowtype, a.model, a.scenario, 0.17 * a.idx + 0.83 * b.idx idx
        from a
        join b on (
            a.envid = b.envid and a.season = b.season and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario
        )
        where a.envid in (select distinct envid from crosscut.envelopes where ind = '{ind}');
        """
        cur2.execute(query, (ind, season, st,))
        myconn.commit()
        print('idx computed')
    else:
        # Create temp table with gro and sm snowtype
        i = 0
        for typezone in zones:
            # typezone = typezone[0]
            srctabgro = f'snow_{typezone}_{model}_{scenario}_gro_{season}'
            srctabsnow = f'snow_{typezone}_{model}_{scenario}_{snowtype}_{season}'
            subq = f"""
            select * from crocus.{srctabgro}
            where loc in (
                select distinct loc from crosscut.loc_weight_env a
                join crosscut.envelopes b on a.envid = b.envid
                where ind = '{ind}'
                )
             
            union
            
            select * from crocus.{srctabsnow}
            where loc in (
                select distinct loc from crosscut.loc_weight_env a
                join crosscut.envelopes b on a.envid = b.envid
                where ind = '{ind}'
                )"""
            if i == 0:
                query = subq
            else:
                query = f"""
                    {query}
                    
                    union
                    
                    {subq}
                """
            i = i + 1
        
        tmpsnow = f'tmp_{model}_{scenario}_{snowtype}_{season}'
        query = f"""
        drop table if exists crocus.{tmpsnow};
        create table crocus.{tmpsnow} as
        {query};"""
        cur2.execute(query)
        myconn.commit()
        print(f'{tmpsnow} done')
        
        # test if rm are present
        query = "select ind from crosscut.resort_lifts where ind = %s"
        curm = myconn.cursor()
        curm.execute(query, (ind,))
        nbrm = curm.rowcount 
        if nbrm > 0:
            varagg = "mp_part"
        else:
            varagg = "surf_part * 100"
        
        tmpidx = f'tmp_idx_{model}_{scenario}_{snowtype}_{season}'
        # Final "LEFT" join with coalesce has been added to deal with case when there's no intersection between envelope and snowmaking area
        query = f"""
        drop table if exists crocus.{tmpidx};
        create table crocus.{tmpidx} as 
        with a as (
            select envid, ddate, season, substr(a.snowtype,1, length(a.snowtype) - position('_' in reverse(a.snowtype))) snowtype,
            model, scenario, sum(case when swe >= 100 then {varagg} else 0 end) didx
            from crosscut.loc_weight_env a
            join crocus.{tmpsnow} b on a.loc = b.loc
            where substr(a.snowtype,length(a.snowtype) - position('_' in reverse(a.snowtype)) + 2, length(a.snowtype)) = 'nosm'
            and b.snowtype = 'gro'
            and (ddate between (season||'-12-20')::date and ((season + 1)||'-01-05')::date
                or ddate between ((season + 1)||'-02-05')::date and ((season + 1)||'-03-05')::date
            )
            group by envid, ddate, season, a.snowtype, model, scenario
        ), b as (
            select envid, ddate, season, substr(a.snowtype,1, length(a.snowtype) - position('_' in reverse(a.snowtype))) snowtype,
            model, scenario, sum(case when swe >= 100 then {varagg} else 0 end) didx
            from crosscut.loc_weight_env a
            join crocus.{tmpsnow} b on a.loc = b.loc
            where substr(a.snowtype,length(a.snowtype) - position('_' in reverse(a.snowtype)) + 2, length(a.snowtype)) = 'sm'
            and b.snowtype = %s
            and (ddate between (season||'-12-20')::date and ((season + 1)||'-01-05')::date
                or ddate between ((season + 1)||'-02-05')::date and ((season + 1)||'-03-05')::date
            )
            group by envid, ddate, season, a.snowtype, model, scenario
        ), c as (
			select distinct envid, ddate, season, snowtype, model, scenario
			from a
			where envid in (select distinct envid from crosscut.envelopes where ind = '{ind}')
			union
			select distinct envid, ddate, season, snowtype, model, scenario
			from b
			where envid in (select distinct envid from crosscut.envelopes where ind = '{ind}')
		)
        
        select c.envid, c.ddate, c.season, c.snowtype||'_'||%s snowtype, c.model, c.scenario, coalesce(a.didx,0) + coalesce(b.didx,0) didx
        from c
		left join a on (
            c.envid = a.envid and c.ddate = a.ddate and c.season = a.season and c.snowtype = a.snowtype and c.model = a.model and c.scenario = a.scenario
        )
        left join b on (
            c.envid = b.envid and c.ddate = b.ddate and c.season = b.season and c.snowtype = b.snowtype and c.model = b.model and c.scenario = b.scenario
        )
        ;"""
        cur2.execute(query,(snowtype,snowtype,))
        myconn.commit()
        print(f'{tmpidx} done')
        
        table = f"idx_{model}_{scenario}"
        query = f"""
        create table if not exists viability.{table}(
            CONSTRAINT {table}_check
                CHECK (model = '{model}' and scenario = '{scenario}')
        )
        inherits(viability.idx);
        create index if not exists
            idx_{table}_envid on viability.{table}(envid);
        create index if not exists
            idx_{table}_season on viability.{table}(season);
        create index if not exists
            idx_{table}_snowtype on viability.{table}(snowtype);
        """
        cur2.execute(query)
        myconn.commit()
        
        query = f"select distinct snowtype from crocus.{tmpidx}"
        cur2.execute(query)
        
        st = []
        for s in cur2:
            st.append(s[0])
        st = tuple(st)
        
        query = f"""
        delete from viability.{table} where envid in (
            select envid from crosscut.envelopes where ind = %s
        )
        and season = %s and snowtype in %s;
        insert into viability.{table}
        with a as (
                select envid, season, snowtype, model, scenario, avg(didx) idx
                from crocus.{tmpidx}
                where ddate between (season||'-12-20')::date and ((season + 1)||'-01-05')::date
                group by envid, season, snowtype, model, scenario
            ), b as (
                select envid, season, snowtype, model, scenario, avg(didx) idx
                from crocus.{tmpidx}
                where ddate between ((season + 1)||'-02-05')::date and ((season + 1)||'-03-05')::date
                group by envid, season, snowtype, model, scenario
            )
        
        select a.envid, a.season, a.snowtype, a.model, a.scenario, 0.17 * a.idx + 0.83 * b.idx idx
        from a
        join b on (
            a.envid = b.envid and a.season = b.season and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario
        );
        """
        cur2.execute(query, (ind, season, st,))
        myconn.commit()
        print('idx computed')
        
    cur2.execute(f"""
        drop table if exists crocus.{tmpsnow};
        drop table if exists crocus.{tmpidx};
        """)
    myconn.commit()
    
inds = ['0906A']

for ind in inds:
    print(ind)
    compute_idx_launcher(ind)