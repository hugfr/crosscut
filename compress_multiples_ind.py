import psycopg2
import os
import conn_param

myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()

inds = ['7331A']

dstdir = "/home/francois/data/"
if os.path.isdir(dstdir) == False:
    os.mkdir(dstdir)
srcdir = "/home/francois/data/"

batchfile = os.path.join(dstdir, "mv_script.sh")
if os.path.isfile(batchfile):
    os.remove(batchfile)
with open(batchfile, 'w') as f:
    f.write("#! /bin/sh\n")


for ind in inds:
    query = "select * from crosscut.resorts where ind = %s"
    cur.execute(query, (ind,))
    dirname = cur.fetchone()[1]
    dstname = dirname.replace(" ","_").replace("-","_").replace("(","").replace(")","").replace("'","").lower()
    dstname = f"{dstname}.7z"
    dst = os.path.join(dstdir, dstname)
    src = os.path.join(srcdir, dirname).replace(" ","\\ ").replace("'","\\'").replace("(","\\(").replace(")","\\)")
    src = f"{src}/"
    print(dst)
    print(src)
    
    print(f"7z a -t7z -v100m '-xr!*.tif' {dst} {src}")
    os.system(f"7z a -t7z -v100m '-xr!*.tif' {dst} {src}")

    with open(batchfile, 'a') as f:
        f.write(f"sudo mv {dst}* /home/francois/crosscutweb/Crosscut/\n")