import psycopg2
import numpy as np
import conn_param
import multiprocessing

def nbdays_launcher():
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    #Create the dst table 
    cur.execute(f"""
        CREATE TABLE IF NOT EXISTS viability.nbdays_mth_tracc(
            loc integer,
            typezone varchar(50),
            tracc float8,
            snowtype varchar(50),
            nov_mean float8,
            nov_stddev float8,
            nov_q20 float8,
            nov_q50 float8,
            dec_mean float8,
            dec_stddev float8,
            dec_q20 float8,
            dec_q50 float8,
            jan_mean float8,
            jan_stddev float8,
            jan_q20 float8,
            jan_q50 float8,
            feb_mean float8,
            feb_stddev float8,
            feb_q20 float8,
            feb_q50 float8,
            mar_mean float8,
            mar_stddev float8,
            mar_q20 float8,
            mar_q50 float8,
            apr_mean float8,
            apr_stddev float8,
            apr_q20 float8,
            apr_q50 float8,
            djf_mean float8,
            djf_stddev float8,
            djf_q20 float8,
            djf_q50 float8,
            ndjfma_mean float8,
            ndjfma_stddev float8,
            ndjfma_q20 float8,
            ndjfma_q50 float8
            );
            
        create index if not exists idx_nbdays_mth_tracc_loc on viability.nbdays_mth_tracc(loc);
        create index if not exists idx_nbdays_mth_tracc_typezone on viability.nbdays_mth_tracc(typezone);
        create index if not exists idx_nbdays_mth_tracc_tracc on viability.nbdays_mth_tracc(tracc);
        create index if not exists idx_nbdays_mth_tracc_snowtype on viability.nbdays_mth_tracc(snowtype);
    """)
    myconn.commit()
    # Retrieve available tracc
    query = """select distinct tracc from crocus.tracc_gwl"""
    cur.execute(query)    

    tracc = [0.6]
    for t in cur:
        tracc.append(t[0])
    print(f"args done: {len(tracc)}")
    pool = multiprocessing.Pool(processes = 10)
    pool.map(nbdays, tracc)
    pool.close()

def nbdays(args):
    print(args)
    tracc = args
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur2 = myconn.cursor()
    
    if tracc == 0.6:
    #reference 1976-2005 / tracc 0.6
        query = """
            select distinct model, 'historical' scenario, 1976 an_deb, 2005 an_fin
            from  crocus.tracc_model
            """
        cur2.execute(query,(tracc,))
        
        counter = 0
        for ms in cur2:
            cur3 = myconn.cursor()
            model = ms[0]
            scenario = ms[1]
            b = ms[2]
            e = ms[3]
            
            srctab = f"nbdays_mth_{model}_{scenario}"
            
            subq = f"""
                with a as(
                    select loc, typezone, season, snowtype, nov, dec
                    from viability.{srctab}
                    where season >= {b} and season <= {e}
                ), b as (
                    select loc, typezone, season + 1 season, snowtype, jan, feb, mar, apr
                    from viability.{srctab}
                    where (season + 1) >= {b} and (season + 1) <= {e}
                ), c as (
                    select a.loc, a.typezone, a.season, a.snowtype, dec + jan + feb djf, nov + dec + jan + feb + mar + apr ndjfma
                    from a
                    join b on a.loc = b.loc and a.typezone = b.typezone and a.season = b.season and a.snowtype = b.snowtype
                )
                
                select a.loc, a.typezone, a.season, {tracc} tracc, a.snowtype, nov, dec, jan, feb, mar, apr, djf, ndjfma
                from a
                join b on a.loc = b.loc and a.typezone = b.typezone and a.season = b.season and a.snowtype = b.snowtype
                join c on a.loc = c.loc and a.typezone = c.typezone and a.season = c.season and a.snowtype = c.snowtype
            """
            
            tmptab = f"tmp_nbdays_mth_tracc{tracc}".replace(".","")
            if counter == 0:
                query = f"""
                    drop table if exists viability.{tmptab};
                    create table viability.{tmptab} as 
                    {subq}
                """
                cur3.execute(query)
                myconn.commit()
                
                counter = 1
            else:
                query = f"""
                    insert into viability.{tmptab}
                    {subq}
                """
                cur3.execute(query)
                myconn.commit()
        
        query = f"""    
        create index idx_{tmptab}_loc on viability.{tmptab}(loc);
        create index idx_{tmptab}_typezone on viability.{tmptab}(typezone);
        create index idx_{tmptab}_snowtype on viability.{tmptab}(snowtype);
        create index idx_{tmptab}_season on viability.{tmptab}(season);
        """
        
        cur2.execute(query)
        myconn.commit()
        print(f"{tmptab} done ")
        
        # Create destination table if not exists
        table = f"nbdays_mth_tracc{tracc}".replace(".","")
        query = f"""
        create table if not exists viability.{table}(
            CONSTRAINT {table}_check
                CHECK (tracc = {tracc})
        )
        inherits(viability.nbdays_mth_tracc);
        create index if not exists
            nbdays_{table}_loc on viability.{table}(loc);
        create index if not exists
            nbdays_{table}_typezone on viability.{table}(typezone);
        create index if not exists
            nbdays_{table}_tracc on viability.{table}(tracc);
        create index if not exists
            nbdays_{table}_snowtype on viability.{table}(snowtype);
        """
        cur2.execute(query)
        myconn.commit()
        print(f'{table} done')
        
        # Insert nbdays for each loc  in the table
        query = f"""
            delete from viability.{table}
                where tracc = %s;
                
            insert into viability.{table}
            select loc, typezone, tracc, snowtype,
                round(avg(nov)), round(stddev(nov)), percentile_disc(.2) within group(order by nov), percentile_disc(.5) within group(order by nov),
                round(avg(dec)), round(stddev(dec)), percentile_disc(.2) within group(order by dec), percentile_disc(.5) within group(order by dec),
                round(avg(jan)), round(stddev(jan)), percentile_disc(.2) within group(order by jan), percentile_disc(.5) within group(order by jan),
                round(avg(feb)), round(stddev(feb)), percentile_disc(.2) within group(order by feb), percentile_disc(.5) within group(order by feb),
                round(avg(mar)), round(stddev(mar)), percentile_disc(.2) within group(order by mar), percentile_disc(.5) within group(order by mar),
                round(avg(apr)), round(stddev(apr)), percentile_disc(.2) within group(order by apr), percentile_disc(.5) within group(order by apr),
                round(avg(djf)), round(stddev(djf)), percentile_disc(.2) within group(order by djf), percentile_disc(.5) within group(order by djf),
                round(avg(ndjfma)), round(stddev(ndjfma)), percentile_disc(.2) within group(order by ndjfma), percentile_disc(.5) within group(order by ndjfma)
            from viability.{tmptab}
            group by loc, typezone, tracc, snowtype;
        """
        cur2.execute(query,(tracc,))
        myconn.commit()
        
        cur2.execute(f"drop table viability.{tmptab}")
        myconn.commit()
    
    else:
        #compute for tracc
        query = """
            select distinct model, scen scenario, an_deb, an_fin
            from crocus.tracc a
            join crocus.tracc_model b on a.gcm = b.gcm and a.rcm = b.rcm
            join crocus.tracc_gwl c on a.gwl = c.gwl
            where tracc = %s
            """
        cur2.execute(query,(tracc,))
        
        counter = 0
        for ms in cur2:
            cur3 = myconn.cursor()
            model = ms[0]
            scenario = ms[1]
            b = ms[2]
            e = ms[3]
            
            srctab = f"nbdays_mth_{model}_{scenario}"
            
            subq = f"""
                with a as(
                    select loc, typezone, season, snowtype, nov, dec
                    from viability.{srctab}
                    where season >= {b} and season <= {e}
                ), b as (
                    select loc, typezone, season + 1 season, snowtype, jan, feb, mar, apr
                    from viability.{srctab}
                    where (season + 1) >= {b} and (season + 1) <= {e}
                ), c as (
                    select a.loc, a.typezone, a.season, a.snowtype, dec + jan + feb djf, nov + dec + jan + feb + mar + apr ndjfma
                    from a
                    join b on a.loc = b.loc and a.typezone = b.typezone and a.season = b.season and a.snowtype = b.snowtype
                )
                
                select a.loc, a.typezone, a.season, {tracc} tracc, a.snowtype, nov, dec, jan, feb, mar, apr, djf, ndjfma
                from a
                join b on a.loc = b.loc and a.typezone = b.typezone and a.season = b.season and a.snowtype = b.snowtype
                join c on a.loc = c.loc and a.typezone = c.typezone and a.season = c.season and a.snowtype = c.snowtype
            """
            
            tmptab = f"tmp_nbdays_mth_tracc{tracc}".replace(".","")
            if counter == 0:
                query = f"""
                    drop table if exists viability.{tmptab};
                    create table viability.{tmptab} as 
                    {subq}
                """
                cur3.execute(query)
                myconn.commit()
                
                counter = 1
            else:
                query = f"""
                    insert into viability.{tmptab}
                    {subq}
                """
                cur3.execute(query)
                myconn.commit()
        
        query = f"""    
        create index idx_{tmptab}_loc on viability.{tmptab}(loc);
        create index idx_{tmptab}_typezone on viability.{tmptab}(typezone);
        create index idx_{tmptab}_snowtype on viability.{tmptab}(snowtype);
        create index idx_{tmptab}_season on viability.{tmptab}(season);
        """
        
        cur2.execute(query)
        myconn.commit()
        print(f"{tmptab} done ")
        
        # Create destination table if not exists
        table = f"nbdays_mth_tracc{tracc}".replace(".","")
        query = f"""
        create table if not exists viability.{table}(
            CONSTRAINT {table}_check
                CHECK (tracc = {tracc})
        )
        inherits(viability.nbdays_mth_tracc);
        create index if not exists
            nbdays_{table}_loc on viability.{table}(loc);
        create index if not exists
            nbdays_{table}_typezone on viability.{table}(typezone);
        create index if not exists
            nbdays_{table}_tracc on viability.{table}(tracc);
        create index if not exists
            nbdays_{table}_snowtype on viability.{table}(snowtype);
        """
        cur2.execute(query)
        myconn.commit()
        print(f'{table} done')
        
        # Insert nbdays for each loc  in the table
        query = f"""
            delete from viability.{table}
                where tracc = %s;
                
            insert into viability.{table}
            select loc, typezone, tracc, snowtype,
                round(avg(nov)), round(stddev(nov)), percentile_disc(.2) within group(order by nov), percentile_disc(.5) within group(order by nov),
                round(avg(dec)), round(stddev(dec)), percentile_disc(.2) within group(order by dec), percentile_disc(.5) within group(order by dec),
                round(avg(jan)), round(stddev(jan)), percentile_disc(.2) within group(order by jan), percentile_disc(.5) within group(order by jan),
                round(avg(feb)), round(stddev(feb)), percentile_disc(.2) within group(order by feb), percentile_disc(.5) within group(order by feb),
                round(avg(mar)), round(stddev(mar)), percentile_disc(.2) within group(order by mar), percentile_disc(.5) within group(order by mar),
                round(avg(apr)), round(stddev(apr)), percentile_disc(.2) within group(order by apr), percentile_disc(.5) within group(order by apr),
                round(avg(djf)), round(stddev(djf)), percentile_disc(.2) within group(order by djf), percentile_disc(.5) within group(order by djf),
                round(avg(ndjfma)), round(stddev(ndjfma)), percentile_disc(.2) within group(order by ndjfma), percentile_disc(.5) within group(order by ndjfma)
            from viability.{tmptab}
            group by loc, typezone, tracc, snowtype;
        """
        cur2.execute(query,(tracc,))
        myconn.commit()
        
        cur2.execute(f"drop table viability.{tmptab}")
        myconn.commit()

nbdays_launcher()

# psycopg2 connection to DB
myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()
query = f"""
    drop materialized view if exists viability.nbdays_mth_tracc_available_simulations;
    
    create materialized view viability.nbdays_mth_tracc_available_simulations as
    select distinct typezone, tracc, snowtype
    from viability.nbdays_mth_tracc
    """
cur.execute(query)
myconn.commit()