import os
import psycopg2
import conn_param

# PSQL connection
myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()

cur.execute("""create table if not exists crocus.models(
    model varchar(50) primary key,
    name varchar(255)
    )"""
)
myconn.commit()

root_dir = "/mnt/syno2/climatedata.umr-cnrm.fr/public/dcsc/projects/RUN_CROCUS_2020"
dirlist = os.listdir(root_dir)
dirlist = [d for d in dirlist if d[0:3] != "cor" and d != "index.html"]
# dirlist = [d for d in dirlist if d[0:3] == "sym"]

dir = dirlist[0]
zone_id = dir[0:3]
print(zone_id)
dirlist = os.listdir(os.path.join(root_dir, dir))
dirlist = [d for d in dirlist if d[0:10] == "experiment" or (d[0:10] == "reanalysis" and d[11:len(d)].lower() != "spandre")]
dirlist.sort()
models = []
models_short = []
for src_dir in dirlist:
    idxlist = []
    s = 0
    test = True
    while test:
        try:
            idx = src_dir.index("_", s, len(src_dir))
            idxlist.append(idx)
            s = idx + 1
        except ValueError:
            test = False
    if src_dir[0:10] == "reanalysis":
        model = 'safran'
        models.append(model)
        models_short.append(model)
    if src_dir[0:10] == "experiment":
        model = src_dir[idxlist[0] + 1:idxlist[len(idxlist) - 2]].lower()
        models.append(model)
        if "aladin53" in model:
            cor = "a53"
        elif "aladin63" in model:
            cor = "a63"
        else:
            cor = "ok"
        model = model.split("_")
        model = [s[0] for s in model]
        model = "_".join(model)
        if cor != "ok":
            model = model.replace("a", cor)
        models_short.append(model)
    src_dir = os.path.join(root_dir, dir, src_dir, 'pro')

modelist = []
for i in range(0,len(models)):
    modelist.append((models_short[i], models[i]))
modelist = list(set(modelist))
args_str = b','.join(cur.mogrify("(%s,%s)", m) for m in modelist)
args_str = args_str.decode('utf8')
print(f"INSERT INTO crocus.models VALUES {args_str}")
cur.execute(f"INSERT INTO crocus.models VALUES {args_str}")
myconn.commit()