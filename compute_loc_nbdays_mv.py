import psycopg2
import numpy as np
import conn_param
import multiprocessing

def nbdays_launcher(period):
    # period should be an odd integer
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    #Create the dst table sccordingly to the period vars
    cur.execute(f"""
        CREATE TABLE IF NOT EXISTS viability.nbdays_mv{period}(
            loc integer,
            typezone varchar(50),
            season integer,
            scenario varchar(50),
            snowtype varchar(50),
            mean float8,
            stddev float8,
            q20 float8
            );
            
        create index if not exists idx_nbdays_mv{period}_loc on viability.nbdays_mv{period}(loc);
        create index if not exists idx_nbdays_mv{period}_typezone on viability.nbdays_mv{period}(typezone);
        create index if not exists idx_nbdays_mv{period}_season on viability.nbdays_mv{period}(season);
        create index if not exists idx_nbdays_mv{period}_scenario on viability.nbdays_mv{period}(scenario);
        create index if not exists idx_nbdays_mv{period}_snowtype on viability.nbdays_mv{period}(snowtype);
    """)
    myconn.commit()
    # Retrieve available simulations for these zones
    query = """select distinct scenario, min(season) smin, max(season) smax
        from viability.nbdays_available_simulations
        where model != 'safran' 
        group by scenario
        order by scenario"""
    cur.execute(query)    

    
    delta = (period - 1) / 2
    args = []
    for sim in cur:
        scenario = sim[0]
        if scenario == 'historical':
            slist = range(int(sim[1] + delta), int(sim[2]) + 1)
        else:
            slist = range(int(sim[1] + delta), int(sim[2] - delta + 1))
        for season in slist:
            args.append((scenario, season, period))
    print(f"args done: {len(args)}")
    pool = multiprocessing.Pool(processes = 10)
    pool.map(nbdays, args)
    pool.close()

def nbdays(args):
    scenario, season, period = args
    delta = (period - 1) / 2
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur2 = myconn.cursor()
    
    if scenario == 'historical':
        query = """
            select max(season) smax
            from viability.nbdays_available_simulations
            where scenario = 'historical'
            """
        cur2.execute(query)
        hsmax = cur2.fetchone()[0] - delta
    
    #List models with available for a given scenario
    query = """select distinct model
        from viability.nbdays_available_simulations
        where scenario = %s"""
    cur2.execute(query,(scenario,))
    
    #Create the required temp table from nbdays
    counter = 0
    for model in cur2:
        model = model[0]
        if scenario == 'historical' and season > hsmax:
            query2 = """
                select distinct scenario
                from viability.nbdays_available_simulations
                where model = %s
            """
            scur = myconn.cursor()
            scur.execute(query2,(model,))

            for s in scur:
                if counter == 0:
                    tmptab = f"tmp_nbdays_{scenario}_{season}"
                    srctab = f"nbdays_{model}_{s[0]}"
                    query = f"""
                    drop table if exists viability.{tmptab};
                    create table viability.{tmptab} as
                    select loc, typezone, model, scenario, snowtype, season, nbdays
                    from viability.{srctab}
                    where season >= {season} - {delta} and season <= {season} + {delta}
                    """
                else:
                    srctab = f"nbdays_{model}_{s[0]}"
                    query = f"""
                    {query}
                    
                    union
                    
                    select loc, typezone, model, scenario, snowtype, season, nbdays
                    from viability.{srctab}
                    where season >= {season} - {delta} and season <= {season} + {delta}
                    """
                counter = counter + 1
        else:
            if counter == 0:
                tmptab = f"tmp_nbdays_{scenario}_{season}"
                srctab = f"nbdays_{model}_{scenario}"
                query = f"""
                drop table if exists viability.{tmptab};
                create table viability.{tmptab} as
                select loc, typezone, model, scenario, snowtype, season, nbdays
                from viability.{srctab}
                where season >= {season} - {delta} and season <= {season} + {delta}
                """
            else:
                srctab = f"nbdays_{model}_{scenario}"
                query = f"""
                {query}
                
                union
                
                select loc, typezone, model, scenario, snowtype, season, nbdays
                from viability.{srctab}
                where season >= {season} - {delta} and season <= {season} + {delta}
                """
            counter = counter + 1
    
    query = f"""
    {query};
    
    create index idx_{tmptab}_loc on viability.{tmptab}(loc);
    create index idx_{tmptab}_typezone on viability.{tmptab}(typezone);
    create index idx_{tmptab}_model on viability.{tmptab}(model);
    create index idx_{tmptab}_scenario on viability.{tmptab}(scenario);
    create index idx_{tmptab}_snowtype on viability.{tmptab}(snowtype);
    create index idx_{tmptab}_season on viability.{tmptab}(season);
    """
    
    cur2.execute(query)
    myconn.commit()
    
    # Create destination table if not exists
    table = f"nbdays_mv{period}_{scenario}"
    query = f"""
    create table if not exists viability.{table}(
        CONSTRAINT {table}_check
            CHECK (scenario = '{scenario}')
    )
    inherits(viability.nbdays_mv{period});
    create index if not exists
        nbdays_{table}_typezone on viability.{table}(typezone);
    create index if not exists
        nbdays_{table}_season on viability.{table}(season);
    create index if not exists
        nbdays_{table}_snowtype on viability.{table}(snowtype);
    """
    cur2.execute(query)
    myconn.commit()
    print(f'{table} done')
    
    # Insert nbdays for each loc  in the table
    if scenario == 'historical' and season > hsmax:
        query = f"""
            delete from viability.{table}
                where season = %s;
                
            insert into viability.{table}
            with a as(
               SELECT a.loc, a.typezone, a.season, a.model, 'historical' scenario, a.snowtype, avg(b.nbdays) AS mean
               FROM viability.{tmptab} a
               JOIN viability.{tmptab} b ON
                    b.season >= (a.season - {delta}) AND b.season <= (a.season + {delta}) AND a.loc = b.loc AND a.scenario::text = b.scenario::text AND a.snowtype::text = b.snowtype::text AND a.model::text = b.model::text
              WHERE a.season = %s
              GROUP BY a.loc, a.typezone, a.season, a.model, a.snowtype
            ), b as (
               SELECT a.loc, a.typezone, a.season, 'historical' scenario, a.snowtype, round(percentile_cont(0.2) within group (order by b.nbdays)) q20
               FROM viability.{tmptab} a
               JOIN viability.{tmptab} b ON
                    b.season >= (a.season - {delta}) AND b.season <= (a.season + {delta}) AND a.loc = b.loc AND a.scenario::text = b.scenario::text AND a.snowtype::text = b.snowtype::text AND a.model::text = b.model::text
              WHERE a.season = %s
              GROUP BY a.loc, a.typezone, a.season, a.snowtype
            )
            
            select a.loc, a.typezone, a.season, a.scenario, a.snowtype, round(avg(mean)) mean, round(stddev(mean)) stddev, q20
            from a
            join b on a.loc = b.loc and a.typezone = b.typezone and a.season = b.season and a.scenario = b.scenario and a.snowtype = b.snowtype
            group by a.loc, a.typezone, a.season, a.scenario, a.snowtype, q20;
        """
    else:
        query = f"""
            delete from viability.{table}
                where season = %s;
                
            insert into viability.{table}
            with a as(
               SELECT a.loc, a.typezone, a.season, a.model, a.scenario, a.snowtype, avg(b.nbdays) AS mean
               FROM viability.{tmptab} a
               JOIN viability.{tmptab} b ON
                    b.season >= (a.season - {delta}) AND b.season <= (a.season + {delta}) AND a.loc = b.loc AND a.scenario::text = b.scenario::text AND a.snowtype::text = b.snowtype::text AND a.model::text = b.model::text
              WHERE a.season = %s
              GROUP BY a.loc, a.typezone, a.season, a.model, a.scenario, a.snowtype
            ), b as (
               SELECT a.loc, a.typezone, a.season, a.scenario, a.snowtype, round(percentile_cont(0.2) within group (order by b.nbdays)) q20
               FROM viability.{tmptab} a
               JOIN viability.{tmptab} b ON
                    b.season >= (a.season - {delta}) AND b.season <= (a.season + {delta}) AND a.loc = b.loc AND a.scenario::text = b.scenario::text AND a.snowtype::text = b.snowtype::text AND a.model::text = b.model::text
              WHERE a.season = %s
              GROUP BY a.loc, a.typezone, a.season, a.scenario, a.snowtype
            )
            
            select a.loc, a.typezone, a.season, a.scenario, a.snowtype, round(avg(mean)) mean, round(stddev(mean)) stddev, q20
            from a
            join b on a.loc = b.loc and a.typezone = b.typezone and a.season = b.season and a.scenario = b.scenario and a.snowtype = b.snowtype
            group by a.loc, a.typezone, a.season, a.scenario, a.snowtype, q20;
        """
    cur2.execute(query,(season,season,season,))
    myconn.commit()
    print(f'data {scenario} {season} inserted')
    
    cur2.execute(f"drop table viability.{tmptab}")
    myconn.commit()

period = 15
nbdays_launcher(period)

# psycopg2 connection to DB
myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()
query = f"""
    drop materialized view if exists viability.nbdays_mv{period}_available_simulations;
    
    create materialized view viability.nbdays_mv{period}_available_simulations as
    select distinct typezone, scenario, snowtype, season
    from viability.nbdays_mv{period}
    """
cur.execute(query)
myconn.commit()
