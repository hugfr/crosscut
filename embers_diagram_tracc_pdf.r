library(RPostgreSQL)
rm(list = ls())
library(extrafont)
loadfonts()

#GLOBAL PARAMETER
drv = dbDriver("PostgreSQL")
host = "10.38.192.30"
user = "postgres"
pwd = "lceslt"
db = "db"

plot_embers_mth_relative = function(dstdir, envid, colvec, target, breaks, snowtypes){
    # Build non linear color ramp for ember bars
    nbcols = 400
    ycex = target / nbcols

    for (i in 1:length(breaks)){
      if (i == 1){
        nbc = round(breaks[i] / ycex)
        cols = rep("#ffffff00", nbc)
      } else {
        if (i == 2){
          nbc = round((breaks[i] - breaks[i - 1]) / ycex)
          cols = c(cols, colorRampPalette(c(colvec[i - 1], colvec[i]), alpha = TRUE)(nbc))
        } else if (i == length(breaks)){
          nbc = round((breaks[i] - breaks[i - 1]) / ycex) + 1
          cols = c(cols, colorRampPalette(c(colvec[i - 1], colvec[i]), alpha = TRUE)(nbc)[2:nbc])
          nbc = nbcols - length(cols)
          cols = c(cols, rep(colvec[i], nbc))      
        } else {
          nbc = round((breaks[i] - breaks[i - 1]) / ycex) + 1
          cols = c(cols, colorRampPalette(c(colvec[i - 1], colvec[i]), alpha = TRUE)(nbc)[2:nbc])
        }
      }
    }

    #########for each massif draw djf embers for each snowtype + q20
    #loop on massif
    query = paste0("
		with /*ncpart as (
			select a.envid, st_area(st_intersection(a.geom, b.geom))/st_area(a.geom) ncpart
			from crosscut.envelopes a
			join crosscut.sm_resort b on a.ind = b.ind
			where a.envid = ", envid, " and st_intersects(a.geom, b.geom)
		), */a as (
			select snowtype, 0.6 tracc,
			percentile_cont(0.2) within group(order by nov) refnov,
			percentile_cont(0.2) within group(order by dec) refdec
			from viability.idx_mth
			where model in (select distinct model from  crocus.tracc_model)
			and envid = ", envid, " and scenario = 'historical'
			and season >= 1976 and season <= 2005
			group by 1,2
		), b as (
			select snowtype, 0.6 tracc,
			percentile_cont(0.2) within group(order by jan) refjan,
			percentile_cont(0.2) within group(order by feb) reffeb,
			percentile_cont(0.2) within group(order by mar) refmar,
			percentile_cont(0.2) within group(order by apr) refapr,
			percentile_cont(0.2) within group(order by djf) refdjf,
			percentile_cont(0.2) within group(order by ndjfma) refndjfma
			from viability.idx_mth
			where model in (select distinct model from  crocus.tracc_model)
			and envid = ", envid, " and scenario = 'historical'
			and season + 1 >= 1976 and season + 1 <= 2005
			group by 1,2
		),

		--snow supply risk to ski tourism for each tracc
		c as (
			select distinct d.snowtype, c.tracc, 
			sum(case when nov <= refnov then 1 else 0 end)::numeric/count(*) freqnov, 
			sum(case when dec <= refdec then 1 else 0 end)::numeric/count(*) freqdec
			from crocus.tracc a
			join crocus.tracc_model b on a.gcm = b.gcm and a.rcm = b.rcm
			join crocus.tracc_gwl c on a.gwl = c.gwl
			join viability.idx_mth d on b.model = d.model and scen = scenario
				and season >= an_deb and season <= an_fin
			join a e on d.snowtype = e.snowtype
			where d.envid = ", envid, "
			group by 1, 2
		), d as (
			select distinct d.snowtype, c.tracc, 
			sum(case when jan <= refjan then 1 else 0 end)::numeric/count(*) freqjan, 
			sum(case when feb <= reffeb then 1 else 0 end)::numeric/count(*) freqfeb, 
			sum(case when mar <= refmar then 1 else 0 end)::numeric/count(*) freqmar, 
			sum(case when apr <= refapr then 1 else 0 end)::numeric/count(*) freqapr, 
			sum(case when djf <= refdjf then 1 else 0 end)::numeric/count(*) freqdjf, 
			sum(case when ndjfma <= refndjfma then 1 else 0 end)::numeric/count(*) freqndjfma
			from crocus.tracc a
			join crocus.tracc_model b on a.gcm = b.gcm and a.rcm = b.rcm
			join crocus.tracc_gwl c on a.gwl = c.gwl
			join viability.idx_mth d on b.model = d.model and scen = scenario
				and season + 1 >= an_deb and season + 1 <= an_fin
			join b e on d.snowtype = e.snowtype
			--left join ncpart f on d.envid = f.envid
			where d.envid = ", envid, "
			group by 1, 2
		), e as (
			select c.*, freqjan, freqfeb, freqmar, freqapr, freqdjf, freqndjfma
			from c
			join d on c.snowtype = d.snowtype and c.tracc = d.tracc
			order by 1,2
		), f as (
			select 'November' mth, snowtype, tracc, null q20, freqnov freq20
			from e
			union
			select 'December' mth, snowtype, tracc, null q20, freqdec
			from e
			union
			select 'January' mth, snowtype, tracc, null q20, freqjan
			from e
			union
			select 'February' mth, snowtype, tracc, null q20, freqfeb
			from e
			union
			select 'March' mth, snowtype, tracc, null q20, freqmar
			from e
			union
			select 'April' mth, snowtype, tracc, null q20, freqapr
			from e
			union
			select 'DJF' mth, snowtype, tracc, null q20, freqdjf
			from e
			union
			select 'NDJFMA' mth, snowtype, tracc, null q20, freqndjfma
			from e
		)
		--  massif, snowtype, gwl, sum(q20 * rsize) / sum(rsize) q20, sum(freq20 * rsize) / sum(rsize) freq20
		select * from f
		order by case 
			when mth ilike 'nov%' then 1
			when mth ilike 'dec%' then 2
			when mth ilike 'jan%' then 3
			when mth ilike 'feb%' then 4
			when mth ilike 'mar%' then 5
			when mth ilike 'apr%' then 6
			when mth ilike 'djf' then 7
		else 8 end
		
		/*union
		select distinct mth, snowtype, 0.6, null, 0.2
		from f
		order by 1,2,3*/
    ")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    data <- dbGetQuery(con, query)
    dbDisconnect(con)
    
    ### sort massif 
    massifs = unique(data[,1])
    print(massifs)
    
    library(svglite)
    path = paste0(dstdir, envid, "_embers_mth_q20_relative_to_snowtype.pdf")
    cairo_pdf(path, width = 10, height = 8, family = "Nimbus Sans L")
    nf = layout(matrix(c(
                rep(1,4),rep(2,4),rep(3,1)
              ),9,1))
    # layout.show(nf)
    # m = "Alps-AT"
    # massifs = unique(data[data[,1]==m,1])


    ymax = 200
    ylim = c(-.1*ymax,ymax + .05*ymax)

    barw = 80
    xmar = 70
    nbmassifs = length(massifs)
    nbrows = 2
    mperrow = ceiling(nbmassifs / nbrows)
    print(nbmassifs)
    nbbars = length(snowtypes)
    xfactor = mperrow * nbbars
    xmax = barw * xfactor
    w = xmar + xmax
    xlim = c(0,w)
    xinter = barw * .1
    xinterbar = barw * .6
    xw = (xmax - xinter * xfactor  - xinterbar * mperrow) / xfactor

    for (rnum in 1:nbrows){
        par(mar = c(0,0,0,0), family = "Nimbus Sans L")
        plot(0,0, type = "n", xlim = xlim, ylim = ylim,
             xlab = NA, ylab = NA, axes = F
        )
        
        
        x = xmar + xinterbar
        
        m2 = mperrow * rnum
        m1 = m2 - mperrow + 1
        if (m2 > nbmassifs){
            m2  = nbmassifs
        }
        for (mnum in m1:m2){
          m = massifs[mnum]
          # print(m)
          mdata = data[data[,1] == m,]
          # mcol = unique(mdata[,6])
          mcol = "black"
          # print(mcol)
          
          # add massif name
          xtxt = x - 0.2 * xinterbar
          ytxt = ymax / 2
          mlab = mapply(paste,strwrap(m,width=36,simplify = FALSE),collapse = "\n")
          text(xtxt, ytxt, labels = mlab, adj = c(.5,0), srt = 90, cex = 1.2, col = mcol, font = 2)
          
               
          # add plot for each snowtype
          for (st in snowtypes){        
            stdata = mdata[mdata[,2] == st,]
            # draw ember diagrams
            ycex = 4 / ymax
            
            gwls = sort(unique(stdata[,3]))
            gwls = c(.6,gwls)
            y = 0
            scol = 1
            if (is.na(stdata[1,3]) == FALSE){
                for (gwl in gwls){
                    #print(paste0("month: ", mth, ", GWL: ", gwl))
                    ecol = round(stdata[stdata[,3] == gwl,5]*nbcols)
                    #ecol = cols[ecolidx]
                    yy = gwl / ycex
                    if (gwl == gwls[1]){
                        gwlcols = rep(cols[scol], (yy-y))
                        ecol = round(breaks[1]*nbcols)
                    } else {
                        gwlcols = colorRampPalette(cols[scol:ecol])(yy-y)
                    }
                    for (i in 1:(yy-y)){
                      polygon(
                        c(x, x + xw, x + xw, x),
                        c(y, y, y + 1, y + 1),
                        col = gwlcols[i],
                        border = NA
                      )
                      y = y + 1
                    }

                    scol = ecol + 1
                    if (scol >= nbcols){scol = nbcols}
                }
                polygon(
                x = c(x, x + xw, x + xw, x),
                y = c(0,0,y,y),
                col = NA,
                border = mcol, lwd = .8
                )
                
            } else {
                # print(mcol)
                polygon(
                x = c(x, x + xw, x + xw, x),
                y = c(0,0,ymax,ymax),
                col = "lightgrey",
                border = mcol, lwd = .5
                )
            }

          
          x = x + xw + xinter
          
          }
          
          x = x + xinterbar
        }
        
        x = xmar - 5
        lines(c(x, x), c(0,ymax), lwd = .5)
        # labval = gwls[gwls != .6]
        labval = gwls
        lab = paste0(labval,"°C")
        labval = labval / ycex
        x2 = x - 5
        for (i in 1:length(labval)){
            y = labval[i]
            lines(c(x, x2), c(y,y), lwd = .5)
            if (y != max(labval)){
                lines(c(x, w), c(y, y), lty = 3, lwd = 1, col = "grey")
            }
            text(x2 - 1, y, labels = lab[i], adj = c(1,.5), cex = 1)
        }

        text(-60, ymax/2, labels = "Global warming level (°C)", adj = c(.5,0), cex = 1.6, font = 2, srt = 90)
    }
    
    # add footer colorscale
    xfootmax = (nbcols / 8) * 2 + nbcols
    par(mar = c(0,0,0,0), xpd = T, family = "Nimbus Sans L")
    plot(0,0, type = "n", xlim = c(0, xfootmax), ylim = c(0,4),
       xlab = NA, ylab = NA, axes = F
    )


    lab = c(0.2, breaks[2:length(breaks)])
    xoffset = (xfootmax / 2) - ((((max(lab) + .1) * nbcols) - ((min(lab) - .1) * nbcols)) / 2)
    xoffset = 0
    
    
    text(xoffset + (max(lab) * nbcols + xoffset) / 2, 3.4, labels = "Risk level", adj = c(.5,0), cex = 1.5, font = 2, col = "black")
    
    # xoffset version
    labx = lab * nbcols + xoffset - (.1 *  nbcols)
    
    x = xoffset
    y = 1
    for (i in ((min(lab) - .1) * nbcols):((max(lab) + .1) * nbcols - 1)){
    polygon(
        c(x, x + 1, x + 1, x),
        c(y, y , y + 2, y + 2),
        col = cols[i], border = NA            
    )
    x = x + 1     
    }
    
    polygon(
    c(xoffset, (max(lab)) * nbcols + xoffset, (max(lab)) * nbcols + xoffset, xoffset),
    c(y, y , y + 2, y + 2),
    col = NA, border = "black"
    )
    
    tck = .2
    for (i in 1:length(lab)){
        lines(c(labx[i], labx[i]), c(y, y + tck))
        lines(c(labx[i], labx[i]), c(y + 2, y + 2 - tck))
        text(labx[i], y + 1, labels = lab[i], adj = c(.5, .5), cex = 1, font = 2)    
    }

    x = c()
    for (i in 1:(length(breaks) + 1)){
      if (i == 1){
        x = (min(lab)) * nbcols + xoffset - (.1 *  nbcols)
      } else if (i < length(breaks)) {
        x = c(x, xoffset - (.1 *  nbcols) + breaks[i] * nbcols)
      } else {
        x = c(x, xoffset - (.1 *  nbcols) + breaks[i] * nbcols)
        # x = c(x, (nbcols / 8) + (breaks[i - 1] + (breaks[i] - breaks[i - 1]) / 2) * nbcols)
      }
    }
    # x = c((nbcols / 8), 150, 300, xfootmax - (nbcols / 8))
    y = (rep(y - .2, 4))
    adj = data.frame(c(.5, .5, .5, .5), c(1, 1, 1, 1))
    lab = c("Undetect.", "Moderate", "High", "Very high")
    for (i in 1:length(lab)){
    text(x[i], y[i], labels = lab[i], adj = c(adj[i,1], adj[i,2]), cex = 1, font = 2)
    }

    dev.off()
    embed_fonts(path)
}

plot_embers_mth_gro = function(dstdir, envid, colvec, target, breaks, snowtypes){
    # Build non linear color ramp for ember bars
    nbcols = 400
    ycex = target / nbcols

    for (i in 1:length(breaks)){
      if (i == 1){
        nbc = round(breaks[i] / ycex)
        cols = rep("#ffffff00", nbc)
      } else {
        if (i == 2){
          nbc = round((breaks[i] - breaks[i - 1]) / ycex)
          cols = c(cols, colorRampPalette(c(colvec[i - 1], colvec[i]), alpha = TRUE)(nbc))
        } else if (i == length(breaks)){
          nbc = round((breaks[i] - breaks[i - 1]) / ycex) + 1
          cols = c(cols, colorRampPalette(c(colvec[i - 1], colvec[i]), alpha = TRUE)(nbc)[2:nbc])
          nbc = nbcols - length(cols)
          cols = c(cols, rep(colvec[i], nbc))      
        } else {
          nbc = round((breaks[i] - breaks[i - 1]) / ycex) + 1
          cols = c(cols, colorRampPalette(c(colvec[i - 1], colvec[i]), alpha = TRUE)(nbc)[2:nbc])
        }
      }
    }

    #########for each massif draw djf embers for each snowtype + q20
    #loop on massif
    query = paste0("
		with ncpart as (
			select a.envid, status||'_lance' snowtype, st_area(st_intersection(a.geom, b.geom))/st_area(a.geom) * 100 ncpart
			from crosscut.envelopes a
			join crosscut.sm_resort b on a.ind = b.ind
			where a.envid = ", envid, " and st_intersects(a.geom, b.geom)
		), a as (
			select envid, 0.6 tracc,
			percentile_cont(0.2) within group(order by nov) refnov,
			percentile_cont(0.2) within group(order by dec) refdec
			from viability.idx_mth
			where model in (select distinct model from  crocus.tracc_model)
			and envid = ", envid, " and scenario = 'historical'
			and season >= 1976 and season <= 2005 and snowtype = 'ns&gro_gro'
			group by 1,2
		), b as (
			select envid, 0.6 tracc,
			percentile_cont(0.2) within group(order by jan) refjan,
			percentile_cont(0.2) within group(order by feb) reffeb,
			percentile_cont(0.2) within group(order by mar) refmar,
			percentile_cont(0.2) within group(order by apr) refapr,
			percentile_cont(0.2) within group(order by djf) refdjf,
			percentile_cont(0.2) within group(order by ndjfma) refndjfma
			from viability.idx_mth
			where model in (select distinct model from  crocus.tracc_model)
			and envid = ", envid, " and scenario = 'historical'
			and season + 1 >= 1976 and season + 1 <= 2005 and snowtype = 'ns&gro_gro'
			group by 1,2
		),

		--snow supply risk to ski tourism for each tracc
		c as (
			select distinct d.snowtype, c.tracc, 
			sum(case when nov <= refnov then 1 else 0 end)::numeric/count(*) freqnov, 
			sum(case when dec <= refdec then 1 else 0 end)::numeric/count(*) freqdec
			from crocus.tracc a
			join crocus.tracc_model b on a.gcm = b.gcm and a.rcm = b.rcm
			join crocus.tracc_gwl c on a.gwl = c.gwl
			join viability.idx_mth d on b.model = d.model and scen = scenario
				and season >= an_deb and season <= an_fin
			join a e on d.envid = e.envid
			where d.envid = ", envid, "
			group by 1, 2
		), d as (
			select distinct d.snowtype, c.tracc, 
			sum(case when jan <= refjan then 1 else 0 end)::numeric/count(*) freqjan, 
			sum(case when feb <= reffeb then 1 else 0 end)::numeric/count(*) freqfeb, 
			sum(case when mar <= refmar then 1 else 0 end)::numeric/count(*) freqmar, 
			sum(case when apr <= refapr then 1 else 0 end)::numeric/count(*) freqapr, 
			sum(case
					when (djf <= refdjf and refdjf < 100) or (djf < refdjf and refdjf = 100) then 1
					when djf > refdjf and f.snowtype is not null and djf < ncpart then 1
				else 0 end)::numeric/count(*) freqdjf, 
			sum(case
					when (ndjfma <= refndjfma and refdjf < 100) or (ndjfma < refndjfma and refndjfma = 100) then 1
					when ndjfma > refndjfma and f.snowtype is not null and ndjfma < ncpart then 1
				else 0 end)::numeric/count(*) freqndjfma
			from crocus.tracc a
			join crocus.tracc_model b on a.gcm = b.gcm and a.rcm = b.rcm
			join crocus.tracc_gwl c on a.gwl = c.gwl
			join viability.idx_mth d on b.model = d.model and scen = scenario
				and season + 1 >= an_deb and season + 1 <= an_fin
			join b e on d.envid = e.envid
			left join ncpart f on d.envid = f.envid and d.snowtype = f.snowtype
			where d.envid = ", envid, "
			group by 1, 2
		), e as (
			select c.*, freqjan, freqfeb, freqmar, freqapr, freqdjf, freqndjfma
			from c
			join d on c.snowtype = d.snowtype and c.tracc = d.tracc
			order by 1,2
		), f as (
			select 'November' mth, snowtype, tracc, null q20, freqnov freq20
			from e
			union
			select 'December' mth, snowtype, tracc, null q20, freqdec
			from e
			union
			select 'January' mth, snowtype, tracc, null q20, freqjan
			from e
			union
			select 'February' mth, snowtype, tracc, null q20, freqfeb
			from e
			union
			select 'March' mth, snowtype, tracc, null q20, freqmar
			from e
			union
			select 'April' mth, snowtype, tracc, null q20, freqapr
			from e
			union
			select 'DJF' mth, snowtype, tracc, null q20, freqdjf
			from e
			union
			select 'NDJFMA' mth, snowtype, tracc, null q20, freqndjfma
			from e
		)
		--  massif, snowtype, gwl, sum(q20 * rsize) / sum(rsize) q20, sum(freq20 * rsize) / sum(rsize) freq20
		select * from f
		order by case 
			when mth ilike 'nov%' then 1
			when mth ilike 'dec%' then 2
			when mth ilike 'jan%' then 3
			when mth ilike 'feb%' then 4
			when mth ilike 'mar%' then 5
			when mth ilike 'apr%' then 6
			when mth ilike 'djf' then 7
		else 8 end
		
		/*union
		select distinct mth, snowtype, 0.6, null, 0.2
		from f
		order by 1,2,3*/
    ")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    data <- dbGetQuery(con, query)
    dbDisconnect(con)
    
    ### sort massif 
    massifs = unique(data[,1])
    print(massifs)
    
    library(svglite)
    path = paste0(dstdir, envid, "_embers_mth_q20_gro_snowtype.pdf")
    cairo_pdf(path, width = 10, height = 8, family = "Nimbus Sans L")
    nf = layout(matrix(c(
                rep(1,4),rep(2,4),rep(3,1)
              ),9,1))
    # layout.show(nf)
    # m = "Alps-AT"
    # massifs = unique(data[data[,1]==m,1])


    ymax = 200
    ylim = c(-.1*ymax,ymax + .05*ymax)

    barw = 80
    xmar = 70
    nbmassifs = length(massifs)
    nbrows = 2
    mperrow = ceiling(nbmassifs / nbrows)
    print(nbmassifs)
    nbbars = length(snowtypes)
    xfactor = mperrow * nbbars
    xmax = barw * xfactor
    w = xmar + xmax
    xlim = c(0,w)
    xinter = barw * .1
    xinterbar = barw * .6
    xw = (xmax - xinter * xfactor  - xinterbar * mperrow) / xfactor

    for (rnum in 1:nbrows){
        par(mar = c(0,0,0,0), family = "Nimbus Sans L")
        plot(0,0, type = "n", xlim = xlim, ylim = ylim,
             xlab = NA, ylab = NA, axes = F
        )
        
        
        x = xmar + xinterbar
        
        m2 = mperrow * rnum
        m1 = m2 - mperrow + 1
        if (m2 > nbmassifs){
            m2  = nbmassifs
        }
        for (mnum in m1:m2){
          m = massifs[mnum]
          # print(m)
          mdata = data[data[,1] == m,]
          # mcol = unique(mdata[,6])
          mcol = "black"
          # print(mcol)
          
          # add massif name
          xtxt = x - 0.2 * xinterbar
          ytxt = ymax / 2
          mlab = mapply(paste,strwrap(m,width=36,simplify = FALSE),collapse = "\n")
          text(xtxt, ytxt, labels = mlab, adj = c(.5,0), srt = 90, cex = 1.2, col = mcol, font = 2)
          
               
          # add plot for each snowtype
          for (st in snowtypes){        
            stdata = mdata[mdata[,2] == st,]
            # draw ember diagrams
            ycex = 4 / ymax
            
            gwls = sort(unique(stdata[,3]))
            gwls = c(.6,gwls)
            y = 0
            scol = 1
            if (is.na(stdata[1,3]) == FALSE){
                for (gwl in gwls){
                    #print(paste0("month: ", mth, ", GWL: ", gwl))
                    ecol = round(stdata[stdata[,3] == gwl,5]*nbcols)
                    #ecol = cols[ecolidx]
                    yy = gwl / ycex
                    if (gwl == gwls[1]){
                        gwlcols = rep(cols[scol], (yy-y))
                        ecol = round(breaks[1]*nbcols)
                    } else {
                        gwlcols = colorRampPalette(cols[scol:ecol])(yy-y)
                    }
                    for (i in 1:(yy-y)){
                      polygon(
                        c(x, x + xw, x + xw, x),
                        c(y, y, y + 1, y + 1),
                        col = gwlcols[i],
                        border = NA
                      )
                      y = y + 1
                    }

                    scol = ecol + 1
                    if (scol >= nbcols){scol = nbcols}
                }
                polygon(
                x = c(x, x + xw, x + xw, x),
                y = c(0,0,y,y),
                col = NA,
                border = mcol, lwd = .8
                )
                
            } else {
                # print(mcol)
                polygon(
                x = c(x, x + xw, x + xw, x),
                y = c(0,0,ymax,ymax),
                col = "lightgrey",
                border = mcol, lwd = .5
                )
            }

          
          x = x + xw + xinter
          
          }
          
          x = x + xinterbar
        }
        
        x = xmar - 5
        lines(c(x, x), c(0,ymax), lwd = .5)
        # labval = gwls[gwls != .6]
        labval = gwls
        lab = paste0(labval,"°C")
        labval = labval / ycex
        x2 = x - 5
        for (i in 1:length(labval)){
            y = labval[i]
            lines(c(x, x2), c(y,y), lwd = .5)
            if (y != max(labval)){
                lines(c(x, w), c(y, y), lty = 3, lwd = 1, col = "grey")
            }
            text(x2 - 1, y, labels = lab[i], adj = c(1,.5), cex = 1)
        }

        text(-60, ymax/2, labels = "Global warming level (°C)", adj = c(.5,0), cex = 1.6, font = 2, srt = 90)
    }
    
    # add footer colorscale
    xfootmax = (nbcols / 8) * 2 + nbcols
    par(mar = c(0,0,0,0), xpd = T, family = "Nimbus Sans L")
    plot(0,0, type = "n", xlim = c(0, xfootmax), ylim = c(0,4),
       xlab = NA, ylab = NA, axes = F
    )


    lab = c(0.2, breaks[2:length(breaks)])
    xoffset = (xfootmax / 2) - ((((max(lab) + .1) * nbcols) - ((min(lab) - .1) * nbcols)) / 2)
    xoffset = 0
    
    
    text(xoffset + (max(lab) * nbcols + xoffset) / 2, 3.4, labels = "Risk level", adj = c(.5,0), cex = 1.5, font = 2, col = "black")
    
    # xoffset version
    labx = lab * nbcols + xoffset - (.1 *  nbcols)
    
    x = xoffset
    y = 1
    for (i in ((min(lab) - .1) * nbcols):((max(lab) + .1) * nbcols - 1)){
    polygon(
        c(x, x + 1, x + 1, x),
        c(y, y , y + 2, y + 2),
        col = cols[i], border = NA            
    )
    x = x + 1     
    }
    
    polygon(
    c(xoffset, (max(lab)) * nbcols + xoffset, (max(lab)) * nbcols + xoffset, xoffset),
    c(y, y , y + 2, y + 2),
    col = NA, border = "black"
    )
    
    tck = .2
    for (i in 1:length(lab)){
        lines(c(labx[i], labx[i]), c(y, y + tck))
        lines(c(labx[i], labx[i]), c(y + 2, y + 2 - tck))
        text(labx[i], y + 1, labels = lab[i], adj = c(.5, .5), cex = 1, font = 2)    
    }

    x = c()
    for (i in 1:(length(breaks) + 1)){
      if (i == 1){
        x = (min(lab)) * nbcols + xoffset - (.1 *  nbcols)
      } else if (i < length(breaks)) {
        x = c(x, xoffset - (.1 *  nbcols) + breaks[i] * nbcols)
      } else {
        x = c(x, xoffset - (.1 *  nbcols) + breaks[i] * nbcols)
        # x = c(x, (nbcols / 8) + (breaks[i - 1] + (breaks[i] - breaks[i - 1]) / 2) * nbcols)
      }
    }
    # x = c((nbcols / 8), 150, 300, xfootmax - (nbcols / 8))
    y = (rep(y - .2, 4))
    adj = data.frame(c(.5, .5, .5, .5), c(1, 1, 1, 1))
    lab = c("Undetect.", "Moderate", "High", "Very high")
    for (i in 1:length(lab)){
    text(x[i], y[i], labels = lab[i], adj = c(adj[i,1], adj[i,2]), cex = 1, font = 2)
    }

    dev.off()
    embed_fonts(path)
}

################FUNCTIONS END

inds = c('7331A')

for (i in 1:length(inds)){
    ind = inds[i]
    query = paste0("select name from crosscut.resorts where ind = '",ind,"';")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    sta <- dbGetQuery(con, query)
    dbDisconnect(con)

    src_sta = paste0("/home/francois/data/", sta, "/results/")

    query = paste0("select distinct envid, name from crosscut.envelopes
    where ind = '",ind,"'
    --and envid = 349
    order by 1")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    listenv <- dbGetQuery(con, query)
    dbDisconnect(con)
	
	for (envid in 1:nrow(listenv)){
		envid = listenv[envid,1]
		query = paste0("
			select distinct status from crosscut.sm_resort a, crosscut.envelopes b
			where envid = ",envid," and a.ind = b.ind and st_intersects(a.geom, b.geom);")
		con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
		sm <- dbGetQuery(con, query)
		dbDisconnect(con)
		snowtypes = c("ns&gro_nn", "ns&gro_gro") #ordered snowtypes to be drawn
		nbsm = nrow(sm)
		if (nbsm > 0){
			sm = paste0(sm[,1], "_lance")
			snowtypes = c(snowtypes, sm)
		}
		# print(snowtypes)
		colvec= c("#ffffff00", "#fbc907ff", "#c41f26ff", "#7b1a62ff") 
		target = 1
		breaks = c(0.2, .3, .4, .5) # breaks must be sorted, length(breaks) == length(colvec), and max(breaks) < target
		dstdir = paste0(src_sta, envid, "/plots/") 

		plot_embers_mth_relative(dstdir, envid, colvec, target, breaks, snowtypes)
		plot_embers_mth_gro(dstdir, envid, colvec, target, breaks, snowtypes)
	}
}