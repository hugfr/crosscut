from osgeo import gdal
from osgeo import ogr
from osgeo import osr
import psycopg2
import numpy as np
import os
from subprocess import call
from shutil import copyfile
import multiprocessing
import conn_param

gdal.UseExceptions()

def create_geotiff (dst_file, xsize, ysize, src_trans, src_proj):
    format = "GTiff"
    driver = gdal.GetDriverByName(format)
    if os.path.isfile(dst_file):
        os.remove(dst_file)
    dst_ds = driver.Create(dst_file, xsize, ysize, 1, gdal.GDT_Byte, [ 'TILED=YES', 'COMPRESS=LZW' ])
    dst_ds.SetGeoTransform(src_trans)
    dst_ds.SetProjection(src_proj)
    return dst_ds

def season_nbdays_raster_launcher(srcdir, envid, period):
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()   
    srcdir = os.path.join(srcdir, f"{envid}", "seasonal_rasters")
    dirlist = os.listdir(srcdir)
    # if "lance" in dirlist or "fan" in dirlist:
        # snowtypes = [st for st in dirlist if st in ["fan", "lance"]]
    # else :
        # snowtypes = ["gro"]
    snowtypes = [st for st in dirlist if st in ["gro", "fan", "lance"]]
    print(snowtypes)
    
    dstdir = os.path.join(srcdir, "mixed")
    if os.path.isdir(dstdir) == False:
        os.mkdir(dstdir)
     
    # add seasons filter
    end = "XXXX.tif"
    seasons = [2020,2035,2050,2065, 2080, 2092]
    
    args = []
    for snowtype in snowtypes:
        # print(snowtype)
        # input()
        snowsrc = os.path.join(srcdir, snowtype)
        snowdst = os.path.join(dstdir, snowtype)
        if os.path.isdir(snowdst) == False:
            os.mkdir(snowdst)
        scenarios = os.listdir(snowsrc)
        
        for scenario in scenarios:
            # print(scenario)
            scensrc = os.path.join(snowsrc, scenario)
            scendst = os.path.join(snowdst, scenario)
            if os.path.isdir(scendst) == False:
                os.mkdir(scendst)
            vars = os.listdir(scensrc)
            
            for var in vars:
                # print(var)
                if var in ["mean", "q20"]:
                    varsrc = os.path.join(scensrc, var)
                    vardst = os.path.join(scendst, var)
                    if os.path.isdir(vardst) == False:
                        os.mkdir(vardst)
                        
                    files = os.walk(varsrc)
                    for f in files:
                        files = f[2]
                    
                    for file in files:
                        # print(file)
                        season = int(file[len(file) - len(end):len(file)].replace(".tif", ""))
                        if season in seasons:
                            args.append((srcdir, dstdir, envid, snowtype, scenario, var, file))
                    
    print("args done")
    
    pool = multiprocessing.Pool(processes = 20)
    pool.map(season_nbdays_raster, args)
    pool.close()
    
def season_nbdays_raster(args):
    srcdir, dstdir, envid, snowtype, scenario, var, file = args
    print(f"{envid} ##### {snowtype} : {scenario} ({var}) ##### {file}")
    
    # Open the reference ns&gro_nn raster
    if var == "stddev":
        end = f"len_sd_XXXX.tif"
    else:
        end = f"len_{var}_XXXX.tif"
        
    nnfile = file[len(file)-len(end):len(file)]
    rastfile = os.path.join(srcdir, "nn", scenario, var, nnfile)
    nnrast = gdal.Open(rastfile)
    src_trans = nnrast.GetGeoTransform()
    src_proj = nnrast.GetProjection()
    nnband = nnrast.GetRasterBand(1)
    xsize = nnband.XSize
    ysize = nnband.YSize
    ndv = nnband.GetNoDataValue()
    nnarr = nnband.ReadAsArray()
    
    #save bb shapefile
    bbfile = os.path.join(dstdir, f"{envid}_rast_bb.shp")
    driver = ogr.GetDriverByName('ESRI Shapefile')
    # print(bbfile)
    # print(os.path.exists(bbfile))
    # input()
    if os.path.exists(bbfile) == False:
        bbshp = driver.CreateDataSource(bbfile)
        srs = osr.SpatialReference()
        srs.ImportFromEPSG(2154)
        bblayer = bbshp.CreateLayer('bb', srs)
        bblayer.CreateField(ogr.FieldDefn("id", ogr.OFTInteger))
        bbfeat = ogr.Feature(bblayer.GetLayerDefn())
        bbfeat.SetField("id", envid)
        x0 = src_trans[0]
        x1 = x0 + xsize * src_trans[1]
        y0 = src_trans[3]
        y1 = y0 + ysize * src_trans[5]
        bbwkt = f"POLYGON(({x0} {y0}, {x1} {y0}, {x1} {y1}, {x0} {y1}, {x0} {y0}))"
        bb = ogr.CreateGeometryFromWkt(bbwkt)
        bbfeat.SetGeometry(bb)
        bblayer.CreateFeature(bbfeat)
        bbfeat = None
        bbshp = None       
    
    if snowtype == 'gro':
        # Cutting to sql cutline with gdalwarp and create tmp gro raster        
        rastfile = os.path.join(srcdir, snowtype, scenario, var, file)
        grorast = gdal.Open(rastfile)
        dstdir = os.path.join(dstdir, snowtype, scenario, var)
        dst_file = os.path.join(dstdir, file)
        
        connString = f"PG: host = {conn_param.host} dbname = {conn_param.dbname} user={conn_param.user} password={conn_param.password}"
        sql = f"""
            select st_buffer(st_union(st_makevalid(a.geom)), 0.001) geom
            from crosscut.tracks a
            where a.ind = (select ind from crosscut.envelopes where envid = {envid})
            """
        
        sql = sql.replace("\n", " ").replace("\r", " ")
        
        gdal.Warp(dst_file, grorast,
                format = "GTiff",
                warpOptions = ['COMPRESS=LZW', 'TILED=YES'],
                cutlineDSName = connString,
                cutlineSQL = sql,
                dstNodata = ndv
            )
            
        grorast = None
        
        # Replace pix values, write and close raster
        grorast = gdal.Open(dst_file, gdal.GA_Update)
        groband = grorast.GetRasterBand(1)
        groarr = groband.ReadAsArray()
        groarr[np.logical_and(groarr == ndv, nnarr != ndv)] = nnarr[np.logical_and(groarr == ndv, nnarr != ndv)]
        groband.WriteArray(groarr)
        
    else:
        # Warp sm rast and create dst raster 
        rastfile = os.path.join(srcdir, snowtype, scenario, var, file)
        rast = gdal.Open(rastfile)
        dstdir = os.path.join(dstdir, snowtype, scenario, var)
        dst_file = os.path.join(dstdir, file)
        
        # Cutting to sql cutline with gdalwarp and create dst raster
        connString = f"PG: host = {conn_param.host} dbname = {conn_param.dbname} user={conn_param.user} password={conn_param.password}"
        status = file[0:len(file)-len(end)-1]
        # print(status)
        sql = f"""
            select status, st_collectionextract(st_intersection(st_union(st_makevalid(a.geom)), st_union(b.geom)),3) geom
            from crosscut.tracks a, crosscut.sm_resort b
            where a.ind = (select ind from crosscut.envelopes where envid = {envid}) 
            and a.ind = b.ind and status = '{status}'
            group by 1
            """
        
        sql = sql.replace("\n", " ").replace("\r", " ")
        
        gdal.Warp(dst_file, rast,
                format = "GTiff",
                warpOptions = ['COMPRESS=LZW', 'TILED=YES'],
                cutlineDSName = connString,
                cutlineSQL = sql,
                dstNodata = ndv
            )
        
        # Cutting to sql cutline with gdalwarp and create tmp gro raster
        rastfile = os.path.join(srcdir, "gro", scenario, var, nnfile)
        tmpfile = os.path.join(dstdir, f"tmpgro_{status}_{nnfile}")
        tmprast = gdal.Open(rastfile)
        sql = f"""
            select status, st_buffer(st_collectionextract(st_difference(st_union(st_makevalid(a.geom)), st_collectionextract(st_intersection(st_union(st_makevalid(a.geom)), st_union(b.geom)),3)),3),0.001) geom
            from crosscut.tracks a, crosscut.sm_resort b
            where a.ind = (select ind from crosscut.envelopes where envid = {envid}) 
            and a.ind = b.ind and status = '{status}'
            group by 1
            """
        
        sql = sql.replace("\n", " ").replace("\r", " ")
        
        gdal.Warp(tmpfile, tmprast,
                format = "GTiff",
                warpOptions = ['COMPRESS=LZW', 'TILED=YES'],
                cutlineDSName = connString,
                cutlineSQL = sql,
                dstNodata = ndv
            )
            
        tmprast = None
        tmprast = gdal.Open(tmpfile)
        tmparr = tmprast.GetRasterBand(1).ReadAsArray()
        
        # Replace pix values, write and close raster
        smrast = gdal.Open(dst_file, gdal.GA_Update)
        smband = smrast.GetRasterBand(1)
        smarr = smband.ReadAsArray()
        # print(np.count_nonzero(smarr != ndv))
        smarr[tmparr != ndv] = tmparr[tmparr != ndv]
        # print(np.count_nonzero(smarr != ndv))
        smarr[np.logical_and(smarr == ndv, nnarr != ndv)] = nnarr[np.logical_and(smarr == ndv, nnarr != ndv)]
        # print(np.count_nonzero(smarr != ndv))
        smband.WriteArray(smarr)
        os.remove(tmpfile)
        
    smrast = None
    tmprast = None
    grorast = None
    nnrast = None
    
    
    
# psycopg2 connection to DB
myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()

inds = ['0507S','3807', '3809']
# inds = ['0507S']
period = 15

for ind in inds:
    query = """
        select name from crosscut.resorts
        where ind = %s
    """
    cur.execute(query,(ind,))
    name = cur.fetchone()[0]
    srcdir = os.path.join("/home/francois/data/", name, "results/")

    query = """
        select distinct envid from crosscut.envelopes
        where ind = %s
    """
    cur.execute(query,(ind,))

    for envid in cur:
        envid = envid[0]
        if envid >= 105:
            season_nbdays_raster_launcher(srcdir, envid, period)
    # args = (os.path.join(srcdir, f"{91}", "seasonal_rasters"),
            # os.path.join(srcdir, f"{91}", "seasonal_rasters", "mixed"),
            # 91, "gro", "rcp85", "mean",
            # f"len_mean_2037.tif"
    # )
    # season_nbdays_raster(args)