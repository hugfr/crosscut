library(rgdal)
library(raster)
library(curl)
library(RPostgreSQL)
# library(OpenStreetMap)
rm(list = ls())

generate_ign_wms_gdalxml <- function(src_dir,ulx,uly,lrx,lry){
  myxml <- paste0("<GDAL_WMTS>
                  <GetCapabilitiesUrl>http://wxs.ign.fr/an7nvfzojv5wa96dsga5nk8w/geoportail/wmts?SERVICE=WMTS&amp;REQUEST=GetCapabilities</GetCapabilitiesUrl>
                  <Layer>GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD</Layer>
                  <Style>normal</Style>
                  <TileMatrixSet>PM</TileMatrixSet>
                  <TileMatrix>15</TileMatrix>
                  <Format>image/jpeg</Format>
                  <DataWindow>
                  <UpperLeftX>",ulx,"</UpperLeftX>
                  <UpperLeftY>",uly,"</UpperLeftY>
                  <LowerRightX>",lrx,"</LowerRightX>
                  <LowerRightY>",lry,"</LowerRightY>
                  </DataWindow>
                  <BandsCount>4</BandsCount>
                  <Cache />
                  <UnsafeSSL>true</UnsafeSSL>
                  <ZeroBlockHttpCodes>404</ZeroBlockHttpCodes>
                  <ZeroBlockOnServerException>true</ZeroBlockOnServerException>
                  </GDAL_WMTS>")
  write(myxml,paste0(src_dir,"tmp.xml"))
}

findtiles <- function(xmin,xmax,ymin,ymax,pixsize,myext){
  wmtspixsize.df = t(data.frame(
    c(0,156543.0339280410,1.40625000000000000000,"1 : 559082264"),
    c(1,78271.5169640205,0.70312500000000000000,"1 : 279541132"),
    c(2,39135.7584820102,0.35156250000000000000,"1 : 139770566"),
    c(3,19567.8792410051,0.17578125000000000000,"1 : 69885283"),
    c(4,9783.9396205026,0.08789062500000000000,"1 : 34942642"),
    c(5,4891.9698102513,0.04394531250000000000,"1 : 17471321"),
    c(6,2445.9849051256,0.02197265625000000000,"1 : 8735660"),
    c(7,1222.9924525628,0.01098632812500000000,"1 : 4367830"),
    c(8,611.4962262814,0.00549316406250000000,"1 : 2183915"),
    c(9,305.7481131407,0.00274658203125000000,"1 : 1091958"),
    c(10,152.8740565704,0.00137329101562500000,"1 : 545979"),
    c(11,76.4370282852,0.00068664550781250000,"1 : 272989"),
    c(12,38.2185141426,0.00034332275390625000,"1 : 136495"),
    c(13,19.1092570713,0.00017166137695312500,"1 : 68247"),
    c(14,9.5546285356,0.00008583068847656250,"1 : 34124"),
    c(15,4.7773142678,0.00004291534423828120,"1 : 17062"),
    c(16,2.3886571339,0.00002145767211914060,"1 : 8531"),
    c(17,1.1943285670,0.00001072883605957030,"1 : 4265"),
    c(18,0.5971642835,0.00000536441802978516,"1 : 2133"),
    c(19,0.2985821417,0.00000268220901489258,"1 : 1066"),
    c(20,0.1492910709,0.00000134110450744629,"1 : 533"),
    c(21,0.0746455354,0.00000067055225372315,"1 : 267")
  ))
  rownames(wmtspixsize.df)= wmtspixsize.df[,1]
  
  tlc = c(-20037508.3427892476320267,20037508.3427892476320267)
  
  #find zoom level
  wmtspixsize.df = cbind(wmtspixsize.df, as.numeric(wmtspixsize.df[,2]) - pixsize)
  zoomlevel = as.vector(wmtspixsize.df[wmtspixsize.df[,5] == max(as.numeric(wmtspixsize.df[as.numeric(wmtspixsize.df[,5])<=0,5])),1:2])
  height = ceiling(myext / as.numeric(zoomlevel[2]))
  #find xlim and ylim
  xcenter = (xmin + xmax) / 2
  ycenter = (ymin + ymax) / 2
  xmin = tlc[1] + round((xcenter - tlc[1]) / as.numeric(zoomlevel[2])) * as.numeric(zoomlevel[2]) - (round(height / 2) * as.numeric(zoomlevel[2]))
  xmax = xmin + as.numeric(zoomlevel[2]) *  height
  # xmax = tlc[1] + ceiling((xmax - tlc[1]) / as.numeric(zoomlevel[2])) * as.numeric(zoomlevel[2])
  # ymin = tlc[2] - ceiling((tlc[2] - ymin) / as.numeric(zoomlevel[2])) * as.numeric(zoomlevel[2])
  ymax = tlc[2] - round((tlc[2] - ycenter) / as.numeric(zoomlevel[2])) * as.numeric(zoomlevel[2]) + (round(height / 2) * as.numeric(zoomlevel[2]))
  ymin = ymax - as.numeric(zoomlevel[2]) * height
  
  #find tilecol and tilerow
  tilesize = 256 * as.numeric(zoomlevel[2])
  tilecolmin = floor((xmin - tlc[1])/ (256 * as.numeric(zoomlevel[2])))
  tilecolmax = ceiling((xmax - xmin) / (256 * as.numeric(zoomlevel[2]))) + tilecolmin
  tilerowmin = floor((tlc[2] - ymax) / (256 * as.numeric(zoomlevel[2])))
  tilerowmax = ceiling((ymax - ymin) / (256 * as.numeric(zoomlevel[2]))) + tilerowmin
  tileset = c(zoomlevel[1],tilecolmin, tilecolmax, tilerowmin, tilerowmax, zoomlevel[2], tlc[1], tlc[2], xmin, xmax, ymin, ymax, height)
  # print(c((tlc[2] - ymax) / (256 * as.numeric(zoomlevel[2])),(ymax - ymin) / (256 * as.numeric(zoomlevel[2]))))
  return(tileset)
}

graph_altitude <-function(dst_dir, envid, mycol){
  query <- paste0("
    with type_sta as (
	select case
		when mp_tot <= 2500 then 'Petites stations'
		when mp_tot <= 6000 then 'Stations moyennes'
		when mp_tot <= 15000 then 'Grandes stations'
	else 'Très grandes stations' end type_sta, val, surf, mp_part,mp_tot
	from stations.dsg_properties a
	join (
		select ind, sum(mp) mp_tot from stations.meteo_crocus_alt_mp group by 1
	) b on a.ind = b.ind
	where rast_type = 'crocus_altitude'
)

select 3::integer ordering, name id, val, surf*100/sum(surf) over() surf_part, mp_part
from crosscut.env_prop a
join crosscut.envelopes b on a.envid = b.envid
where a.envid = ", envid, " and rast_type = 'altitude'

union

select 2, 'Département', val,sum(surf)*100/ sum(sum(surf)) over(),
sum(mp_part*mp_tot/100)*100/sum(sum(mp_part*mp_tot/100)) over()
from stations.dsg_properties a
join (
	select ind, sum(mp) mp_tot from stations.meteo_crocus_alt_mp group by 1
) b on a.ind = b.ind
where substr(a.ind,1,2) = (
	select substr(b.ind,1,2)
	from crosscut.envelopes b
	where envid = ", envid, "
) and rast_type = 'crocus_altitude'
group by 1,val

union

select 1, type_sta, val,
sum(surf)*100/ sum(sum(surf)) over(),
sum(mp_part*mp_tot/100)*100/sum(sum(mp_part*mp_tot/100)) over()
from type_sta
where type_sta = (
	select distinct  case
	when mp_tot <= 2500 then 'Petites stations'
	when mp_tot <= 6000 then 'Stations moyennes'
	when mp_tot <= 15000 then 'Grandes stations'
	else 'Très grandes stations' end type_sta
	from crosscut.envelopes
	join crosscut.env_mp_alt on envelopes.envid = env_mp_alt.envid
	where envelopes.envid = ", envid, "
)
group by 1, type_sta, val
order by 1,3
  ")

con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  data <- dbGetQuery(con, query)
  dbDisconnect(con)
  
  png(paste0(dst_dir,envid,"_altitude_graph.png"),
  width = 500, height = 600, bg = "transparent", res = 150)
  layout(matrix(c(1,1,1,1,2,2),1,6))
  par(mar=c(8,3,1.2,0))
  
  # # prepare data for plotting surf_part
  # data <- data[data[,3]!=0,1:4]
  # prepare data for plotting mp_part
  data <- data[data[,3]!=0,c(1:3,5)]
  
  data <- cast(data,val~id)
  data <- as.matrix(data)
  data[is.na(data)] <- 0
  min = as.numeric(rownames(data)[1])/300-1
  max = as.numeric(rownames(data)[nrow(data)])/300-1
  mycolgraph = mycol[min:max]
  colnames(data) = mapply(paste,strwrap(colnames(data),width=20,simplify = FALSE),collapse = "\n")
  barplot(data, col=mycolgraph,
          axes = F, ylab = NA, cex.names = 1, las = 2)
  axis(side = 2, tck = -.01, labels = NA)
  axis(side=2, line = -.7, cex.axis =1, lwd = 0, font.lab=2)
  mtext(side = 2, line = 1.9, "Part du moment de puissance (%)", cex = 0.8)
  
  par(mar=c(0,0,0,0))
  plot.new()
  legend("center", paste0(rev(rownames(data))," m"),
         bty = "n", fill =  rev(mycolgraph), cex=1, title = "Altitude")
  dev.off()
  return(data)
}

graph_slope <-function(dst_dir, envid){
    query <- 
    paste0("
    with type_sta as (
        select case
            when mp_tot <= 2500 then 'Petites stations'
            when mp_tot <= 6000 then 'Stations moyennes'
            when mp_tot <= 15000 then 'Grandes stations'
        else 'Très grandes stations' end type_sta, val, surf, mp_part,mp_tot
        from stations.dsg_properties a
        join (
            select ind, sum(mp) mp_tot from stations.meteo_crocus_alt_mp group by 1
        ) b on a.ind = b.ind
        where rast_type = 'crocus_slope'
    )

    select 3::integer ordering, name id, val, surf*100/sum(surf) over() surf_part, mp_part
    from crosscut.env_prop a
    join crosscut.envelopes b on a.envid = b.envid
    where a.envid = ", envid, " and rast_type = 'slope'

    union

    select 2, 'Département', val,sum(surf)*100/ sum(sum(surf)) over(),
    sum(mp_part*mp_tot/100)*100/sum(sum(mp_part*mp_tot/100)) over()
    from stations.dsg_properties a
    join (
        select ind, sum(mp) mp_tot from stations.meteo_crocus_alt_mp group by 1
    ) b on a.ind = b.ind
    where substr(a.ind,1,2) = (
        select substr(b.ind,1,2)
        from crosscut.envelopes b
        where envid = ", envid, "
    ) and rast_type = 'crocus_slope'
    group by 1,val

    union

    select 1, type_sta, val,
    sum(surf)*100/ sum(sum(surf)) over(),
    sum(mp_part*mp_tot/100)*100/sum(sum(mp_part*mp_tot/100)) over()
    from type_sta
    where type_sta = (
        select distinct  case
        when mp_tot <= 2500 then 'Petites stations'
        when mp_tot <= 6000 then 'Stations moyennes'
        when mp_tot <= 15000 then 'Grandes stations'
        else 'Très grandes stations' end type_sta
        from crosscut.envelopes
        join crosscut.env_mp_alt on envelopes.envid = env_mp_alt.envid
        where envelopes.envid = ", envid, "
    )
    group by 1, type_sta, val
    order by 1,3
    "
    )

    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    data <- dbGetQuery(con, query)
    dbDisconnect(con)

    mycol <- c('#1f78b4','#33a02c','#ff7f00')
    mylab <- vector()
    png(paste0(dst_dir,envid,"_slope_graph.png"),
     width = 600, height = 450, res = 150, bg = "transparent")
    par(mar=c(2,2,0,0))
    plot.new()
    for (i in 1:3){
    mylab[i] <- as.vector(mapply(paste,strwrap(unique(data[data[,1]==i,2]),width=25,simplify = FALSE),collapse = "\n"))
    x <- data[data[,1]==i,3]
    y <- data[data[,1]==i,4]
    par(new=T)
    plot(x,y, type = "p",
    xlim = c(min(data[,3]),max(data[,3])),
    ylim = c(0,max(data[,4])),
    axes = F,
    xlab = NA,
    ylab = NA,
    pch = 20,
    col = mycol[i]
    )

    polygon(
        c(min(x), x, max(x)), c(0, y, 0), 
        col = paste0(mycol[i],"70"), lwd =2, border = mycol[i] )
    }

    axis(side = 1, tck = -.01, labels = NA)
    axis(side=1, line = -.8, lwd = 0, cex.axis = 0.6, font.lab=2)
    mtext(side=1, line=1.05, cex=0.7, colnames(data)[3])

    axis(side = 2, tck = -.01, labels = NA)
    axis(side=2, line = -.6, lwd = 0, cex.axis = 0.6, font.lab=2)
    mtext(side=2, line=1.1, cex=0.7, colnames(data)[4])

    grid(col = "lightgray", lty = "dotted",
         lwd = par("lwd"), equilogs = TRUE)

    legend(
        "topright",
        rev(mylab),
        bty = "n",
        fill =  paste0(rev(mycol),"70"),
        border = rev(mycol),
        cex=0.7)
    dev.off()
}

graph_aspect <-function(dst_dir, envid){
	query <- paste0("
	with type_sta as (
	select case
		when mp_tot <= 2500 then 'Petites stations'
		when mp_tot <= 6000 then 'Stations moyennes'
		when mp_tot <= 15000 then 'Grandes stations'
	else 'Très grandes stations' end type_sta, val, surf, mp_part,mp_tot
	from stations.dsg_properties a
	join (
		select ind, sum(mp) mp_tot from stations.meteo_crocus_alt_mp group by 1
	) b on a.ind = b.ind
	where rast_type = 'crocus_aspect'
)

select 3::integer ordering, name id, val, surf*100/sum(surf) over() surf_part, mp_part
from crosscut.env_prop a
join crosscut.envelopes b on a.envid = b.envid
where a.envid = ", envid, " and rast_type = 'aspect'

union

select 2, 'Département', val,sum(surf)*100/ sum(sum(surf)) over(),
sum(mp_part*mp_tot/100)*100/sum(sum(mp_part*mp_tot/100)) over()
from stations.dsg_properties a
join (
	select ind, sum(mp) mp_tot from stations.meteo_crocus_alt_mp group by 1
) b on a.ind = b.ind
where substr(a.ind,1,2) = (
	select substr(b.ind,1,2)
	from crosscut.envelopes b
	where envid = ", envid, "
) and rast_type = 'crocus_aspect'
group by 1,val

union

select 1, type_sta, val,
sum(surf)*100/ sum(sum(surf)) over(),
sum(mp_part*mp_tot/100)*100/sum(sum(mp_part*mp_tot/100)) over()
from type_sta
where type_sta = (
	select distinct  case
	when mp_tot <= 2500 then 'Petites stations'
	when mp_tot <= 6000 then 'Stations moyennes'
	when mp_tot <= 15000 then 'Grandes stations'
	else 'Très grandes stations' end type_sta
	from crosscut.envelopes
	join crosscut.env_mp_alt on envelopes.envid = env_mp_alt.envid
	where envelopes.envid = ", envid, "
)
group by 1, type_sta, val
order by 1,3"
	)
    
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	data <- dbGetQuery(con, query)
	dbDisconnect(con)

	png(paste0(dst_dir,envid,"_aspect_graph.png"),
	width = 450, height = 300, bg = "white")
	layout(matrix(c(1,1,1,1,1,2,2),1,7))
	par(mar=c(3,4,1,0), cex.axis  = 1, cex.lab = 1.1)
	mycol <- c('#1f78b4','#33a02c','#ff7f00')
	mylab <- vector()
	mylim <- ceiling(max(data[,4])/5)*5

	mylab[1] <- as.vector(mapply(paste,strwrap(unique(data[data[,1]==1,2]),width=15,simplify = FALSE),collapse = "\n"))
	data2 <- cast(data[data[,1]==1,2:4], id ~ val, value = "surf_part")
	data2 <- as.matrix(data2)
	data[is.na(data)] <- 0
	radial.plot(
		data2[,1:length(data2)],
		labels=c("N","NE","E","SE","S","SO","W","N0"),
		rp.type="p",
		radial.lim=c(0,mylim),
		radial.labels=pretty(c(0,mylim)),
		boxed.radial=F,
		grid.unit ="%",
		line.col=mycol[1],
		lwd = 2,
		start=pi/2,
		clockwise = T,
		poly.col=paste0(mycol[1],"70")
	)


	for (i in 2:3){
	mylab[i] <- as.vector(mapply(paste,strwrap(unique(data[data[,1]==i,2]),width=15,simplify = FALSE),collapse = "\n"))
	par(new=T)
	data2 <- cast(data[data[,1]==i,2:4], id ~ val, value = "surf_part")
	data2 <- as.matrix(data2)
	data[is.na(data)] <- 0
	radial.plot(
		data2[,1:length(data2)],
		labels=NA,
		rp.type="p",
		radial.lim=c(0,mylim),
		radial.labels=NA,
		show.grid=FALSE,
		boxed.radial=F,
		line.col=mycol[i],
		lwd = 2,
		start=pi/2,
		clockwise = T,
		poly.col=paste0(mycol[i],"70")
	)
	}

	par(mar=c(0,0,0,0))
	plot.new()
	legend("center",
		rev(mylab),
		bty = "n",
		fill =  paste0(rev(mycol),"70"),
		border = rev(mycol),
		cex=1.6)
	dev.off()
}

plot_tiles <- function(dst_dir, tileset, tilesize, xmin, xmax, ymin, ymax){
  for (rows in tileset[4]:tileset[5]){
    for (cols in tileset[2]:tileset[3]){
      tilesize = 256 * as.numeric(tileset[6])
      ulx = as.numeric(tileset[7]) + (cols * tilesize)
      uly = as.numeric(tileset[8]) - (rows * tilesize)
      lrx = ulx + (tilesize)
      lry = uly - (tilesize)
	  # print(c(ulx,lrx,lry,uly))
      if (
        (lrx >= xmin && ulx <= xmax) && (lry <= ymax && uly >= ymin)
      ){
        query = paste0("http://wxs.ign.fr/an7nvfzojv5wa96dsga5nk8w/geoportail/wmts?LAYER=GEOGRAPHICALGRIDSYSTEMS.MAPS.SCAN-EXPRESS.STANDARD&EXCEPTIONS=text/xml&FORMAT=image/jpeg&SERVICE=WMTS&VERSION=1.0.0&REQUEST=GetTile&STYLE=normal&TILEMATRIXSET=PM&TILEMATRIX=",tileset[1],"&TILEROW=",rows,"&TILECOL=",cols,"&"
        )
        #print(query)
        tmpimg = paste0(dst_dir,"tmptile.jpg")
        UA = "Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US) AppleWebKit/532.0 (KHTML, like Gecko) Chrome/3.0.195.0 Safari/532.0"
        h = new_handle()
        handle_setheaders(h,
                          "Content-Type" = "image/jpeg",
                          "User-Agent" = UA
        )
        curl_download(query,tmpimg,handle = h)
		
        tmptile = paste0(dst_dir,"tmptile.tif")
        system(paste0("gdal_translate -a_srs \"EPSG:3857\" -a_ullr ",ulx," ",uly," ",lrx," ",lry," \"", tmpimg, "\" \"",tmptile,"\""))
        tile = stack(tmptile)
        #par(new=T)
        plotRGB(tile,add=T)
      }
    }
  }
}

plot_rast <- function(src_dir,dst_dir,envid, rastype){
  src_ds = paste0(src_dir, "crocus_",rastype,".tif")
  print(src_ds)
  rast = raster(src_ds)
  rast = projectRaster(rast, crs=CRS("+init=epsg:3857"), method = "ngb")
  # ATTENTION GRAPH COMMENTES POUR ANDORRE
  if (rastype == "altitude"){
	library (reshape)
    # create a unique colorramp for all alt range and keep only values regarding rasters values
    # color ramp for alt between 300 and 4800 with 300 step
    
    # Scale of blues
    mycol = colorRampPalette(c("#f7fbff","#08306b"), alpha =F)(16)
    # # Scale of greys
    # mycol = colorRampPalette(c("#FEFEFE","#010101"), alpha =F)(16)
    
    min = cellStats(rast,min)/300
    max = cellStats(rast,max)/300
    mycolrast = mycol[min:max]
    mymain = "Plages d'altitude"
	myleg = paste0(as.character(unique(rast)), " masl")
    graph_altitude(dst_dir, envid, mycol)
    myalpha = 0.9
    
    # Create csv mp file with the altitude information (mp and mp_part)
    query = paste0(
        "select distinct alt, mp, mp/mp_tot*100 mp_part
        from crosscut.env_mp_alt
        where envid = ",envid,";"
        )
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    mpalt <- dbGetQuery(con, query)
    dbDisconnect(con)
    write.csv(mpalt, paste0(dst_dir,envid,"_alt_mp.csv"), row.names = FALSE)
  }
  
  if (rastype == "slope"){
    # Yellow=>red color scale
    mycol = colorRampPalette(c("yellow","red"))(6)
    # Scale of greys
    # mycol = colorRampPalette(c("#FEFEFE","#010101"))(6)
	mycol = data.frame(seq(0,50,10),mycol)
	uval = sort(unique(rast))
	mycolrast = c()
	myleg = c()
	for (i in uval){
		mycolrast = c(mycolrast, as.vector(mycol[mycol[,1] == i,2]))
		if (i == 0){
			myleg = c(myleg, "Moins de 5°")
		} else if (i < 50){
			myleg = c(myleg, paste0(as.character(i - 5),"-",as.character(i + 5),"°"))
		} else {
			myleg = c(myleg, "Plus de 45°")
		}
	}
	mymain = "Pentes"
	myalpha = 0.8
	graph_slope(dst_dir,envid)
  }
  
  if (rastype == "aspect"){
	library (plotrix)
	library (reshape)
    #Various colors palette
    mycol = colorRampPalette(c("darkblue","yellow","orange","red","pink","purple","blue"))(8)
    # # Scale of greys
    # mycol = colorRampPalette(c("#FEFEFE","#010101"))(8)
    
	mycol = data.frame(seq(0,315,45),mycol)
	legtot = c("Nord","Nord-Est","Est","Sud-Est","Sud","Sud-Ouest","Ouest","Nord-Ouest")
	legtot = data.frame(seq(0,315,45),legtot)
	uval = sort(unique(rast))
	mycolrast = c()
	myleg = c()
	for (i in uval){
		mycolrast = c(mycolrast, as.vector(mycol[mycol[,1] == i,2]))
		myleg = c(myleg, as.vector(legtot[legtot[,1] == i,2]))
	}
	mymain = "Orientations"
	myalpha = 0.8
	graph_aspect(dst_dir,envid)
  }
  
  print("graph done")
  
  height = 600

  xmin = floor(rast@extent@xmin/100) * 100
  xmax = ceiling(rast@extent@xmax/100) * 100
  ymin = floor(rast@extent@ymin/100) * 100
  ymax = ceiling(rast@extent@ymax/100) * 100
  if ((xmax-xmin) >= (ymax-ymin)){
    myext = xmax-xmin
  } else {
    myext = ymax-ymin
  }
  xmin = ((xmax+xmin) / 2) - (myext/2)
  xmax = ((xmax+xmin) / 2) + (myext/2)
  ymin = ((ymax+ymin) / 2) - (myext/2)
  ymax = ((ymax+ymin) / 2) + (myext/2)
  # print(c(xmin,xmax,ymin,ymax))

  pixsize = (xmax - xmin)/height
  
  tileset = findtiles(xmin,xmax,ymin,ymax,pixsize,myext)
  xmin = as.numeric(tileset[9])
  xmax = as.numeric(tileset[10])
  ymin = as.numeric(tileset[11])
  ymax = as.numeric(tileset[12])
  x = c(xmin,xmax)
  y = c(ymin,ymax)
  xy = data.frame(x,y)
  
  height = as.numeric(tileset[13])
  width = round(height / 4) * 5
  png(paste0(dst_dir,envid,"_",rastype,"_map.png"),width = width, height = height, bg = "transparent")
  par(mar=c(0,0,0,0), oma=c(0,0,0,0))
  layout(matrix(c(1,1,1,1,2),1,5))
  
  plot(xy[,1], xy[,2], type = "n", xlim = x, ylim = y, col = "white", bty ="n")
  plot_tiles(dst_dir, tileset, tilesize, xmin, xmax, ymin, ymax)
  
  plot(rast,
       col=mycolrast, alpha = myalpha,
       xlim = x, ylim = y,
       add=T, legend = F)
  
  plot.new()
  plot.new()
  plot.new()
  plot.new()
  legend("center",
         myleg,
         fill = mycolrast,
         bty = 'n',
         cex = 2,
         title = mymain,
         title.col = "darkblue")
  dev.off()
  return(myleg)
}

plot_rast_period <- function(src_dir, dst_dir, ind, rastype, season, snowtype, scenario, period){
  # retrieve the right file
  if (rastype == 'mean' | rastype == 'len_mean'){
	mycolrast = c("#ffffcc", "#a1dab4","#41b6c4", "#2c7fb8", "#253494")
	myval = c(0,50,80,100,150,365)
    filetype = "len_mean"
	}
  if (rastype == 'stddev' | rastype == 'len_sd'){
	mycolrast = c("#3288bd", "#66c2a5","#e6f598", "#fdae61", "#f46d43")
	myval = c(0,10,20,30,40,365)
    filetype = "len_sd"
	}
  mymain = "Nombre de jours"
  myleg = c(paste0("Moins de ",myval[2]), paste0("Moins de ",myval[3]), paste0("Moins de ",myval[4]), paste0("Moins de ",myval[5]), paste0("Plus de ",myval[5]))
  myalpha = 0.9
  
  src_dir = paste0(src_dir, "seasonal_rasters/", snowtype, "/", scenario,"/",rastype,"/")
  if (snowtype == 'nn' | snowtype == 'gro'){
    stypes = c(snowtype)
    filelist = c(paste0(src_dir, filetype,"_", season,".tif"))
  } else {
    filelist = c()
    stypes = c()
    query = paste0("select distinct status from crosscut.sm_resort where ind = '",ind,"';")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    sm <- dbGetQuery(con, query)
    dbDisconnect(con)
    for (status in sm){
        st = paste0(snowtype,"_", status)
        file = c(paste0(src_dir, status, "_", filetype,"_", season,".tif"))
        stypes = c(stypes, st)
        filelist = c(filelist, file)
    }
  }
  
  for (i in 1:length(filelist)){
      src_ds = filelist[i]
      st = stypes[i]
      print(src_ds)
      rast = raster(src_ds)
      rast = projectRaster(rast, crs=CRS("+init=epsg:3857"), method = "ngb")
      
      height = 600

      xmin = floor(rast@extent@xmin/100) * 100
      xmax = ceiling(rast@extent@xmax/100) * 100
      ymin = floor(rast@extent@ymin/100) * 100
      ymax = ceiling(rast@extent@ymax/100) * 100
      if ((xmax-xmin) >= (ymax-ymin)){
        myext = xmax-xmin
      } else {
        myext = ymax-ymin
      }
      xmin = ((xmax+xmin) / 2) - (myext/2)
      xmax = ((xmax+xmin) / 2) + (myext/2)
      ymin = ((ymax+ymin) / 2) - (myext/2)
      ymax = ((ymax+ymin) / 2) + (myext/2)
      
      pixsize = (xmax - xmin)/height
      
      tileset = findtiles(xmin,xmax,ymin,ymax,pixsize,myext)
      xmin = as.numeric(tileset[9])
      xmax = as.numeric(tileset[10])
      ymin = as.numeric(tileset[11])
      ymax = as.numeric(tileset[12])
      x = c(xmin,xmax)
      y = c(ymin,ymax)
      xy = data.frame(x,y)
      
      height = as.numeric(tileset[13])
      width = round(height / 4) * 5
      
      # output
      delta = (period - 1) / 2
      img = paste0(dst_dir, rastype,"_", season - delta,"_", season + delta,"_",scenario,"_",st,"_map.png")
      png(img,width = width, height = height, bg = "transparent")

      par(mar=c(0,0,0,0), oma=c(0,0,0,0))
      layout(matrix(c(1,1,1,1,2),1,5))
      plot(xy[,1], xy[,2], type = "n", xlim = x, ylim = y, col = "white", bty ="n")
      # osmext = data.frame(lon = c(xmax,xmin), lat = c(ymin,ymax))
      # coordinates(osmext) = c("lon","lat")
      # proj4string(osmext) = CRS("+init=epsg:3857")
      # newcrs = CRS("+init=epsg:4326")
      # ext4326 = spTransform(osmext,newcrs)
      plot_tiles(dst_dir, tileset, tilesize, xmin, xmax, ymin, ymax)
      # print(ext4326@coords[1])
      # map = openmap(c(ext4326@coords[1,1],ext4326@coords[1,2]), c(ext4326@coords[2,1],ext4326@coords[2,2]), type = "osm", minNumTiles = (round(height/256)*round(height/256)))
      # map = openproj(map, projection = "+proj=merc +a=6378137 +b=6378137 +lat_ts=0.0 +lon_0=0.0 +x_0=0.0 +y_0=0 +k=1.0 +units=m +nadgrids=@null +wktext  +no_defs")
      # plot(map,raster=T)
      
      par(new = T)
      plot(rast, breaks = myval, col=mycolrast, alpha = myalpha,
           xlim = x, ylim = y,
           add=T, legend = F)
      
      plot.new()
      plot.new()
      plot.new()
      plot.new()
      legend("center",
             myleg,
             fill = mycolrast,
             bty = 'n',
             cex = 2,
             title = mymain,
             title.col = "darkblue")
      dev.off()
      return(myleg)
  }
}

plot_seasonal_png <- function(src_dir, dst_dir){
  mymain = "Nombre de jours"
  myalpha = 1
  # rastypes = c("mean", "stddev", "q20")
  rastypes = c("mean", "q20")
  
  for (rastype in rastypes){
      tmp_src = paste0(src_dir, rastype, "/")
      filelist = list.files(tmp_src)
      
      for (i in 1:length(filelist)){
           # print(envid)
            # readline(prompt="Press [enter] to continue")
          src_ds = filelist[i]
          season = substr(src_ds, nchar(src_ds) - 7, nchar(src_ds) - 4)
          print(paste0(envid, " ", snowtype, " ", scenario, " ",src_ds))
          
          # # retrieve the right file
          # if (grepl('len_mean', src_ds)){
            # mycolrast = c("#ffffcc", "#a1dab4","#41b6c4", "#2c7fb8", "#253494")
            # myval = c(0,50,80,100,150,365)
            # }
          # if (grepl('len_sd', src_ds)){
            # mycolrast = c("#3288bd", "#66c2a5","#e6f598", "#fdae61", "#f46d43")
            # myval = c(0,10,20,30,40,365)
            # }
            
         
          rast = raster(paste0(tmp_src,src_ds))
          # rast = projectRaster(rast, crs=CRS("+init=epsg:3857"), method = "ngb")
          mycolrast = colorRampPalette(c("#FEFEFE","#010101"))(180)
          myval = sort(unique(rast))
          
          if(0 %in% myval == FALSE){
            mycolrast = mycolrast[myval]
            myval = c(0, myval)
          } else {
            mycolrast = c("#FFFFFF",mycolrast[myval[myval > 0]])
            myval = c(-.1, myval)
          }
          
          xmin = rast@extent@xmin
          xmax = rast@extent@xmax
          ymin = rast@extent@ymin
          ymax = rast@extent@ymax
          rastdims = dim(rast)
          height = rastdims[1]
          width = rastdims[2]
          
          x = c(xmin,xmax)
          y = c(ymin,ymax)
          xy = data.frame(x,y)
          
          
          # output
          img = paste0(substr(src_ds,1,nchar(src_ds) - 3), "png")
          tmp_dst = paste0(dst_dir,rastype,"/")
          dir.create(tmp_dst)
          img = paste0(tmp_dst,img)
          png(img,width = width, height = height, bg = "transparent")

          par(mar=c(0,0,0,0), oma=c(0,0,0,0))
          # layout(matrix(c(1,1,1,1,2),1,5))
          plot(xy[,1], xy[,2], type = "n", xlim = x, ylim = y, col = "white", bty ="n", xaxs = "i", yaxs = "i")
          
          # if (length(myval) > 1){
              plot(rast, breaks = myval, col=mycolrast, alpha = myalpha,
                   add=T, legend = F)
          # } else {
              # myval = c(0,1)
              # mycolrast = c("#FFFFFF")
              # plot(rast, breaks = myval, col=mycolrast, alpha = myalpha,
                   # # xlim = c(xmin,xmax), ylim = c(ymin,ymax),
                   # add=T, legend = F)
          # }
          dev.off()
          
          # # Create 2020 difference png
          # if (rastype == "mean" && season > 2020){
              # rast2020 = paste0(substr(src_ds, 1, nchar(src_ds) - 8), "2020.tif")
              # rast2020 = raster(paste0(tmp_src,rast2020))
              # rast = rast - rast2020
              
              # #Define colors based on normalization of negative values
              # mycolrast = colorRampPalette(c("#010101","#FEFEFE"))(240)
              # myval = sort(unique(rast))
              # v = seq(-180,59)
              # myval2= c()
              # for (i in 1:length(myval)){
                # myval2 = c(myval2, match(myval[i],v))
              # }
              # mycolrast = mycolrast[myval2]
              # myval = c(myval[1] - 1, myval)
              
              # img = paste0(substr(src_ds,1,nchar(src_ds) - 3), "png")
              # tmp_dst = paste0(dst_dir,"diff2020/")
              # dir.create(tmp_dst)
              # img = paste0(tmp_dst,img)
              # png(img,width = width, height = height, bg = "transparent")

              # par(mar=c(0,0,0,0), oma=c(0,0,0,0))
              # # layout(matrix(c(1,1,1,1,2),1,5))
              # plot(xy[,1], xy[,2], type = "n", xlim = x, ylim = y, col = "white", bty ="n", xaxs = "i", yaxs = "i")
              
              # plot(rast, breaks = myval, col=mycolrast, alpha = myalpha,
                   # # xlim = c(xmin,xmax), ylim = c(ymin,ymax),
                   # add=T, legend = F)
              
            # dev.off()
          # }
          
          # # plot.new()
          # # plot.new()
          # # plot.new()
          # # plot.new()
          # # legend("center",
                 # # myleg,
                 # # fill = mycolrast,
                 # # bty = 'n',
                 # # cex = 2,
                 # # title = mymain,
                 # # title.col = "darkblue")
          # return(myleg)
      }
  }
}

plot_seasonal_safran_png <- function(src_dir, dst_dir){
  mymain = "Nombre de jours"
  myalpha = 1
  
  tmp_src = paste0(src_dir, "/")
  filelist = list.files(tmp_src)
  
  for (i in 1:length(filelist)){
       # print(envid)
        # readline(prompt="Press [enter] to continue")
      src_ds = filelist[i]
      season = substr(src_ds, nchar(src_ds) - 7, nchar(src_ds) - 4)
      print(paste0(envid, " ", snowtype, " - ",src_ds))
      
      # # retrieve the right file
      # if (grepl('len_mean', src_ds)){
        # mycolrast = c("#ffffcc", "#a1dab4","#41b6c4", "#2c7fb8", "#253494")
        # myval = c(0,50,80,100,150,365)
        # }
      # if (grepl('len_sd', src_ds)){
        # mycolrast = c("#3288bd", "#66c2a5","#e6f598", "#fdae61", "#f46d43")
        # myval = c(0,10,20,30,40,365)
        # }
        
     
      rast = raster(paste0(tmp_src,src_ds))
      # rast = projectRaster(rast, crs=CRS("+init=epsg:3857"), method = "ngb")
      mycolrast = colorRampPalette(c("#FEFEFE","#010101"))(190)
      myval = sort(unique(rast))
      
      mycolclass = c("#ffffcc", "#a1dab4","#41b6c4", "#2c7fb8", "#253494")
      myclass = c(0,50,80,100,150,365)
      
      if(0 %in% myval == FALSE){
        mycolrast = mycolrast[myval]
        myval = c(0, myval)
      } else {
        mycolrast = c("#FFFFFF",mycolrast[myval[myval > 0]])
        myval = c(-.1, myval)
      }
      
      xmin = rast@extent@xmin
      xmax = rast@extent@xmax
      ymin = rast@extent@ymin
      ymax = rast@extent@ymax
      rastdims = dim(rast)
      height = rastdims[1]
      width = rastdims[2]
      
      x = c(xmin,xmax)
      y = c(ymin,ymax)
      xy = data.frame(x,y)
      
      
      # output
      img = paste0(substr(src_ds,1,nchar(src_ds) - 3), "png")
      tmp_dst = paste0(dst_dir,"/")
      dir.create(tmp_dst)
      img = paste0(tmp_dst,img)
      png(img,width = width, height = height, bg = "transparent")

      par(mar=c(0,0,0,0), oma=c(0,0,0,0))
      # layout(matrix(c(1,1,1,1,2),1,5))
      plot(xy[,1], xy[,2], type = "n", xlim = x, ylim = y, col = "white", bty ="n", xaxs = "i", yaxs = "i")
      
      # if (length(myval) > 1){
          plot(rast, breaks = myval, col=mycolrast, alpha = myalpha,
               add=T, legend = F)
      # } else {
          # myval = c(0,1)
          # mycolrast = c("#FFFFFF")
          # plot(rast, breaks = myval, col=mycolrast, alpha = myalpha,
               # # xlim = c(xmin,xmax), ylim = c(ymin,ymax),
               # add=T, legend = F)
      # }
      dev.off()
      
      img = paste0(substr(src_ds,1,nchar(src_ds) - 4), "_classedcolors.png")
      img = paste0(tmp_dst,img)
      png(img,width = width, height = height, bg = "transparent")

      par(mar=c(0,0,0,0), oma=c(0,0,0,0))
      # layout(matrix(c(1,1,1,1,2),1,5))
      plot(xy[,1], xy[,2], type = "n", xlim = x, ylim = y, col = "white", bty ="n", xaxs = "i", yaxs = "i")
      
      plot(rast, breaks = myclass, col=mycolclass, alpha = myalpha,
           add=T, legend = F)
     
      dev.off()
  }
}

plot_vector <- function(dst_dir, envid, map_type){
  # create view for subsetting data
  # the view must be added to geometry columns
  #View ski area
  query <- paste0(
    "drop table if exists crosscut.tmp_plot_ski_area;
    create table crosscut.tmp_plot_ski_area as
    select envid, st_multi(st_transform(geom,3857))::geometry(MultiPolygon, 3857) the_geom
    from crosscut.envelopes
    where envid = '", envid,"';"
  )
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  dbSendQuery(con, query)
  dbDisconnect(con)
  
  #Get extent
  query <- "select st_xmin(the_geom), st_xmax(the_geom), st_ymin(the_geom), st_ymax(the_geom)
  from crosscut.tmp_plot_ski_area"
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  ext <- dbGetQuery(con, query)
  dbDisconnect(con)
  
  xmin = floor(ext[,1]/100) * 100
  xmax = ceiling(ext[,2]/100) * 100
  ymin = floor(ext[,3]/100) * 100
  ymax = ceiling(ext[,4]/100) * 100
  if ((xmax-xmin) >= (ymax-ymin)){
    myext = xmax-xmin
  } else {
    myext = ymax-ymin
  }
  xmin = ((xmax+xmin) / 2) - (myext/2)
  xmax = ((xmax+xmin) / 2) + (myext/2)
  ymin = ((ymax+ymin) / 2) - (myext/2)
  ymax = ((ymax+ymin) / 2) + (myext/2)
  
  height = 600
  pixsize = (xmax - xmin)/height

	tileset = findtiles(xmin,xmax,ymin,ymax,pixsize,myext)
	xmin = as.numeric(tileset[9])
	xmax = as.numeric(tileset[10])
	ymin = as.numeric(tileset[11])
	ymax = as.numeric(tileset[12])
	x = c(xmin,xmax)
	y = c(ymin,ymax)
	xy = data.frame(x,y)
	
	height = as.numeric(tileset[13])
  
  dsn = paste0("PG: dbname = '",db,"' host='",host,"' port = '5432' user = '",user,"' password = '",pwd,"'")
  
  if (map_type == "snowmaking"){
    # View for snowmaking area
    query <- paste0(
      "drop table if exists crosscut.tmp_plot_snowmaking;
      create table crosscut.tmp_plot_snowmaking as
      select envid, status, st_area(st_intersection(a.geom, b.geom)) area,
      st_multi(st_transform(st_collectionextract(st_intersection(a.geom, b.geom),3),3857))::geometry(MultiPolygon, 3857) geom
      from crosscut.envelopes a
      join crosscut.sm_resort b on a.ind = b.ind 
      where envid = ",envid,";" 
    )
	#print(query)
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    dbSendQuery(con, query)
	dbDisconnect(con)
	
    query = "select st_isempty(geom) from crosscut.tmp_plot_snowmaking"
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    emptest = dbGetQuery(con, query)
	dbDisconnect(con)
    if (emptest[1,1] == TRUE){
        print('TRUE IS NOT OK')
    } else {
        #set plot area and layout => compute pix size and get tiles
        
        width = round(height / 4) * 5
        png(paste0(dst_dir,map_type,".png"),width = width, height = height, bg = "transparent")
        par(mar=c(0,0,0,0), oma=c(0,0,0,0))
        layout(matrix(c(1,1,1,1,2),1,5))
        plot(xy[,1], xy[,2], type = "n", xlim = x, ylim = y, col = "white", bty ="n")
        
        plot_tiles(dst_dir, tileset, tilesize, xmin, xmax, ymin, ymax)
        
        layer = "crosscut.tmp_plot_ski_area"
        lay = readOGR(dsn, layer)
        plot(lay, xlim = x, ylim = y, col = "#FFFFFF00", border = "black", lwd = 2, add = T)
        
        layer = "crosscut.tmp_plot_snowmaking"
        query = "select count(*) from crosscut.tmp_plot_snowmaking"
        con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
        nbfeat = dbGetQuery(con, query)
        dbDisconnect(con)
         mycol = colorRampPalette(c("#08306b", "#6baed6"))(10)
        alpha = "B3"
        mycol = paste0(mycol,alpha)
        lay = readOGR(dsn, layer)
        i = 0
        for (sp in sort(as.vector(unique(lay$area)), decreasing = T)){
            i = i + 1
            plot(lay[lay$area == sp,], xlim = x, ylim = y, col = mycol[i], border = NA, lwd = 1.2, add=T)
        }
        
        # Create csv mp file for the snowmaking area information (mp and mp_part)
        query = paste0(
            "select envid, round(sum(mp_part),2) mp_part_nc, sum(surf_part) surf_part_nc
            from crosscut.loc_weight_env
            where right(snowtype,3) = '_sm' and envid = ",envid,"
            group by 1;"
            )
        con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
        mpnc <- dbGetQuery(con, query)
        dbDisconnect(con)
        write.csv(mpnc, paste0(dst_dir,envid,"_ncpart.csv"), row.names = FALSE)
        
        query = "select status from crosscut.tmp_plot_snowmaking
        order by area"
        con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
        leg = dbGetQuery(con, query)
        dbDisconnect(con)
        plot.new()
        mytitle = paste0("Part de l'enveloppe\n gravitaire couverte\n par la neige de culture (",mpnc[1,2],"%)")
        legend("center", legend = leg, fill = rev(mycol), title = mytitle, bty = 'n', cex = 1.8)
        dev.off()
    }
}
  if (map_type == "skiarea"){
    # View for slopes and rm
    query <- paste0("
    drop table if exists crosscut.tmp_plot_osm_pistes;
    create table crosscut.tmp_plot_osm_pistes as
    select envid , case when col is null then 'white' else col end col, st_transform(a.geom,3857) the_geom
	from crosscut.tracks a
    join crosscut.envelopes b on st_intersects(a.geom, b.geom) and a.ind = b.ind
    where envid = ", envid,";
    
	drop table if exists crosscut.tmp_plot_rm;
    create table crosscut.tmp_plot_rm as
    select gid, st_transform(a.geom, 3857) the_geom
	from crosscut.resort_lifts a
    join crosscut.envelopes b on st_intersects(a.geom, b.geom) and a.ind = b.ind
    where envid = '",envid,"';"
    )
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    dbSendQuery(con, query)
	dbDisconnect(con)
	
	 #set plot area and layout => compute pix size and get tiles
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	 query = "select * from crosscut.tmp_plot_osm_pistes"
	data <- dbGetQuery(con, query)
	dbDisconnect(con)
	
	if (nrow(data) > 0){
        width = height
        png(paste0(dst_dir,envid,"_",map_type,"_skitracks.png"),width = width, height = height, bg = "transparent")
        par(mar=c(0,0,0,0), oma=c(0,0,0,0))
        plot(xy[,1], xy[,2], type = "n", xlim = x, ylim = y, col = "white", bty ="n")

        plot_tiles(dst_dir, tileset, tilesize, xmin, xmax, ymin, ymax)
        layer = "crosscut.tmp_plot_osm_pistes"
        lay = readOGR(dsn, layer)
        for (col in sort(as.vector(unique(lay$col)))){
            plot(lay[lay$col == col,], xlim = x, ylim = y, col = col, lwd = 1.8, add=T)
        }
        
        layer = "crosscut.tmp_plot_rm"
        lay = readOGR(dsn, layer)
        plot(lay, xlim = x, ylim = y, col = "#000000", lwd = 6, lty = "16", add = T)
        plot(lay, xlim = x, ylim = y, col = "#000000", lwd = 1.4, add = T)

        layer = "crosscut.tmp_plot_ski_area"
        lay = readOGR(dsn, layer)
        plot(lay, xlim = x, ylim = y, col = "#FFFFFF40", border = "orange", lwd = 2, add = T)

	dev.off()
	}
	
	width = height
	png(paste0(dst_dir,envid,"_",map_type,"_rm.png"),width = width, height = height, bg = "transparent")
	par(mar=c(0,0,0,0), oma=c(0,0,0,0))
	plot(xy[,1], xy[,2], type = "n", xlim = x, ylim = y, col = "white", bty ="n")
	
	plot_tiles(dst_dir, tileset, tilesize, xmin, xmax, ymin, ymax)
	
	layer = "crosscut.tmp_plot_ski_area"
	lay = readOGR(dsn, layer)
	plot(lay, xlim = x, ylim = y, col = "#FFFFFF40", border = "black", lwd = 2, add = T)
	
	layer = "crosscut.tmp_plot_rm"
	lay = readOGR(dsn, layer)
	plot(lay, xlim = x, ylim = y, col = "#000000", lwd = 6, lty = "16", add = T)
	plot(lay, xlim = x, ylim = y, col = "#000000", lwd = 1.4, add = T)
	dev.off()
	
	width = height / 4
	png(paste0(dst_dir,envid,"_",map_type,"_legend.png"),width = width, height = height, bg = "transparent")
	layout(matrix(c(1,2),2,1))
	par(mar=c(0,0,0,0), oma=c(0,0,0,0))
	plot.new()
	legend("bottom", "            \n            ", lwd = 6, lty = "16", col = "black", bty = "n", cex = 1.2, y.intersp = 1.2)
	legend("bottom", c("Remontée\nmécanique"),
	       col = c("black"),lwd =1.4 , bty = 'n', cex = 1.2, y.intersp = 1.2)
	plot.new()
	legend("top", c("Piste verte","Piste bleue","Piste rouge","Piste noire"),
	       col = c("green","blue","red","black"),lwd =1.8 , bty = 'n', cex = 1.2, y.intersp = 1.2)
	dev.off()
  }
  
  query = "
    drop table if exists crosscut.tmp_plot_ski_area;
    drop table if exists crosscut.tmp_plot_snowmaking;
    drop table if exists crosscut.tmp_plot_osm_pistes;
    drop table if exists crosscut.tmp_plot_rm;
  "
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    dbSendQuery(con, query)
	dbDisconnect(con)
}

plot_paca <-function(ind, sta, envid, src_dir, dst_dir, snowtype){
    # Open raster to plot, transform to EPSG 3035, get extent to set the plt area lim
    rastypes = c("mean", "q20")
    seasons = seq(2020, 2092, 5)
    
    # create tmp tables for common vector layers: envelope, retenues, ski slopes, ski lifts
    
    query <- paste0(
        "-- envelope
        drop table if exists crosscut.tmp_plot_envelopes;
        create table crosscut.tmp_plot_envelopes as
        select envid, st_transform(geom,3857) geom
        from crosscut.envelopes
        where envid = ", envid,";
        
        --reservoirs
        drop table if exists crosscut.tmp_plot_reservoirs;
        create table crosscut.tmp_plot_reservoirs as
        select gid, st_transform(geom,3857) geom
        from crosscut.reservoirs
        where ind = '", ind,"';
        
        --ski tracks
        drop table if exists crosscut.tmp_plot_tracks;
        create table crosscut.tmp_plot_tracks as
        select track_id, st_transform(geom,3857) geom
        from crosscut.tracks
        where ind = '", ind,"';
        
        --ski lifts
        drop table if exists crosscut.tmp_plot_lifts;
        create table crosscut.tmp_plot_lifts as
        select gid, st_transform(geom,3857) geom
        from crosscut.resort_lifts
        where ind = '", ind,"';
        
        --buildings
        drop table if exists crosscut.tmp_ext;
        create table crosscut.tmp_ext as
        select st_makeenvelope(
            st_xmin(geom) - (st_xmax(geom) - st_xmin(geom)) * 0.05,
            st_ymin(geom) - (st_ymax(geom) - st_ymin(geom)) * 0.05,
            st_xmax(geom) + (st_xmax(geom) - st_xmin(geom)) * 0.05,
            st_ymax(geom) + (st_ymax(geom) - st_ymin(geom)) * 0.05,
            2154) geom
        from crosscut.envelopes a
        where envid = ",envid,";

        create index idx_tmp_ext_geom on crosscut.tmp_ext using gist(geom);
        
        drop table if exists crosscut.tmp_plot_buildings;
        create table crosscut.tmp_plot_buildings as
        select b.gid, st_transform(the_geom, 3857) geom
        from crosscut.tmp_ext a, stations.geo_bati_sta_bas_rm_300_200_ind b
        where st_intersects(b.the_geom,geom);"
    )
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    dbSendQuery(con, query)
    dbDisconnect(con)
    print("tmp table created")
    
    dsn = paste0("PG: dbname = '",db,"' host='",host,"' port = '5432' user = '",user,"' password = '",pwd,"'")
    
    for (rastype in rastypes){
        for (season in seasons){
            print(snowtype)
            if (snowtype == "lance" | snowtype == "fan"){
                query <- paste0(
                    "select distinct status
                    from crosscut.sm_resort
                    where ind = '", ind,"';
                "
                )
                con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
                status <- dbGetQuery(con, query)
                dbDisconnect(con)
                
                for (sm in 1:nrow(status)){
                    sm = status[sm,1]
                    
                    query <- paste0(
                        "select gid, st_x(st_transform(geom,3857)) x, st_y(st_transform(geom,3857)) y
                        from crosscut.sm_points
                        where ind = '", ind,"' and status = '",sm,"';"
                    )
                    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
                    smpoints <- dbGetQuery(con, query)
                    dbDisconnect(con)
                    
                    src_ds = paste0(src_dir, rastype, "/", sm, "_len_", rastype, "_", season, ".tif")
                    rast = raster(src_ds)
                    rast = projectRaster(rast, crs=CRS("+init=epsg:3857"), method = "ngb")
                    
                    xmin = rast@extent@xmin
                    xmax = rast@extent@xmax
                    ymin = rast@extent@ymin
                    ymax = rast@extent@ymax
                    
                    xext = xmax - xmin
                    xmin = xmin - xext * .05
                    xmax = xmax + xext * .05
                    
                    ytitle = ymax
                    yext = ymax - ymin
                    ymin = ymin - yext * .05
                    ymax = ymax + yext * .05
                    
                    if((xmax - xmin) >= (ymax - ymin)){
                        ext = xmax - xmin
                        ymin = ymin + (ymax - ymin)/2 - ext/2
                        ymax = ymin + (ymax - ymin)/2 + ext/2
                    } else {
                        ext = ymax - ymin
                        xmin = xmin + (xmax - xmin)/2 - ext/2
                        xmax = xmin + (xmax - xmin)/2 + ext/2
                    }
                    
                    xleg = xmax
                    xmax = xmax + (xmax - xmin) / 3
                    xlim = c(xmin, xmax)
                    ylim = c(ymin, ymax)
                    
                    #Setup the adapted coloramp
                    mycolrastot = colorRampPalette(c("#ef6548","#ffeda0","#006d2c"))(180)
                    myval = sort(unique(rast))

                    if(0 %in% myval == FALSE){
                        mycolrast = mycolrastot[myval]
                        myval = c(0, myval)
                    } else {
                        mycolrast = c("#ef6548",mycolrastot[myval[myval > 0]])
                        myval = c(-.1, myval)
                    }
                    
                    #Instantiate plot device
                    height = 600
                    width = round(height * 4/3)
                    img = paste0(dst_dir, sm, "len_", rastype, "_", season, ".png")
                    png(img,width = width, height = height, bg = "white")

                    par(mar=c(0,0,0,0), oma=c(0,0,0,0))
                    plot(0, 0, type = "n", xlim = xlim, ylim = ylim, col = "white", bty ="n", xaxs = "i", yaxs = "i", axes = F)

                    #Add baselayer 
                    baserast = "/home/francois/data/source_rasters/eudem_3857.tif"
                    baserast = raster(baserast)
                    baserast = crop(baserast, extent(c(xmin, xleg, ylim)))
                    # myalpha = .5
                    # plot(baserast, col=grey(0:224/224), alpha = myalpha,
                       # add = T, legend = F)
                    # print("raster plotted")
                    
                    lcstep = 50
                    lcmin = ceiling(minValue(baserast)/lcstep) * lcstep
                    lcmax = floor(maxValue(baserast)/lcstep) * lcstep
                    lev = seq(lcmin, lcmax, lcstep)
                    
                    lcstep = 250
                    lcmin = ceiling(minValue(baserast)/lcstep) * lcstep
                    lcmax = floor(maxValue(baserast)/lcstep) * lcstep
                    levlab = seq(lcmin, lcmax, lcstep)
                    lev = lev[!lev %in% levlab]
                    
                    lc = rasterToContour(baserast, levels = lev)
                    
                    contour(baserast, levels = levlab, add = T, labcex = .8, axes = F)
                    contour(baserast, levels = lev, add = T, drawlabels = F, axes = F)
                    
                    #Add data layer
                    myalpha = .8
                    plot(rast, breaks = myval, col=mycolrast, alpha = myalpha,
                       add=T, legend = F, bty = "n", box = F)
                    
                    ######ADD vector data
                    # #envelope
                    # layer = "crosscut.tmp_plot_envelopes"
                    # lay = readOGR(dsn, layer)
                    # plot(lay, xlim = x, ylim = y, col = NA, border = "darkgrey", lwd = 2, add = T)
                    
                    #reservoirs
                    layer = "crosscut.tmp_plot_reservoirs"
                    lay = readOGR(dsn, layer)
                    plot(lay, xlim = x, ylim = y, col = "#4eb3d380", border = "#808080", lwd = 1, add = T)
                    
                    #buildings
                    layer = "crosscut.tmp_plot_buildings"
                    lay = readOGR(dsn, layer)
                    plot(lay, xlim = x, ylim = y, col = "#000000", border = "#808080", lwd = 1, add = T)
                    
                    #ski tracks
                    layer = "crosscut.tmp_plot_tracks"
                    lay = readOGR(dsn, layer)
                    plot(lay, xlim = x, ylim = y, col = NA, border = "black", lwd = 1, add = T)
                    
                    #lifts
                    layer = "crosscut.tmp_plot_lifts"
                    lay = readOGR(dsn, layer)
                    plot(lay, xlim = x, ylim = y, col = "#000000", lwd = 5, lty = "16", add = T)
                    plot(lay, xlim = x, ylim = y, col = "#000000", lwd = 1, add = T)
                    
                    #add smpoints
                    icon = "/home/francois/data/source_rasters/snowflake_icon.png"
                    icon = readPNG(icon)
                    psize = ext / height
                    isize = dim(icon)
                    hoffset = psize * isize[1] / 4
                    voffset = psize * isize[2] / 4
                    for (p in 1:nrow(smpoints)){
                        rasterImage(icon, smpoints[p,2] - hoffset, smpoints[p,3] - voffset, smpoints[p,2] + hoffset, smpoints[p,3] + voffset)
                    }
                    
                    ###### add legend and texts
            
                    x0 = xleg + .3 * (xmax - xleg)
                    x1 = x0 + .5 * (xmax - xleg)
                    
                    y0 = ymin + .05 * (ymax - ymin)
                    y1 = ymax - .05 * (ymax - ymin)
                    ysize = (y1 - y0) / 180            
                    
                    for (i in 1:180){
                        polygon(c(x0, x0, x1, x1), c(y0 + (i-1) * ysize, y0 + i * ysize, y0 + i * ysize, y0 + (i-1) * ysize),
                        col = mycolrastot[i], border = NA)
                    }
                    
                    
                    polygon(c(x0,x0,x1,x1), c(y0,y1,y1,y0), col = NA, border = "black", lwd = 1.5)
                    text(x0 - 0.05 * (xmax - xleg), y0 + (y1 - y0)/2, labels = "Nombre de jours d'enneigement fiable", adj=c(.5,0), srt = 90, cex = 1.6)
                    
                    x0 = x1
                    x1 = x0 + 0.03 * (xmax - xleg)
                    x2 = x1 + 0.01 * (xmax - xleg)
                    
                    # lines(c(x0,x0), c(y0,y1), col = "black", lwd = 1.5)
                    
                    labs = seq(0,180,10)
                    ystep = (y1 - y0) / (length(labs) - 1)
                    at = seq(y0, y1, ystep)
                    
                    for (i in 1:length(labs)){
                        lines(c(x0,x1), c(at[i], at[i]), col = "black", lwd = 1)
                        text(x2,at[i], labs[i], adj = c(0,.5))
                    }
                    
                    # Add title
                    if (rastype == "mean"){
                        rtitle = "moyenne"
                    } else if (rastype == "q20"){
                        rtitle = rastype
                    }
                    mytitle = paste0(sta, " : ", season)
                    mysub = paste0("(", rtitle, " ", season - 7, "-", season + 7,")")
                    print(mytitle)
                    ytop = ymax - (ymax - ytitle) / 4
                    ybot = ymax - (ymax - ytitle) * 3/4
                    text(xmin + (xleg - xmin)/2, ytop , labels = mytitle, adj = c(.5,.5), cex = 1.8, col = "black", font = 2)
                    text(xmin + (xleg - xmin)/2, ybot , labels = mysub, adj = c(.5,.5), cex = 1.6, col = "black", font = 2)
                    
                    dev.off()
                }
            } else {
                src_ds = paste0(src_dir, rastype, "/len_", rastype, "_", season, ".tif")
                print(src_ds)
                rast = raster(src_ds)
                rast = projectRaster(rast, crs=CRS("+init=epsg:3857"), method = "ngb")
                
                xmin = rast@extent@xmin
                xmax = rast@extent@xmax
                ymin = rast@extent@ymin
                ymax = rast@extent@ymax
                
                xext = xmax - xmin
                xmin = xmin - xext * .05
                xmax = xmax + xext * .05
                
                ytitle = ymax
                yext = ymax - ymin
                ymin = ymin - yext * .05
                ymax = ymax + yext * .05
                
                if((xmax - xmin) >= (ymax - ymin)){
                    ext = xmax - xmin
                    ymin = ymin + (ymax - ymin)/2 - ext/2
                    ymax = ymin + (ymax - ymin)/2 + ext/2
                } else {
                    ext = ymax - ymin
                    xmin = xmin + (xmax - xmin)/2 - ext/2
                    xmax = xmin + (xmax - xmin)/2 + ext/2
                }
                
                xleg = xmax
                xmax = xmax + (xmax - xmin) / 3
                xlim = c(xmin, xmax)
                ylim = c(ymin, ymax)
                
                #Setup the adapted coloramp
                mycolrastot = colorRampPalette(c("#ef6548","#ffeda0","#006d2c"))(180)
                myval = sort(unique(rast))

                if(0 %in% myval == FALSE){
                    mycolrast = mycolrastot[myval]
                    myval = c(0, myval)
                } else {
                    mycolrast = c("#ef6548",mycolrastot[myval[myval > 0]])
                    myval = c(-.1, myval)
                }
                
                #Instantiate plot device
                height = 600
                width = round(height * 4/3)
                img = paste0(dst_dir, "len_", rastype, "_", season, ".png")
                png(img,width = width, height = height, bg = "white")

                par(mar=c(0,0,0,0), oma=c(0,0,0,0))
                plot(0, 0, type = "n", xlim = xlim, ylim = ylim, col = "white", bty ="n", xaxs = "i", yaxs = "i", axes = F)

                #Add baselayer 
                baserast = "/home/francois/data/source_rasters/eudem_3857.tif"
                baserast = raster(baserast)
                baserast = crop(baserast, extent(c(xmin, xleg, ylim)))
                # myalpha = .5
                # plot(baserast, col=grey(0:224/224), alpha = myalpha,
                   # add = T, legend = F)
                # print("raster plotted")
                
                lcstep = 50
                lcmin = ceiling(minValue(baserast)/lcstep) * lcstep
                lcmax = floor(maxValue(baserast)/lcstep) * lcstep
                lev = seq(lcmin, lcmax, lcstep)
                
                lcstep = 250
                lcmin = ceiling(minValue(baserast)/lcstep) * lcstep
                lcmax = floor(maxValue(baserast)/lcstep) * lcstep
                levlab = seq(lcmin, lcmax, lcstep)
                lev = lev[!lev %in% levlab]
                
                lc = rasterToContour(baserast, levels = lev)
                
                contour(baserast, levels = levlab, add = T, labcex = .8, axes = F)
                contour(baserast, levels = lev, add = T, drawlabels = F, axes = F)
                
                #Add data layer
                myalpha = .7
                plot(rast, breaks = myval, col=mycolrast, alpha = myalpha,
                   add=T, legend = F, bty = "n", box = F)
                
                ######ADD vector data
                # #envelope
                # layer = "crosscut.tmp_plot_envelopes"
                # lay = readOGR(dsn, layer)
                # plot(lay, xlim = x, ylim = y, col = NA, border = "darkgrey", lwd = 2, add = T)
                
                #reservoirs
                layer = "crosscut.tmp_plot_reservoirs"
                lay = readOGR(dsn, layer)
                plot(lay, xlim = x, ylim = y, col = "#4eb3d380", border = "#808080", lwd = 1, add = T)
                    
                #buildings
                layer = "crosscut.tmp_plot_buildings"
                lay = readOGR(dsn, layer)
                plot(lay, xlim = x, ylim = y, col = "#000000", border = "#808080", lwd = 1, add = T)
                
                #ski tracks
                layer = "crosscut.tmp_plot_tracks"
                lay = readOGR(dsn, layer)
                plot(lay, xlim = x, ylim = y, col = NA, border = "black", lwd = 1, add = T)
                
                #lifts
                layer = "crosscut.tmp_plot_lifts"
                lay = readOGR(dsn, layer)
                plot(lay, xlim = x, ylim = y, col = "#000000", lwd = 5, lty = "16", add = T)
                plot(lay, xlim = x, ylim = y, col = "#000000", lwd = 1, add = T)
                
                ###### add legend and texts
            
                x0 = xleg + .3 * (xmax - xleg)
                x1 = x0 + .5 * (xmax - xleg)
                
                y0 = ymin + .05 * (ymax - ymin)
                y1 = ymax - .05 * (ymax - ymin)
                ysize = (y1 - y0) / 180            
                
                for (i in 1:180){
                    polygon(c(x0, x0, x1, x1), c(y0 + (i-1) * ysize, y0 + i * ysize, y0 + i * ysize, y0 + (i-1) * ysize),
                    col = mycolrastot[i], border = NA)
                }
                
                
                polygon(c(x0,x0,x1,x1), c(y0,y1,y1,y0), col = NA, border = "black", lwd = 1.5)
                text(x0 - 0.05 * (xmax - xleg), y0 + (y1 - y0)/2, labels = "Nombre de jours d'enneigement fiable", adj=c(.5,0), srt = 90, cex = 1.6)
                
                x0 = x1
                x1 = x0 + 0.03 * (xmax - xleg)
                x2 = x1 + 0.01 * (xmax - xleg)
                
                # lines(c(x0,x0), c(y0,y1), col = "black", lwd = 1.5)
                
                labs = seq(0,180,10)
                ystep = (y1 - y0) / (length(labs) - 1)
                at = seq(y0, y1, ystep)
                
                for (i in 1:length(labs)){
                    lines(c(x0,x1), c(at[i], at[i]), col = "black", lwd = 1)
                    text(x2,at[i], labs[i], adj = c(0,.5))
                }
                
                # Add title
                if (rastype == "mean"){
                    rtitle = "moyenne"
                } else if (rastype == "q20"){
                    rtitle = rastype
                }
                mytitle = paste0(sta, " : ", season)
                mysub = paste0("(", rtitle, " ", season - 7, "-", season + 7,")")
                print(mytitle)
                ytop = ymax - (ymax - ytitle) / 4
                ybot = ymax - (ymax - ytitle) * 3/4
                text(xmin + (xleg - xmin)/2, ytop , labels = mytitle, adj = c(.5,.5), cex = 1.8, col = "black", font = 2)
                text(xmin + (xleg - xmin)/2, ybot , labels = mysub, adj = c(.5,.5), cex = 1.6, col = "black", font = 2)
                
                dev.off()
                # readline(prompt="Press [enter] to continue")
            }
            # readline(prompt="Press [enter] to continue")
        }
    }
}

#GLOBAL PARAMETER
drv = dbDriver("PostgreSQL")
host = "195.221.110.200"
user = "postgres"
pwd = "lceslt"
db = "db"

inds = c("0537")
period = 15

for (i in 1:length(inds)){
    ind = inds[i]
    query = paste0("select name from crosscut.resorts where ind = '",ind,"';")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    sta <- dbGetQuery(con, query)
    dbDisconnect(con)

    src_sta = paste0("/home/francois/data/", sta, "/results/")

    query = paste0("select distinct envid, name from crosscut.envelopes
    where ind = '",ind,"'
    order by 1")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    listenv <- dbGetQuery(con, query)
    dbDisconnect(con)

    # listenv = c(6)

    # ###############plot rasters dsg properties resort envid
    # rastypes = c("altitude", "slope", "aspect")
    # for (envid in 1:nrow(listenv)){
        # envid = listenv[envid,1]
        # src_dir = paste0(src_sta, envid, "/")
        # dir.create(src_dir)
        # dst_dir = paste0(src_sta, envid, "/maps/")
        # dir.create(dst_dir)
        # for (rastype in rastypes){
            # print(paste0(envid," - ", rastype))
            # plot_rast(src_dir, dst_dir,envid, rastype)
         # }
    # }


    # ###############plot snowdays maps
    # seasons = c(2030, 2050, 2090)
    # snowtypes = c("nn", "gro","lance", "fan")
    # rastypes = c("mean","stddev")
    # scenarios = c("rcp26", "rcp45", "rcp85")

    # for (envid in 1:nrow(listenv)){
        # envid = listenv[envid,1]
        # src_dir = paste0(src_sta, envid, "/")
        # dir.create(src_dir)
        # dst_dir = paste0(src_sta, envid, "/maps/")
        # dir.create(dst_dir)
        # for (snowtype in snowtypes){
            # for (rastype in rastypes){
                # for (season in seasons){
                    # for (scenario in scenarios){
                        # plot_rast_period(src_dir, dst_dir, ind, rastype, season, snowtype, scenario, period)
                    # }
                # }
            # }
        # }
    # }

    # ###############plot seasonnal pngs
    # #seasons = c(2030, 2050, 2090)
    # snowtypes = c("nn", "gro","lance", "fan")
    # scenarios = c("rcp26", "rcp45", "rcp85")

    # for (envid in 1:nrow(listenv)){
        # envid = listenv[envid,1]
        # for (snowtype in snowtypes){
            # for (scenario in scenarios){
                # src_dir = paste0(src_sta, envid, "/seasonal_rasters/", snowtype, "/", scenario,"/")
                # dir.create(src_dir)
                # dst_dir = paste0(src_sta, envid, "/maps/")
                # dir.create(dst_dir)
                # dst_dir = paste0(src_sta, envid, "/maps/",snowtype,"/")
                # dir.create(dst_dir)
                # dst_dir = paste0(src_sta, envid, "/maps/",snowtype,"/",scenario,"/")
                # dir.create(dst_dir)
                # plot_seasonal_png(src_dir, dst_dir)
            # }
        # }
    # }

    ###############plot PACA
    #seasons = c(2030, 2050, 2090)
    snowtypes = c("nn", "gro","lance", "fan")
    # snowtypes = c("lance", "fan")
    scenarios = c("rcp26", "rcp45", "rcp85")
    library(png)

    for (envid in 1:nrow(listenv)){
        envid = listenv[envid,1]
        for (snowtype in snowtypes){
            for (scenario in scenarios){
                src_dir = paste0(src_sta, envid, "/seasonal_rasters/", snowtype, "/", scenario,"/")
                dir.create(src_dir)
                dst_dir = paste0(src_sta, envid, "/maps/")
                dir.create(dst_dir)
                dst_dir = paste0(src_sta, envid, "/maps/",snowtype,"/")
                dir.create(dst_dir)
                dst_dir = paste0(src_sta, envid, "/maps/",snowtype,"/",scenario,"/")
                dir.create(dst_dir)
                plot_paca(ind, sta, envid, src_dir, dst_dir, snowtype)
            }
        }
    }

    # ###############plot ski area vectors
    # #plot vectors
    # map_types = c("skiarea", "snowmaking")

    # for (envid in 1:nrow(listenv)){
        # envid = listenv[envid,1]
        # dst_dir = paste0(src_sta, envid, "/maps/")
        # dir.create(dst_dir)
        # for (map_type in map_types){
            # print(paste0(envid," - ", map_type))
            # plot_vector(dst_dir,envid, map_type)
        # }
    # }

    # ##############plot seasonnal safran pngs
    # snowtypes = c("nn", "gro","lance", "fan")

    # for (envid in 1:nrow(listenv)){
        # envid = listenv[envid,1]
        # for (snowtype in snowtypes){
            # src_dir = paste0(src_sta, envid, "/seasonal_rasters/", snowtype, "/safran/")
            # dir.create(src_dir)
            # dst_dir = paste0(src_sta, envid, "/seasonal_rasters/png/")
            # dir.create(dst_dir)
            # dst_dir = paste0(src_sta, envid, "/seasonal_rasters/png/",snowtype,"/")
            # dir.create(dst_dir)
            # dst_dir = paste0(src_sta, envid, "/seasonal_rasters/png/",snowtype,"/safran/")
            # dir.create(dst_dir)
            # plot_seasonal_safran_png(src_dir, dst_dir)
        # }
    # }
    
}
    