
import numpy as np
import conn_param
import multiprocessing
import psycopg2


def compute_idx_launcher(ind):
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    # count number of snowmaking areas
    query = """
        select distinct status from crosscut.sm_resort
        where ind = %s"""
    cur.execute(query,(ind,))
    nbsm = cur.rowcount

    # Retrieve zone type for the ski resort
    query = """select distinct c.zonetype from crosscut.loc_weight_env a
    join crosscut.envelopes b on a.envid = b.envid
    join crocus.loc_zone c on a.loc = c.loc
    where ind = %s"""
    cur.execute(query,(ind,))
    
    zones = []
    for zt in cur:
        zones.append(zt[0])
    zones = tuple(zones)
    
    if nbsm > 0:
        if "sym" in zones: # add an exception in order to limit season <= 2014 and season => 1961 in case of sym zone mixed with other zones
            query = """
                select distinct model, scenario, snowtype, season
                from crocus.available_simulations
                where typezone in %s and model != 'safran'
                
                union
                
                select distinct model, scenario, snowtype, season
                from crocus.available_simulations
                where typezone in %s and model = 'safran'
                and season::integer >= 1961 and season::integer <= 2014
                """
            cur.execute(query,(zones,zones,))
        else:
            # Retrieve available simulations for these zones
            query = """select distinct model, scenario, snowtype, season from crocus.available_simulations
            where typezone in %s"""
            cur.execute(query,(zones,))
    else: # Case when there is no snowmaking area => limit sim to nn and gro
        if "sym" in zones: # add an exception in order to limit season <= 2014 and season => 1961 in case of sym zone mixed with other zones 
            query = """
                select distinct model, scenario, snowtype, season
                from crocus.available_simulations
                where typezone in %s and model != 'safran' and (snowtype = 'nn' or snowtype = 'gro')
                
                union
                
                select distinct model, scenario, snowtype, season
                from crocus.available_simulations
                where typezone in %s and model = 'safran' and (snowtype = 'nn' or snowtype = 'gro')
                and season::integer >= 1961 and season::integer <= 2014 
                """
            cur.execute(query,(zones,zones,))
        else:
            # Retrieve available simulations for these zones
            query = """select distinct model, scenario, snowtype, season from crocus.available_simulations
            where typezone in %s and (snowtype = 'nn' or snowtype = 'gro')"""
            cur.execute(query,(zones,))
        
    args = []
    # Create the required temp table looping on available sims
    for sim in cur:
        model = sim[0]
        scenario = sim[1]
        snowtype = sim[2]
        season = sim[3]
        args.append((ind, zones, model, scenario, snowtype, season))
    print("args done")
    # args.append((os.path.join(src_dir, flist[0]), zone, model, scenario, snowtype))
    pool = multiprocessing.Pool(processes = 10)
    pool.map(compute_idx, args)
    pool.close()
        
def compute_idx(args):
    ind, zones, model, scenario, snowtype, season = args
    print(f'launching idx computing for {ind}: {season}, {model}, {scenario}, {snowtype}')
    
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur2 = myconn.cursor()
    if snowtype == 'gro' or snowtype == 'nn':
        # Dealing with ns&gro envelopes snowtype
        i = 0
        for typezone in zones:
            # typezone = typezone[0]
            srctab = f'snow_{typezone}_{model}_{scenario}_{snowtype}_{season}'
            subq = f"""
            select * from crocus.{srctab}
            where loc in (
                select distinct loc from crosscut.loc_weight_env a
                join crosscut.envelopes b on a.envid = b.envid
                where ind = '{ind}'
                )"""
            if i == 0:
                query = subq
            else:
                query = f"""
                    {query}
                    
                    union
                    
                    {subq}
                """
            i = i + 1
        
        tmpsnow = f'tmp_snow_{model}_{scenario}_{snowtype}_{season}'
        query = f"""
        drop table if exists crocus.{tmpsnow};
        create table crocus.{tmpsnow} as
        {query};"""
        cur2.execute(query)
        myconn.commit()
        print(f'{tmpsnow} done')
        
        # test if rm are present
        query = "select ind from crosscut.resort_lifts where ind = %s"
        curm = myconn.cursor()
        curm.execute(query, (ind,))
        nbrm = curm.rowcount 
        if nbrm > 0:
            varagg = "mp_part"
        else:
            varagg = "surf_part * 100"
        
        table = f"idx_mth_{model}_{scenario}"
        query = f"""
        create table if not exists viability.{table}(
            CONSTRAINT {table}_check
                CHECK (model = '{model}' and scenario = '{scenario}')
        )
        inherits(viability.idx_mth);
        create index if not exists
            idx_{table}_envid on viability.{table}(envid);
        create index if not exists
            idx_{table}_season on viability.{table}(season);
        create index if not exists
            idx_{table}_snowtype on viability.{table}(snowtype);
        """
        cur2.execute(query)
        myconn.commit()
        
        st = [f"ns&gro_{snowtype}"]
        st = tuple(st)
        
        query = f"""
        delete from viability.{table} where envid in (
            select envid from crosscut.envelopes where ind = %s
        )
        and season = %s and snowtype in %s;
        
        insert into viability.{table}
        with a as (
            select envid, season, a.snowtype||'_'||%s snowtype, model, scenario,
                sum(case when swe >= 100 and extract(month from ddate) = 11 then {varagg} else 0 end) nov,
                sum(case when swe >= 100 and extract(month from ddate) = 12 then {varagg} else 0 end) as dec,
                sum(case when swe >= 100 and extract(month from ddate) = 1 then {varagg} else 0 end) jan,
                sum(case when swe >= 100 and extract(month from ddate) = 2 then {varagg} else 0 end) feb,
                sum(case when swe >= 100 and extract(month from ddate) = 3 then {varagg} else 0 end) mar,
                sum(case when swe >= 100 and extract(month from ddate) = 4 then {varagg} else 0 end) apr,
                sum(case when swe >= 100 and extract(month from ddate) in (12,1,2) then {varagg} else 0 end) djf,
                sum(case when swe >= 100 and extract(month from ddate) in (11,12,1,2,3,4) then {varagg} else 0 end) ndjfma
            from crosscut.loc_weight_env a
            join crocus.{tmpsnow} b on a.loc = b.loc
            where a.snowtype = 'ns&gro'
            and ddate between (season||'-11-01')::date and ((season + 1)||'-04-30')::date
            and a.envid in (select distinct envid from crosscut.envelopes where ind = '{ind}')
            group by envid, season, a.snowtype, model, scenario
        )
        
        select envid, season, snowtype, model, scenario,
            nov / date_part('days', (season||'-'||11||'-01')::date + interval '1 month - 1') nov,
            dec / date_part('days', (season||'-'||12||'-01')::date + interval '1 month - 1') as dec,
            jan / date_part('days', (season + 1||'-'||01||'-01')::date + interval '1 month - 1') jan,
            feb / date_part('days', (season + 1||'-'||02||'-01')::date + interval '1 month - 1') feb,
            mar / date_part('days', (season + 1||'-'||03||'-01')::date + interval '1 month - 1') mar,
            apr / date_part('days', (season + 1||'-'||04||'-01')::date + interval '1 month - 1') apr,
            djf / (date_part('days', (season||'-'||12||'-01')::date + interval '1 month - 1')
                + date_part('days', (season + 1||'-'||01||'-01')::date + interval '1 month - 1')
                + date_part('days', (season + 1||'-'||02||'-01')::date + interval '1 month - 1')) djf,
            ndjfma / (date_part('days', (season||'-'||11||'-01')::date + interval '1 month - 1')
                + date_part('days', (season||'-'||12||'-01')::date + interval '1 month - 1')
                + date_part('days', (season + 1||'-'||01||'-01')::date + interval '1 month - 1')
                + date_part('days', (season + 1||'-'||02||'-01')::date + interval '1 month - 1')
                + date_part('days', (season + 1||'-'||03||'-01')::date + interval '1 month - 1')
                + date_part('days', (season + 1||'-'||04||'-01')::date + interval '1 month - 1')) ndjfma
        from a;
        """
        cur2.execute(query,(ind,season,st,snowtype,))
        myconn.commit()
        print('idx computed')
    else:
        # Create temp table with gro and sm snowtype
        i = 0
        for typezone in zones:
            # typezone = typezone[0]
            srctabgro = f'snow_{typezone}_{model}_{scenario}_gro_{season}'
            srctabsnow = f'snow_{typezone}_{model}_{scenario}_{snowtype}_{season}'
            subq = f"""
            select * from crocus.{srctabgro}
            where loc in (
                select distinct loc from crosscut.loc_weight_env a
                join crosscut.envelopes b on a.envid = b.envid
                where ind = '{ind}'
                )
             
            union
            
            select * from crocus.{srctabsnow}
            where loc in (
                select distinct loc from crosscut.loc_weight_env a
                join crosscut.envelopes b on a.envid = b.envid
                where ind = '{ind}'
                )"""
            if i == 0:
                query = subq
            else:
                query = f"""
                    {query}
                    
                    union
                    
                    {subq}
                """
            i = i + 1
        
        tmpsnow = f'tmp_snow_{model}_{scenario}_{snowtype}_{season}'
        query = f"""
        drop table if exists crocus.{tmpsnow};
        create table crocus.{tmpsnow} as
        {query};"""
        cur2.execute(query)
        myconn.commit()
        print(f'{tmpsnow} done')
        
        # test if rm are present
        query = "select ind from crosscut.resort_lifts where ind = %s"
        curm = myconn.cursor()
        curm.execute(query, (ind,))
        nbrm = curm.rowcount 
        if nbrm > 0:
            varagg = "mp_part"
        else:
            varagg = "surf_part * 100"      
        
        table = f"idx_mth_{model}_{scenario}"
        query = f"""
        create table if not exists viability.{table}(
            CONSTRAINT {table}_check
                CHECK (model = '{model}' and scenario = '{scenario}')
        )
        inherits(viability.idx_mth);
        create index if not exists
            idx_{table}_envid on viability.{table}(envid);
        create index if not exists
            idx_{table}_season on viability.{table}(season);
        create index if not exists
            idx_{table}_snowtype on viability.{table}(snowtype);
        """
        cur2.execute(query)
        myconn.commit()
        
        query = """select distinct substr(a.snowtype,1, length(a.snowtype) - position('_' in reverse(a.snowtype)))||'_'||%s snowtype
                from crosscut.loc_weight_env a
                where substr(a.snowtype,length(a.snowtype) - position('_' in reverse(a.snowtype)) + 2, length(a.snowtype)) = 'sm'
                and envid in (select envid from crosscut.envelopes where ind = %s)
                """
        cur2.execute(query,(snowtype,ind,))
        
        st = []
        for s in cur2:
            st.append(s[0])
        st = tuple(st)
        
        # Final "LEFT" join with coalesce has been added to deal with case when there's no intersection between envelope and snowmaking area
        query = f"""
        delete from viability.{table} where envid in (
            select envid from crosscut.envelopes where ind = %s
        )
        and season = %s and snowtype in %s;
        insert into viability.{table}
        with a as (
            select envid, season, substr(a.snowtype,1, length(a.snowtype) - position('_' in reverse(a.snowtype))) snowtype,
            model, scenario,
            sum(case when swe >= 100 and extract(month from ddate) = 11 then {varagg} else 0 end) nov,
            sum(case when swe >= 100 and extract(month from ddate) = 12 then {varagg} else 0 end) as dec,
            sum(case when swe >= 100 and extract(month from ddate) = 1 then {varagg} else 0 end) jan,
            sum(case when swe >= 100 and extract(month from ddate) = 2 then {varagg} else 0 end) feb,
            sum(case when swe >= 100 and extract(month from ddate) = 3 then {varagg} else 0 end) mar,
            sum(case when swe >= 100 and extract(month from ddate) = 4 then {varagg} else 0 end) apr,
            sum(case when swe >= 100 and extract(month from ddate) in (12,1,2) then {varagg} else 0 end) djf,
            sum(case when swe >= 100 and extract(month from ddate) in (11,12,1,2,3,4) then {varagg} else 0 end) ndjfma
            from crosscut.loc_weight_env a
            join crocus.{tmpsnow} b on a.loc = b.loc
            where substr(a.snowtype,length(a.snowtype) - position('_' in reverse(a.snowtype)) + 2, length(a.snowtype)) = 'nosm'
            and a.envid in (select distinct envid from crosscut.envelopes where ind = '{ind}')
            and b.snowtype = 'gro'
            and ddate between (season||'-11-01')::date and ((season + 1)||'-04-30')::date
            group by envid, season, a.snowtype, model, scenario
        ), b as (
            select envid, season, substr(a.snowtype,1, length(a.snowtype) - position('_' in reverse(a.snowtype))) snowtype,
            model, scenario,
            sum(case when swe >= 100 and extract(month from ddate) = 11 then {varagg} else 0 end) nov,
            sum(case when swe >= 100 and extract(month from ddate) = 12 then {varagg} else 0 end) as dec,
            sum(case when swe >= 100 and extract(month from ddate) = 1 then {varagg} else 0 end) jan,
            sum(case when swe >= 100 and extract(month from ddate) = 2 then {varagg} else 0 end) feb,
            sum(case when swe >= 100 and extract(month from ddate) = 3 then {varagg} else 0 end) mar,
            sum(case when swe >= 100 and extract(month from ddate) = 4 then {varagg} else 0 end) apr,
            sum(case when swe >= 100 and extract(month from ddate) in (12,1,2) then {varagg} else 0 end) djf,
            sum(case when swe >= 100 and extract(month from ddate) in (11,12,1,2,3,4) then {varagg} else 0 end) ndjfma
            from crosscut.loc_weight_env a
            join crocus.{tmpsnow} b on a.loc = b.loc
            where substr(a.snowtype,length(a.snowtype) - position('_' in reverse(a.snowtype)) + 2, length(a.snowtype)) = 'sm'
            and a.envid in (select distinct envid from crosscut.envelopes where ind = '{ind}')
            and b.snowtype = %s
            and ddate between (season||'-11-01')::date and ((season + 1)||'-04-30')::date
            group by envid, season, a.snowtype, model, scenario
        ), c as (
			select distinct envid, season, snowtype, model, scenario
			from a
			where envid in (select distinct envid from crosscut.envelopes where ind = '{ind}')
			union
			select distinct envid, season, snowtype, model, scenario
			from b
			where envid in (select distinct envid from crosscut.envelopes where ind = '{ind}')
		)
        
        select c.envid, c.season, c.snowtype||'_'||%s snowtype, c.model, c.scenario,
        (coalesce(a.nov,0) + coalesce(b.nov,0)) / date_part('days', (c.season||'-'||11||'-01')::date + interval '1 month - 1') nov,
        (coalesce(a.dec,0) + coalesce(b.dec,0)) / date_part('days', (c.season||'-'||12||'-01')::date + interval '1 month - 1') as dec,
        (coalesce(a.jan,0) + coalesce(b.jan,0)) / date_part('days', (c.season + 1||'-'||01||'-01')::date + interval '1 month - 1') jan,
        (coalesce(a.feb,0) + coalesce(b.feb,0)) / date_part('days', (c.season + 1||'-'||02||'-01')::date + interval '1 month - 1') feb,
        (coalesce(a.mar,0) + coalesce(b.mar,0)) / date_part('days', (c.season + 1||'-'||03||'-01')::date + interval '1 month - 1') mar,
        (coalesce(a.apr,0) + coalesce(b.apr,0)) / date_part('days', (c.season + 1||'-'||04||'-01')::date + interval '1 month - 1') apr,
        (coalesce(a.djf,0) + coalesce(b.djf,0)) / (date_part('days', (c.season||'-'||12||'-01')::date + interval '1 month - 1')
                                                    + date_part('days', (c.season + 1||'-'||01||'-01')::date + interval '1 month - 1')
                                                    + date_part('days', (c.season + 1||'-'||02||'-01')::date + interval '1 month - 1')) djf,
        (coalesce(a.ndjfma,0) + coalesce(b.ndjfma,0)) / (date_part('days', (c.season||'-'||11||'-01')::date + interval '1 month - 1')
                                                            + date_part('days', (c.season||'-'||12||'-01')::date + interval '1 month - 1')
                                                            + date_part('days', (c.season + 1||'-'||01||'-01')::date + interval '1 month - 1')
                                                            + date_part('days', (c.season + 1||'-'||02||'-01')::date + interval '1 month - 1')
                                                            + date_part('days', (c.season + 1||'-'||03||'-01')::date + interval '1 month - 1')
                                                            + date_part('days', (c.season + 1||'-'||04||'-01')::date + interval '1 month - 1')) ndjfma
        from c
		left join a on (
            c.envid = a.envid and c.season = a.season and c.snowtype = a.snowtype and c.model = a.model and c.scenario = a.scenario
        )
        left join b on (
            c.envid = b.envid and c.season = b.season and c.snowtype = b.snowtype and c.model = b.model and c.scenario = b.scenario
        );
        """
        cur2.execute(query, (ind, season, st,snowtype,snowtype,))
        myconn.commit()
        print('idx computed')
        
    cur2.execute(f"""
        drop table if exists crocus.{tmpsnow};
        """)
    myconn.commit()
    
inds = ['7344A','7344B','7344C','7345']

for ind in inds:
    print(ind)
    compute_idx_launcher(ind)