library(RPostgreSQL)
library(png)

#GLOBAL PARAMETER
drv = dbDriver("PostgreSQL")
host = "10.38.192.30"
user = "postgres"
pwd = "lceslt"
db = "db"

plot_idx_bxp_mth_tracc = function(dstdir, envid){
	query = paste0("
		with hist as (
			select model, snowtype, 0.6 tracc,
			percentile_cont(0.2) within group (order by nov) nov, 
			percentile_cont(0.2) within group (order by \"dec\") \"dec\",  
			percentile_cont(0.2) within group (order by jan) jan,  
			percentile_cont(0.2) within group (order by feb) feb,  
			percentile_cont(0.2) within group (order by mar) mar,  
			percentile_cont(0.2) within group (order by apr) apr,  
			percentile_cont(0.2) within group (order by ndjfma) ndjfma
			from viability.idx_mth
			where model in (select distinct model from  crocus.tracc_model)
			and envid = ", envid, " and scenario = 'historical'
			and season >= 1975 and season <= 2004 and snowtype not like '%_fan' and snowtype not like '%_nn'
			group by 1,2,3
		)
		  select a.tracc, case when a.snowtype like '%_gro' then 'aaa' else a.snowtype end,
			percentile_cont(0.5) within group (order by nov) nov, 
			percentile_cont(0.5) within group (order by \"dec\") \"dec\",  
			percentile_cont(0.5) within group (order by jan) jan,  
			percentile_cont(0.5) within group (order by feb) feb,  
			percentile_cont(0.5) within group (order by mar) mar,  
			percentile_cont(0.5) within group (order by apr) apr,  
			percentile_cont(0.5) within group (order by ndjfma) ndjfma
		  from hist a
		  group by 1,2"
	)

	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	q20 <- dbGetQuery(con, query)
	dbDisconnect(con)
	
	query = paste0("
		with hist as (
			select season + 1 season, model, scenario, snowtype, 0.6 tracc, nov, \"dec\", jan, feb, mar, apr, ndjfma
			from viability.idx_mth
			where model in (select distinct model from  crocus.tracc_model)
			and envid = ", envid, " and scenario = 'historical'
			and season >= 1975 and season <= 2004 and snowtype not like '%_fan' and snowtype not like '%_nn'
		), proj as (
			select distinct season + 1 season, b.model, scenario, d.snowtype, c.tracc, nov, \"dec\", jan, feb, mar, apr, ndjfma
			from crocus.tracc a
			join crocus.tracc_model b on a.gcm = b.gcm and a.rcm = b.rcm
			join crocus.tracc_gwl c on a.gwl = c.gwl
			join viability.idx_mth d on b.model = d.model and scen = scenario
				and season >= an_deb - 1 and season <= an_fin - 1
			where d.envid = ", envid, " and snowtype not like '%_fan' and snowtype not like '%_nn'
		), tot as (
			select 'SAFRAN' tracc, case when a.snowtype like '%_gro' then 'aaa' else a.snowtype end, nov, \"dec\", jan, feb, mar, apr, ndjfma
			from viability.idx_mth_safran_reanalysis a
			where envid = ", envid, "	and season >= 1975 and season <= 2004
			and snowtype not like '%_fan' and snowtype not like '%_nn'

			union

			select a.tracc::varchar, case when a.snowtype like '%_gro' then 'aaa' else a.snowtype end, nov, \"dec\", jan, feb, mar, apr, ndjfma
			from hist a

			union

			select a.tracc::varchar, case when a.snowtype like '%_gro' then 'aaa' else a.snowtype end, nov, \"dec\", jan, feb, mar, apr, ndjfma
			from proj a
		)
		
		select * from tot
		order by case when tracc like 'S%' then '0' else tracc end, case when snowtype like '%_gro' then 'aaa' else snowtype end"
	)

	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	data <- dbGetQuery(con, query)
	dbDisconnect(con)

	nbp = seq(3,ncol(data))
	
	h = 800
	w = 200 * length(nbp)
	png(width = w, height = h, paste0(dstdir, "mth_tracc_boxplot_", envid, ".png"))
	# layout(matrix(seq(1,length(nbp)), 1, length(nbp)))
	layout(matrix(c(rep(seq(1,length(nbp)), 7), rep(length(nbp) + 1, length(nbp))),8, length(nbp), byrow = TRUE))
	print(matrix(c(rep(seq(1,length(nbp)), 7), rep(length(nbp) + 1, length(nbp))),8, length(nbp), byrow = TRUE))
	traccs = unique(data[,1])
	sts = unique(data[,2])

	# for(i in nbp){
	  # cols = c(rep('grey',2),rep('yellow',2),rep('orange',2),rep('red',2))
	  # tmp = data[,c(1,2,i)]
	  # par(mar = c(12,0,0,0))
	  # boxplot(tmp[,3]~data[,2]+data[,1], ylim = c(0,100), col = cols, axes = F, names = NA, xlab = NA)
	  # at = seq(1,length(traccs) * length(sts))
	  # lab = rep(c('gro', 'lan'), length(traccs))
	  # axis(1, line = -1, at = at, labels = lab, las = 2, cex.axis = 2)
	  # at = seq(1.5,length(traccs) * length(sts),2)
	  # lab = paste0("+", traccs, "°C")
	  # axis(1, line = 3, at = at, labels = lab, las = 2, lwd = 0, cex.axis = 1.8)
	  # at = seq(2.5,length(traccs) * length(sts),2)
	  # lab = rep("|", length(traccs) - 1)
	  # axis(1, line = 5, at = at, labels = lab, las = 1, lwd = 0, cex.axis = 3)
	  
	  # mtext(colnames(tmp)[3], side = 1, line = 10, cex = 1.6)
	# }

	for(i in nbp){
	  cols = rep(c('lightgrey','grey','yellow','orange','red'),2)
	  tmp = data[,c(1,2,i)]
	  par(mar = c(12,0,0,0), xpd = TRUE)
	  tmp[,1] = factor(tmp[,1], levels = traccs)
	  boxplot(tmp[,3]~tmp[,1]+tmp[,2], ylim = c(0,100), col = cols, axes = F, names = NA, xlab = NA)
	  at = seq(1,length(traccs) * length(sts))
	  lab = rep(c(traccs[1], paste0("+", traccs[2:length(traccs)], "°C")),2)
	  axis(1, line = -1, at = at, labels = lab, las = 2, cex.axis = 2)
	  # lab = c("Natural\ngroomed snow", "Natural and man made\ngroomed snow")
	  # at = seq(2.5,length(traccs) * length(sts),length(traccs))
	  # print(at)
	  # axis(1, line = 5, at = at, labels = lab, las = 1, lwd = 0, cex.axis = 1.8)
		y = -16
		for (j in 1:length(sts)) {
			if (j == 1) {
				icon = "/home/francois/data/source_rasters/picto_gro.png"
			} else {
				icon = "/home/francois/data/source_rasters/picto_gro_+_snowmaking.png"
			}
			icon = readPNG(icon)
			isize = dim(icon)
			at = seq(1 + (j - 1) * length(traccs), j * length(traccs))
			xlen = max(at) - min(at)
			x = min(at) + xlen * 0.5
			xsize = xlen * 0.5
			xl = x - xsize / 2
			xr = x + xsize / 2
			
			p.xsize = dev.size()[1] / w
			p.ysize = dev.size()[2] / h
			p.xval = (par("usr")[2] - par("usr")[1]) / (par("pin")[1] / p.xsize)
			p.yval = (par("usr")[4] - par("usr")[3]) / (par("pin")[2] / p.ysize)
			
			ysize = (xsize * isize[2] / isize[1]) * (p.yval / p.xval)
			yb = y - ysize / 2
			yt = y + ysize / 2
			
			# xl = x - (isize[1] * p.xval / 2 / r)
			# yb = y - (isize[2] * p.yval / 2 / r)
			# xr = x + (isize[1] * p.xval / 2 / r)
			# yt = y + (isize[2] * p.yval / 2 / r)
			rasterImage(icon, xl, yb, xr, yt)
		}
		
	  at = seq(5.5,length(traccs) * length(sts),length(traccs))
	  lab = rep("|", length(sts) - 1)
	  axis(1, line = 5, at = at, labels = lab, las = 1, lwd = 0, cex.axis = 3)
	  
	  tmpq20 = q20[, c(2,i)]
	  for (j in 1:nrow(tmpq20)){
		if (tmpq20[j,1] == 'aaa'){
			col = '#6baed6'
		} else {
			col = '#08519c'
		}
		lines(c(0,length(traccs) * length(sts) + 1), c(tmpq20[j,2], tmpq20[j,2]), col = col, lty = 5, lwd = 1.5)
	  }
	  
	  mtext(colnames(tmp)[3], side = 1, line = 10, cex = 1.6)
	}
	
	par(mar=c(0,0,0,0))
	plot(0,0,type = "n", xlim = c(0,1), ylim = c(0,1), axes = FALSE, xlab = NA, ylab = NA)
	legend("center",
		c(traccs, "Q20 natural groomed snow", "Q20 natural + man made groomed snow"), 
		fill = c(unique(cols), NA, NA), border = c(rep("black", length(traccs)), NA, NA),
		col = c(rep(NA, length(traccs)), '#6baed6',rep('#08519c', length(sts) - 1)),
		lwd = c(rep(NA, length(traccs)), rep(1.5, length(sts))),
		lty = c(rep(NA, length(traccs)), rep(5, length(sts))),
		ncol = length(traccs), cex = 2, bty = "n", text.width = 0.05, x.intersp = 0.5
	)
	
	dev.off()
}

inds = c('7331A')

for (i in 1:length(inds)){
    ind = inds[i]
    query = paste0("select name from crosscut.resorts where ind = '",ind,"';")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    sta <- dbGetQuery(con, query)
    dbDisconnect(con)

    src_sta = paste0("/home/francois/data/", sta, "/results/")

    query = paste0("select distinct envid, name from crosscut.envelopes
    where ind = '",ind,"'
    and envid = 224
    order by 1")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    listenv <- dbGetQuery(con, query)
    dbDisconnect(con)
	
	for (envid in 1:nrow(listenv)){
		envid = listenv[envid,1]
		dstdir = paste0(src_sta, envid, "/plots/") 

		plot_idx_bxp_mth_tracc(dstdir, envid)
	}
}