import psycopg2
import numpy as np
import conn_param
import multiprocessing

def nbdays_launcher():
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()

    # Retrieve available simulations for these zones
    query = """select distinct a.typezone, a.model, a.scenario, a.snowtype, a.season
            from crocus.available_simulations a
            left join viability.nbdays_mth_available_simulations b on
	            a.typezone = b.typezone
	            and a.model = b.model
	            and a.scenario = b.scenario
	            and a.snowtype = b.snowtype
	            and a.season::integer = b.season
            where b.typezone is null
        order by a.typezone, a.model, a.scenario, a.snowtype, a.season"""
    cur.execute(query)
    print(cur.rowcount)
    args = []
    for sim in cur:
        typezone = sim[0]
        model = sim[1]
        scenario = sim[2]
        snowtype = sim[3]
        season = sim[4]
        args.append((typezone, model, scenario, snowtype, season))
    print("args done")
    pool = multiprocessing.Pool(processes = 10)
    pool.map(nbdays, args)
    pool.close()

def nbdays(args):
    typezone, model, scenario, snowtype, season = args
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur2 = myconn.cursor()
    
    # Create destination table if not exists
    table = f"nbdays_mth_{model}_{scenario}"
    query = f"""
    create table if not exists viability.{table}(
        CONSTRAINT {table}_check
            CHECK (model = '{model}' and scenario = '{scenario}')
    )
    inherits(viability.nbdays_mth);
    create index if not exists
        nbdays_{table}_loc on viability.{table}(loc);
    create index if not exists
        nbdays_{table}_typezone on viability.{table}(typezone);
    create index if not exists
        nbdays_{table}_season on viability.{table}(season);
    create index if not exists
        nbdays_{table}_snowtype on viability.{table}(snowtype);
    """
    cur2.execute(query)
    myconn.commit()
    print(f'{table} done')
    
    # Insert nbdays for each loc  in the table
    srctab = f'snow_{typezone}_{model}_{scenario}_{snowtype}_{season}'
    query = f"""
        delete from viability.{table}
            where typezone = %s and snowtype = %s and season = %s;
        insert into viability.{table}
        select loc, typezone, season, snowtype, model, scenario,
            sum(case when swe >= 100 and extract(month from ddate) = 11 then 1 else 0 end) nov,
            sum(case when swe >= 100 and extract(month from ddate) = 12 then 1 else 0 end) as dec,
            sum(case when swe >= 100 and extract(month from ddate) = 1 then 1 else 0 end) jan,
            sum(case when swe >= 100 and extract(month from ddate) = 2 then 1 else 0 end) feb,
            sum(case when swe >= 100 and extract(month from ddate) = 3 then 1 else 0 end) mar,
            sum(case when swe >= 100 and extract(month from ddate) = 4 then 1 else 0 end) apr,
            sum(case when swe >= 100 and extract(month from ddate) in (12,1,2) then 1 else 0 end) djf,
            sum(case when swe >= 100 then 1 else 0 end) ndjfma
        from crocus.{srctab}
        where ddate between (season||'-11-01')::date and ((season + 1)||'-04-30')::date
        group by loc, typezone, season, model, scenario, snowtype;
    """
    cur2.execute(query,(typezone, snowtype, season,))
    myconn.commit()
    print(f'data {srctab} inserted')

nbdays_launcher()
myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()
cur.execute("refresh materialized view viability.nbdays_mth_available_simulations")
myconn.commit()
