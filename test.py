import os
import multiprocessing
import numpy as np
import pandas as pd
import xarray as xr
import datetime as dt
import psycopg2
import conn_param
from sqlalchemy import create_engine
from collections import Counter

root_dir = "/home/francois/pasto-data/"
zone_id = "flat"
fdir = os.path.join(root_dir, f"alp_{zone_id}")
var = 'spi-3month'
model = 'SAFRAN'
f = os.path.join(fdir, f"{var}_{model}.nc")

ds_tmp = xr.open_dataset(f)
ds_tmp = ds_tmp.drop(['LAT', 'LON', 'ZS', 'aspect', 'massif_number', 'slope'])
varnames = list(ds_tmp.keys())
vardict = {varnames[0]:var.replace("-","_")}
ds_tmp = ds_tmp.rename(vardict)
var = list(ds_tmp.keys())[0]

if var == "spi_3month":
    ts = ds_tmp.time.values
    months = ['03', '04', '05', '06', '07', '08', '09', '10', '11']
    for m in months:
        newts = [t for t in ts if pd.to_datetime(t).strftime('%m') == '03']
        ds2 = ds_tmp.sel(time = newts)
        newts = [np.datetime64(pd.to_datetime(t).replace(month = 1)) for t in newts]
        ds2 = ds2.assign_coords(time = newts)
        newvar = {var:f"{var}_{m}"}
        ds2 = ds2.rename(newvar)
        if m == months[0]:
            ds_tmp2 = ds2
        else:
            ds_tmp2 = ds_tmp2.merge(ds2)
        print(ds_tmp2.keys())
    ds_tmp = ds_tmp2