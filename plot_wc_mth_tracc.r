library(RPostgreSQL)

#GLOBAL PARAMETER
drv = dbDriver("PostgreSQL")
host = "10.38.192.30"
user = "postgres" 
pwd = "lceslt"
db = "db"

plot_wc_tracc <-function(dst_dir, envid){
	#get data
	query = paste0("
		with a as (
			select tracks.ind, status, st_union(tracks.geom) slopes_geom
			from crosscut.tracks
			join crosscut.sm_resort on tracks.ind = sm_resort.ind
			where tracks.ind = (select ind from crosscut.envelopes where envid = ", envid, ")
			group by 1,2
		), part as (
			select envid, status, st_area(st_intersection(slopes_geom, geom))/st_area(geom) part
			from a
			join crosscut.envelopes b on a.ind = b.ind
			where envid = ", envid, "
		), hist1 as (
			select wc_mth.envid, status, 0.6 tracc,
			percentile_cont(0.5) within group(order by nov * part) novq50,
			avg(nov * part) novmean,
			percentile_cont(0.5) within group(order by dec * part) decq50,
			avg(dec * part) decmean
			from viability.wc_mth
			join part on wc_mth.envid = part.envid and replace(snowtype, '_lance','') = status
			where model in (select distinct model from  crocus.tracc_model)
			and wc_mth.envid = ", envid, " and scenario = 'historical' and snowtype ilike '%_lance'
			and season >= 1976 and season <= 2005
			group by 1,2,3
		), hist2 as (
			select wc_mth.envid, status, 0.6 tracc,
			percentile_cont(0.5) within group(order by jan * part) janq50,
			avg(jan * part) janmean,
			percentile_cont(0.5) within group(order by feb * part) febq50,
			avg(feb * part) febmean,
			percentile_cont(0.5) within group(order by mar * part) marq50,
			avg(mar * part) marmean
			from viability.wc_mth
			join part on wc_mth.envid = part.envid and replace(snowtype, '_lance','') = status
			where model in (select distinct model from  crocus.tracc_model)
			and wc_mth.envid = ", envid, " and scenario = 'historical' and snowtype ilike '%_lance'
			and season + 1 >= 1976 and season + 1 <= 2005
			group by 1,2,3
		), tracc1 as (
			select d.envid, status, tracc,
			percentile_cont(0.5) within group(order by nov * part) novq50,
			avg(nov * part) novmean,
			percentile_cont(0.5) within group(order by dec * part) decq50,
			avg(dec * part) decmean
			from crocus.tracc a
			join crocus.tracc_model b on a.gcm = b.gcm and a.rcm = b.rcm
			join crocus.tracc_gwl c on a.gwl = c.gwl
			join viability.wc_mth d on b.model = d.model and scen = scenario
				and season >= an_deb and season <= an_fin
			join part on d.envid = part.envid and replace(snowtype, '_lance','') = status
			where d.envid = ", envid, " and snowtype ilike '%_lance'
			group by 1,2,3
		), tracc2 as (
			select d.envid, status, tracc,
			percentile_cont(0.5) within group(order by jan * part) janq50,
			avg(jan * part) janmean,
			percentile_cont(0.5) within group(order by feb * part) febq50,
			avg(feb * part) febmean,
			percentile_cont(0.5) within group(order by mar * part) marq50,
			avg(mar * part) marmean
			from crocus.tracc a
			join crocus.tracc_model b on a.gcm = b.gcm and a.rcm = b.rcm
			join crocus.tracc_gwl c on a.gwl = c.gwl
			join viability.wc_mth d on b.model = d.model and scen = scenario
				and season + 1 >= an_deb and season + 1 <= an_fin
			join part on d.envid = part.envid and replace(snowtype, '_lance','') = status
			where d.envid = ", envid, " and snowtype ilike '%_lance'
			group by 1,2,3
		)

		select a.*, janq50, janmean, febq50, febmean, marq50, marmean
		from hist1 a
		join hist2 b on a.envid = b.envid and a.status = b.status and a.tracc = b.tracc

		union

		select a.*, janq50, janmean, febq50, febmean, marq50, marmean
		from tracc1 a
		join tracc2 b on a.envid = b.envid and a.status = b.status and a.tracc = b.tracc

		order by tracc"
	)
	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)	
	data_all = dbGetQuery(con, query)
	dbDisconnect(con)
	
	startvalues = c(4,5)
	for (st in startvalues){
		for (status in unique(data_all[,2])){
			data = data_all[data_all[,2] == status, c(1,2,3, seq(st, ncol(data_all), 2))]
			#instantiate device
			cnames = colnames(data)[4:ncol(data)]
			valtype = unique(substr(cnames,4,nchar(cnames)))
			traccs = data[,3]
			nbbars = length(traccs)
			bw = 5
			sp = 0.1 * bw
			xmax = (bw + sp) * nbbars
			rsum = rowSums(data[,4:ncol(data)])
			xlim = c(0, xmax)
			mycol = colorRampPalette(c('lightblue', 'darkblue'))(ncol(data) - 3)
			
			suffixe = c("absval", "relval")
			dstfile = paste0(dst_dir, "wc_", status, "_lance_", valtype, "_tracc.png")
			png(dstfile, width = 800, height = 600, bg = "transparent", res = 150)
			layout(matrix(c(1,1,3,2,2,3), 3, 2))
			
			for (suf in suffixe){
				if (suf == 'absval'){
					#dstfile = paste0(dst_dir, "wc_", status, "_lance_", valtype, "_tracc_", suf, ".png")
					ymax = max(rsum)
					ymax = ceiling(ymax / 10^(floor(log10(ymax)) - 1)) * 10^(floor(log10(ymax)) - 1)
					ylim = c(0, ymax)
				} else {
					#dstfile = paste0(dst_dir, "wc_", status, "_lance_", valtype, "_tracc_relval.png")
					for (i in 1:nrow(data)){
						for (j in 4:ncol(data)){
							data[i,j] = data[i,j] * 100 / rsum[i]
						}
					}
					ylim = c(0, 100)
				}
				
				par(mar = c(1,2,2,0), xpd = TRUE)
				
				query = paste0("select name from crosscut.envelopes where envid = ", envid)
				con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
				mytitle = dbGetQuery(con, query)
				mytitle = mytitle[1,1]
				dbDisconnect(con)
				mytitle = paste0("Demande en eau pour la production de neige\nen fonction des degrés de réchauffement en France\n", mytitle)
				plot(0, 0, type = 'n', xlim = xlim, ylim = ylim, axes = FALSE, xlab = NA, ylab = NA, main = mytitle, cex.main = .6)
				x = 0
				xat = c()
				xlabs = c()
				for (tracc in traccs){
					x = x + sp
					x2 = x + bw
					y = 0
					for (i in 4:ncol(data)){
						y2 = y + data[data[,3] == tracc, i]
						print(c(y, y2, x, x2))
						polygon(c(x,x,x2,x2), c(y,y2,y2,y), col = mycol[i - 3])
						y = y2
					}
					xat = c(xat, x + (x2 - x) / 2)
					xlabs = c(xlabs, paste0("+", tracc, "°C"))
					
					x = x2
				}
				lines(xlim, c(0,0))
				axis(side=1, line = -1, lwd = 0, at = xat, labels = xlabs, cex.axis = 0.8, font.lab=2, las = 0)
				axis(2)
				for (y in pretty(ylim)){
					lines(xlim, c(y,y), col = "grey", lty = 3)
				}
			}	
				
			par(mar = c(0,0,0,0))
			plot(0, 0, type = 'n', xlim = xlim, ylim = ylim, axes = FALSE, xlab = NA, ylab = NA)
			myleg = c("Novembre", "Décembre", "Janvier", "Février", "Mars", "Avril")
			legend(xlim[2]/2, ylim[2]/2, myleg, fill = mycol, xjust = .5, yjust = .5, bty = 'n', ncol = 3, horiz = TRUE)
			dev.off()			
		}
	}
}

##############################FUNCTION END

inds = c("7331A")

for (ind in inds){
	query = paste0("select name from crosscut.resorts where ind = '", ind, "'")
	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	rname = dbGetQuery(con, query)
	dbDisconnect(con)
	rname = rname[1,1]
	
	query = paste0("select distinct envid from crosscut.envelopes where ind = '", ind, "'")
	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	env = dbGetQuery(con, query)
	dbDisconnect(con)
	print(env)
	for (i in 1:nrow(env)){
		envid = env[i,1]
		dst_dir = paste0("/home/francois/data/", rname, "/results/", envid, "/plots/")
		dir.create(dst_dir)
		plot_wc_tracc(dst_dir, envid)
	}
}