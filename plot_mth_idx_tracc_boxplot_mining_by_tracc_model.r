library(RPostgreSQL)
library(png)

#GLOBAL PARAMETER
drv = dbDriver("PostgreSQL")
host = "10.38.192.30"
user = "postgres"
pwd = "lceslt"
db = "db"

plot_idx_bxp_mth_tracc = function(dstdir, envid){
	query = paste0("
		with hist1 as (
			select season, gcm||'_'||rcm model, scenario, snowtype, 0.6 tracc, nov, \"dec\"
			from viability.idx_mth a
			join crocus.tracc_model b on a.model = b.model
			and envid = ", envid, " and scenario = 'historical'
			and season >= 1976 and season <= 2005 and snowtype not like '%_fan' and snowtype not like '%_nn'
		), hist2 as (
			select season + 1 season, gcm||'_'||rcm model, scenario, snowtype, 0.6 tracc, jan, feb, mar, apr, ndjfma
			from viability.idx_mth a
			join crocus.tracc_model b on a.model = b.model
			and envid = ", envid, " and scenario = 'historical'
			and season + 1 >= 1976 and season + 1 <= 2005 and snowtype not like '%_fan' and snowtype not like '%_nn'
		), proj1 as (
			select distinct season, b.gcm||'_'||b.rcm model, scenario, d.snowtype, c.tracc, nov, \"dec\"
			from crocus.tracc a
			join crocus.tracc_model b on a.gcm = b.gcm and a.rcm = b.rcm
			join crocus.tracc_gwl c on a.gwl = c.gwl
			join viability.idx_mth d on b.model = d.model and scen = scenario
				and season >= an_deb and season <= an_fin
			where d.envid = ", envid, " and snowtype not like '%_fan' and snowtype not like '%_nn'
		), proj2 as (
			select distinct season + 1 season, b.gcm||'_'||b.rcm model, scenario, d.snowtype, c.tracc, jan, feb, mar, apr, ndjfma
			from crocus.tracc a
			join crocus.tracc_model b on a.gcm = b.gcm and a.rcm = b.rcm
			join crocus.tracc_gwl c on a.gwl = c.gwl
			join viability.idx_mth d on b.model = d.model and scen = scenario
				and season + 1 >= an_deb and season + 1 <= an_fin
			where d.envid = ", envid, " and snowtype not like '%_fan' and snowtype not like '%_nn'
		), tot as (		
		  select a.tracc, a.snowtype, a.model, nov, \"dec\", jan, feb, mar, apr, ndjfma
		  from hist1 a
		  join hist2 b on a.season = b.season and a.model = b.model and a.scenario = b.scenario and a.snowtype = b.snowtype and a.tracc = b.tracc
		  
		  union
		  
		  select a.tracc, a.snowtype, a.model, nov, \"dec\", jan, feb, mar, apr, ndjfma
		  from proj1 a
		  join proj2 b on a.season = b.season and a.model = b.model and a.scenario = b.scenario and a.snowtype = b.snowtype and a.tracc = b.tracc
		)
		
		select * from tot
		order by 1, case when snowtype like '%_gro' then 'aaa' else snowtype end,
		model
				   "
	)

	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	datall <- dbGetQuery(con, query)
	dbDisconnect(con)

	#cols = c(rep('grey',2),rep('yellow',2),rep('orange',2),rep('red',2))
	#)
	
	datall[,3] = gsub("_", "\n", datall[,3])
	datall2 = datall
	datall2[,3] = "All models"
	# datall = rbind(datall, datall2)

	traccs = unique(datall[,1])
	snowtypes = unique(datall[,2])
	for (st in snowtypes){
		for (t in traccs){
		  data = datall[datall[,1] == t & datall[,2] == st, c(3:ncol(datall))]
		  data2 = datall2[datall2[,1] == t & datall2[,2] == st, c(3:ncol(datall2))]
		  sts = unique(datall[,2])
		  nbp = seq(2,ncol(data))
		  for(i in nbp){
			h = 600
			w = 1000
			png(width = w, height = h, paste0(dstdir, "mth_tracc_boxplot_",colnames(data)[i], "_", st, "_+",t,"°C_", envid, ".png"))
			layout(matrix(c(1,1,1,2),1,4))

			tmp = data[,c(1,i)]
			models = sort(unique(tmp[,1]))
			
			par(mar = c(14,3,4,0))
			bp = boxplot(tmp[,2]~tmp[,1], plot = FALSE)		
			
			for (j in 1:length(bp$names)){
				percentiles = tmp[tmp[,1] == bp$names[j],2]
				percentiles = quantile(percentiles, probs = c(0.2, 0.8))
				bp$stats[2,j] = percentiles[1]
				bp$stats[4,j] = percentiles[2]
			}
			mycol = terrain.colors(length(models))
			bxp(bp,
				ylim = c(0,100), boxfill = mycol, las = 2, frame = F,
				main = paste0("+", t, "°C (", colnames(data)[i], ") - ", st), cex.main = 1.8)
			
			# # print(models)
			# # models = unique(tmp[,2])
			# models = gsub("_", "\n", models)
			# # at = seq(1,length(models))	
			# axis(1, line = 0, at = seq(1,length(models)), labels = models, las = 2)
			# axis(2, line = 0, las = 2)
			for (y in pretty(c(0,100))){
			  lines(c(par("usr")[1], par("usr")[2]), c(y,y), lty = 2, col = "lightgrey")
			}
			
			if (t != 0.6){
				for (j in 1:length(models)){
					q20ref = datall[datall[,1] == 0.6 & datall[,2] == 'ns&gro_gro' & datall[,3] == models[j], i + 2]
					q20ref = quantile(q20ref, probs = c(0.2))
					q20freq = tmp[tmp[,2] <= q20ref,]
					q20freq = round(nrow(q20freq) / nrow(tmp),2)
					text(j, 101.5, q20freq, adj = c(.5,0), cex = 1.4, col = "purple")
					if (j == 1){
						allfreq = c("freq. retour\nq20 gro hist.", q20freq * 100)
					} else {
						allfreq = rbind(allfreq, c("freq. retour\nq20 gro hist.", q20freq * 100))
					}
				}
			}
			
			par(mar = c(14,0,4,0))
			z = 0
			plist = c(0.2,0.5,0.8)
			for (m in models){
			  for (p in plist) {
				vec = c(paste0("q", p * 100), quantile(tmp[tmp[,1] == m, 2], probs = c(p)))
				if (z == 0){
				  z = 1
				  tmp2 = vec
				} else {
				  tmp2 = rbind(tmp2, vec)
				}
			  }
			}
			tmpall = data2[,c(1,i)]
			# colnames(tmp2) = colnames(tmpall)
			# tmp2 = rbind(tmp2, data2[,c(1,i)])
			# print(unique(tmp2[,1]))
			if (t != 0.6){
				# colnames(allfreq) = colnames(tmp2)
				# tmp2 = rbind(tmp2, allfreq)
				val = c(as.vector(as.double(tmp2[,2])), as.vector(as.double(tmpall[,2])), as.vector(as.double(allfreq[,2])))
				typ = c(as.vector(tmp2[,1]), as.vector(tmpall[,1]), as.vector(allfreq[,1]))
				mylev = c("q20", "q50", "q80", "All models", "freq. retour\nq20 gro hist.")
			} else {
				val = c(as.vector(as.double(tmp2[,2])), as.vector(as.double(tmpall[,2])))
				typ = c(as.vector(tmp2[,1]), as.vector(tmpall[,1]))
				mylev = c("q20", "q50", "q80", "All models")
			}
			tmp2 = data.frame(typ, val)
			# print(tmp2)
			tmp2$typ <- factor(tmp2$typ , levels= mylev)
			
			bp = boxplot(as.numeric(tmp2[,2])~tmp2[,1], plot = FALSE)
			
			for (j in 1:length(bp$names)){
				percentiles = tmp2[tmp2[,1] == bp$names[j],2]
				percentiles = quantile(percentiles, probs = c(0.2, 0.8))
				bp$stats[2,j] = percentiles[1]
				bp$stats[4,j] = percentiles[2]
			}
			mycol = terrain.colors(length(models))
			bxp(bp,	ylim = c(0,100), boxfill = c("lightblue", "blue", "darkblue", "darkgrey", "purple"), frame = F, las = 2)
			
			
			if (t != 0.6){
				q20ref = datall2[datall2[,1] == 0.6 & datall2[,2] == 'ns&gro_gro', i + 2]
				q20ref = quantile(q20ref, probs = c(0.2))
				q20freq = data2[data2[,i] <= q20ref,]
				q20freq = round(nrow(q20freq) / nrow(data2),2)
				text(4, 101.5, q20freq, adj = c(.5,0), cex = 1.4, col = "purple")
				q20freq = q20freq * 100
				lines(c(4.6,5.4), c(q20freq, q20freq), lwd = 1.5, col = "red")
			}
			
			for (y in pretty(c(0,100))){
			  lines(c(par("usr")[1], par("usr")[2]), c(y,y), lty = 2, col = "lightgrey")
			}
		  
		  dev.off()
		  }
		}
		
		##### scatter plot #####
		nbp = seq(4, ncol(datall))
		for (i in nbp){
			h = 600
			w = 800
			png(width = w, height = h, paste0(dstdir, "mth_tracc_scatterplot_",colnames(datall)[i], "_", st, "_", envid, ".png"))
			probs = c(0.2, 0.5, 0.8)
			qval = c()
			tval = c()
			x = c()
			y = c()
			for (p in probs){
				for (t in traccs){
					qval = c(qval, paste0("q", p * 100))
					tval = c(tval, t)
					tmpdf = datall[datall[,1] == t & datall[,2] == st,]
					x = c(x, quantile(tmpdf[,i], probs = c(p)))
					mq20 = c()
					for (m in models){
						mq20 = c(mq20, quantile(tmpdf[tmpdf[,3] == m, i], probs = c(p)))
					}
					y = c(y, quantile(mq20, probs = c(0.5)))
					
					if (t != 0.6){
						qval = c(qval, "freq_q20")
						tval = c(tval, t)
						q20ref = quantile(datall[datall[,1] == 0.6 & datall[,2] == st, i], probs = c(0.2))
						x = c(x, nrow(tmpdf[tmpdf[,i] <= q20ref,]) * 100 / nrow(tmpdf))
						mq20 = c()
						for (m in  models){
							q20ref = quantile(datall[datall[,1] == 0.6 & datall[,2] == st & datall[,3] == m, i], probs = c(0.2))
							mq20 = c(mq20, nrow(tmpdf[tmpdf[,3] == m & tmpdf[,i] <= q20ref,]) * 100 / nrow(tmpdf[tmpdf[,3] == m,]))
						}
						
						y = c(y, quantile(mq20, probs = c(0.5)))
					}
				}
			}
			df = data.frame(qval, tval, x, y, stringsAsFactors = FALSE)
			xlim = c(0, ceiling(max(df[,3]) / 10) * 10)
			ylim = c(0, ceiling(max(df[,4]) / 10) * 10)
			pchs = c(21,22,23,24)
			cols = c('#fef0d9','#fdcc8a','#fc8d59','#d7301f')
			par(mar = c(3,3,2,0))
			plot(0,0, type = 'n', xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab = NA,
				main = paste0(colnames(datall)[i], " - ", st), cex.main = 1.8
			)
			axis(side = 1, tck = -.01, labels = NA)
			axis(side = 1, line = -.7, cex.axis =1, lwd = 0, font.lab=2)
			mtext(side = 1, line = 1.7, "All models distribution", cex = 1.6)
			
			axis(side = 2, tck = -.01, labels = NA)
			axis(side = 2, line = -.7, cex.axis =1, lwd = 0, font.lab=2)
			mtext(side = 2, line = 1.7, "Inter-models median", cex = 1.6)
			
			abline(0,1)
			
			ip = 0
			qval = sort(unique(df[,1]))
			tval = sort(unique(df[,2]))
			for (q in qval){
				ip = ip + 1
				pch = pchs[ip]
				ic = 0
				for (t in tval){
					ic = ic + 1
					col = cols[ic]
					points(df[df[,1] == q & df[,2] == t, 3], df[df[,1] == q & df[,2] == t, 4],
						pch = pch, col = "black", bg = col, cex = 1.4
					)
				}
			}
			
			legend("topleft", c(qval, paste0("+", tval, "°C")), bty = 'n', cex = 1.4,
				pch = c(pchs, rep(NA, length(tval))), pt.cex = c(rep(1.6, length(qval)), rep(NA, length(tval))),
				fill = c(rep(NA, length(qval)), cols), border = c(rep(NA, length(qval)), rep("black", length(tval)))
			)
			
			dev.off()
		}
	}
}

inds = c('7331A')

for (i in 1:length(inds)){
    ind = inds[i]
    query = paste0("select name from crosscut.resorts where ind = '",ind,"';")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    sta <- dbGetQuery(con, query)
    dbDisconnect(con)

    src_sta = paste0("/home/francois/data/", sta, "/results/")

    query = paste0("select distinct envid, name from crosscut.envelopes
    where ind = '",ind,"'
    and envid = 224
    order by 1")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    listenv <- dbGetQuery(con, query)
    dbDisconnect(con)
	
	for (envid in 1:nrow(listenv)){
		envid = listenv[envid,1]
		dstdir = paste0(src_sta, envid, "/plots/") 

		plot_idx_bxp_mth_tracc(dstdir, envid)
	}
}