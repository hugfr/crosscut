library(RPostgreSQL)


plot_idx_matrix = function(dst_dir, envid, snowtype){

	query = paste0("
		with tot as (
			--HISTORICAL
			with hist as (
				select season, model, scenario, nov, dec, jan, feb, mar, apr, ndjfma
				from viability.idx_mth
				where model != 'safran' and scenario = 'historical'
				and season >= 1985
				and snowtype = '", snowtype,"' and envid = ", envid, "
				
				union
				
				select season, model, 'historical', nov, dec, jan, feb, mar, apr, ndjfma
				from viability.idx_mth
				where model != 'safran' and scenario = 'rcp85'
				and season <= 2014
				and snowtype = '", snowtype,"' and envid = ", envid, "
			)

			select '1986-2015' tperiod, scenario, 'Novembre' mth,
				percentile_cont(0.2) within group (order by nov) q20_nov,
				percentile_cont(0.5) within group (order by nov) q50_nov,
				percentile_cont(0.8) within group (order by nov) q80_nov
			from hist
			group by scenario

			union

			select '1986-2015' tperiod, scenario, 'Décembre' mth,
				percentile_cont(0.2) within group (order by dec) q20_dec,
				percentile_cont(0.5) within group (order by dec) q50_dec,
				percentile_cont(0.8) within group (order by dec) q80_dec
			from hist
			group by scenario

			union

			select '1986-2015' tperiod, scenario, 'Janvier' mth,
				percentile_cont(0.2) within group (order by jan) q20_jan,
				percentile_cont(0.5) within group (order by jan) q50_jan,
				percentile_cont(0.8) within group (order by jan) q80_jan
			from hist
			group by scenario

			union

			select '1986-2015' tperiod, scenario, 'Février' mth,
				percentile_cont(0.2) within group (order by feb) q20_feb,
				percentile_cont(0.5) within group (order by feb) q50_feb,
				percentile_cont(0.8) within group (order by feb) q80_feb
			from hist
			group by scenario

			union

			select '1986-2015' tperiod, scenario, 'Mars' mth,
				percentile_cont(0.2) within group (order by mar) q20_mar,
				percentile_cont(0.5) within group (order by mar) q50_mar,
				percentile_cont(0.8) within group (order by mar) q80_mar
			from hist
			group by scenario

			union

			select '1986-2015' tperiod, scenario, 'Avril' mth,
				percentile_cont(0.2) within group (order by apr) q20_apr,
				percentile_cont(0.5) within group (order by apr) q50_apr,
				percentile_cont(0.8) within group (order by apr) q80_apr
			from hist
			group by scenario

			union

			select '1986-2015' tperiod, scenario, 'Novembre - Avril' mth,
				percentile_cont(0.2) within group (order by ndjfma) q20_ndjfma,
				percentile_cont(0.5) within group (order by ndjfma) q50_ndjfma,
				percentile_cont(0.8) within group (order by ndjfma) q80_ndjfma
			from hist
			group by scenario

			--REANALYSIS
			union

			select '1986-2015' tperiod, scenario, 'Novembre' mth,
				percentile_cont(0.2) within group (order by nov) q20_nov,
				percentile_cont(0.5) within group (order by nov) q50_nov,
				percentile_cont(0.8) within group (order by nov) q80_nov
			from viability.idx_mth
			where snowtype = '", snowtype,"' and season >= 1985 and season <= 2014
			and model = 'safran'
			and envid = ", envid, "
			group by scenario

			union

			select '1986-2015' tperiod, scenario, 'Décembre' mth,
				percentile_cont(0.2) within group (order by dec) q20_dec,
				percentile_cont(0.5) within group (order by dec) q50_dec,
				percentile_cont(0.8) within group (order by dec) q80_dec
			from viability.idx_mth
			where snowtype = '", snowtype,"' and season >= 1985 and season <= 2014
			and model = 'safran'
			and envid = ", envid, "
			group by scenario

			union

			select '1986-2015' tperiod, scenario, 'Janvier' mth,
				percentile_cont(0.2) within group (order by jan) q20_jan,
				percentile_cont(0.5) within group (order by jan) q50_jan,
				percentile_cont(0.8) within group (order by jan) q80_jan
			from viability.idx_mth
			where snowtype = '", snowtype,"' and season >= 1985 and season <= 2014
			and model = 'safran'
			and envid = ", envid, "
			group by scenario

			union

			select '1986-2015' tperiod, scenario, 'Février' mth,
				percentile_cont(0.2) within group (order by feb) q20_feb,
				percentile_cont(0.5) within group (order by feb) q50_feb,
				percentile_cont(0.8) within group (order by feb) q80_feb
			from viability.idx_mth
			where snowtype = '", snowtype,"' and season >= 1985 and season <= 2014
			and model = 'safran'
			and envid = ", envid, "
			group by scenario

			union

			select '1986-2015' tperiod, scenario, 'Mars' mth,
				percentile_cont(0.2) within group (order by mar) q20_mar,
				percentile_cont(0.5) within group (order by mar) q50_mar,
				percentile_cont(0.8) within group (order by mar) q80_mar
			from viability.idx_mth
			where snowtype = '", snowtype,"' and season >= 1985 and season <= 2014
			and model = 'safran'
			and envid = ", envid, "
			group by scenario

			union

			select '1986-2015' tperiod, scenario, 'Avril' mth,
				percentile_cont(0.2) within group (order by apr) q20_apr,
				percentile_cont(0.5) within group (order by apr) q50_apr,
				percentile_cont(0.8) within group (order by apr) q80_apr
			from viability.idx_mth
			where snowtype = '", snowtype,"' and season >= 1985 and season <= 2014
			and model = 'safran'
			and envid = ", envid, "
			group by scenario

			union

			select '1986-2015' tperiod, scenario, 'Novembre - Avril' mth,
				percentile_cont(0.2) within group (order by ndjfma) q20_ndjfma,
				percentile_cont(0.5) within group (order by ndjfma) q50_ndjfma,
				percentile_cont(0.8) within group (order by ndjfma) q80_ndjfma
			from viability.idx_mth
			where snowtype = '", snowtype,"' and season >= 1985 and season <= 2014
			and model = 'safran'
			and envid = ", envid, "
			group by scenario
			--RCP 2035
			union

			select '2028-2042' tperiod, scenario, 'Novembre' mth,
				percentile_cont(0.2) within group (order by nov) q20_nov,
				percentile_cont(0.5) within group (order by nov) q50_nov,
				percentile_cont(0.8) within group (order by nov) q80_nov
			from viability.idx_mth
			where model != 'safran' and season >= 2027 and season <= 2041
			and snowtype = '", snowtype,"' and envid = ", envid, "
			group by scenario

			union

			select '2028-2042' tperiod, scenario, 'Décembre' mth,
				percentile_cont(0.2) within group (order by dec) q20_dec,
				percentile_cont(0.5) within group (order by dec) q50_dec,
				percentile_cont(0.8) within group (order by dec) q80_dec
			from viability.idx_mth 
			where model != 'safran' and season >= 2027 and season <= 2041 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2028-2042' tperiod, scenario, 'Janvier' mth,
				percentile_cont(0.2) within group (order by jan) q20_jan,
				percentile_cont(0.5) within group (order by jan) q50_jan,
				percentile_cont(0.8) within group (order by jan) q80_jan
			from viability.idx_mth 
			where model != 'safran' and season >= 2027 and season <= 2041 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2028-2042' tperiod, scenario, 'Février' mth,
				percentile_cont(0.2) within group (order by feb) q20_feb,
				percentile_cont(0.5) within group (order by feb) q50_feb,
				percentile_cont(0.8) within group (order by feb) q80_feb
			from viability.idx_mth
			where model != 'safran' and season >= 2027 and season <= 2041 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2028-2042' tperiod, scenario, 'Mars' mth,
				percentile_cont(0.2) within group (order by mar) q20_mar,
				percentile_cont(0.5) within group (order by mar) q50_mar,
				percentile_cont(0.8) within group (order by mar) q80_mar
			from viability.idx_mth 
			where model != 'safran' and season >= 2027 and season <= 2041 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2028-2042' tperiod, scenario, 'Avril' mth,
				percentile_cont(0.2) within group (order by apr) q20_apr,
				percentile_cont(0.5) within group (order by apr) q50_apr,
				percentile_cont(0.8) within group (order by apr) q80_apr
			from viability.idx_mth
			where model != 'safran' and season >= 2027 and season <= 2041 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2028-2042' tperiod, scenario, 'Novembre - Avril' mth,
				percentile_cont(0.2) within group (order by ndjfma) q20_ndjfma,
				percentile_cont(0.5) within group (order by ndjfma) q50_ndjfma,
				percentile_cont(0.8) within group (order by ndjfma) q80_ndjfma
			from viability.idx_mth
			where model != 'safran' and season >= 2027 and season <= 2041
			and snowtype = '", snowtype,"' and envid = ", envid, "
			group by scenario

			union

			--RCP 2050
			select '2043-2057' tperiod, scenario, 'Novembre' mth,
				percentile_cont(0.2) within group (order by nov) q20_nov,
				percentile_cont(0.5) within group (order by nov) q50_nov,
				percentile_cont(0.8) within group (order by nov) q80_nov
			from viability.idx_mth
			where model != 'safran' and season >= 2042 and season <= 2056
			and snowtype = '", snowtype,"' and envid = ", envid, "
			group by scenario

			union

			select '2043-2057' tperiod, scenario, 'Décembre' mth,
				percentile_cont(0.2) within group (order by dec) q20_dec,
				percentile_cont(0.5) within group (order by dec) q50_dec,
				percentile_cont(0.8) within group (order by dec) q80_dec
			from viability.idx_mth 
			where model != 'safran' and season >= 2042 and season <= 2056 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2043-2057' tperiod, scenario, 'Janvier' mth,
				percentile_cont(0.2) within group (order by jan) q20_jan,
				percentile_cont(0.5) within group (order by jan) q50_jan,
				percentile_cont(0.8) within group (order by jan) q80_jan
			from viability.idx_mth 
			where model != 'safran' and season >= 2042 and season <= 2056 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2043-2057' tperiod, scenario, 'Février' mth,
				percentile_cont(0.2) within group (order by feb) q20_feb,
				percentile_cont(0.5) within group (order by feb) q50_feb,
				percentile_cont(0.8) within group (order by feb) q80_feb
			from viability.idx_mth
			where model != 'safran' and season >= 2042 and season <= 2056 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2043-2057' tperiod, scenario, 'Mars' mth,
				percentile_cont(0.2) within group (order by mar) q20_mar,
				percentile_cont(0.5) within group (order by mar) q50_mar,
				percentile_cont(0.8) within group (order by mar) q80_mar
			from viability.idx_mth 
			where model != 'safran' and season >= 2042 and season <= 2056 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2043-2057' tperiod, scenario, 'Avril' mth,
				percentile_cont(0.2) within group (order by apr) q20_apr,
				percentile_cont(0.5) within group (order by apr) q50_apr,
				percentile_cont(0.8) within group (order by apr) q80_apr
			from viability.idx_mth
			where model != 'safran' and season >= 2042 and season <= 2056 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2043-2057' tperiod, scenario, 'Novembre - Avril' mth,
				percentile_cont(0.2) within group (order by ndjfma) q20_ndjfma,
				percentile_cont(0.5) within group (order by ndjfma) q50_ndjfma,
				percentile_cont(0.8) within group (order by ndjfma) q80_ndjfma
			from viability.idx_mth
			where model != 'safran' and season >= 2042 and season <= 2056
			and snowtype = '", snowtype,"' and envid = ", envid, "
			group by scenario

			--RCP 2090
			union

			select '2083-2097' tperiod, scenario, 'Novembre' mth,
				percentile_cont(0.2) within group (order by nov) q20_nov,
				percentile_cont(0.5) within group (order by nov) q50_nov,
				percentile_cont(0.8) within group (order by nov) q80_nov
			from viability.idx_mth
			where model != 'safran' and season >= 2082 and season <= 2096
			and snowtype = '", snowtype,"' and envid = ", envid, "
			group by scenario

			union

			select '2083-2097' tperiod, scenario, 'Décembre' mth,
				percentile_cont(0.2) within group (order by dec) q20_dec,
				percentile_cont(0.5) within group (order by dec) q50_dec,
				percentile_cont(0.8) within group (order by dec) q80_dec
			from viability.idx_mth 
			where model != 'safran' and season >= 2082 and season <= 2096 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2083-2097' tperiod, scenario, 'Janvier' mth,
				percentile_cont(0.2) within group (order by jan) q20_jan,
				percentile_cont(0.5) within group (order by jan) q50_jan,
				percentile_cont(0.8) within group (order by jan) q80_jan
			from viability.idx_mth 
			where model != 'safran' and season >= 2082 and season <= 2096 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2083-2097' tperiod, scenario, 'Février' mth,
				percentile_cont(0.2) within group (order by feb) q20_feb,
				percentile_cont(0.5) within group (order by feb) q50_feb,
				percentile_cont(0.8) within group (order by feb) q80_feb
			from viability.idx_mth
			where model != 'safran' and season >= 2082 and season <= 2096 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2083-2097' tperiod, scenario, 'Mars' mth,
				percentile_cont(0.2) within group (order by mar) q20_mar,
				percentile_cont(0.5) within group (order by mar) q50_mar,
				percentile_cont(0.8) within group (order by mar) q80_mar
			from viability.idx_mth 
			where model != 'safran' and season >= 2082 and season <= 2096 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2083-2097' tperiod, scenario, 'Avril' mth,
				percentile_cont(0.2) within group (order by apr) q20_apr,
				percentile_cont(0.5) within group (order by apr) q50_apr,
				percentile_cont(0.8) within group (order by apr) q80_apr
			from viability.idx_mth
			where model != 'safran' and season >= 2082 and season <= 2096 
			and snowtype = '", snowtype,"' and envid = ", envid, " 
			group by scenario

			union

			select '2083-2097' tperiod, scenario, 'Novembre - Avril' mth,
				percentile_cont(0.2) within group (order by ndjfma) q20_ndjfma,
				percentile_cont(0.5) within group (order by ndjfma) q50_ndjfma,
				percentile_cont(0.8) within group (order by ndjfma) q80_ndjfma
			from viability.idx_mth
			where model != 'safran' and season >= 2082 and season <= 2096
			and snowtype = '", snowtype,"' and envid = ", envid, "
			group by scenario
		)
		select * from tot
		order by tperiod,
			case when scenario ilike 'rea%' then 'aaaa'
				when scenario ilike 'hist%' then 'bbbb'
				else scenario
			end,
			case when mth like 'Novembre %' then 1
				when mth like 'No%' then 2
				when mth like 'D%' then 3
				when mth like 'J%' then 4
				when mth like 'F%' then 5
				when mth like 'M%' then 6
				when mth like 'A%' then 7
			end
				"
	)
	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	data = dbGetQuery(con, query)
	dbDisconnect(con)
	
	mths = unique(data[,3])
	periods = unique(data[,1])
	perscen = unique(data[,1:2])
	# print(perscen)
	# print(mths)
	# readline(prompt="Press [enter] to continue")


	#layout and plot parameters  
	nbcol = nrow(perscen)
	xlim = c(0,nbcol)
	yleg = 3
	ymax = length(mths) + yleg
	y = ymax - yleg
	ylim = c(0,ymax)

	file = paste0(dst_dir, "matrix_idx_mth_", snowtype,".png")
	png(width = 1000, height = ymax * 80, file, res = 150, bg = "transparent")
	layout(matrix(c(2,2,2,1,1,1,1,1,1,3,3),1,11))

	par(mar=c(0,0,0,0))
	plot(0,0,type = 'n', xlim = xlim, ylim = ylim,
	   axes = F, xlab = NA, ylab = NA)
	
	mycol = rev(colorRampPalette(c('white','yellow','orange','red','darkred'))(200))

        ############### plot matrix in layout1
        sc = 0
        ymar = 0.1
        # Add ref column
		nc = 0
        for (p in periods){
			pcount = nrow(perscen[perscen[,1] == p,])
			xtxt = nc + pcount / 2
			ytxt = ymax - 1.5
			if (p == "1986-2015"){				
				text(xtxt, ytxt, labels = p, adj = c(.5, 0), cex= 1.2, font = 2)
			} else {
				season = strsplit(p, "-")[[1]]
				season = (as.numeric(season[1]) + as.numeric(season[2])) / 2
				text(xtxt, ytxt, labels = paste0(season), adj = c(.5, 0), cex= 1.4, font = 2)
				text(xtxt, ytxt - .1, labels = paste0("(", p,")"), adj = c(.5, 1), cex= .8, font = 2, col = "darkgrey")
			}
			nc = nc + pcount
			scens = unique(perscen[perscen[,1] == p, 2])
			for (s in scens){
				xtxt = .5 + sc
				ytxt = ymax - yleg + 0.05
				if (s == 'reanalysis'){
					txt = "Réanalyse\nSAFRAN"
				} else if (s == 'historical'){
					txt = "Référence\nhistorique"
				} else {
					txt = toupper(paste0(substr(s,1,4),".",substr(s,5,5)))
				}
				text(xtxt, ytxt, labels = txt, adj = c(0, .5), font = 2, srt = 90)
				
				for (i in 1:length(mths)){
					stval = data[data[,1] == p & data[,2]== s & data[,3] == mths[i], 4:6]
					if (s == 'reanalysis'){
						bgcol = "lightgrey"
					} else if (s == 'historical'){
						bgcol = "white"
					} else {
						# refval = data[data[,2]== 'historical' & data[,3] == mths[i], 5]
						# bgcol = (refval - stval[1,2]) * 100 / refval
						bgcol = stval[1,2]
						if (bgcol < 0) {
							bgcol = "sandgreen"
						} else if (floor(bgcol * 2) == 0){
							bgcol = mycol[1]
						} else {
							bgcol = mycol[floor(bgcol * 2)]
						}
					}
					if (sum(col2rgb(bgcol) * c(299, 587,114))/1000 < 125){
						tcol = "white"
					} else {
						tcol = "black"
					}
										
					polygon(c(sc, sc + 1, sc + 1, sc), c(y - i + 1, y - i + 1, y - i, y - i),
							col = bgcol, border = "black")
					xtxt = sc + .5
					ytxt = y - i + .5
					# text(xtxt, ytxt, labels = round(stval[1,1],2), adj = c(.5,.5))
					text(xtxt, y - i + ymar, labels = round(stval[1,1]), adj = c(.5,0), col = tcol, cex = .95)
					text(xtxt, ytxt, labels = round(stval[1,2]), adj = c(.5,.5), col = tcol, font = 2)
					text(xtxt, y - i + 1 - ymar, labels = round(stval[1,3]), adj = c(.5,1), col = tcol, cex = .95)
				}
				
				sc = sc + 1
			}        
        }

        
        #add title
		query = paste0("
			select b.name rname, a.name ename
			from crosscut.envelopes a
			join crosscut.resorts b on a.ind = b.ind
			where envid = ", envid)
		con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
		envname = dbGetQuery(con, query)
		dbDisconnect(con)
		
		st = strsplit(snowtype, "_")[[1]]
		st = st[length(st)]
		if (st == 'nn'){
            name = 'Neige naturelle'
        } else if (st == 'gro'){
            name = 'Neige naturelle damée'
        } else {
            if (st == 'lance') {
                nc = 'perches'
            } else {
                nc = "ventilateurs"
            }

        name = paste0("Neige de culture damée (", nc,")")
        }
		
        xtxt = xlim[2] / 2
        ytxt = ymax - .5
        name = paste0(envname[1,1], " (", envname[1,2], ")\n", name)
        text(xtxt, ytxt, labels = name, adj = c(.5, .5), font = 2, cex = 1.2)
        
        ############### Add row names in layout2
        par(mar=c(0,0,0,0))
        plot(0,0,type = 'n', xlim = xlim, ylim = ylim,
           axes = F, xlab = NA, ylab = NA)
        for (i in 1:length(mths)){
			m = mths[i]

			xtxt = .5
			ytxt = ymax - yleg + .5 - i
			text(xtxt, ytxt, labels = m, adj = c(0, .5), font = 2)
        }


        #Add colorbar scale in layout3
        xlim = c(0,3.5)
        ymax = length(mycol)/length(mths)*(length(mths) + yleg)
        ylim = c(0,ymax)
        par(mar=c(0,0,0,0), xpd = T)
        plot(0,0,type = 'n', xlim = xlim, ylim = ylim,
           axes = F, xlab = NA, ylab = NA)
        # mycol = rev(mycol)
        for (i in 1:length(mycol)){
        polygon(c(1.3, 2, 2, 1.3), c(i - 1, i-1, i, i),
                col = mycol[i], border = NA)
        }
        y = ymax - length(mycol)/length(mths)*3
        text(0, (y)/2,
           # labels ="Variation par rapport à la référence historique",
           labels ="Valeur de l'indice de fiabilité de l'enneigement (%)",
           adj = c(.5,1), srt = 90, font = 2, cex = 1)
        x = 2 + .1
        text(c(x,x), c(y,0), labels = c("100%", "0%"), adj = c(0,.5), cex = 1.2, font = 2)

        dev.off()
    }

plot_idx_matrix_tracc = function(dst_dir, envid, snowtype){
	print(snowtype)
	query = paste0("
		with hist as (
			select 0.6 tracc, a.model, nov, dec, jan, feb, mar, apr, ndjfma
			from viability.idx_mth a
			join crocus.tracc_model b on a.model = b.model
			where scenario = 'historical'
			and season >= 1975 and season <= 2004
			and snowtype = '", snowtype, "' and envid = ", envid, "
		), proj as (
			select tracc, a.model, nov, dec, jan, feb, mar, apr, ndjfma
			from viability.idx_mth a
			join crocus.tracc_model b on a.model = b.model
			join crocus.tracc c on b.gcm = c.gcm and b.rcm = c.rcm
			join crocus.tracc_gwl d on c.gwl = d.gwl
			where season + 1 >= an_deb and season + 1 <= an_fin and scenario = 'rcp85'
			and snowtype = '", snowtype, "' and envid = ", envid, "
		), bymod as (
			--HISTORICAL
			select tracc, model, 'Novembre' mth,
				percentile_cont(0.2) within group (order by nov) q20,
				percentile_cont(0.5) within group (order by nov) q50,
				percentile_cont(0.8) within group (order by nov) q80
			from hist
			group by tracc, model

			union

			select tracc, model, 'Décembre' mth,
				percentile_cont(0.2) within group (order by dec) q20,
				percentile_cont(0.5) within group (order by dec) q50,
				percentile_cont(0.8) within group (order by dec) q80
			from hist
			group by tracc, model

			union

			select tracc, model, 'Janvier' mth,
				percentile_cont(0.2) within group (order by jan) q20,
				percentile_cont(0.5) within group (order by jan) q50,
				percentile_cont(0.8) within group (order by jan) q80
			from hist
			group by tracc, model

			union

			select tracc, model, 'Février' mth,
				percentile_cont(0.2) within group (order by feb) q20,
				percentile_cont(0.5) within group (order by feb) q50,
				percentile_cont(0.8) within group (order by feb) q80
			from hist
			group by tracc, model

			union

			select tracc, model, 'Mars' mth,
				percentile_cont(0.2) within group (order by mar) q20,
				percentile_cont(0.5) within group (order by mar) q50,
				percentile_cont(0.8) within group (order by mar) q80
			from hist
			group by tracc, model

			union

			select tracc, model, 'Avril' mth,
				percentile_cont(0.2) within group (order by apr) q20,
				percentile_cont(0.5) within group (order by apr) q50,
				percentile_cont(0.8) within group (order by apr) q80
			from hist
			group by tracc, model

			union

			select tracc, model, 'Novembre - Avril' mth,
				percentile_cont(0.2) within group (order by ndjfma) q20,
				percentile_cont(0.5) within group (order by ndjfma) q50,
				percentile_cont(0.8) within group (order by ndjfma) q80
			from hist
			group by tracc, model

			--REANALYSIS
			union

			select -999.0 tracc, model, 'Novembre' mth,
				percentile_cont(0.2) within group (order by nov) q20,
				percentile_cont(0.5) within group (order by nov) q50,
				percentile_cont(0.8) within group (order by nov) q80
			from viability.idx_mth
			where snowtype = '", snowtype, "' and envid = ", envid, " and season >= 1975 and season <= 2004
			and model = 'safran'
			group by 1,2

			union

			select -999.0 tracc, model, 'Décembre' mth,
				percentile_cont(0.2) within group (order by dec) q20,
				percentile_cont(0.5) within group (order by dec) q50,
				percentile_cont(0.8) within group (order by dec) q80
			from viability.idx_mth
			where snowtype = '", snowtype, "' and envid = ", envid, " and season >= 1985 and season <= 2014
			and model = 'safran'
			group by 1,2

			union

			select -999.0 tracc, model, 'Janvier' mth,
				percentile_cont(0.2) within group (order by jan) q20,
				percentile_cont(0.5) within group (order by jan) q50,
				percentile_cont(0.8) within group (order by jan) q80
			from viability.idx_mth
			where snowtype = '", snowtype, "' and envid = ", envid, " and season >= 1985 and season <= 2014
			and model = 'safran'
			group by 1,2

			union

			select -999.0 tracc, model, 'Février' mth,
				percentile_cont(0.2) within group (order by feb) q20,
				percentile_cont(0.5) within group (order by feb) q50,
				percentile_cont(0.8) within group (order by feb) q80
			from viability.idx_mth
			where snowtype = '", snowtype, "' and envid = ", envid, " and season >= 1985 and season <= 2014
			and model = 'safran'
			group by 1,2

			union

			select -999.0 tracc, model, 'Mars' mth,
				percentile_cont(0.2) within group (order by mar) q20,
				percentile_cont(0.5) within group (order by mar) q50,
				percentile_cont(0.8) within group (order by mar) q80
			from viability.idx_mth
			where snowtype = '", snowtype, "' and envid = ", envid, " and season >= 1985 and season <= 2014
			and model = 'safran'
			group by 1,2

			union

			select -999.0 tracc, model, 'Avril' mth,
				percentile_cont(0.2) within group (order by apr) q20,
				percentile_cont(0.5) within group (order by apr) q50,
				percentile_cont(0.8) within group (order by apr) q80
			from viability.idx_mth
			where snowtype = '", snowtype, "' and envid = ", envid, " and season >= 1985 and season <= 2014
			and model = 'safran'
			group by 1,2

			union

			select -999.0 tracc, model, 'Novembre - Avril' mth,
				percentile_cont(0.2) within group (order by ndjfma) q20,
				percentile_cont(0.5) within group (order by ndjfma) q50,
				percentile_cont(0.8) within group (order by ndjfma) q80
			from viability.idx_mth
			where snowtype = '", snowtype, "' and envid = ", envid, " and season >= 1985 and season <= 2014
			and model = 'safran'
			group by 1,2
			
			--TRACC
			union

			select tracc, model, 'Novembre' mth,
				percentile_cont(0.2) within group (order by nov) q20,
				percentile_cont(0.5) within group (order by nov) q50,
				percentile_cont(0.8) within group (order by nov) q80
			from proj
			group by tracc, model

			union

			select tracc, model, 'Décembre' mth,
				percentile_cont(0.2) within group (order by dec) q20,
				percentile_cont(0.5) within group (order by dec) q50,
				percentile_cont(0.8) within group (order by dec) q80
			from proj
			group by tracc, model

			union

			select tracc, model, 'Janvier' mth,
				percentile_cont(0.2) within group (order by jan) q20,
				percentile_cont(0.5) within group (order by jan) q50,
				percentile_cont(0.8) within group (order by jan) q80
			from proj
			group by tracc, model

			union

			select tracc, model, 'Février' mth,
				percentile_cont(0.2) within group (order by feb) q20,
				percentile_cont(0.5) within group (order by feb) q50,
				percentile_cont(0.8) within group (order by feb) q80
			from proj
			group by tracc, model

			union

			select tracc, model, 'Mars' mth,
				percentile_cont(0.2) within group (order by mar) q20,
				percentile_cont(0.5) within group (order by mar) q50,
				percentile_cont(0.8) within group (order by mar) q80
			from proj
			group by tracc, model

			union

			select tracc, model, 'Avril' mth,
				percentile_cont(0.2) within group (order by apr) q20,
				percentile_cont(0.5) within group (order by apr) q50,
				percentile_cont(0.8) within group (order by apr) q80
			from proj
			group by tracc, model

			union

			select tracc, model, 'Novembre - Avril' mth,
				percentile_cont(0.2) within group (order by ndjfma) q20,
				percentile_cont(0.5) within group (order by ndjfma) q50,
				percentile_cont(0.8) within group (order by ndjfma) q80
			from proj
			group by tracc, model
		)

		select tracc, mth,
			percentile_cont(0.5) within group (order by q20) q20,
			percentile_cont(0.5) within group (order by q50) q50,
			percentile_cont(0.5) within group (order by q80) q80
		from bymod
		group by tracc, mth
		order by tracc,
			case when mth like 'Novembre %' then 1
				when mth like 'No%' then 2
				when mth like 'D%' then 3
				when mth like 'J%' then 4
				when mth like 'F%' then 5
				when mth like 'M%' then 6
				when mth like 'A%' then 7
			end
		"
	)
	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	data = dbGetQuery(con, query)
	dbDisconnect(con)
	
	traccs = unique(data[,1])
	mths = unique(data[,2])
	# print(perscen)
	# print(mths)
	# readline(prompt="Press [enter] to continue")


	#layout and plot parameters  
	nbcol = length(traccs)
	xlim = c(0,nbcol)
	yleg = 3
	ymax = length(mths) + yleg
	y = ymax - yleg
	ylim = c(0,ymax)

	file = paste0(dst_dir, "matrix_idx_mth_tracc_", snowtype,".png")
	png(width = 1000, height = ymax * 80, file, res = 150, bg = "transparent")
	layout(matrix(c(2,2,2,1,1,1,1,1,1,3,3),1,11))

	par(mar=c(0,0,0,0))
	plot(0,0,type = 'n', xlim = xlim, ylim = ylim,
	   axes = F, xlab = NA, ylab = NA)
	
	mycol = colorRampPalette(rev(c('white','yellow','orange','red','darkred')))(200)

	############### plot matrix in layout1
	sc = 0
	ymar = 0.1
	for (t in traccs){
		xtxt = sc + .5
		ytxt = y + 0.05
		if (t == -999.0){
			lab = "Réanalyse\nSAFRAN\n1976-2005"
		} else if (t == 0.6){
			lab = "Référence\nhistorique\n1976-2005"
		} else {
			lab = paste0("+", t, " °C")
		}
		
		text(xtxt, ytxt, labels = lab, adj = c(0, .5), cex= 1.2, font = 2, srt = 90)
			
		for (i in 1:length(mths)){
			stval = data[data[,1] == t & data[,2] == mths[i], 3:5]
			if ( t == -999.0){
				bgcol = "lightgrey"
			} else if (t == 0.6){
				bgcol = "white"
			} else {
				if (floor(stval[1,2] * 2) == 0){
					bgcol = mycol[1]
				} else {
					bgcol = mycol[floor(stval[1,2] * 2)]
				}
			}
			if (sum(col2rgb(bgcol) * c(299, 587,114))/1000 < 125){
				tcol = "white"
			} else {
				tcol = "black"
			}
								
			polygon(c(sc, sc + 1, sc + 1, sc), c(y - i + 1, y - i + 1, y - i, y - i),
					col = bgcol, border = "black")
			xtxt = sc + .5
			ytxt = y - i + .5
			# text(xtxt, ytxt, labels = round(stval[1,1],2), adj = c(.5,.5))
			text(xtxt, y - i + ymar, labels = round(stval[1,1]), adj = c(.5,0), col = tcol, cex = .95)
			text(xtxt, ytxt, labels = round(stval[1,2]), adj = c(.5,.5), col = tcol, font = 2)
			text(xtxt, y - i + 1 - ymar, labels = round(stval[1,3]), adj = c(.5,1), col = tcol, cex = .95)
		}
		
		sc = sc + 1      
	}

		
	#add title
	query = paste0("
		select b.name rname, a.name ename
		from crosscut.envelopes a
		join crosscut.resorts b on a.ind = b.ind
		where envid = ", envid)
	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	envname = dbGetQuery(con, query)
	dbDisconnect(con)
	
	if (endsWith(snowtype, 'nn')){
		name = 'Neige naturelle'
	} else if (endsWith(snowtype, 'gro')){
		name = 'Neige naturelle damée'
	} else {
		if (endsWith(snowtype, 'lance')) {
		print('THIS IS NC')
			nc = paste0('Perches (bi-fluide/haute pression) - ', gsub("_lance", "", snowtype))
			print(nc)
		} else {
			nc = paste0('Ventilateurs (mono-fluide/basse pression) - ', gsub("_fan", "", snowtype))
		}
		name = paste0("Neige de culture damée\n", nc)
	}
	
	xtxt = xlim[2] / 2
	ytxt = ymax - .5
	name = paste0(envname[1,1], " (", envname[1,2], ")\n", name)
	text(xtxt, ytxt, labels = name, adj = c(.5, .5), font = 2, cex = 1.2)
	
	############### Add row names in layout2
	par(mar=c(0,0,0,0))
	plot(0,0,type = 'n', xlim = xlim, ylim = ylim,
	   axes = F, xlab = NA, ylab = NA)
	for (i in 1:length(mths)){
		m = mths[i]

		xtxt = .5
		ytxt = ymax - yleg + .5 - i
		text(xtxt, ytxt, labels = m, adj = c(0, .5), font = 2)
	}


	#Add colorbar scale in layout3
	xlim = c(0,3.5)
	ymax = length(mycol)/length(mths)*(length(mths) + yleg)
	ylim = c(0,ymax)
	par(mar=c(0,0,0,0), xpd = T)
	plot(0,0,type = 'n', xlim = xlim, ylim = ylim,
	   axes = F, xlab = NA, ylab = NA)
	# mycol = rev(mycol)
	for (i in 1:length(mycol)){
	polygon(c(1.3, 2, 2, 1.3), c(i - 1, i-1, i, i),
			col = mycol[i], border = NA)
	}
	y = ymax - length(mycol)/length(mths)*3
	text(0, (y)/2,
	   # labels ="Variation par rapport à la référence historique",
	   labels ="Valeur de l'indice de fiabilité de l'enneigement (%)",
	   adj = c(.5,1), srt = 90, font = 2, cex = 1)
	x = 2 + .1
	text(c(x,x), c(y,0), labels = c("100%", "0%"), adj = c(0,.5), cex = 1.2, font = 2)

	dev.off()

}


################################FUNCTIONS END###################################

#GLOBAL PARAMETER
drv = dbDriver("PostgreSQL")
host = "10.38.192.30"
user = "postgres"
pwd = "lceslt"
db = "db"

#Retrieve inds
period = 15
ndv = -9999
src_mnt = "/home/francois/data/source_rasters/france_mnt_2154.tif"
dst_data = "/home/francois/data/"

inds = c('7331A')

for (i in 1:length(inds)){
  ind = inds[i]
  print(ind)
  query = paste0("select name from crosscut.resorts where ind = '", ind, "';")
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  rname = dbGetQuery(con, query)
  dbDisconnect(con)
  
  rname = rname[1,1]
  dst_dir = paste0(dst_data, rname, "/")
  dir.create(dst_dir)
  dst_base = paste0(dst_dir, "results/")
  dir.create(dst_dir)
  
  query = paste0("select envid from crosscut.envelopes where ind = '", ind, "';")
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  envlist = dbGetQuery(con, query)
  dbDisconnect(con)
  
  for (j in 1:nrow(envlist)){
    envid = envlist[j,1]
    dst_dir = paste0(dst_base, envid, "/")
    dir.create(dst_dir)
    dst_dir = paste0(dst_dir, "mth_matrix/")
    dir.create(dst_dir)
	
	query = paste0("select distinct snowtype from viability.idx_mth where envid = ", envid)
	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	snowtypes = dbGetQuery(con, query)
	dbDisconnect(con)
	for (snowtype in snowtypes[,1]){
		print(paste0(envid, "-", snowtype))
		# plot_idx_matrix(dst_dir, envid, snowtype)
		plot_idx_matrix_tracc(dst_dir, envid, snowtype)
	} 
    }
}

