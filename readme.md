*The Crosscut matching software was developed as part of the work shared between INRAE Grenoble (LESSEM) and Météo France (Centre d'Etudes de la Neige) about snow reliability assessment in ski areas, taking into account grooming and snowmaking, under the constraints of climate change. The code is adapted to the environment in which it was developed, in particular the PostgreSQL/PostGis DBMS allowing to store concomitantly the ADAMONT-SAFRAN-Crocus Resort simulation outputs and the spatial information describing the ski areas.*

# Global parameters
- Database configuration information is stored in a four line text file called "conn_param.py" containing four lines:
    ````
    host = 'HOST'
    dbname = 'DB'
    user='USER'
    password='PWD'
    ````
- For each resort, it is required to define IND and NAME parameters set up that will be used all along the process.
- Set-up a virtualenv based on the desired python version (in the current version we used python 3.7). The following python scripts are supposed to be run from this virtual environment.

# Import process
**NOT YET FULLY DOCUMENTED (NB DAYS => NB DAYS MV SHOULD BOTH CARE ABOUT AVAILABLE SIMS AND ALREADY COMPUTED)**
- Import MF data is fully automated
````
python PATH_TO_SCRIPTS/import_crocus_data.py
````
- Compute the seasonnal number of snow reliable days
- Compute the moving average number of snow reliable days

# Import spatial data
- Path dependent: base directory is /home/francois/data/RAWDATA/
- A single file gather all sql queries: import_crosscut_data.sql
- Before importation: create the resort in the crosscut.resorts table:

        INSERT INTO crosscut.resorts
        VALUES(IND, NAME)

- Import mandatory data with ogr2ogr and SQL post treatment
    - Ski area envelopes sectors
        ````
        ogr2ogr -f PostgreSQL PG:"dbname='DB' host='HOST' port='5432' user='USER' password='PWD'" PATH_TO_GEODATA_FILE -nln "crosscut.import_NAME_ski"
        ````
        ````
        INSERT INTO crosscut.resort_sectors
        SELECT 'IND', ogc_fid, nom, ST_MULTI(ST_TRANSFORM(wkb_geometry,2154))
        FROM crosscut.import_NAME_ski
        ````

        ***Special case*** when one wants to add envelope directly
        ````
        INSERT INTO crosscut.envelopes(ind, name, geom)
        SELECT 'IND', nom, ST_MULTI(ST_TRANSFORM(wkb_geometry,2154))
        FROM crosscut.import_NAME_ski
        ````
            
        
    - Ski-lifts
        ````
        ogr2ogr -f PostgreSQL PG:"dbname='DB' host='HOST' port='5432' user='USER' password='PWD'" PATH_TO_GEODATA_FILE -nln "crosscut.import_NAME_rm"
        ````
        ````
        INSERT INTO crosscut.resort_lifts
        --warning: SLP value should be checked carefuly to know wether we have to apply the above computation or not
        SELECT 'IND', ogc_fid, nom, ROUND(moment_de_/1000), ST_TRANSFORM(wkb_geometry,2154)
        from crosscut.import_NAME_rm
        ````
        
    - Snowmaking area
        ````
        ogr2ogr -f PostgreSQL PG:"dbname='DB' host='HOST' port='5432' user='USER' password='PWD'" PATH_TO_GEODATA_FILE -nln "crosscut.import_NAME_nc"
        ````
        ````
        INSERT INTO crosscut.sm_resort
        --"nom" have to be checked to know wether it has to be updated and the single geometries merged to multi or not
        SELECT 'IND', nom, ST_MULTI(ST_TRANSFORM(ST_UNION(wkb_geometry),2154))
        FROM crosscut.import_NAME_nc
        GROUP BY 1,2
        ````
        - One more query for snowmaking (data consistency):
        ````
        UPDATE crosscut.sm_resort foo
        SET geom = ST_MULTI(bar.geom)
            FROM
            (SELECT ind, status, ST_Collectionextract(ST_Intersection(a.geom, b.geom),3) geom
            FROM crosscut.sm_resort a,
                (SELECT ST_Union(geom) geom FROM crosscut.resort_sectors
                WHERE ind = 'IND') b
            WHERE ind = 'IND' AND ST_Intersects(a.geom,b.geom)
            ) bar
        WHERE foo.ind = bar.ind AND foo.status = bar.status
        ````
        
    - Ski tracks
        ````
        ogr2ogr -f PostgreSQL PG:"dbname='DB' host='HOST' port='5432' user='USER' password='PWD'" PATH_TO_GEODATA_FILE -nln "crosscut.import_NAME_pistes" -nlt "multipolygon"
        ````
        ````
        INSERT INTO crosscut.tracks(ind,name, col,geom)
        select 'IND', nom, couleur, ST_FORCE2D(ST_MULTI(ST_TRANSFORM(ST_COLLECTIONEXTRACT(ST_MAKEVALID(wkb_geometry),3),2154)))
        from crosscut.import_NAME_pistes;

        UPDATE crosscut.tracks
        SET col = CASE
            WHEN col ilike '%bleu%' or col ilike 'blue' THEN 'blue'
            WHEN col ilike '%vert%' or col ilike 'green' THEN 'green'
            WHEN col ilike '%rouge%' or col ilike 'red' THEN 'red'
            WHEN col ilike '%noir%' or col ilike 'black' THEN 'black'
            WHEN col ilike '%violet%' or col ilike 'purple' THEN 'purple'
            WHEN col ilike '%orange%' or col ilike 'orange' THEN 'orange'
            --WHEN col IS NULL THEN NULL
            ELSE 'pink' END
        WHERE ind = 'IND';
        ````

    - *Optionnal:* Water consumption values    
        ````
        ogr2ogr -f PostgreSQL PG:"dbname='db' host='localhost' port='5432' user='postgres' password='lceslt'" PATH_TO_CSV_FILE -nln "crosscut.import_NAME_wc"
        ````
        **/!\ Get envid value from crosscut.envelopes**
        ````
        INSERT INTO crosscut.actual_wc
        SELECT ENVID, season::integer, actual_wc::float8
        FROM crosscut.import_NAME_wc
        ````
    
    - *Optionnal:* Mask SM for mixed snowtypes PNG
        ````
        INSERT INTO crosscut.sm_track(ind, status, geom)
        SELECT 'IND', lower(exist_proj), ST_MULTI(ST_TRANSFORM(ST_UNION(ST_COLLECTIONEXTRACT(ST_MAKEVALID(wkb_geometry),3)),2154))
        FROM crosscut.import_NAME_nc_pistes
        GROUP BY 1,2
        ````

- Optionnal: required to plot PACA type map
    - Water reservoirs
        ````
        ogr2ogr -f PostgreSQL PG:"dbname='DB' host='HOST' port='5432' user='USER' password='PWD'" PATH_TO_GEODATA_FILE -nln "crosscut.import_NAME_retenues"
        ````
        ````
        INSERT INTO crosscut.reservoirs(ind, geom)
        SELECT 'IND', ST_MULTI(ST_TRANSFORM(wkb_geometry,2154))
        from crosscut.import_NAME_retenues
        ````

    - Points for snowmaking coverage
        ````
        ogr2ogr -f PostgreSQL PG:"dbname='DB' host='HOST' port='5432' user='USER' password='PWD'" PATH_TO_GEODATA_FILE -nln "crosscut.import_NAME_points_neige"
        ````
        ````
        INSERT INTO crosscut.sm_points(ind, status, geom)
        -- Check names: must matche the ones in crosscut.sm_resorts
        SELECT 'IND', gm_type, ST_TRANSFORM(wkb_geometry,2154)
        from crosscut.import_NAME_points_neige
        ````


# Computations
## Compute envelopes based on sectors and the associated information
- Edit the "crosscut.py" script with the correct IND at the end of the file
- Run the script
````
python PATH_TO_SCRIPTS/crosscut.py
````
- Check the processed data:
````
SELECT envid, snowtype, SUM(mp_part), SUM(surf_part)
FROM crosscut.loc_weight_env
WHERE envid IN (SELECT DISTINCT envid FROM crosscut.envelopes where ind = 'IND')
GROUP BY 1,2
ORDER BY 1,2
````
## Compute reliability index
- Edit the "compute_idx.py" script with the correct IND at the end of the file
- Run the script
````
python PATH_TO_SCRIPTS/compute_idx.py
````
## Compute water consumption
- Edit the "compute_wc.py" script with the correct IND at the end of the file
- Run the script
````
python PATH_TO_SCRIPTS/compute_wc.py
````
## Create seasonnal rasters
**REQUIRES the moving average table to be created and filled with the consistent data**
- Edit the "create_days_raster_proj.py" script with the correct IND at the end of the file
- Run the script
````
python PATH_TO_SCRIPTS/create_days_raster_proj.py
````
- Optional: Safran seasonal raster (after editing the following script to set the correct IND)
````
python PATH_TO_SCRIPTS/create_days_raster_safran.py
````
- Optional: Create mixed rasters type **obsolete: mixed png are now generated using imagemagick through the according R script**
````
python PATH_TO_SCRIPTS/create_mixed_snowtypes_days_raster_proj.py
````

# And now... Let's plot the data!
**Plot uses R software with the libraries called in the following scripts (RPostGreSQL, plotrix, raster, rgdal, rcurl, reshape)**

## Plot envelopes properties and rasters (mainly maps)
- Edit the "envid_properties_draw_raster.r" script with the correct IND at the end of the file
    - Optional: uncomment last lines to draw Safran PNGs
- Run the script in R
````
source("PATH_TO_SCRIPTS/envid_properties_draw_raster.r") 
````
## Plot reliability index and water consumption
- Edit the "plot_graph_viability.r" script with the correct IND at the end of the file
- Run the script in R
````
source("PATH_TO_SCRIPTS/plot_graph_viability.r") 
````
## Plot WBT timeslots
- Edit the "plot_graph_wbt.r" script with the correct IND at the end of the file
- Run the script in R
````
source("PATH_TO_SCRIPTS/plot_graph_wbt.r")
````

# Snow reliability lines
## Compute lift default stats
- Edit the "calculate_env_stats.py" script with the correct IND at the end of the file
- Run the script
````
python PATH_TO_SCRIPTS/calculate_env_stats.py
````
## Plot SRL matrixes
- Edit the "plot_matrix_srl.r" script with the correct IND at the end of the file
    - Optional: Edit also CUSTOM_ALTS
- Run the script in R
````
source("PATH_TO_SCRIPTS/plot_matrix_srl.r")
````
