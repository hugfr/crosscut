import psycopg2
import numpy as np
import conn_param
import multiprocessing


def compute_wc_launcher(ind):
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
     # count number of snowmaking areas
    query = """
        select distinct status from crosscut.sm_resort
        where ind = %s"""
    cur.execute(query,(ind,))
    nbsm = cur.rowcount
    
    if nbsm > 0:
        # Retrieve zone type for the ski resort
        query = """select distinct c.zonetype from crosscut.loc_weight_env a
        join crosscut.envelopes b on a.envid = b.envid
        join crocus.loc_zone c on a.loc = c.loc
        where ind = %s"""
        cur.execute(query,(ind,))

        zones = []
        for zt in cur:
            zones.append(zt[0])
        zones = tuple(zones)        
        
        if "sym" in zones: # add an exception in order to limit season <= 2014 in case of sym zone mixed with other zones
            query = """
                select distinct model, scenario, snowtype, season
                from crocus.available_simulations
                where typezone in %s  and snowtype not in ('nn' , 'gro') and model != 'safran'
                
                union
                
                select distinct model, scenario, snowtype, season
                from crocus.available_simulations
                where typezone in %s and snowtype not in ('nn' , 'gro') and model = 'safran'
                and season::integer >= 1961 and season::integer <= 2014
                """
            cur.execute(query,(zones,zones,))
        else:
            # Retrieve available simulations for these zones
            query = """select distinct model, scenario, snowtype, season from crocus.available_simulations
            where typezone in %s and snowtype not in ('nn' , 'gro')"""
            cur.execute(query,(zones,))
        
        args = []
        # Create the required temp table looping on available sims
        for sim in cur:
            model = sim[0]
            scenario = sim[1]
            snowtype = sim[2]
            season = sim[3]
            args.append((ind, zones, model, scenario, snowtype, season))
        print("args done")
        pool = multiprocessing.Pool(processes = 10)
        pool.map(compute_wc, args)
        pool.close()
    else:
        print("no snowmaking = no water consumption")
        
def compute_wc(args):
    ind, zones, model, scenario, snowtype, season = args
    pxsize = 25
    print(f'launching wc computing for {ind} - {season}, {model}, {scenario}, {snowtype}')
    
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur2 = myconn.cursor()
    
    # Create temp table with gro and sm snowtype
    i = 0
    for typezone in zones:
        # typezone = typezone[0]
        srctabgro = f'snow_{typezone}_{model}_{scenario}_gro_{season}'
        srctabsnow = f'snow_{typezone}_{model}_{scenario}_{snowtype}_{season}'
        subq = f"""/*lines commented because there's no need of gro to compute water consumption for snowmaking
        select * from crocus.{srctabgro}
        where loc in (
            select distinct loc from crosscut.loc_weight_env a
            join crosscut.envelopes b on a.envid = b.envid
            where ind = '{ind}'
            )
         
        union
        */
        select * from crocus.{srctabsnow}
        where loc in (
            select distinct loc from crosscut.loc_weight_env a
            join crosscut.envelopes b on a.envid = b.envid
            where ind = '{ind}'
            )"""
        if i == 0:
            query = subq
        else:
            query = f"""
                {query}
                
                union
                
                {subq}
            """
        i = i + 1
    
    tmpsnow = f'tmp_snow_{model}_{scenario}_{snowtype}_{season}'
    query = f"""
    drop table if exists crocus.{tmpsnow};
    create table crocus.{tmpsnow} as
    {query};"""
    cur2.execute(query)
    myconn.commit()
    print(f'{tmpsnow} done')
    
    table = f"wc_mth_{model}_{scenario}"
    query = f"""
    create table if not exists viability.{table}(
        CONSTRAINT {table}_check
            CHECK (model = '{model}' and scenario = '{scenario}')
    )
    inherits(viability.wc_mth);
    create index if not exists
        wc_{table}_envid on viability.{table}(envid);
    create index if not exists
        wc_{table}_season on viability.{table}(season);
    create index if not exists
        wc_{table}_snowtype on viability.{table}(snowtype);
    """
    cur2.execute(query)
    myconn.commit()
    
    query = """select distinct substr(a.snowtype,1, length(a.snowtype) - position('_' in reverse(a.snowtype)))||'_'||%s snowtype
    from crosscut.loc_weight_env a
    join crosscut.envelopes c on a.envid = c.envid
    where ind = %s
    and substr(a.snowtype,length(a.snowtype) - position('_' in reverse(a.snowtype)) + 2, length(a.snowtype)) = 'sm'"""
    cur2.execute(query,(snowtype,ind,))
    
    st = []
    for s in cur2:
        st.append(s[0])
    st = tuple(st)
    
    query = f"""
    delete from viability.{table} where envid in (
        select envid from crosscut.envelopes where ind = %s
    )
    and season = %s and snowtype in %s;
    
    insert into viability.{table}
    with loc_mth as (
        select a.envid, a.loc, nb_pix, season, substr(a.snowtype,1, length(a.snowtype) - position('_' in reverse(a.snowtype)))||'_'||%s snowtype,
        model, scenario, 
        max(case when extract(month from ddate) = 11 then wc else 0 end) nov,
        max(case when extract(month from ddate) = 12 then wc else 0 end) - max(case when extract(month from ddate) = 11 then wc else 0 end) as dec,
        max(case when extract(month from ddate) = 1 then wc else 0 end) - max(case when extract(month from ddate) = 12 then wc else 0 end) as jan,
        max(case when extract(month from ddate) = 2 then wc else 0 end) - max(case when extract(month from ddate) = 1 then wc else 0 end) as feb,
        max(case when extract(month from ddate) = 3 then wc else 0 end) - max(case when extract(month from ddate) = 2 then wc else 0 end) as mar,
        max(case when extract(month from ddate) = 2 then wc else 0 end) - max(case when extract(month from ddate) = 11 then wc else 0 end) as djf,
        max(case when extract(month from ddate) = 3 then wc else 0 end) ndjfma
        from crosscut.loc_weight_env a
        join crocus.{tmpsnow} b on a.loc = b.loc
        join crosscut.envelopes c on a.envid = c.envid
        where ind = %s
        and substr(a.snowtype,length(a.snowtype) - position('_' in reverse(a.snowtype)) + 2, length(a.snowtype)) = 'sm'
        and b.snowtype = %s
        and ddate between ((season)||'-11-01')::date and ((season + 1)||'-03-30')::date
        group by a.envid, a.loc, nb_pix, season, 5, model, scenario
    )
    select envid, season, snowtype, model, scenario,
        sum(nb_pix * %s * %s * nov / 1000) nov,
        sum(nb_pix * %s * %s * dec / 1000) as dec,
        sum(nb_pix * %s * %s * jan / 1000) jan,
        sum(nb_pix * %s * %s * feb / 1000) feb,
        sum(nb_pix * %s * %s * mar / 1000) mar,
        sum(nb_pix * %s * %s * djf / 1000) djf,
        sum(nb_pix * %s * %s * ndjfma / 1000) ndjfma
    from loc_mth
    group by envid, season, snowtype, model, scenario;
    """
    cur2.execute(query, (ind,season,st,snowtype,ind,snowtype,pxsize,pxsize,pxsize,pxsize,pxsize,pxsize,pxsize,pxsize,pxsize,pxsize,pxsize,pxsize,pxsize,pxsize,))
    myconn.commit()
    print('wc computed')
        
    cur2.execute(f"drop table if exists crocus.{tmpsnow};")
    myconn.commit()


inds = ['7344A','7344B','7344C','7345']

for ind in inds:
    compute_wc_launcher(ind)