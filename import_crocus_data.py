import os
import multiprocessing
import numpy as np
import pandas as pd
import xarray as xr
import datetime as dt
import psycopg2
import conn_param
from sqlalchemy import create_engine
from collections import Counter

def K2C(ds, var):
    # Convert K to C for at variable
    t = ds['time'].values
    tsize = ds.dims['time']
    l = ds['locs'].values
    lsize = ds.dims['locs']
  
    newval = ds[var].values - 273.15
    newval = newval.reshape((tsize, lsize))
  
    newval = xr.DataArray(
        newval,
        coords = {'time':t, 'locs':l},
        dims = ['time','locs']
        )
    ds[var] = newval
    print(f'{var} converted')
    
    return ds

def available_con(looptime = 2, conthd = 3):
    # PSQL connection
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    #Test query
    query = """
        select max_conn,used,res_for_super,max_conn-used-res_for_super res_for_normal 
        from 
          (select count(*) used from pg_stat_activity) t1,
          (select setting::int res_for_super from pg_settings where name=$$superuser_reserved_connections$$) t2,
          (select setting::int max_conn from pg_settings where name=$$max_connections$$) t3
    """
    cur.execute(query)
    conav = cur.fetchone()[3]
    
    #Test available connections
    while conav < conthd:
        print(f"waiting {looptime}s for, at least, {conthd} available con")
        time.sleep(looptime)
        cur.execute(query)
        conav = cur.fetchone()[3]

def pro2pg_launcher(src_dir, zone_id, model, scenario, snowtype):
    #Open datasets looping over directory
    flist = []
    for f in os.walk(src_dir):
        flist = [ff for ff in f[2] if 'PRO' in ff]
        
    flist.sort()
    args = []
    for f in flist:
        season = int(f[4:8])
        f = os.path.join(src_dir, f)
        args.append((f, zone_id, model, scenario, snowtype, season))
        
    print("args done")
    # args.append((os.path.join(src_dir, flist[0]), zone, model, scenario, snowtype))
    pool = multiprocessing.Pool(processes = 10)
    pool.map(pro2pg, args)
    pool.close()

# def pro2pg(src_dir, zone, model, scenario, snowtype):
def pro2pg(args):
    # PSQL connection
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    # ##### WITHOUT MULTIPROCESSING
    # #Open datasets looping over directory
    # flist = []
    # for f in os.walk(src_dir):
        # flist = [ff for ff in f[2] if 'PRO' in ff]
    # flist.sort()
    # args = []
    # for f in flist:
        # f = os.path.join(src_dir, f)

    f, zone_id, model, scenario, snowtype, season = args
    ds = xr.open_dataset(f)
    print(f)
        
    # Rename var
    vardict = {'TG1':'soil_temp5mm', 'TG4':'soil_temp8cm', 'MMP_VEG':'wc', 
    'DRAIN_ISBA':'drain', 'RUNOFF_ISBA':'runoff', 'SNOMLT_ISBA':'snowmelt', 'WSN_T_ISBA':'swe', 'DSN_T_ISBA':'sd',
    'WBT':'wbt',
    'Number_of_points':'locs'}
    ds = ds.rename(vardict)
    
    # define zone
    if zone_id == "sym":
        zone = "sympo"
    else:
        zone = "massif"
    
    # Get loc id
    nbpt = ds.locs.values
    locid = []
    ndv = -1
    for pt in nbpt:
        # print(pt)
        ds_tmp = ds.sel(locs=pt)
        alt = ds_tmp.ZS.values
        slope = ds_tmp.slope.values
        aspect = ds_tmp.aspect.values
        if zone == "massif":
            id_mf = ds_tmp.massif_num.values
            query = """
                select gid from crocus.loc_zonage_safran_france
                where type_zone = %s and id_mf = %s
                and alt = %s and slope = %s and aspect = %s
                """
            available_con()
            cur.execute(query,(zone, id_mf.tolist(), alt.tolist(), slope.tolist(), aspect.tolist(),))
        else:
            lon = ds_tmp.longitude.values
            lat = ds_tmp.latitude.values
            query = """
                select gid from crocus.loc_zonage_safran_france
                join crocus.zones_sympo on id_mf = id
                where type_zone = %s and lon/100 = %s and lat/100 = %s
                and alt = %s and slope = %s and aspect = %s
                """
            available_con()
            cur.execute(query,(zone, lon.tolist(), lat.tolist(), alt.tolist(), slope.tolist(), aspect.tolist(),))
        nbrow = cur.rowcount
        if nbrow == 0:
            locid.append(ndv)
            ndv = ndv - 1
        else:
            locid.append(cur.fetchone()[0])
    print("loc done")
    
    # ds post-treatment    
    ds = ds.assign_coords(locs = locid)
    locid = [l for l in locid if l > 0]
    ds = ds.sel(locs = locid)
    ds = ds.assign_coords(time = ds['time'].values) # CHECK TIME VALUES BETWEEN VAR AND DIM
    if snowtype == 'nn':
        if zone == 'massif':
            ds = ds.drop(['massif_num', 'ZS', 'slope', 'aspect', 'longitude', 'latitude', 'SD_1DY_ISBA', 'SD_3DY_ISBA', 'SWE_1DY_ISBA', 'SWE_3DY_ISBA'])
        else:
            ds = ds.drop(['station', 'ZS', 'slope', 'aspect', 'longitude', 'latitude', 'SD_1DY_ISBA', 'SD_3DY_ISBA', 'SWE_1DY_ISBA', 'SWE_3DY_ISBA'])
    else:
        if zone == 'massif':
            ds = ds.drop(['massif_num', 'ZS', 'slope', 'aspect', 'longitude', 'latitude'])
        else:
            ds = ds.drop(['station', 'ZS', 'slope', 'aspect', 'longitude', 'latitude'])
    ds = ds.squeeze('Number_of_Patches')
    # ds = K2C(ds, "soil_temp5mm")
    # ds = K2C(ds, "soil_temp8cm")
    print("post-treatment done")
    
    df = ds.to_dataframe()
    df.drop("Projection_Type", axis = 1, inplace = True)
    df.index.rename(["loc","ddate"], inplace = True)
    df["season"] = season
    df["typezone"] = [zone_id] * df.shape[0]
    df["model"] = [model] * df.shape[0]
    df["scenario"] = [scenario] * df.shape[0]
    df["snowtype"] = [snowtype] * df.shape[0]
    print(f"{season} dataframe done")
    table = f"snow_{zone_id}_{model}_{scenario}_{snowtype}_{season}"
    query = f"""
    drop table if exists crocus.{table};
    create table crocus.{table}(
        CONSTRAINT {table}_check
            CHECK (typezone = '{zone_id}' and model = '{model}' and scenario = '{scenario}' and snowtype = '{snowtype}' and season = {season})
    )
    inherits(crocus.snow);
    create index if not exists
        pk_{table}_ddate
        on crocus.{table}(ddate);
    create index if not exists
        pk_{table}_loc
        on crocus.{table}(loc);
    """
    available_con()
    cur.execute(query)
    myconn.commit()
    print(f"{table} created")
    
    con = create_engine(f'postgresql://{conn_param.user}:{conn_param.password}@{conn_param.host}:5432/{conn_param.dbname}')
    available_con()
    df.to_sql(table, con, schema = "crocus", if_exists = "append")
    print(f"{season} append done")
    
# #Create table for future insertion with trigger and daughters'tables [Kept for memory => database sql file]
# myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
# cur = myconn.cursor()

# cur. execute(
# create table if not exists crocus.snow(
    # season int4,
    # ddate date,
    # loc int4,
    # typezone varchar(5),
    # model varchar(50),
    # scenario varchar(50),
    # snowtype varchar(10),
    # soil_temp5mm float8,
    # soil_temp8cm float8,
    # wc float8,
    # drain float8,
    # runoff float8,
    # snowmelt float8,
    # swe float8,
    # sd float8,
    # wbt float8
    # )
# )
# myconn.commit()

# PSQL connection
myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()

cur.execute("select distinct typezone, model, scenario, snowtype from crocus.available_simulations --where typezone = 'sym'")
sim_done = [s for s in cur]

root_dir = "/mnt/syno2/climatedata.umr-cnrm.fr/public/dcsc/projects/RUN_CROCUS_2020"
dirlist = os.listdir(root_dir)
dirlist = [d for d in dirlist if d[0:3] != "cor" and d != "index.html"]
# dirlist = [d for d in dirlist if d[0:3] == "alp"]
for dir in dirlist:
    zone_id = dir[0:3]
    print(zone_id)
    dirlist = os.listdir(os.path.join(root_dir, dir))
    # dirlist = [d for d in dirlist if d[0:10] == "experiment"]
    dirlist = [d for d in dirlist if d[0:10] == "reanalysis" and d[11:len(d)].lower() != "spandre"]
    # dirlist = [d for d in dirlist if d[0:10] == "reanalysis" and (d[11:len(d)].lower() == "fan" or d[11:len(d)].lower() == "nn")]
    dirlist.sort()
    # print(dirlist)
    listest = []
    for src_dir in dirlist:
        idxlist = []
        s = 0
        test = True
        while test:
            try:
                idx = src_dir.index("_", s, len(src_dir))
                idxlist.append(idx)
                s = idx + 1
            except ValueError:
                test = False
                
        if src_dir[0:10] == "experiment":
            #not tested (previously this line was above if)
            scenario = src_dir[idxlist[len(idxlist) - 2] + 1:idxlist[len(idxlist) - 1]].lower()
            snowtype = src_dir[idxlist[len(idxlist) - 1] + 1:len(src_dir)].lower()
            model = src_dir[idxlist[0] + 1:idxlist[len(idxlist) - 2]].lower()
            
            if "aladin53" in model:
                cor = "a53"
            elif "aladin63" in model:
                cor = "a63"
            else:
                cor = "ok"
            model = model.split("_")
            model = [s[0] for s in model]
            model = "_".join(model)
            if cor != "ok":
                model = model.replace("a", cor)
            listest.append(model)
            
        if src_dir[0:10] == "reanalysis":
            model = "safran"
            scenario = "reanalysis"
            snowtype = src_dir[idxlist[len(idxlist) - 1] + 1:len(src_dir)].lower()
            
        src_dir = os.path.join(root_dir, dir, src_dir, 'pro')
        
        sim = (zone_id, model, scenario, snowtype)
        print(f"{model} ({scenario}): {snowtype}")
        models = ["c_c_8_1_c_c_c_c", "c_c_8_1_i_e_e"] #done for pyr
        
        # if zone_id == 'alp' and model == 'c_c_8_1_c_c_c_c' and scenario == 'historical' and snowtype == 'nn':
        pro2pg_launcher(src_dir, zone_id, model, scenario, snowtype)
        
        if (sim not in sim_done):
            print(sim)
            pro2pg_launcher(src_dir, zone_id, model, scenario, snowtype)
        else:
            print("ALREADY DONE")
        
        cur.execute("refresh materialized view crocus.available_simulations;")
        myconn.commit()