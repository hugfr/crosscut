import os
import multiprocessing
import numpy as np
import pandas as pd
import xarray as xr
import datetime as dt
import psycopg2
import conn_param
from sqlalchemy import create_engine
from collections import Counter

def K2C(ds, var):
    # Convert K to C for at variable
    t = ds['time'].values
    tsize = ds.dims['time']
    l = ds['locs'].values
    lsize = ds.dims['locs']
  
    newval = ds[var].values - 273.15
    newval = newval.reshape((tsize, lsize))
  
    newval = xr.DataArray(
        newval,
        coords = {'time':t, 'locs':l},
        dims = ['time','locs']
        )
    ds[var] = newval
    print(f'{var} converted')
    
    return ds

def available_con(looptime = 2, conthd = 3):
    # PSQL connection
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    #Test query
    query = """
        select max_conn,used,res_for_super,max_conn-used-res_for_super res_for_normal 
        from 
          (select count(*) used from pg_stat_activity) t1,
          (select setting::int res_for_super from pg_settings where name=$$superuser_reserved_connections$$) t2,
          (select setting::int max_conn from pg_settings where name=$$max_connections$$) t3
    """
    cur.execute(query)
    conav = cur.fetchone()[3]
    
    #Test available connections
    while conav < conthd:
        print(f"waiting {looptime}s for, at least, {conthd} available con")
        time.sleep(looptime)
        cur.execute(query)
        conav = cur.fetchone()[3]

def pro2pg_launcher(src_dir, zone, model, scenario):
    #Open datasets looping over directory
    flist = []
    for f in os.walk(src_dir):
        flist = [ff for ff in f[2] if ff[0:3] == 'wbt']
    flist.sort()
    flist = [f for f in flist if f[3:7] == '-1-4' or f[3:7] == '-4-6' or f[3:5] == '-6']
    args = []
    for f in flist:
        if f[3:5] == '-6':
            wbt = f[3:5]
            period = f[10:19]
        else:
            wbt = f[3:7]
            period = f[12:21]
        f = os.path.join(src_dir, f)
        args.append((f, zone_id, model, scenario,period, wbt))
    print("args done")
    
    pool = multiprocessing.Pool(processes = 10)
    pool.map(pro2pg, args)
    pool.close()

# def pro2pg(src_dir, zone, model, scenario, snowtype):
def pro2pg(args):
    # PSQL connection
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    # ##### WITHOUT MULTIPROCESSING
    # #Open datasets looping over directory
    # flist = []
    # for f in os.walk(src_dir):
        # flist = [ff for ff in f[2] if 'PRO' in ff]
    # flist.sort()
    # args = []
    # for f in flist:
        # f = os.path.join(src_dir, f)

    f, zone_id, model, scenario, period, wbt = args
    ds = xr.open_dataset(f)
    print(f"wbt: {wbt}, period: {period}, scenario: {scenario} and model: {model}")
    vars = list(ds.data_vars.keys())
    var = vars[len(vars) - 1]
    # Rename var
    vardict = {'LAT':'latitude', 'LON':'longitude', var:'nbh', 'Number_of_points':'locs'}
    ds = ds.rename(vardict)
    
    # define zone
    if zone_id == "sym":
        zone = "sympo"
    else:
        zone = "massif"
    
    # Get loc id
    nbpt = ds.locs.values
    locid = []
    ndv = -1
    for pt in nbpt:
        # print(pt)
        ds_tmp = ds.sel(locs=pt)
        alt = ds_tmp.ZS.values
        slope = ds_tmp.slope.values
        aspect = ds_tmp.aspect.values
        if zone == "massif":
            id_mf = ds_tmp.massif_number.values
            query = """
                select gid from crocus.loc_zonage_safran_france
                where type_zone = %s and id_mf = %s
                and alt = %s and slope = %s and aspect = %s
                """
            available_con()
            cur.execute(query,(zone, id_mf.tolist(), alt.tolist(), slope.tolist(), aspect.tolist(),))
        else:
            lon = ds_tmp.longitude.values
            lat = ds_tmp.latitude.values
            query = """
                select gid from crocus.loc_zonage_safran_france
                join crocus.zones_sympo on id_mf = id
                where type_zone = %s and lon/100 = %s and lat/100 = %s
                and alt = %s and slope = %s and aspect = %s
                """
            available_con()
            cur.execute(query,(zone, lon.tolist(), lat.tolist(), alt.tolist(), slope.tolist(), aspect.tolist(),))
        nbrow = cur.rowcount
        if nbrow == 0:
            locid.append(ndv)
            ndv = ndv - 1
        else:
            locid.append(cur.fetchone()[0])
    print("loc done")
    
    # ds post-treatment    
    ds = ds.assign_coords(locs = locid)
    locid = [l for l in locid if l > 0]
    ds = ds.sel(locs = locid)
    ds = ds.drop(['massif_number', 'ZS', 'slope', 'aspect', 'longitude', 'latitude'])
    seasons = pd.DatetimeIndex(ds.time.values).year
    ds = ds.assign_coords(time = seasons)    
    print("post-treatment done")
    
    df = ds.to_dataframe()
    df.index.rename(["loc","season"], inplace = True)
    df["typezone"] = [zone_id] * df.shape[0]
    df["model"] = [model] * df.shape[0]
    df["scenario"] = [scenario] * df.shape[0]
    df["wbt"] = [wbt] * df.shape[0]
    df["period"] = [period] * df.shape[0]
    print("dataframe done")
    
    #create parent table if not exists
    query = """
        create table if not exists crocus.wbt(
            loc integer,
            season integer,
            typezone varchar(50),
            model varchar(50),
            scenario varchar(50),
            wbt varchar(10),
            period varchar(10),
            nbh float8
        );
    """
    available_con()
    cur.execute(query)
    myconn.commit()
    
    #create child table
    table = f"wbt_{model}_{scenario}"
    query = f"""
    --drop table if exists crocus.{table};
    create table if not exists crocus.{table}(
        CONSTRAINT {table}_check
            CHECK (model = '{model}' and scenario = '{scenario}')
    )
    inherits(crocus.wbt);
    create index if not exists
        pk_{table}_season
        on crocus.{table}(season);
    create index if not exists
        pk_{table}_loc
        on crocus.{table}(loc);
    create index if not exists
        pk_{table}_period
        on crocus.{table}(period);
    create index if not exists
        pk_{table}_wbt
        on crocus.{table}(wbt);
    """
    available_con()
    cur.execute(query)
    myconn.commit()
    
    query = f"""
    delete from crocus.{table}
    where typezone = %s and period = %s and wbt = %s;
    """
    available_con()
    cur.execute(query,(zone_id, period, wbt,))
    myconn.commit()
    print(f"{table} created")
    
    con = create_engine(f'postgresql://{conn_param.user}:{conn_param.password}@{conn_param.host}:5432/{conn_param.dbname}')
    available_con()
    df.to_sql(table, con, schema = "crocus", if_exists = "append")
    print(f"append done")
    
# #Create table for future insertion with trigger and daughters'tables [Kept for memory => database sql file]
# myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
# cur = myconn.cursor()

# cur. execute(
# create table if not exists crocus.snow(
    # season int4,
    # ddate date,
    # loc int4,
    # typezone varchar(5),
    # model varchar(50),
    # scenario varchar(50),
    # snowtype varchar(10),
    # soil_temp5mm float8,
    # soil_temp8cm float8,
    # wc float8,
    # drain float8,
    # runoff float8,
    # snowmelt float8,
    # swe float8,
    # sd float8,
    # wbt float8
    # )
# )
# myconn.commit()

# PSQL connection
myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()

# models_done = []
# cur.execute("select distinct model from crocus.snow where typezone = 'sym'")
# for m in cur:
    # models_done.append(m[0])
# cur.close()

root_dir = "/home/francois/data/rawdata/adamont2020/indicateurs_annuels"
dirlist = os.listdir(root_dir)
dirlist = [d for d in dirlist if d[0:8] == "sym_flat" or d[0:8] == "alp_flat" or d[0:8] == "pyr_flat"]
# dirlist = [d for d in dirlist if d[0:3] == "sym"]
for dir in dirlist:
    zone_id = dir[0:3]
    print(zone_id)
    dirlist = os.listdir(os.path.join(root_dir, dir))
    # dirlist = [d for d in dirlist if d[0:10] == "experiment"]
    dirlist = [d for d in dirlist if d[0:6].lower() == "safran" and d != "index.html"]
    print(dirlist)
    # dirlist = [d for d in dirlist if d[0:10] == "reanalysis" and (d[11:len(d)].lower() == "fan" or d[11:len(d)].lower() == "nn")]
    # print(dirlist)
    dirlist.sort()
    listest = []
    for src_dir in dirlist:
        if src_dir[0:6].lower() == 'safran':
            scenario = 'reanalysis'
            model = 'safran'
        else:
            idxlist = []
            s = 0
            test = True
            while test:
                try:
                    idx = src_dir.index("_", s, len(src_dir))
                    idxlist.append(idx)
                    s = idx + 1
                except ValueError:
                    test = False
            scenario = src_dir[idxlist[len(idxlist) - 1] + 1:len(src_dir)].lower()
            model = src_dir[0:idxlist[len(idxlist) - 1]].lower()
            if "aladin53" in model:
                cor = "a53"
            elif "aladin63" in model:
                cor = "a63"
            else:
                cor = "ok"
            ms = []
            for m in model.split("_"):
                ms = ms + m.split("-")
                # print(ms)
            model = ms
            model = [s[0] for s in model]
            model = "_".join(model)
            if cor != "ok":
                model = model.replace("a", cor)
            listest.append(model)
        src_dir = os.path.join(root_dir, dir, src_dir)
        print(f"{model} ({scenario})")
        # pro2pg(src_dir, zone, model, scenario, snowtype)
        # if (model == "c_c_8_1_c_c_c_c" and zone_id != 'pyr' and snowtype != nn) or (model != "c_c_8_1_c_c_c_c" and snowtype != "nn" and):
        # if (zone_id == 'sym' and snowtype != "nn" and snowtype != "fan" and (model not in models_done or model == 'd_h_n_n_m')):
        pro2pg_launcher(src_dir, zone_id, model, scenario)
        # query = """
            # create table if not exists crocus.available_sims(
                # sim_id serial
                # typezone varchar(5),
                # model varchar(50),
                # scenario varchar(50),
                # snowtype varchar(50)
                # );
            # insert into crocus.available_sims(typezone, model, scenario,snowtype)
            # select %s, %s, %s, %s;
        # """
        # cur.execute(query,(zone_id, model, scenario, snowtype,))
        # myconn.commit()