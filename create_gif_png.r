library(rgdal)
library(raster)
library(curl)
library(RPostgreSQL)
library(magick)
rm(list = ls())

plot_paca <-function(ind, sta, envid, src_dir, dst_dir, snowtype, scenario){
    # Open raster to plot, transform to EPSG 3035, get extent to set the plt area lim
    rastypes = c("mean", "q20")
    smin = 2020
    smax = 2050
    seasons = seq(smin, smax)
    
    delta = 7
    
    # create tmp tables for common vector layers: envelope, retenues, ski slopes, ski lifts
    
    query <- paste0(
        "-- envelope
        drop table if exists crosscut.tmp_plot_envelopes;
        create table crosscut.tmp_plot_envelopes as
        select envid, st_transform(geom,3857) geom
        from crosscut.envelopes
        where envid = ", envid,";
        
        --reservoirs
        drop table if exists crosscut.tmp_plot_reservoirs;
        create table crosscut.tmp_plot_reservoirs as
        select gid, st_transform(geom,3857) geom
        from crosscut.reservoirs
        where ind = '", ind,"';
        
        --ski tracks
        drop table if exists crosscut.tmp_plot_tracks;
        create table crosscut.tmp_plot_tracks as
        select track_id, st_transform(geom,3857) geom
        from crosscut.tracks
        where ind = '", ind,"';
        
        --ski lifts
        drop table if exists crosscut.tmp_plot_lifts;
        create table crosscut.tmp_plot_lifts as
        select gid, st_transform(geom,3857) geom
        from crosscut.resort_lifts
        where ind = '", ind,"';
        
        --buildings
        drop table if exists crosscut.tmp_ext;
        create table crosscut.tmp_ext as
        select st_makeenvelope(
            st_xmin(geom) - (st_xmax(geom) - st_xmin(geom)) * 0.05,
            st_ymin(geom) - (st_ymax(geom) - st_ymin(geom)) * 0.05,
            st_xmax(geom) + (st_xmax(geom) - st_xmin(geom)) * 0.05,
            st_ymax(geom) + (st_ymax(geom) - st_ymin(geom)) * 0.05,
            2154) geom
        from crosscut.envelopes a
        where envid = ",envid,";

        create index idx_tmp_ext_geom on crosscut.tmp_ext using gist(geom);
        
        drop table if exists crosscut.tmp_plot_buildings;
        create table crosscut.tmp_plot_buildings as
        select b.gid, st_transform(the_geom, 3857) geom
        from crosscut.tmp_ext a, stations.geo_bati_sta_bas_rm_300_200_ind b
        where st_intersects(b.the_geom,geom);"
    )
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    dbSendQuery(con, query)
    dbDisconnect(con)
    print("tmp table created")
    
    dsn = paste0("PG: dbname = '",db,"' host='",host,"' port = '5432' user = '",user,"' password = '",pwd,"'")
    
    for (rastype in rastypes){
        # print(snowtype)
        if (snowtype == "lance" | snowtype == "fan"){
            query <- paste0(
                "select distinct status
                from crosscut.sm_resort
                where ind = '", ind,"';
            "
            )
            con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
            status <- dbGetQuery(con, query)
            dbDisconnect(con)
            
            for (sm in 1:nrow(status)){
                sm = status[sm,1]
                print(sm)
                
                imgif = c()
                for (season in seasons){
                    #test if file already exists
                    img = paste0(dst_dir, sm, "len_", rastype, "_", season, ".png")
                    if (file.exists(img) == FALSE){                   
                        query <- paste0(
                            "select gid, st_x(st_transform(geom,3857)) x, st_y(st_transform(geom,3857)) y
                            from crosscut.sm_points
                            where ind = '", ind,"' and status = '",sm,"';"
                        )
                        con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
                        smpoints <- dbGetQuery(con, query)
                        dbDisconnect(con)
                        
                        
                        query <- paste0(
                            "--viability quantiles
                            drop table if exists crosscut.tmp_viability_quantiles;
                            create table crosscut.tmp_viability_quantiles as
                            with a as (
                                select a.season,
                                min(min(a.season)) over() smin, max(max(a.season)) over() smax,
                                percentile_cont(0.2) within group (order by b.idx) q20,
                                percentile_cont(0.5) within group (order by b.idx) q50,
                                percentile_cont(0.8) within group (order by b.idx) q80
                                from viability.idx a
                                join viability.idx b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
                                and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
                                where a.snowtype = '",sm, "_", snowtype,"' and a.scenario = '",scenario,"'
                                and a.envid = ", envid,"
                                group by a.season
                            )

                            select season, q20, q50, q80
                            from a 
                            where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
                            order by season;"
                        )
                        con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
                        dbSendQuery(con, query)
                        dbDisconnect(con)      
                        
                        src_ds = paste0(src_dir, rastype, "/", sm, "_len_", rastype, "_", season, ".tif")
                        rast = raster(src_ds)
                        rast = projectRaster(rast, crs=CRS("+init=epsg:3857"), method = "ngb")
                        
                        xmin = rast@extent@xmin
                        xmax = rast@extent@xmax
                        ymin = rast@extent@ymin
                        ymax = rast@extent@ymax
                        
                        xext = xmax - xmin
                        xmin = xmin - xext * .05
                        xmax = xmax + xext * .05
                        
                        ytitle = ymax
                        yext = ymax - ymin
                        ymin = ymin - yext * .05
                        ymax = ymax + yext * .05
                        
                        if((xmax - xmin) >= (ymax - ymin)){
                            ext = xmax - xmin
                            ymin = ymin + (ymax - ymin)/2 - ext/2
                            ymax = ymin + (ymax - ymin)/2 + ext/2
                        } else {
                            ext = ymax - ymin
                            xmin = xmin + (xmax - xmin)/2 - ext/2
                            xmax = xmin + (xmax - xmin)/2 + ext/2
                        }
                        
                        xleg = xmax
                        xmax = xmax + (xmax - xmin) / 3
                        xlim = c(xmin, xmax)
                        
                        yplot = ymin
                        ymin = ymin - (ymax - ymin) /3
                        ylim = c(ymin, ymax)
                        
                        #Setup the adapted coloramp
                        mycolrastot = colorRampPalette(c("#ef6548","#ffeda0","#006d2c"))(180)
                        myval = sort(unique(rast))

                        if(0 %in% myval == FALSE){
                            mycolrast = mycolrastot[myval]
                            myval = c(0, myval)
                        } else {
                            mycolrast = c("#ef6548",mycolrastot[myval[myval > 0]])
                            myval = c(-.1, myval)
                        }
                        
                        #Instantiate plot device
                        height = 800
                        width = height
                        png(img,width = width, height = height, bg = "white")

                        par(mar=c(0,0,0,0), oma=c(0,0,0,0))
                        plot(0, 0, type = "n", xlim = xlim, ylim = ylim, col = "white", bty ="n", xaxs = "i", yaxs = "i", axes = F)

                        #Add baselayer 
                        baserast = "/home/francois/data/source_rasters/eudem_3857.tif"
                        baserast = raster(baserast)
                        baserast = crop(baserast, extent(c(xmin, xleg, yplot, ymax)))
                        # myalpha = .5
                        # plot(baserast, col=grey(0:224/224), alpha = myalpha,
                           # add = T, legend = F)
                        # print("raster plotted")
                        
                        lcstep = 50
                        lcmin = ceiling(minValue(baserast)/lcstep) * lcstep
                        lcmax = floor(maxValue(baserast)/lcstep) * lcstep
                        lev = seq(lcmin, lcmax, lcstep)
                        
                        lcstep = 250
                        lcmin = ceiling(minValue(baserast)/lcstep) * lcstep
                        lcmax = floor(maxValue(baserast)/lcstep) * lcstep
                        levlab = seq(lcmin, lcmax, lcstep)
                        lev = lev[!lev %in% levlab]
                        
                        lc = rasterToContour(baserast, levels = lev)
                        
                        contour(baserast, levels = levlab, add = T, labcex = .8, axes = F)
                        contour(baserast, levels = lev, add = T, drawlabels = F, axes = F)
                        
                        #Add data layer
                        myalpha = .8
                        plot(rast, breaks = myval, col=mycolrast, alpha = myalpha,
                           add=T, legend = F, bty = "n", box = F)
                        
                        ######ADD vector data
                        # #envelope
                        # layer = "crosscut.tmp_plot_envelopes"
                        # lay = readOGR(dsn, layer)
                        # plot(lay, xlim = x, ylim = y, col = NA, border = "darkgrey", lwd = 2, add = T)
                        
                        #reservoirs
                        query = "select * from crosscut.tmp_plot_reservoirs"
                        con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
                        nbres <- dbGetQuery(con, query)
                        dbDisconnect(con)
                        nbres = nrow(nbres)
                        if (nbres > 0){
                            layer = "crosscut.tmp_plot_reservoirs"
                            lay = readOGR(dsn, layer)
                            plot(lay, xlim = x, ylim = y, col = "#4eb3d380", border = "#808080", lwd = 1, add = T)
                        }
                        #buildings
                        layer = "crosscut.tmp_plot_buildings"
                        lay = readOGR(dsn, layer)
                        plot(lay, xlim = x, ylim = y, col = "#000000", border = "#808080", lwd = 1, add = T)
                        
                        #ski tracks
                        layer = "crosscut.tmp_plot_tracks"
                        lay = readOGR(dsn, layer)
                        plot(lay, xlim = x, ylim = y, col = NA, border = "black", lwd = 1, add = T)
                        
                        #lifts
                        layer = "crosscut.tmp_plot_lifts"
                        lay = readOGR(dsn, layer)
                        plot(lay, xlim = x, ylim = y, col = "#000000", lwd = 5, lty = "16", add = T)
                        plot(lay, xlim = x, ylim = y, col = "#000000", lwd = 1, add = T)
                        
                        #add smpoints
                        if (nrow(smpoints) > 0){
                            icon = "/home/francois/data/source_rasters/snowflake_icon.png"
                            icon = readPNG(icon)
                            psize = ext / height
                            isize = dim(icon)
                            hoffset = psize * isize[1] / 4
                            voffset = psize * isize[2] / 4
                            for (p in 1:nrow(smpoints)){
                                rasterImage(icon, smpoints[p,2] - hoffset, smpoints[p,3] - voffset, smpoints[p,2] + hoffset, smpoints[p,3] + voffset)
                            }
                        }
                        ###### add legend and texts
                
                        x0 = xleg + .3 * (xmax - xleg)
                        x1 = x0 + .5 * (xmax - xleg)
                        
                        y0 = yplot + .05 * (yplot - ymin)
                        y1 = ymax - .05 * (yplot - ymin)
                        ysize = (y1 - y0) / 180            
                        
                        for (i in 1:180){
                            polygon(c(x0, x0, x1, x1), c(y0 + (i-1) * ysize, y0 + i * ysize, y0 + i * ysize, y0 + (i-1) * ysize),
                            col = mycolrastot[i], border = NA)
                        }
                        
                        
                        polygon(c(x0,x0,x1,x1), c(y0,y1,y1,y0), col = NA, border = "black", lwd = 1.5)
                        text(x0 - 0.05 * (xmax - xleg), y0 + (y1 - y0)/2, labels = "Nombre de jours d'enneigement fiable", adj=c(.5,0), srt = 90, cex = 1.6)
                        
                        x0 = x1
                        x1 = x0 + 0.03 * (xmax - xleg)
                        x2 = x1 + 0.01 * (xmax - xleg)
                        
                        # lines(c(x0,x0), c(y0,y1), col = "black", lwd = 1.5)
                        
                        labs = seq(0,180,10)
                        ystep = (y1 - y0) / (length(labs) - 1)
                        at = seq(y0, y1, ystep)
                        
                        for (i in 1:length(labs)){
                            lines(c(x0,x1), c(at[i], at[i]), col = "black", lwd = 1)
                            text(x2,at[i], labs[i], adj = c(0,.5))
                        }
                        
                        # Add title
                        if (rastype == "mean"){
                            rtitle = "moyenne"
                        } else if (rastype == "q20"){
                            rtitle = rastype
                        }
                        # mytitle = paste0(sta, " : ", season)
                        # mysub = paste0("(", rtitle, " ", season - 7, "-", season + 7,")")
                        
                        mytitle = paste0(season, " (", toupper(scenario), ")")
                        ytop = ymax - (ymax - ytitle) / 4
                        ybot = ymax - (ymax - ytitle) * 3/4
                        text(xmin + (xleg - xmin)/2, ytop , labels = mytitle, adj = c(.5,.5), cex = 1.8, col = "black", font = 2)
                        # text(xmin + (xleg - xmin)/2, ybot , labels = mysub, adj = c(.5,.5), cex = 1.6, col = "black", font = 2)
                        
                        ##### add logo
                        icon = "/home/francois/data/source_rasters/logo_climsnow.png"
                        icon = readPNG(icon)
                        px = xleg + (xmax - xleg) / 2
                        py = yplot - (yplot - ymin) / 2
                        hsize = .8 * (xmax - xleg)
                        isize = dim(icon)
                        vsize = hsize * isize[1] / isize[2]
                        hoffset = hsize / 2
                        voffset = vsize / 2
                        rasterImage(icon, px - hoffset, py - voffset, px + hoffset, py + voffset)
                        
                        ##### add plot
                        y0 = yplot - .1 * (yplot - ymin)
                        y1 = ymin + .2 * (yplot - ymin)
                        
                        x0 = xmin + .05 * (xleg - xmin)                    
                        xsize = (xleg - x0) / ((smax - smin) - 1)                
                        
                        query = paste0("select * from crosscut.tmp_viability_quantiles order by season")
                        con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
                        data <- dbGetQuery(con, query)
                        dbDisconnect(con)
                        
                        # create vectors from data
                        x = c()
                        y20 = c()
                        y50 = c()
                        y80 = c()
                        s = seq(smin,smax)
                        for (i in 1:length(s)){
                            x = c(x, x0 + (i-1) * xsize)
                            y20 = c(y20, y1 + data[i,2] / 100 * (y0 - y1))
                            y50 = c(y50, y1 + data[i,3] / 100 * (y0 - y1))
                            y80 = c(y80, y1 + data[i,4] / 100 * (y0 - y1))
                            if (s[i] == season){
                                xs = x0 + (i-1) * xsize
                                ys = y1 + data[i,c(2,4,3)] / 100 * (y0 - y1)
                                qs = round(data[i,c(2,4,3)],2)
                            }
                        }
                        
                        # add curve
                        polygon(c(x,rev(x)), c(y20, rev(y80)), col = "#deebf7", border = NA)
                        lines(x, y50, col = "#2171b5", lwd = 1.4)
                        
                        # add plot title
                        y  = y1 + .1 * (y1 - ymin)
                        x =  x0 + (xleg - x0) / 2
                        text(x,y, "Indice de fiabilité de l'enneigement", cex = .8, col = "darkgrey", adj = c(.5,0))    
                        
                        #add season data
                        lines(c(xs, xs), c(y1,y0), col = "red", lty = 2, lwd = .8)
                        ys = ys[1,] + 0.05 * (y0 - y1)
                        xs = xs + 0.02 * (xleg - x0)
                        
                        text(rep(xs, length(ys)), ys, labels = qs, col = c("black", "black", "#2171b5"), cex = .8, adj = c(0,.5))
                        
                        # add axis
                        lines(c(x0, xleg), c(y1, y1), col = "black")
                        lines(c(x0, x0), c(y1, y0), col = "black")
                        
                        ylab = seq(0,100,20)
                        x = x0 - 0.1 * (x0 - xmin)
                        for (i in 1:length(ylab)){
                            y = y1 + ylab[i] / 100 * (y0 - y1)
                            lines(c(x, x0), c(y, y), col = "black")
                            text(x - 0.05 * (x0 - xmin), y, ylab[i], adj = c(1,.5), cex = .6)
                        }
                        
                        xlab = seq(smin, smax, 10)
                        y = y1 - 0.05 * (y1 - ymin)
                        for (i in 1:length(xlab)){
                            x = x0 + (xlab[i] - smin) / (smax - smin) * (xleg - x0)
                            lines(c(x, x), c(y1, y), col = "black")
                            text(x, y - 0.02 * (y1 - ymin), xlab[i], adj = c(1,.5), srt = 90, cex = .6)
                        }
                        
                        dev.off()
                    }
                    # readline(prompt="Press [enter] to continue")
                    # add img in vector
                    # print(img)
                    tmpimg = image_read(img)
                    imgif = c(imgif, tmpimg)
                }
                # create_gif
                imgif = image_join(imgif)
                gif = paste0(substr(img, 1, nchar(img)-8),smin,"_", smax,".gif")
                print(gif)
                anim = image_animate(imgif, fps = 1, optimize = TRUE)
                image_write(anim, gif)
                print("gif done")
            }
        } else {
            imgif = c()
            for (season in seasons){
                # Test if file exists
                img = paste0(dst_dir, "len_", rastype, "_", season, ".png")
                if (file.exists(img) == FALSE){
                    query <- paste0(
                        "--viability quantiles
                        drop table if exists crosscut.tmp_viability_quantiles;
                        create table crosscut.tmp_viability_quantiles as
                        with a as (
                            select a.season,
                            min(min(a.season)) over() smin, max(max(a.season)) over() smax,
                            percentile_cont(0.2) within group (order by b.idx) q20,
                            percentile_cont(0.5) within group (order by b.idx) q50,
                            percentile_cont(0.8) within group (order by b.idx) q80
                            from viability.idx a
                            join viability.idx b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
                            and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
                            where a.snowtype = 'ns&gro_", snowtype,"' and a.scenario = '",scenario,"'
                            and a.envid = ", envid,"
                            group by a.season
                        )

                        select season, q20, q50, q80
                        from a 
                        where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
                        order by season;"
                    )
                    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
                    dbSendQuery(con, query)
                    dbDisconnect(con)      
                    
                    src_ds = paste0(src_dir, rastype, "/len_", rastype, "_", season, ".tif")
                    rast = raster(src_ds)
                    rast = projectRaster(rast, crs=CRS("+init=epsg:3857"), method = "ngb")
                    
                    xmin = rast@extent@xmin
                    xmax = rast@extent@xmax
                    ymin = rast@extent@ymin
                    ymax = rast@extent@ymax
                    
                    xext = xmax - xmin
                    xmin = xmin - xext * .05
                    xmax = xmax + xext * .05
                    
                    ytitle = ymax
                    yext = ymax - ymin
                    ymin = ymin - yext * .05
                    ymax = ymax + yext * .05
                    
                    if((xmax - xmin) >= (ymax - ymin)){
                        ext = xmax - xmin
                        ymin = ymin + (ymax - ymin)/2 - ext/2
                        ymax = ymin + (ymax - ymin)/2 + ext/2
                    } else {
                        ext = ymax - ymin
                        xmin = xmin + (xmax - xmin)/2 - ext/2
                        xmax = xmin + (xmax - xmin)/2 + ext/2
                    }
                    
                    xleg = xmax
                    xmax = xmax + (xmax - xmin) / 3
                    xlim = c(xmin, xmax)
                    
                    yplot = ymin
                    ymin = ymin - (ymax - ymin) /3
                    ylim = c(ymin, ymax)
                    
                    #Setup the adapted coloramp
                    mycolrastot = colorRampPalette(c("#ef6548","#ffeda0","#006d2c"))(180)
                    myval = sort(unique(rast))

                    if(0 %in% myval == FALSE){
                        mycolrast = mycolrastot[myval]
                        myval = c(0, myval)
                    } else {
                        mycolrast = c("#ef6548",mycolrastot[myval[myval > 0]])
                        myval = c(-.1, myval)
                    }
                    
                    #Instantiate plot device
                    height = 800
                    width = height
                    png(img,width = width, height = height, bg = "white")

                    par(mar=c(0,0,0,0), oma=c(0,0,0,0))
                    plot(0, 0, type = "n", xlim = xlim, ylim = ylim, col = "white", bty ="n", xaxs = "i", yaxs = "i", axes = F)

                    #Add baselayer 
                    baserast = "/home/francois/data/source_rasters/eudem_3857.tif"
                    baserast = raster(baserast)
                    baserast = crop(baserast, extent(c(xmin, xleg, yplot, ymax)))
                    # myalpha = .5
                    # plot(baserast, col=grey(0:224/224), alpha = myalpha,
                       # add = T, legend = F)
                    # print("raster plotted")
                    lcstep = 50
                    lcmin = ceiling(minValue(baserast)/lcstep) * lcstep
                    lcmax = floor(maxValue(baserast)/lcstep) * lcstep
                    lev = seq(lcmin, lcmax, lcstep)
                    
                    lcstep = 250
                    lcmin = ceiling(minValue(baserast)/lcstep) * lcstep
                    lcmax = floor(maxValue(baserast)/lcstep) * lcstep
                    levlab = seq(lcmin, lcmax, lcstep)
                    lev = lev[!lev %in% levlab]
                    
                    lc = rasterToContour(baserast, levels = lev)
                    
                    contour(baserast, levels = levlab, add = T, labcex = .8, axes = F)
                    contour(baserast, levels = lev, add = T, drawlabels = F, axes = F)
                    
                    #Add data layer
                    myalpha = .8
                    plot(rast, breaks = myval, col=mycolrast, alpha = myalpha,
                       add=T, legend = F, bty = "n", box = F)
                    
                    ######ADD vector data
                    # #envelope
                    # layer = "crosscut.tmp_plot_envelopes"
                    # lay = readOGR(dsn, layer)
                    # plot(lay, xlim = x, ylim = y, col = NA, border = "darkgrey", lwd = 2, add = T)
                    
                    #reservoirs
                    query = "select * from crosscut.tmp_plot_reservoirs"
                    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
                    nbres <- dbGetQuery(con, query)
                    dbDisconnect(con)
                    nbres = nrow(nbres)
                    if (nbres > 0){
                        layer = "crosscut.tmp_plot_reservoirs"
                        lay = readOGR(dsn, layer)
                        plot(lay, xlim = x, ylim = y, col = "#4eb3d380", border = "#808080", lwd = 1, add = T)
                    }
                    #buildings
                    layer = "crosscut.tmp_plot_buildings"
                    lay = readOGR(dsn, layer)
                    plot(lay, xlim = x, ylim = y, col = "#000000", border = "#808080", lwd = 1, add = T)
                    
                    #ski tracks
                    layer = "crosscut.tmp_plot_tracks"
                    lay = readOGR(dsn, layer)
                    plot(lay, xlim = x, ylim = y, col = NA, border = "black", lwd = 1, add = T)
                    
                    #lifts
                    layer = "crosscut.tmp_plot_lifts"
                    lay = readOGR(dsn, layer)
                    plot(lay, xlim = x, ylim = y, col = "#000000", lwd = 5, lty = "16", add = T)
                    plot(lay, xlim = x, ylim = y, col = "#000000", lwd = 1, add = T)
                    
                    ###### add legend and texts
            
                    x0 = xleg + .3 * (xmax - xleg)
                    x1 = x0 + .5 * (xmax - xleg)
                    
                    y0 = yplot + .05 * (yplot - ymin)
                    y1 = ymax - .05 * (yplot - ymin)
                    ysize = (y1 - y0) / 180            
                    
                    for (i in 1:180){
                        polygon(c(x0, x0, x1, x1), c(y0 + (i-1) * ysize, y0 + i * ysize, y0 + i * ysize, y0 + (i-1) * ysize),
                        col = mycolrastot[i], border = NA)
                    }
                    
                    
                    polygon(c(x0,x0,x1,x1), c(y0,y1,y1,y0), col = NA, border = "black", lwd = 1.5)
                    text(x0 - 0.05 * (xmax - xleg), y0 + (y1 - y0)/2, labels = "Nombre de jours d'enneigement fiable", adj=c(.5,0), srt = 90, cex = 1.6)
                    
                    x0 = x1
                    x1 = x0 + 0.03 * (xmax - xleg)
                    x2 = x1 + 0.01 * (xmax - xleg)
                    
                    # lines(c(x0,x0), c(y0,y1), col = "black", lwd = 1.5)
                    
                    labs = seq(0,180,10)
                    ystep = (y1 - y0) / (length(labs) - 1)
                    at = seq(y0, y1, ystep)
                    
                    for (i in 1:length(labs)){
                        lines(c(x0,x1), c(at[i], at[i]), col = "black", lwd = 1)
                        text(x2,at[i], labs[i], adj = c(0,.5))
                    }
                    
                    # Add title
                    if (rastype == "mean"){
                        rtitle = "moyenne"
                    } else if (rastype == "q20"){
                        rtitle = rastype
                    }
                    # mytitle = paste0(sta, " : ", season)
                    # mysub = paste0("(", rtitle, " ", season - 7, "-", season + 7,")")
                    
                    mytitle = paste0(season, " (", toupper(scenario), ")")
                    ytop = ymax - (ymax - ytitle) / 4
                    ybot = ymax - (ymax - ytitle) * 3/4
                    text(xmin + (xleg - xmin)/2, ytop , labels = mytitle, adj = c(.5,.5), cex = 1.8, col = "black", font = 2)
                    # text(xmin + (xleg - xmin)/2, ybot , labels = mysub, adj = c(.5,.5), cex = 1.6, col = "black", font = 2)
                    
                    ##### add logo
                    icon = "/home/francois/data/source_rasters/logo_climsnow.png"
                    icon = readPNG(icon)
                    px = xleg + (xmax - xleg) / 2
                    py = yplot - (yplot - ymin) / 2
                    hsize = .8 * (xmax - xleg)
                    isize = dim(icon)
                    vsize = hsize * isize[1] / isize[2]
                    hoffset = hsize / 2
                    voffset = vsize / 2
                    rasterImage(icon, px - hoffset, py - voffset, px + hoffset, py + voffset)
                    
                    ##### add plot
                    y0 = yplot - .1 * (yplot - ymin)
                    y1 = ymin + .2 * (yplot - ymin)
                    
                    x0 = xmin + .05 * (xleg - xmin)                    
                    xsize = (xleg - x0) / ((smax - smin) - 1)                                 
                    
                    query = paste0("select * from crosscut.tmp_viability_quantiles order by season")
                    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
                    data <- dbGetQuery(con, query)
                    dbDisconnect(con)
                    
                    # create vectors from data
                    x = c()
                    y20 = c()
                    y50 = c()
                    y80 = c()
                    s = seq(smin,smax)
                    for (i in 1:length(s)){
                        x = c(x, x0 + (i-1) * xsize)
                        y20 = c(y20, y1 + data[i,2] / 100 * (y0 - y1))
                        y50 = c(y50, y1 + data[i,3] / 100 * (y0 - y1))
                        y80 = c(y80, y1 + data[i,4] / 100 * (y0 - y1))
                        if (s[i] == season){
                            xs = x0 + (i-1) * xsize
                            ys = y1 + data[i,c(2,4,3)] / 100 * (y0 - y1)
                            qs = round(data[i,c(2,4,3)],2)
                        }
                    }
                    
                    # add curve
                    polygon(c(x,rev(x)), c(y20, rev(y80)), col = "#deebf7", border = NA)
                    lines(x, y50, col = "#2171b5", lwd = 1.4)
                    
                    # add plot title
                    y  = y1 + .1 * (y1 - ymin)
                    x =  x0 + (xleg - x0) / 2
                    text(x,y, "Indice de fiabilité de l'enneigement", cex = .8, col = "darkgrey", adj = c(.5,0))
                    
                    #add season data
                    lines(c(xs, xs), c(y1,y0), col = "red", lty = 2, lwd = .8)
                    ys = ys[1,] + 0.05 * (y0 - y1)
                    xs = xs + 0.02 * (xleg - x0)
                    
                    text(rep(xs, length(ys)), ys, labels = qs, col = c("black", "black", "#2171b5"), cex = .8, adj = c(0,.5))
                    
                    # add axis
                    lines(c(x0, xleg), c(y1, y1), col = "black")
                    lines(c(x0, x0), c(y1, y0), col = "black")
                    
                    ylab = seq(0,100,20)
                    x = x0 - 0.1 * (x0 - xmin)
                    for (i in 1:length(ylab)){
                        y = y1 + ylab[i] / 100 * (y0 - y1)
                        lines(c(x, x0), c(y, y), col = "black")
                        text(x - 0.05 * (x0 - xmin), y, ylab[i], adj = c(1,.5), cex = .6)
                    }
                    
                    xlab = seq(smin, smax, 10)
                    y = y1 - 0.05 * (y1 - ymin)
                    for (i in 1:length(xlab)){
                        x = x0 + (xlab[i] - smin) / (smax - smin) * (xleg - x0)
                        lines(c(x, x), c(y1, y), col = "black")
                        text(x, y - 0.02 * (y1 - ymin), xlab[i], adj = c(1,.5), srt = 90, cex = .6)
                    }
                    
                    dev.off()
                }
                # readline(prompt="Press [enter] to continue")
                # add img in vector
                # print(img)
                tmpimg = image_read(img)
                imgif = c(imgif, tmpimg)
                # readline(prompt="Press [enter] to continue")
            }
        
            # create_gif
            imgif = image_join(imgif)
            gif = paste0(substr(img, 1, nchar(img)-8),smin,"_", smax,".gif")
            print(gif)
            anim = image_animate(imgif, fps = 1, optimize = TRUE)
            image_write(anim, gif)
            print("gif done")
        }
    }
}

#GLOBAL PARAMETER
drv = dbDriver("PostgreSQL")
host = "195.221.110.200"
user = "postgres"
pwd = "lceslt"
db = "db"

inds = c("0537")
period = 15

for (i in 1:length(inds)){
    ind = inds[i]
    query = paste0("select name from crosscut.resorts where ind = '",ind,"';")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    sta <- dbGetQuery(con, query)
    dbDisconnect(con)

    src_sta = paste0("/home/francois/data/results/", sta, "/results/")

    query = paste0("select distinct envid, name from crosscut.envelopes
    where ind = '",ind,"'
    order by 1")
    print(query)
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    listenv <- dbGetQuery(con, query)
    dbDisconnect(con)

    # listenv = c(6)    

    ###############plot PACA
    #seasons = c(2030, 2050, 2090)
    # snowtypes = c("nn", "gro","lance", "fan")
    snowtypes = c("gro","lance")
    # snowtypes = c("nn", "gro")
    scenarios = c("rcp26", "rcp45", "rcp85")
    # scenarios = c("rcp85")
    library(png)
    
    ########################/!\ dst_dir should be modified in order to be resort/envelope specific##############################

    for (envid in 1:nrow(listenv)){
        envid = listenv[envid,1]
        for (snowtype in snowtypes){
            for (scenario in scenarios){
                src_dir = paste0(src_sta, envid, "/seasonal_rasters/", snowtype, "/", scenario,"/")
                dir.create(src_dir)
                dst_dir = paste0("/home/francois/data/tmp_gif/")
                dir.create(dst_dir)
                dst_dir = paste0(dst_dir, snowtype,"/")
                dir.create(dst_dir)
                dst_dir = paste0(dst_dir,scenario,"/")
                dir.create(dst_dir)
                plot_paca(ind, sta, envid, src_dir, dst_dir, snowtype, scenario)
            }
        }
    }
}