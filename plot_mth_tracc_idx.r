library(RPostgreSQL)
rm(list = ls())
library(extrafont)
loadfonts()

#GLOBAL PARAMETER
drv = dbDriver("PostgreSQL")
host = "10.38.192.30"
user = "postgres"
pwd = "lceslt"
db = "db"


plot_idx_tracc = function(dst_dir, envid, mth, snowtype){

	if (endsWith(snowtype, "lance")){
		status = gsub("_lance", "", snowtype)
		query = paste0("
			select distinct st_area(st_union(st_collectionextract(st_intersection(a.geom, b.geom), 3)) over()) / st_area(a.geom)
			from crosscut.envelopes a
			join crosscut.sm_resort b on a.ind = b.ind
			where envid = ", envid, " and status = '", status, "' and st_intersects(a.geom, b.geom)
		")
		con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
		snowpart <- dbGetQuery(con, query)
		dbDisconnect(con)
		snowpart = snowpart[1,1] * 100
	} else {
		snowpart = 0
	}
	query = paste0("
		with q20ref_mod as (
			select a.model,	percentile_cont(0.2) within group (order by \"", mth, "\") q20
			from viability.idx_mth a
			join crocus.tracc_model b on a.model = b.model
			where envid = ", envid, " and snowtype like 'ns&gro_gro' and scenario = 'historical'
			and season + 1 >= 1976 and season + 1 <= 2005
			group by 1
		), q20ref as (
			select '", snowtype, "' snowtype, percentile_cont(0.5) within group (order by q20) q20ref
			from q20ref_mod
		), hist_mod as (
			select 0.6 tracc, a.snowtype, a.model,
				percentile_cont(0.2) within group (order by \"", mth, "\") q20,
				percentile_cont(0.5) within group (order by \"", mth, "\") q50,
				percentile_cont(0.8) within group (order by \"", mth, "\") q80,
				sum(case when \"", mth, "\" <= q20ref or \"", mth, "\" < ", snowpart, " then 1 else 0 end)::numeric / count(*) freq20
			from viability.idx_mth a
			join crocus.tracc_model b on a.model = b.model
			join q20ref c on a.snowtype = c.snowtype 
			where envid = ", envid, " and a.snowtype like '", snowtype, "' and scenario = 'historical'
			and season + 1 >= 1976 and season + 1 <= 2005
			group by 1, 2, 3
		), proj_mod as (
			select d.tracc, a.snowtype, a.model,
				percentile_cont(0.2) within group (order by \"", mth, "\") q20,
				percentile_cont(0.5) within group (order by \"", mth, "\") q50,
				percentile_cont(0.8) within group (order by \"", mth, "\") q80,
				sum(case when \"", mth, "\" <= q20ref or \"", mth, "\" < ", snowpart, " then 1 else 0 end)::numeric / count(*) freq20
			from viability.idx_mth a
			join crocus.tracc_model b on a.model = b.model
			join crocus.tracc c on b.gcm = c.gcm and b.rcm = c.rcm
			join crocus.tracc_gwl d on c.gwl = d.gwl
			join q20ref e on a.snowtype = e.snowtype
			where envid = ", envid, " and a.snowtype like '", snowtype, "' and scenario = 'rcp85'
			and season + 1 >= an_deb and season + 1 <= an_fin
			group by 1, 2, 3
		)
		
		select tracc, snowtype,
			percentile_cont(0.5) within group (order by q20) q20,
			percentile_cont(0.5) within group (order by q50) q50,
			percentile_cont(0.5) within group (order by q80) q80,
			percentile_cont(0.5) within group (order by freq20) freq20
		from hist_mod
		group by tracc, snowtype
		
		union
		
		select tracc, snowtype,
			percentile_cont(0.5) within group (order by q20) q20,
			percentile_cont(0.5) within group (order by q50) q50,
			percentile_cont(0.5) within group (order by q80) q80,
			percentile_cont(0.5) within group (order by freq20) freq20
		from proj_mod
		group by tracc, snowtype
		
		union
		
		select -999.0 tracc, snowtype,
			percentile_cont(0.2) within group (order by \"", mth, "\") q20,
			percentile_cont(0.5) within group (order by \"", mth, "\") q50,
			percentile_cont(0.8) within group (order by \"", mth, "\") q80,
			null
		from viability.idx_mth_safran_reanalysis
		where envid = ", envid, " and snowtype like '", snowtype, "'
		and season + 1 >= 1976 and season + 1 <= 2005
		group by 1, 2
		
		order by 1
	")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    data <- dbGetQuery(con, query)
    dbDisconnect(con)
	
	
    # Build non linear color ramp for ember bars
	colvec= c("#ffffff00", "#fbc907ff", "#c41f26ff", "#7b1a62ff") 
	target = 1
	breaks = c(0.2, .3, .4, .5) # breaks must be sorted, length(breaks) == length(colvec), and max(breaks) < target
    nbcols = 400
    ycex = target / nbcols

    for (i in 1:length(breaks)){
      if (i == 1){
        nbc = round(breaks[i] / ycex)
        cols = rep("#ffffff00", nbc)
      } else {
        if (i == 2){
          nbc = round((breaks[i] - breaks[i - 1]) / ycex)
          cols = c(cols, colorRampPalette(c(colvec[i - 1], colvec[i]), alpha = TRUE)(nbc))
        } else if (i == length(breaks)){
          nbc = round((breaks[i] - breaks[i - 1]) / ycex) + 1
          cols = c(cols, colorRampPalette(c(colvec[i - 1], colvec[i]), alpha = TRUE)(nbc)[2:nbc])
          nbc = nbcols - length(cols)
          cols = c(cols, rep(colvec[i], nbc))      
        } else {
          nbc = round((breaks[i] - breaks[i - 1]) / ycex) + 1
          cols = c(cols, colorRampPalette(c(colvec[i - 1], colvec[i]), alpha = TRUE)(nbc)[2:nbc])
        }
      }
    }
	
	print("colorscale done")
	
	path = paste0(dst_dir, "tracc_idx_", mth, "_", snowtype, ".pdf")
	print(path)
	cairo_pdf(path, width = 4, height = 6, family = "Nimbus Sans L")
	
	layout(matrix(c(2,1,1,1,1,3),6,1))
	
	### plot in main area
	traccs = unique(data[,1])
	nbb = length(traccs)
	bw = 5
	sp = 2
	
	xmax = nbb * bw + nbb * sp
	xlim = c(0, xmax)
	ylim = c(0,100)
	par(mar= c(8,5,0,0), xpd = T, family = "Nimbus Sans L")
	plot(0,0, type = 'n', xlim = xlim, ylim = ylim, xlab = NA, ylab = NA, axes = FALSE)
	
	x0 = sp
	lab = c()
	for (t in traccs){
		if (t == -999){
			lab = c(lab, "SAFRAN\n1976-2005")
			scol = "#74c476"
			bcol = scol
		} else if (t == 0.6) {
			lab = c(lab, "Historique\n1976-2005")
			bcol = round(data[data[,1] == t,6] * 400)
			bcol = cols[bcol]
		} else {
			lab = c(lab, paste0("+", t, " °C"))
			bcol = round(data[data[,1] == t,6] * 400)
			bcol = cols[bcol]
		}
		
		print(paste(t, bcol))
		if (sum(col2rgb(bcol) * c(299, 587,114))/1000 < 125){
			lcol = "white"
		} else {
			lcol = "black"
		}
		
		x1 = x0 + bw
		y0 = data[data[,1] == t, 3]
		y1 = data[data[,1] == t, 5]
		print(paste(x0,x1,y0,y1))
		
		polygon(c(x0, x1, x1, x0), c(y0, y0, y1, y1), col = bcol, border = NA)
		lines(c(x0,x1), rep(data[data[,1] == t, 4], 2), col = lcol)
		polygon(c(x0, x1, x1, x0), c(y0, y0, y1, y1), col = NA, border = "black")
		
		x0 = x1 + sp

	}
	
	print("bar plotted")
	xat = seq(sp + bw / 2, xmax, sp + bw)
	yat = seq(0,100, 10)
	for (y in yat){
		lines(xlim, c(y,y), col = "lightgrey", lty = 3, lwd = .6)
	}
	lines(xlim, c(0,0))
	
	axis(side = 1, line = 0, at = xat, labels = lab, las = 2, cex.axis = 1.4, lwd = 0)
	axis(side = 2, line = -.8, tck = -.01, labels = NA)
	axis(side = 2, line = -.8, at = yat, labels = yat, las = 2, cex.axis = 1.4, lwd = 0)
	axis(side =2, line = -.8, at = ylim, labels=c("",""), lwd.ticks=0)
	mtext(side = 2, line = 3, "Indice de fiabilité (%)", cex = 1.1) 
	print("axis done")
	
	### add title in top area
	mvec = c("nov", "dec", "jan", "feb", "mar", "apr", "djf", "ndjfma")
	mnames = c("Novembre", "Décembre", "Janvier", "Février", "Mars", "Avril", "Décembre-Février", "Novembre-Avril")
	mnames = data.frame(mvec, mnames)
	mlab = mnames[mnames[,1] == mth, 2]
	
	query = paste0("
		select name from crosscut.resorts
		where ind = (select ind from crosscut.envelopes where envid = ", envid,")
		")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    rname <- dbGetQuery(con, query)
    dbDisconnect(con)
	rname = rname[1,1]
	
	query = paste0("select name from crosscut.envelopes where envid = ", envid)
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    envname <- dbGetQuery(con, query)
    dbDisconnect(con)
	envname = envname[1,1]
	
	if (snowtype == "ns&gro_nn"){
		stlab = "Neige naturelle"
	} else if (snowtype == "ns&gro_gro"){
		stlab = "Neige naturelle damée"
	} else if (endsWith(snowtype, "lance")){
		stlab = paste0("Damage + production de neige\nPerches (bi-fluides) - ", gsub("_lance", "", snowtype))
	} else {
		stlab = paste0("Damage + production de neige\nVentilateurs (mono-fluide) - ", gsub("_fan", "", snowtype))
	}
	
	title = paste0(rname, " (", envname, ")\n", mlab, "\n", stlab)
	
	xlim = c(0,100)
	ylim = c(0,100)
	par(mar = c(0,0,0,0), xpd = T, family = "Nimbus Sans L")
	plot(0,0, type = 'n', xlim = xlim, ylim = ylim, xlab = NA, ylab = NA, axes = FALSE)
	
	text(xlim[2]/2, ylim[2]/2, title, cex = 1.4, adj = c(.5, .5), font = 2)
	print("title done")

	### add legend in bottom area
	
	legmin = .1
	legmax = .6
	leglen = (legmax - legmin) * nbcols
	xmax = leglen + 1/4 * leglen
	
	bh = 5
	
	xlim = c(0,xmax)
	ylim = c(0, 15)
	par(mar = c(0,0,0,0), xpd = T, family = "Nimbus Sans L")
	plot(0,0, type = 'n', xlim = xlim, ylim = ylim, xlab = NA, ylab = NA, axes = FALSE)
	
	y0 = 2
	y1 = y0 + bh
	
	x0 = (xmax - leglen)/2
	for (i in (legmin * nbcols):(legmax * nbcols - 1)){
		polygon(c(x0, x0 + 1, x0 + 1, x0), c(y0, y0, y1, y1), col = cols[i], border = NA)
		x0 = x0 + 1
	}
	
	x0 = (xmax - leglen)/2
	x1 = xmax  - x0
	polygon(c(x0, x1, x1, x0), c(y0, y0, y1, y1), col = NA, border = "black")
	
	i = 0
    lab = c("Indétectable", "Modéré", "Elevé", "Très élevé")
	for(tck in seq(legmin + .1, legmax - .1, .1)){
		i = i + 1
		x = x0 + (tck - legmin) * nbcols
		y = .2 * bh
		lines(c(x,x), c(y0, y0 + y))
		lines(c(x,x), c(y1, y1 - y))
		text(x, y0 + (y1 - y0) / 2, tck, adj = c(.5, .5), font = 2)
		y = y0 - .15 * bh
		text(x, y, lab[i], adj = c(.5, 1), cex = 1.1)
	}
	
	x = x0 + leglen / 2
	y = y1 + .2 * bh
	text(x, y, "Niveau de risque de faible enneigement", adj = c(.5,0), cex = 1.2, font = 2)
	
	legcol = scol
	leglab = "SAFRAN (1976-2005)"
	legend(x0, ylim[2], leglab, pch = NA, fill = legcol, bty = 'n', cex = 1.2)	
	
	print("done")
    dev.off()
    embed_fonts(path)
}

plot_wc_tracc = function(dst_dir, envid, mth, snowtype){
	# Get ski slopes envelopes parts
	query = paste0("select st_area(st_collectionextract(st_intersection(st_union(st_makevalid(a.geom)), b.geom),3))/st_area(b.geom)
	from crosscut.tracks a, crosscut.envelopes b
	where envid = ", envid," and st_intersects(a.geom, b.geom)
	group by b.geom"
	)
	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	envpart <- dbGetQuery(con, query)
	dbDisconnect(con)
	envpart = envpart[1,1]

	query = paste0("
		with hist_mod as (
			select 0.6 tracc, a.snowtype, a.model,
				percentile_cont(0.2) within group (order by \"", mth, "\") q20,
				percentile_cont(0.5) within group (order by \"", mth, "\") q50,
				percentile_cont(0.8) within group (order by \"", mth, "\") q80
			from viability.wc_mth a
			join crocus.tracc_model b on a.model = b.model
			where envid = ", envid, " and a.snowtype like '", snowtype, "' and scenario = 'historical'
			and season + 1 >= 1976 and season + 1 <= 2005
			group by 1, 2, 3
		), proj_mod as (
			select d.tracc, a.snowtype, a.model,
				percentile_cont(0.2) within group (order by \"", mth, "\") q20,
				percentile_cont(0.5) within group (order by \"", mth, "\") q50,
				percentile_cont(0.8) within group (order by \"", mth, "\") q80
			from viability.wc_mth a
			join crocus.tracc_model b on a.model = b.model
			join crocus.tracc c on b.gcm = c.gcm and b.rcm = c.rcm
			join crocus.tracc_gwl d on c.gwl = d.gwl
			where envid = ", envid, " and a.snowtype like '", snowtype, "' and scenario = 'rcp85'
			and season + 1 >= an_deb and season + 1 <= an_fin
			group by 1, 2, 3
		), hist as  (		
			select tracc, snowtype,
				percentile_cont(0.5) within group (order by q20) * ",envpart," q20,
				percentile_cont(0.5) within group (order by q50) * ",envpart," q50,
				percentile_cont(0.5) within group (order by q80) * ",envpart," q80
			from hist_mod
			group by tracc, snowtype
		), proj as (
			select tracc, snowtype,
				percentile_cont(0.5) within group (order by q20) * ",envpart," q20,
				percentile_cont(0.5) within group (order by q50) * ",envpart," q50,
				percentile_cont(0.5) within group (order by q80) * ",envpart," q80
			from proj_mod a
			group by tracc, snowtype
		)
		
		select *, 1 coeff from hist
		
		union
		
		select a.*, a.q50 / b.q50
		from proj a, (select q50 from hist) b

		union
		
		select -999.0 tracc, snowtype,
			percentile_cont(0.2) within group (order by \"", mth, "\") * ",envpart," q20,
			percentile_cont(0.5) within group (order by \"", mth, "\") * ",envpart," q50,
			percentile_cont(0.8) within group (order by \"", mth, "\") * ",envpart," q80,
			0
		from viability.wc_mth_safran_reanalysis
		where envid = ", envid, " and snowtype like '", snowtype, "'
		and season + 1 >= 1976 and season + 1 <= 2005
		group by 1, 2
		
		order by 1
	")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    data <- dbGetQuery(con, query)
    dbDisconnect(con)
		
    # Build non linear color ramp for water bars
	coeffmax = ceiling(max(data[, 6]))
	nbcols = coeffmax * 100
	if (nbcols < 400){
		nbcols = round(400 / coeffmax) * coeffmax
	}
	
	colvec= c("#cb181d", "#fb6a4a", "#ffffff")
	nbc = nbcols / coeffmax
	cols = colorRampPalette(colvec)(nbc)
	
	colvec = c('#ffffff','#deebf7','#c6dbef','#9ecae1','#6baed6','#4292c6','#2171b5','#08519c','#08306b')
	nbc = nbcols - nbc + 1
	cols = c(cols, colorRampPalette(colvec)(nbc)[2:nbc])
	
	print("colorscale done")
	
	#dst file
	path = paste0(dst_dir, "tracc_wc_", mth, "_", snowtype, ".pdf")
	print(path)
	cairo_pdf(path, width = 4, height = 6, family = "Nimbus Sans L")
	
	layout(matrix(c(2,1,1,1,1,3),6,1))
	
	### plot in main area
	traccs = unique(data[,1])
	nbb = length(traccs)
	bw = 5
	sp = 2
	
	xmax = nbb * bw + nbb * sp
	xlim = c(0, xmax)
	ylim = c(0, ceiling(max(data[,5]) / 1000) * 1000)
	par(mar= c(8,5,0,0), xpd = T, family = "Nimbus Sans L")
	plot(0,0, type = 'n', xlim = xlim, ylim = ylim, xlab = NA, ylab = NA, axes = FALSE)
	
	x0 = sp
	lab = c()
	for (t in traccs){
		if (t == -999){
			lab = c(lab, "SAFRAN\n1976-2005")
			scol = "#74c476"
			bcol = scol
		} else if (t == 0.6) {
			lab = c(lab, "Historique\n1976-2005")
			bcol = round(data[data[,1] == t,6] * nbcols / coeffmax)
			bcol = cols[bcol]
		} else {
			lab = c(lab, paste0("+", t, " °C"))
			bcol = round(data[data[,1] == t,6] * nbcols / coeffmax)
			bcol = cols[bcol]
		}
		
		print(paste(t, bcol))
		if (sum(col2rgb(bcol) * c(299, 587,114))/1000 < 125){
			lcol = "white"
		} else {
			lcol = "black"
		}
		
		x1 = x0 + bw
		y0 = data[data[,1] == t, 3]
		y1 = data[data[,1] == t, 5]
		print(paste(x0,x1,y0,y1))
		
		polygon(c(x0, x1, x1, x0), c(y0, y0, y1, y1), col = bcol, border = NA)
		lines(c(x0,x1), rep(data[data[,1] == t, 4], 2), col = lcol)
		polygon(c(x0, x1, x1, x0), c(y0, y0, y1, y1), col = NA, border = "black")
		
		x0 = x1 + sp

	}
	
	print("bar plotted")
	xat = seq(sp + bw / 2, xmax, sp + bw)
	yat = seq(0,100, 10)
	for (y in yat){
		lines(xlim, c(y,y), col = "lightgrey", lty = 3, lwd = .6)
	}
	lines(xlim, c(0,0))
	
	axis(side = 1, line = 0, at = xat, labels = lab, las = 2, cex.axis = 1.4, lwd = 0)
	axis(side = 2, line = -.8, tck = -.01, labels = NA)
	axis(side = 2, line = -1.4, at = pretty(ylim), labels = pretty(ylim) / 100000, las = 2, cex.axis = 1.3, lwd = 0)
	axis(side =2, line = -.8, at = ylim, labels=c("",""), lwd.ticks=0)
	mtext(side = 2, line = 1.8, expression(atop("",atop("Besoin en eau","pour la production de neige ("*10^5*" "*m^3*")"))), cex = 1.4) 
	print("axis done")
	
	### add title in top area
	mvec = c("nov", "dec", "jan", "feb", "mar", "apr", "djf", "ndjfma")
	mnames = c("Novembre", "Décembre", "Janvier", "Février", "Mars", "Avril", "Décembre-Février", "Novembre-Avril")
	mnames = data.frame(mvec, mnames)
	mlab = mnames[mnames[,1] == mth, 2]
	
	query = paste0("
		select name from crosscut.resorts
		where ind = (select ind from crosscut.envelopes where envid = ", envid,")
		")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    rname <- dbGetQuery(con, query)
    dbDisconnect(con)
	rname = rname[1,1]
	
	query = paste0("select name from crosscut.envelopes where envid = ", envid)
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    envname <- dbGetQuery(con, query)
    dbDisconnect(con)
	envname = envname[1,1]
	
	if (snowtype == "ns&gro_nn"){
		stlab = "Neige naturelle"
	} else if (snowtype == "ns&gro_gro"){
		stlab = "Neige naturelle damée"
	} else if (endsWith(snowtype, "lance")){
		stlab = paste0("Damage + production de neige\nPerches (bi-fluides) - ", gsub("_lance", "", snowtype))
	} else {
		stlab = paste0("Damage + production de neige\nVentilateurs (mono-fluide) - ", gsub("_fan", "", snowtype))
	}
	
	title = paste0(rname, " (", envname, ")\n", mlab, "\n", stlab)
	
	xlim = c(0,100)
	ylim = c(0,100)
	par(mar = c(0,0,0,0), xpd = T, family = "Nimbus Sans L")
	plot(0,0, type = 'n', xlim = xlim, ylim = ylim, xlab = NA, ylab = NA, axes = FALSE)
	
	text(xlim[2]/2, ylim[2]/2, title, cex = 1.4, adj = c(.5, .5), font = 2)
	print("title done")

	### add legend in bottom area
	
	xmax = nbcols + 1/4 * nbcols
	
	bh = 5
	
	xlim = c(0,xmax)
	ylim = c(0, 15)
	par(mar = c(0,0,0,0), xpd = T, family = "Nimbus Sans L")
	plot(0,0, type = 'n', xlim = xlim, ylim = ylim, xlab = NA, ylab = NA, axes = FALSE)
	
	y0 = 2
	y1 = y0 + bh
	
	x0 = (xmax - nbcols)/2 + 1
	for (i in 1:nbcols){
		polygon(c(x0 - 1, x0, x0, x0 - 1), c(y0, y0, y1, y1), col = cols[i], border = NA)
		x0 = x0 + 1
	}
	
	x0 = (xmax - nbcols)/2
	x1 = xmax  - x0
	polygon(c(x0, x1, x1, x0), c(y0, y0, y1, y1), col = NA, border = "black")
	
	for(i in 0:coeffmax){
		x = x0 + nbcols / coeffmax * i
		y = .2 * bh
		lines(c(x,x), c(y0, y0 + y))
		y = y0 - .15 * bh
		text(x, y, paste0("x", i), adj = c(.5, 1), cex = 1.1, font = 2)
	}
	
	x = x0 + nbcols / 2
	y = y1 + .2 * bh
	text(x, y, "Evolution relative du besoin en eau pour la production\nde neige par rapport à la référence historique (1976-2005)", adj = c(.5,0), cex = 1.2, font = 2)
	
	legcol = scol
	leglab = "SAFRAN (1976-2005)"
	legend(x0, ylim[2] + 2.3, leglab, pch = NA, fill = legcol, bty = 'n', cex = 1.2)	
	
	print("done")
    dev.off()
    embed_fonts(path)
}


######################################################################################
dst_dir_base = '/home/francois/data/'
# mthlist = c('djf')

inds = c('7331A')

for (ind in inds){
	query = paste0("select name from crosscut.resorts where ind = '", ind, "';")
	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	rname <- dbGetQuery(con, query)
	dbDisconnect(con)
	dst_dir_resort = paste0(dst_dir_base, rname[1,1], "/")
	dir.create(dst_dir_resort)
	
	query = paste0("select envid from crosscut.envelopes where ind = '", ind, "';")
	con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	envidlist <- dbGetQuery(con, query)
	dbDisconnect(con)
	for (i in 1:nrow(envidlist)){
		envid = envidlist[i,1]
		dst_dir = paste0(dst_dir_resort, "results/")
		dir.create(dst_dir)
		dst_dir = paste0(dst_dir, envid, "/")
		dir.create(dst_dir)
		dst_dir = paste0(dst_dir, "tracc_mth/")
		dir.create(dst_dir)
		
		#snowtypes
		query = paste0("
			select distinct status from crosscut.sm_resort a
			join crosscut.envelopes b on st_intersects(a.geom, b.geom)
			where a.ind = '",ind,"' and b.envid = ", envid,"
			order by 1
		")
		con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
		liststatus <- dbGetQuery(con, query)
		dbDisconnect(con)
		
		nntypes = c("gro", "nn")
		nntypes = paste0("ns&gro_", nntypes)
		
		if (nrow(liststatus) > 0){
			# smtypes = c("lance", "fan")
			smtypes = c("lance")
			smtypes2 = c()
			for (j in 1:nrow(liststatus)){
				smtypes2 = c(smtypes2, paste(liststatus[j,1], "_", smtypes, sep = ""))
			}
			snowtypes = c(nntypes, smtypes2)
		} else {
			snowtypes = nntypes
		}
		for (snowtype in snowtypes){
			mthlist = c('dec', 'jan', 'apr', 'djf', 'ndjfma')
			for (mth in mthlist){
				print(paste0("envid: ", envid, " - snowtype:", snowtype, " - mth: ", mth))
				plot_idx_tracc(dst_dir, envid, mth, snowtype)
			}
			
			if (endsWith(snowtype, "lance")){
				mthlist = c('nov', 'dec', 'djf', 'ndjfma')
				for (mth in mthlist){
					print(paste0("envid: ", envid, " - snowtype:", snowtype, " - mth: ", mth))
					plot_idx_tracc(dst_dir, envid, mth, snowtype)
						print(paste0("WC envid: ", envid, " - snowtype:", snowtype, " - mth: ", mth))
						plot_wc_tracc(dst_dir, envid, mth, snowtype)
				}
			}
		}
	}	
}


