library(RPostgreSQL)

#GLOBAL PARAMETER
drv = dbDriver("PostgreSQL")
host = "10.38.192.30"
user = "postgres"
pwd = "lceslt"
db = "db"

alphafill = 50
alphaborder = 70

#Clim period configuration
cbyear = 1986
ceyear = 2016 #year exlude by between statement

no_more_no_less = function (x, lim){
  if (x > lim[2]){
    x = lim[2]
  } else if (x < lim[1]){
    x = lim[1]
  }
  return(x)
}

plot_idx = function (dst_dir,envid, name, snowtype, period, q20type, q20 = NULL){
  #####COMPUTE SAFRAN Q20
  if (is.null(q20)) {
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      query = paste0("
      select djf--percentile_cont(0.2) within group (order by djf) q20
      from viability.idx_mth
      where snowtype ilike '",q20type,"' and model = 'safran' and scenario = 'reanalysis'
      and season between ",cbyear," and ",ceyear," and envid = ",envid,";
      ")
      q20 <- dbGetQuery(con, query)
      dbDisconnect(con)
      q20 = quantile(q20[,1], probs = c(0.2))
  }
  
  #####GRAPH PARAMETERS
  scenarios = c("historical", "rcp26", "rcp45", "rcp85")
  cols = c("#878787", "#08519c","#80cdc1", "#cb181d")
  types = c("poly", "lines")
  delta = (period - 1)/2
  
  #XLIM and YLIM
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  query = paste0("
  select min(season) + ",delta," , max(season) - ",delta,"
  from viability.idx_mth
  ")
  xlim <- dbGetQuery(con, query)
  dbDisconnect(con)
  xlim = as.numeric(xlim)
  ylim = c(0,100)
  prettyx = pretty(xlim)
  
  #GRAPH OUTPUT
  png(width = 800, height = 600, paste0(dst_dir, tolower(gsub(" ", "_", paste0("indice_djf_",envid,"_",snowtype,"_",xlim[1],"_",xlim[2],"_q20",q20type,".png")))), res = 150)
  par(mar=c(3,3,2,0))
  
  ######PLOTTING
  #SETUP DEVICE
  if (strsplit(snowtype,'_')[[1]][1] == "ns&gro"){
    if (strsplit(snowtype,'_')[[1]][2] == "nn"){
      st = "neige naturelle"
    } else {
      st = "neige naturelle damée"
    }
  } else {
    typesm = intToUtf8(rev(utf8ToInt(strsplit(intToUtf8(rev(utf8ToInt(snowtype))),'_')[[1]][1])))
    envsm = gsub(paste0("_",typesm),"",snowtype)
    if ( typesm == "lance") {
      st = paste0("neige de culture (",envsm, " - bi-fluide)")
    } else {
      st = paste0("neige de culture (",envsm, " - mono-fluide)")
    }
  }
  main = paste0(name," :\n",st)
  plot(0,0, type = 'n', main = main, cex.main = .8, xlim = xlim, ylim = ylim, , axes = F, xlab = NA, ylab =NA)
  
  #PLOT BACKGROUND
  for (i in 1:(length(prettyx) - 1)){
    if (i / 2 == round(i / 2)){
      col = "#F0F0F0"
    } else {
      col = "#FFFFFF"
    }
    polygon(c(prettyx[i],prettyx[i],prettyx[i+1],prettyx[i+1]), c(ylim,rev(ylim)), col = col, border = NA)
  }
  for (j in pretty(ylim)[2:length(pretty(ylim))]){
    lines(xlim, c(j,j), col = 'lightgrey', lty = 2, lwd = 1)
  }
  
  #PLOT EXPERIMENT TIME SERIES
  for (t in types){
    i = 0
    print(t)
    for (scenario in scenarios){
      print(scenario)
      i = i + 1
      query = paste0("
                     with a as (
                     select a.season, a.model,
                     min(min(a.season)) over() smin, max(max(a.season)) over() smax,
                     avg(b.djf) mean
                     from viability.idx_mth a
                     join viability.idx_mth b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
                     and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
                     where a.snowtype = '",snowtype,"' and a.scenario = '",scenario,"'
                     and a.envid = ", envid,"
                     group by a.season, a.model
                     )
                     
                     select season, avg(mean) mean, stddev(mean) stddev
                     from a 
                     where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
                     group by season
                     order by season"
      )
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      data <- dbGetQuery(con, query)
      dbDisconnect(con)
      
      if (scenario == "historical"){
        query = paste0("
                       with a as (
                       select max(season) - ",delta," hmax from viability.idx_mth
                       where snowtype = '",snowtype,"' and scenario = '",scenario,"'
                       and envid = ", envid,"
                       ), b as (
                       select min(season) + ",delta," emin from viability.idx_mth
                       where snowtype = '",snowtype,"' and scenario != '",scenario,"' and scenario != 'reanalysis'
                       and envid = ", envid,"
                       ), c as (
                       select c.season, c.model, avg(d.djf) mean
                       from viability.idx_mth c
                       join viability.idx_mth d on  d.season >= c.season - ",delta," and d.season <= c.season +",delta,"
                       and d.snowtype = c.snowtype and d.model = c.model and d.scenario = c.scenario and d.envid = c.envid
                       where c.snowtype = '",snowtype,"' and c.envid = ", envid,"
                       and c.season > (select hmax from a) and c.season < (select emin from b)
                       group by c.season, c.model
                       )
                       select c.season, avg(mean) mean, stddev(mean) stddev
                       from c
                       group by season
                       order by season
                       "
        )
        con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
        hist <- dbGetQuery(con, query)
        dbDisconnect(con)
        data = rbind(data,hist)
      }
      
      #export to csv
      # write.csv(data, paste0(dst_dir,"indice_",envid,"_",snowtype,"_",scenario,"_",xlim[1],"_",xlim[2],".csv"), row.names=FALSE)
      
      if (t == "poly"){
        polygon(c(data[,1], rev(data[,1])), c(sapply(data[,2] - data[,3],no_more_no_less,ylim), rev(sapply(data[,2] + data[,3],no_more_no_less,ylim))),
                border = paste0(cols[i],alphaborder), col = paste0(cols[i], alphafill))
      } else {
        par(new = T)
        plot(data[,1], data[,2], type = "l", col = cols[i], lwd = 2,
             xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
        )
      }
    }
  }
  
  #ADD SAFRAN REANALYSIS ROLLING MEAN
  query = paste0("
                 with a as (
                 select a.season, min(min(a.season)) over() smin, max(max(a.season)) over() smax,
                 avg(b.djf) mean, stddev(b.djf) stddev
                 from viability.idx_mth a
                 join viability.idx_mth b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
                 and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
                 where a.snowtype = '",snowtype,"' and a.envid = ", envid,"
                 and a.scenario = 'reanalysis' and a.model = 'safran'
                 group by a.season
                 )
                 
                 
                 select season, mean, stddev
                 from a 
                 where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
                 order by season"
  )
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  data <- dbGetQuery(con, query)
  dbDisconnect(con)
  par(new = T)
  plot(data[,1], data[,2], type = "l", col = "black", lwd = 2,
       xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
  )
  
  #ADD SAFRAN REANALYSIS ANNUAL VALUES
  query = paste0("
                 select a.season, djf
                 from viability.idx_mth a
                 where a.snowtype = '",snowtype,"' and a.envid = ", envid,"
                 and a.scenario = 'reanalysis' and a.model = 'safran'
                 order by season"
  )
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  data <- dbGetQuery(con, query)
  dbDisconnect(con)
  par(new = T)
  plot(data[,1], data[,2], type = "l", col = "#878787", lwd = 1, lty = 2, pch = 1,
       xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
  )
  
  par(new = T)
  plot(data[,1], data[,2], col = "#878787", pch = 1, cex = .6,
       xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
  )
  # text(data[,1], data[,2], round(data[,2],2), cex= .4)
  
  #ADD AXIS AND AXIS LABELS
  axis(side = 1, line = -.5, tck = -.01, labels = NA)
  axis(side = 1, line = -.5, at=xlim, labels=c("",""), lwd.ticks=0)
  axis(side=1, line = -1.2, lwd = 0, cex.axis = 0.8, font.lab=2, las = 0)
  mtext(side = 1, line = 1.5, "Saisons", cex = 1)
  
  axis(side = 2, line = -.8, tck = -.01, labels = NA)
  axis(side =2, line = -.8, at=ylim, labels=c("",""), lwd.ticks=0)
  axis(side=2, line = -1.4, lwd = 0, cex.axis = .7, font.lab=2, las = 2)
  mtext(side = 2, line = 1, "Indice de fiabilité\nde l'enneigement (%)", cex = 1) 
  
  lines(xlim, c(q20, q20), col = 'black', lty = 2, lwd = 1)
  text(xlim[0] + 2, q20 - 2, paste0(as.character(round(q20,2)),"%"), cex= .8, adj = c(0,1))
  # axis(side=2, line = -1.6, at = q20, as.character(round(q20,1)), lwd = 0, cex.axis = .7, font.lab=2, las = 2, font = 2)
  dev.off()
}

plot_idx_quantiles = function (dst_dir,envid, name, snowtype, period, q20type, q20 = NULL){
  #####COMPUTE SAFRAN Q20
  if (is.null(q20)) {
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      query = paste0("
      select djf--percentile_cont(0.2) within group (order by djf) q20
      from viability.idx_mth
      where snowtype ilike '",q20type,"' and model = 'safran' and scenario = 'reanalysis'
      and season between ",cbyear," and ",ceyear," and envid = ",envid,";
      ")
      q20 <- dbGetQuery(con, query)
      dbDisconnect(con)
	  if (nrow(q20) > 0){
		q20 = quantile(q20[,1], probs = c(0.2), na.rm = TRUE)
	  } else {
		q20 = -9999
	  }
	  print(q20)
  }
  
  if (q20 != -9999){
	  #####GRAPH PARAMETERS
	  scenarios = c("historical", "rcp26", "rcp45", "rcp85")
	  cols = c("#878787", "#08519c","#80cdc1", "#cb181d")
	  types = c("poly", "lines")
	  delta = (period - 1)/2
	  
	  #XLIM and YLIM
	  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	  query = paste0("
	  select min(season) + ",delta," , max(season) - ",delta,"
	  from viability.idx_mth
	  ")
	  xlim <- dbGetQuery(con, query)
	  dbDisconnect(con)
	  xlim = as.numeric(xlim)
	  ylim = c(0,100)
	  prettyx = pretty(xlim)
	  
	  #GRAPH OUTPUT
	  png(width = 800, height = 600, paste0(dst_dir,tolower(gsub(" ", "_", paste0("indice_djf_quantiles_",envid,"_",snowtype,"_",xlim[1],"_",xlim[2],"_q20",q20type,".png")))), res = 150)
	  par(mar=c(3,3,2,0))
	  
	  ######PLOTTING
	  #SETUP DEVICE
	  if (strsplit(snowtype,'_')[[1]][1] == "ns&gro"){
		if (strsplit(snowtype,'_')[[1]][2] == "nn"){
		  st = "neige naturelle"
		} else {
		  st = "neige naturelle damée"
		}
	  } else {
		typesm = intToUtf8(rev(utf8ToInt(strsplit(intToUtf8(rev(utf8ToInt(snowtype))),'_')[[1]][1])))
		envsm = gsub(paste0("_",typesm),"",snowtype)
		if ( typesm == "lance") {
		  st = paste0("neige de culture (",envsm, " - bi-fluide)")
		} else {
		  st = paste0("neige de culture (",envsm, " - mono-fluide)")
		}
	  }
	  main = paste0(name," :\n",st)
	  plot(0,0, type = 'n', main = main, cex.main = .8, xlim = xlim, ylim = ylim, , axes = F, xlab = NA, ylab =NA)
	  
	  #PLOT BACKGROUND
	  for (i in 1:(length(prettyx) - 1)){
		if (i / 2 == round(i / 2)){
		  col = "#F0F0F0"
		} else {
		  col = "#FFFFFF"
		}
		polygon(c(prettyx[i],prettyx[i],prettyx[i+1],prettyx[i+1]), c(ylim,rev(ylim)), col = col, border = NA)
	  }
	  for (j in pretty(ylim)[2:length(pretty(ylim))]){
		lines(xlim, c(j,j), col = 'lightgrey', lty = 2, lwd = 1)
	  }
	  
	  #PLOT EXPERIMENT TIME SERIES
	  for (t in types){
		i = 0
		print(t)
		for (scenario in scenarios){
		  print(scenario)
		  i = i + 1
		  query = paste0("
						 with a as (
						 select a.season,
						 min(min(a.season)) over() smin, max(max(a.season)) over() smax,
						 percentile_cont(0.2) within group (order by b.djf) q20,
						 percentile_cont(0.5) within group (order by b.djf) q50,
						 percentile_cont(0.8) within group (order by b.djf) q80
						 from viability.idx_mth a
						 join viability.idx_mth b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
						 and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
						 where a.snowtype = '",snowtype,"' and a.scenario = '",scenario,"'
						 and a.envid = ", envid,"
						 group by a.season
						 )
						 
						 select season, q20, q50, q80
						 from a 
						 where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
						 order by season"
		  )
		  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
		  data <- dbGetQuery(con, query)
		  dbDisconnect(con)
		  
		  if (scenario == "historical"){
			query = paste0("
						   with a as (
						   select max(season) - ",delta," hmax from viability.idx_mth
						   where snowtype = '",snowtype,"' and scenario = '",scenario,"'
						   and envid = ", envid,"
						   ), b as (
						   select min(season) + ",delta," emin from viability.idx_mth
						   where snowtype = '",snowtype,"' and scenario != '",scenario,"' and scenario != 'reanalysis'
						   and envid = ", envid,"
						   )
						   
						   select c.season,
						   percentile_cont(0.2) within group (order by d.djf) q20,
						   percentile_cont(0.5) within group (order by d.djf) q50,
						   percentile_cont(0.8) within group (order by d.djf) q80
						   from viability.idx_mth c
						   join viability.idx_mth d on  d.season >= c.season - ",delta," and d.season <= c.season +",delta,"
						   and d.snowtype = c.snowtype and d.model = c.model and d.scenario = c.scenario and d.envid = c.envid
						   where c.snowtype = '",snowtype,"' and c.envid = ", envid,"
						   and c.season > (select hmax from a) and c.season < (select emin from b)
						   group by c.season
						   order by c.season
						   "
			)
			con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
			hist <- dbGetQuery(con, query)
			dbDisconnect(con)
			data = rbind(data,hist)
		  }
		  
		  #export to csv
		  # write.csv(data, paste0(dst_dir,"indice_quantiles_",envid,"_",snowtype,"_",scenario,"_",xlim[1],"_",xlim[2],".csv"), row.names=FALSE)
		  
		  if (t == "poly"){
			polygon(c(data[,1], rev(data[,1])), c(sapply(data[,2],no_more_no_less,ylim), rev(sapply(data[,4],no_more_no_less,ylim))),
					border = paste0(cols[i],alphaborder), col = paste0(cols[i], alphafill))
		  } else {
			par(new = T)
			plot(data[,1], data[,3], type = "l", col = cols[i], lwd = 2,
				 xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
			)
		  }
		}
	  }
	  
	  #ADD SAFRAN REANALYSIS ROLLING MEAN
	  query = paste0("
					 with a as (
					 select a.season, min(min(a.season)) over() smin, max(max(a.season)) over() smax,
					 percentile_cont(0.2) within group (order by b.djf) q20,
					 percentile_cont(0.5) within group (order by b.djf) q50,
					 percentile_cont(0.8) within group (order by b.djf) q80
					 from viability.idx_mth a
					 join viability.idx_mth b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
					 and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
					 where a.snowtype = '",snowtype,"' and a.envid = ", envid,"
					 and a.scenario = 'reanalysis' and a.model = 'safran'
					 group by a.season
					 )
					 
					 
					 select season, q20, q50, q80
					 from a 
					 where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
					 order by season"
	  )
	  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	  data <- dbGetQuery(con, query)
	  dbDisconnect(con)
	  par(new = T)
	  plot(data[,1], data[,3], type = "l", col = "black", lwd = 2,
		   xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
	  )
	  
	  #ADD SAFRAN REANALYSIS ANNUAL VALUES
	  query = paste0("
					 select a.season, djf
					 from viability.idx_mth a
					 where a.snowtype = '",snowtype,"' and a.envid = ", envid,"
					 and a.scenario = 'reanalysis' and a.model = 'safran'
					 order by season"
	  )
	  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	  data <- dbGetQuery(con, query)
	  dbDisconnect(con)
	  par(new = T)
	  plot(data[,1], data[,2], type = "l", col = "#878787", lwd = 1, lty = 2, pch = 1,
		   xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
	  )
	  
	  par(new = T)
	  plot(data[,1], data[,2], col = "#878787", pch = 1, cex = .6,
		   xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
	  )
	  # text(data[,1], data[,2], round(data[,2],2), cex= .4)
	  
	  #ADD AXIS AND AXIS LABELS
	  axis(side = 1, line = -.5, tck = -.01, labels = NA)
	  axis(side = 1, line = -.5, at=xlim, labels=c("",""), lwd.ticks=0)
	  axis(side=1, line = -1.2, lwd = 0, cex.axis = 0.8, font.lab=2, las = 0)
	  mtext(side = 1, line = 1.5, "Saisons", cex = 1)
	  
	  axis(side = 2, line = -.8, tck = -.01, labels = NA)
	  axis(side =2, line = -.8, at=ylim, labels=c("",""), lwd.ticks=0)
	  axis(side=2, line = -1.4, lwd = 0, cex.axis = .7, font.lab=2, las = 2)
	  mtext(side = 2, line = 1, "Indice de fiabilité\nde l'enneigement (%)", cex = 1) 
	  
	  lines(xlim, c(q20, q20), col = 'black', lty = 2, lwd = 1)
	  text(xlim[0] + 2, q20 - 2, paste0(as.character(round(q20,2)),"%"), cex= .8, adj = c(0,1))
	  # axis(side=2, line = -1.6, at = q20, as.character(round(q20,1)), lwd = 0, cex.axis = .7, font.lab=2, las = 2, font = 2)
	  dev.off()
	}
}

plot_freq = function (dst_dir,envid, name, snowtype, period, q20type, q20 = NULL){
  #####COMPUTE SAFRAN Q20 1961-1990
  if (is.null(q20)) {
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      query = paste0("
      select djf --percentile_cont(0.2) within group (order by djf) q20
      from viability.idx_mth
      where snowtype ilike '",q20type,"' and model = 'safran' and scenario = 'reanalysis'
      and season between ",cbyear," and ",ceyear," and envid = ",envid,";
      ")
      q20 <- dbGetQuery(con, query)
      dbDisconnect(con)
      q20 = quantile(q20[,1], probs = c(0.2))
  }
  
  #####GRAPH PARAMETERS
  scenarios = c("historical", "rcp26", "rcp45", "rcp85")
  cols = c("#878787", "#08519c","#80cdc1", "#cb181d")
  types = c("poly", "lines")
  delta = (period - 1) / 2
  
  #XLIM and YLIM
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  query = paste0("
  select min(season) + ",delta," , max(season) - ",delta,"
  from viability.idx_mth
  ")
  xlim <- dbGetQuery(con, query)
  dbDisconnect(con)
  xlim = as.numeric(xlim)
  ylim = c(0,100)
  prettyx = pretty(xlim)
  
  #GRAPH OUTPUT
  png(width = 800, height = 600, paste0(dst_dir,tolower(gsub(" ", "_", paste0("indice_djf_quantiles_freq20_",envid,"_",snowtype,"_",xlim[1],"_",xlim[2],"_q20",q20type,".png")))), res = 150)
  par(mar=c(3,3,2,0))
  
  ######PLOTTING
  #SETUP DEVICE
  
  typesm = intToUtf8(rev(utf8ToInt(strsplit(intToUtf8(rev(utf8ToInt(snowtype))),'_')[[1]][1])))
  if (strsplit(snowtype,'_')[[1]][1] == "ns&gro"){
    if (strsplit(snowtype,'_')[[1]][2] == "nn"){
      st = "neige naturelle"
    } else {
      st = "neige naturelle damée"
    }
  } else {
    # typesm = intToUtf8(rev(utf8ToInt(strsplit(intToUtf8(rev(utf8ToInt(snowtype))),'_')[[1]][1])))
    envsm = gsub(paste0("_",typesm),"",snowtype)
    if (typesm == "lance") {
      st = paste0("neige de culture (",envsm, "- bi-fluide)")
    } else {
      st = paste0("neige de culture (",envsm, "- mono-fluide)")
    }
  }
  main = paste0(name," :\n",st)
  plot(0,0, type = 'n', main = main, cex.main = .8, xlim = xlim, ylim = ylim, , axes = F, xlab = NA, ylab =NA)
  
  #PLOT BACKGROUND
  for (i in 1:(length(prettyx) - 1)){
    if (i / 2 == round(i / 2)){
      col = "#F0F0F0"
    } else {
      col = "#FFFFFF"
    }
    polygon(c(prettyx[i],prettyx[i],prettyx[i+1],prettyx[i+1]), c(ylim,rev(ylim)), col = col, border = NA)
  }
  for (j in pretty(ylim)[2:length(pretty(ylim))]){
    lines(xlim, c(j,j), col = 'lightgrey', lty = 2, lwd = 1)
  }
  
  #PLOT EXPERIMENT TIME SERIES
  for (t in types){
    i = 0
    print(t)
    for (scenario in scenarios){
      print(scenario)
      i = i + 1
      query = paste0("
                     with a as (
                     select a.season, a.model,
                     min(min(a.season)) over() smin, max(max(a.season)) over() smax,
                     (sum(case when b.djf <= ",q20," then 1 else 0 end))::float8 * 100 / sum(case when b.season is not null then 1 else 0 end) freq
                     from viability.idx_mth a
                     join viability.idx_mth b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
                     and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
                     where a.snowtype = '",snowtype,"' and a.scenario = '",scenario,"'
                     and a.envid = ", envid,"
                     group by a.season, a.model
                     )
                     
                     select season, avg(freq) mean, stddev(freq) stddev
                     from a 
                     where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
                     group by season
                     order by season"
      )
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      data <- dbGetQuery(con, query)
      dbDisconnect(con)
      
      if (scenario == "historical"){
        query = paste0("
                       with a as (
                       select max(season) - ",delta," hmax from viability.idx_mth
                       where snowtype = '",snowtype,"' and scenario = '",scenario,"'
                       and envid = ", envid,"
                       ), b as (
                       select min(season) + ",delta," emin from viability.idx_mth
                       where snowtype = '",snowtype,"' and scenario != '",scenario,"' and scenario != 'reanalysis'
                       and envid = ", envid,"
                       ), c as (
                       select c.season, c.model,
                       (sum(case when d.djf <= ",q20," then 1 else 0 end))::float8 * 100 / sum(case when d.season is not null then 1 else 0 end) freq
                       from viability.idx_mth c
                       join viability.idx_mth d on  d.season >= c.season - ",delta," and d.season <= c.season +",delta,"
                       and d.snowtype = c.snowtype and d.model = c.model and d.scenario = c.scenario and d.envid = c.envid
                       where c.snowtype = '",snowtype,"' and c.envid = ", envid,"
                       and c.season > (select hmax from a) and c.season < (select emin from b)
                       group by c.season, c.model
                       )
                       select c.season, avg(freq) mean, stddev(freq) stddev
                       from c
                       group by season
                       order by season
                       "
        )
        con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
        hist <- dbGetQuery(con, query)
        dbDisconnect(con)
        data = rbind(data,hist)
      }
      
      #export to csv
      # write.csv(data, paste0(dst_dir,"indice_quantiles_freq_q20_",envid,"_",snowtype,"_",scenario,"_",xlim[1],"_",xlim[2],"_q20",q20type,".csv"), row.names=FALSE)
      
      if (t == "poly"){
        polygon(c(data[,1], rev(data[,1])), c(sapply(data[,2] - data[,3], no_more_no_less,ylim), rev(sapply(data[,2] + data[,3],no_more_no_less,ylim))),
                border = paste0(cols[i],alphaborder), col = paste0(cols[i], alphafill))
      } else {
        par(new = T)
        plot(data[,1], data[,2], type = "l", col = cols[i], lwd = 2,
             xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
        )
      }
    }
  }
  
  #ADD SAFRAN REANALYSIS ROLLING MEAN
  query = paste0("
                 with a as (
                 select a.season, min(min(a.season)) over() smin, max(max(a.season)) over() smax,
                 (sum(case when b.djf <= ",q20," then 1 else 0 end))::float8 * 100 / sum(case when b.season is not null then 1 else 0 end) freq
                 from viability.idx_mth a
                 join viability.idx_mth b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
                 and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
                 where a.snowtype = '",snowtype,"' and a.envid = ", envid,"
                 and a.scenario = 'reanalysis' and a.model = 'safran'
                 group by a.season
                 )
                 
                 
                 select season, freq
                 from a 
                 where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
                 order by season"
  )
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  data <- dbGetQuery(con, query)
  dbDisconnect(con)
  par(new = T)
  plot(data[,1], data[,2], type = "l", col = "black", lwd = 2,
       xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
  )
  
  #ADD AXIS AND AXIS LABELS
  axis(side = 1, line = -.5, tck = -.01, labels = NA)
  axis(side = 1, line = -.5, at=xlim, labels=c("",""), lwd.ticks=0)
  axis(side=1, line = -1.2, lwd = 0, cex.axis = 0.8, font.lab=2, las = 0)
  mtext(side = 1, line = 1.5, "Saisons", cex = 1)
  
  axis(side = 2, line = -.8, tck = -.01, labels = NA)
  axis(side =2, line = -.8, at=ylim, labels=c("",""), lwd.ticks=0)
  axis(side=2, line = -1.4, lwd = 0, cex.axis = .7, font.lab=2, las = 2)
  mtext(side = 2, line = 1, "Fréquence des saisons\nsous le Q20 (%)", cex = 1)
  
  if (q20type == 'ns&gro_gro'){
    qname = "Q20 neige naturelle damée"
  } else {
    if (typesm == "lance") {
        qname = "Q20 neige de culture (bi-fluide)"
    } else {
        qname = "Q20 neige de culture (mono-fluide)"
    }
  }
  
  text(xlim[1] + 2, ylim[2] -2, paste0(qname,"\n", as.character(round(q20,2)),"%"), cex= .8, adj = c(0,1))
  
  dev.off()
}

plot_freq_quantiles = function (dst_dir,envid, name, snowtype, period, q20type, q20 = NULL){
  #####COMPUTE SAFRAN Q20 1961-1990
  if (is.null(q20)) {
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      query = paste0("
      select djf --percentile_cont(0.2) within group (order by djf) q20
      from viability.idx_mth
      where snowtype ilike '",q20type,"' and model = 'safran' and scenario = 'reanalysis'
      and season between ",cbyear," and ",ceyear," and envid = ",envid,";
      ")
      q20 <- dbGetQuery(con, query)
      dbDisconnect(con)
	  if (nrow(q20) > 0){
		q20 = quantile(q20[,1], probs = c(0.2), na.rm = TRUE)
	  } else {
		q20 = -9999
	  }
    }
  
  #####GRAPH PARAMETERS
  if (q20 != -9999){
	  scenarios = c("historical", "rcp26", "rcp45", "rcp85")
	  cols = c("#878787", "#08519c","#80cdc1", "#cb181d")
	  types = c("poly", "lines")
	  delta = (period - 1) / 2
	  
	  #XLIM and YLIM
	  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	  query = paste0("
	  select min(season) + ",delta," , max(season) - ",delta,"
	  from viability.idx_mth
	  ")
	  xlim <- dbGetQuery(con, query)
	  dbDisconnect(con)
	  xlim = as.numeric(xlim)
	  ylim = c(0,100)
	  prettyx = pretty(xlim)
	  
	  #GRAPH OUTPUT
	  png(width = 800, height = 600, paste0(dst_dir,tolower(gsub(" ", "_", paste0("indice_djf_freq20_",envid,"_",snowtype,"_",xlim[1],"_",xlim[2],"_q20",q20type,".png")))), res = 150)
	  par(mar=c(3,3,2,0))
	  
	  ######PLOTTING
	  #SETUP DEVICE
	  typesm = intToUtf8(rev(utf8ToInt(strsplit(intToUtf8(rev(utf8ToInt(snowtype))),'_')[[1]][1])))
	  if (strsplit(snowtype,'_')[[1]][1] == "ns&gro"){
		if (strsplit(snowtype,'_')[[1]][2] == "nn"){
		  st = "neige naturelle"
		} else {
		  st = "neige naturelle damée"
		}
	  } else {
		# typesm = intToUtf8(rev(utf8ToInt(strsplit(intToUtf8(rev(utf8ToInt(snowtype))),'_')[[1]][1])))
		envsm = gsub(paste0("_",typesm),"",snowtype)
		if ( typesm == "lance") {
		  st = paste0("neige de culture (",envsm, "- bi-fluide)")
		} else {
		  st = paste0("neige de culture (",envsm, "- mono-fluide)")
		}
	  }
	  main = paste0(name," :\n",st)
	  plot(0,0, type = 'n', main = main, cex.main = .8, xlim = xlim, ylim = ylim, , axes = F, xlab = NA, ylab =NA)
	  
	  #PLOT BACKGROUND
	  for (i in 1:(length(prettyx) - 1)){
		if (i / 2 == round(i / 2)){
		  col = "#F0F0F0"
		} else {
		  col = "#FFFFFF"
		}
		polygon(c(prettyx[i],prettyx[i],prettyx[i+1],prettyx[i+1]), c(ylim,rev(ylim)), col = col, border = NA)
	  }
	  for (j in pretty(ylim)[2:length(pretty(ylim))]){
		lines(xlim, c(j,j), col = 'lightgrey', lty = 2, lwd = 1)
	  }
	  
	  #PLOT EXPERIMENT TIME SERIES
	  for (t in types){
		i = 0
		print(t)
		for (scenario in scenarios){
		  print(scenario)
		  i = i + 1
		  query = paste0("
						 with a as (
						 select a.season, a.model,
						 min(min(a.season)) over() smin, max(max(a.season)) over() smax,
						 (sum(case when b.djf <= ",q20," then 1 else 0 end))::float8 * 100 / sum(case when b.season is not null then 1 else 0 end) freq
						 from viability.idx_mth a
						 join viability.idx_mth b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
						 and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
						 where a.snowtype = '",snowtype,"' and a.scenario = '",scenario,"'
						 and a.envid = ", envid,"
						 group by a.season, a.model
						 )
						 
						 select season,
						 percentile_cont(0.2) within group (order by freq) q20,
						 percentile_cont(0.5) within group (order by freq) q50,
						 percentile_cont(0.8) within group (order by freq) q80
						 from a 
						 where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
						 group by season
						 order by season"
		  )
		  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
		  data <- dbGetQuery(con, query)
		  dbDisconnect(con)
		  
		  if (scenario == "historical"){
			query = paste0("
						   with a as (
						   select max(season) - ",delta," hmax from viability.idx_mth
						   where snowtype = '",snowtype,"' and scenario = '",scenario,"'
						   and envid = ", envid,"
						   ), b as (
						   select min(season) + ",delta," emin from viability.idx_mth
						   where snowtype = '",snowtype,"' and scenario != '",scenario,"' and scenario != 'reanalysis'
						   and envid = ", envid,"
						   ), c as (
						   select c.season, c.model,
						   (sum(case when d.djf <= ",q20," then 1 else 0 end))::float8 * 100 / sum(case when d.season is not null then 1 else 0 end) freq
						   from viability.idx_mth c
						   join viability.idx_mth d on  d.season >= c.season - ",delta," and d.season <= c.season + ",delta,"
						   and d.snowtype = c.snowtype and d.model = c.model and d.scenario = c.scenario and d.envid = c.envid
						   where c.snowtype = '",snowtype,"' and c.envid = ", envid,"
						   and c.season > (select hmax from a) and c.season < (select emin from b)
						   group by c.season, c.model
						   )
						   select c.season,
						   percentile_cont(0.2) within group (order by freq) q20,
						   percentile_cont(0.5) within group (order by freq) q50,
						   percentile_cont(0.8) within group (order by freq) q80
						   from c
						   group by season
						   order by season
						   "
			)
			con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
			hist <- dbGetQuery(con, query)
			dbDisconnect(con)
			data = rbind(data,hist)
		  }
		  
		  #export to csv
		  # write.csv(data, paste0(dst_dir,"indice_freq_q20_",envid,"_",snowtype,"_",scenario,"_",xlim[1],"_",xlim[2],"_q20",q20type,".csv"), row.names=FALSE)
		  
		  if (t == "poly"){
			polygon(c(data[,1], rev(data[,1])), c(sapply(data[,2], no_more_no_less,ylim), rev(sapply(data[,4],no_more_no_less,ylim))),
					border = paste0(cols[i],alphaborder), col = paste0(cols[i], alphafill))
		  } else {
			par(new = T)
			plot(data[,1], data[,3], type = "l", col = cols[i], lwd = 2,
				 xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
			)
		  }
		}
	  }
	  
	  #ADD SAFRAN REANALYSIS ROLLING MEAN
	  query = paste0("
					 with a as (
					 select a.season, min(min(a.season)) over() smin, max(max(a.season)) over() smax,
					 (sum(case when b.djf <= ",q20," then 1 else 0 end))::float8 * 100 / sum(case when b.season is not null then 1 else 0 end) freq
					 from viability.idx_mth a
					 join viability.idx_mth b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
					 and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
					 where a.snowtype = '",snowtype,"' and a.envid = ", envid,"
					 and a.scenario = 'reanalysis' and a.model = 'safran'
					 group by a.season
					 )
					 
					 
					 select season, freq
					 from a 
					 where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
					 order by season"
	  )
	  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
	  data <- dbGetQuery(con, query)
	  dbDisconnect(con)
	  par(new = T)
	  plot(data[,1], data[,2], type = "l", col = "black", lwd = 2,
		   xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
	  )
	  
	  #ADD AXIS AND AXIS LABELS
	  axis(side = 1, line = -.5, tck = -.01, labels = NA)
	  axis(side = 1, line = -.5, at=xlim, labels=c("",""), lwd.ticks=0)
	  axis(side=1, line = -1.2, lwd = 0, cex.axis = 0.8, font.lab=2, las = 0)
	  mtext(side = 1, line = 1.5, "Saisons", cex = 1)
	  
	  axis(side = 2, line = -.8, tck = -.01, labels = NA)
	  axis(side =2, line = -.8, at=ylim, labels=c("",""), lwd.ticks=0)
	  axis(side=2, line = -1.4, lwd = 0, cex.axis = .7, font.lab=2, las = 2)
	  mtext(side = 2, line = 1, "Fréquence des saisons\nsous le Q20 (%)", cex = 1)
	  
	  # if (strsplit(q20type,'_')[[1]][1] == 'ns&gro'){
		# if (strsplit(q20type,'_')[[1]][2] == 'gro') {
			# qname = "Q20 neige naturelle damée"
		# } else if (strsplit(q20type,'_')[[1]][2] == 'nn') {
			# qname = "Q20 neige naturelle"
		# }
	  # } else {
		# typesm = intToUtf8(rev(utf8ToInt(strsplit(intToUtf8(rev(utf8ToInt(q20type))),'_')[[1]][1])))
		# if (typesm == "lance") {
			# qname = "Q20 neige de culture (bi-fluide)"
		# } else {
			# qname = "Q20 neige de culture (mono-fluide)"
		# }
	  # }
	  
	  # text(xlim[1] + 2, ylim[2] -2, paste0(qname,"\n", as.character(round(q20,2)),"%"), cex= .8, adj = c(0,1))
	  
	  text(xlim[1] + 2, ylim[2] -2, paste0("Q20 : ", as.character(round(q20,2)),"%"), cex= .8, adj = c(0,1))
	  
	  dev.off()
	}
}

plot_wc = function (dst_dir,envid, name, snowtype, period){
  #####GRAPH PARAMETERS
  scenarios = c("historical", "rcp26", "rcp45", "rcp85")
  cols = c("#878787", "#08519c","#80cdc1", "#cb181d")
  types = c("poly", "lines")
  delta = (period - 1) / 2
  
  #XLIM and YLIM
  query = paste0("
  select min(season) + ",delta," , max(season) - ",delta,"
  from viability.idx
  ")
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  xlim <- dbGetQuery(con, query)
  dbDisconnect(con)
  xlim = as.numeric(xlim)
  
  query = paste0("
  with a as(
    select season, scenario, avg(wc) wc from viability.wc
    where envid = ",envid," and model != 'safran'
    group by 1,2
    union
    select season, scenario, wc from viability.wc
    where envid = ",envid," and model = 'safran'
    )
    select coalesce(max(wc),0) * 0.1 from a;")
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  ymax <- dbGetQuery(con, query)
  dbDisconnect(con)
  if (ymax == 0) {# case when no snowmaking, i.e. nothing to plot here !
    print("nothing to plot")
  } else {
      ymax = as.numeric(ymax)
      ylim = c(0,ymax)
      
      prettyx = pretty(xlim)
      
      #GRAPH OUTPUT
      png(width = 800, height = 600, paste0(dst_dir,tolower(gsub(" ", "_", paste0("wc_",envid,"_",snowtype,"_",xlim[1],"_",xlim[2],".png")))), res = 150)
      par(mar=c(3,3,2,0))
      
      ######PLOTTING
      #SETUP DEVICE
     
      typesm = intToUtf8(rev(utf8ToInt(strsplit(intToUtf8(rev(utf8ToInt(snowtype))),'_')[[1]][1])))
      envsm = gsub(paste0("_",typesm),"",snowtype)
      if ( typesm == "lance") {
        st = paste0("neige de culture (",envsm, " - bi-fluide)")
      } else {
        st = paste0("neige de culture (",envsm, " - mono-fluide)")
      }
        
      main = paste0(name," :\n",st)
      plot(0,0, type = 'n', main = main, cex.main = .8, xlim = xlim, ylim = ylim, , axes = F, xlab = NA, ylab =NA)
      
      #PLOT BACKGROUND
      for (i in 1:(length(prettyx) - 1)){
        if (i / 2 == round(i / 2)){
          col = "#F0F0F0"
        } else {
          col = "#FFFFFF"
        }
        print(col)
        polygon(c(prettyx[i],prettyx[i],prettyx[i+1],prettyx[i+1]), c(ylim,rev(ylim)), col = col, border = NA)
      }
      for (j in pretty(ylim)[2:length(pretty(ylim))]){
        lines(xlim, c(j,j), col = 'lightgrey', lty = 2, lwd = 1)
      }
      
      #PLOT EXPERIMENT TIME SERIES
      for (t in types){
        i = 0
        for (scenario in scenarios){
          print(t)
          print(scenario)
          i = i + 1
          query = paste0("
                         with a as (
                         select a.season, a.model,
                         min(min(a.season)) over() smin, max(max(a.season)) over() smax,
                         avg(b.wc) mean
                         from viability.wc a
                         join viability.wc b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
                         and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
                         where a.snowtype = '",snowtype,"' and a.scenario = '",scenario,"'
                         and a.envid = ", envid,"
                         group by a.season, a.model
                         )
                         
                         select season, avg(mean) * 0.1 mean, stddev(mean) * 0.1 stddev
                         from a 
                         where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
                         group by season
                         order by season"
          )
          con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
          data <- dbGetQuery(con, query)
          dbDisconnect(con)
          
          if (scenario == "historical"){
            query = paste0("
                           with a as (
                           select max(season) - ",delta," hmax from viability.idx
                           where snowtype = '",snowtype,"' and scenario = '",scenario,"'
                           and envid = ", envid,"
                           ), b as (
                           select min(season) + ",delta," emin from viability.idx
                           where snowtype = '",snowtype,"' and scenario != '",scenario,"' and scenario != 'reanalysis'
                           and envid = ", envid,"
                           ), c as (
                           select c.season, c.model, avg(d.wc) mean
                           from viability.wc c
                           join viability.wc d on  d.season >= c.season - ",delta," and d.season <= c.season +",delta,"
                           and d.snowtype = c.snowtype and d.model = c.model and d.scenario = c.scenario and d.envid = c.envid
                           where c.snowtype = '",snowtype,"' and c.envid = ", envid,"
                           and c.season > (select hmax from a) and c.season < (select emin from b)
                           group by c.season, c.model
                           )
                           select c.season, avg(mean) * 0.1 mean, stddev(mean) * 0.1 stddev
                           from c
                           group by season
                           order by season
                           "
            )
            con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
            hist <- dbGetQuery(con, query)
            dbDisconnect(con)
            data = rbind(data,hist)
          }
          
          #save to csv
          # write.csv(data, paste0(dst_dir,"wc_",envid,"_",snowtype,"_",scenario,"_",xlim[1],"_",xlim[2],".csv"), row.names=FALSE)
      
          if (t == "poly"){
            polygon(c(data[,1], rev(data[,1])), c(sapply(data[,2] - data[,3],no_more_no_less,ylim), rev(sapply(data[,2] + data[,3],no_more_no_less,ylim))),
                    border = paste0(cols[i],alphaborder), col = paste0(cols[i], alphafill))
          } else {
            par(new = T)
            plot(data[,1], data[,2], type = "l", col = cols[i], lwd = 2,
                 xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
            )
          }
        }
      }
      
      #ADD SAFRAN REANALYSIS ROLLING MEAN
      query = paste0("
                     with a as (
                     select a.season, min(min(a.season)) over() smin, max(max(a.season)) over() smax,
                     avg(b.wc) mean, stddev(b.wc) stddev
                     from viability.wc a
                     join viability.wc b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
                     and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
                     where a.snowtype = '",snowtype,"' and a.envid = ", envid,"
                     and a.scenario = 'reanalysis' and a.model = 'safran'
                     group by a.season
                     )
                     
                     
                     select season, mean * 0.1, stddev * 0.1
                     from a 
                     where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
                     order by season"
      )
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      data <- dbGetQuery(con, query)
      dbDisconnect(con)
      par(new = T)
      plot(data[,1], data[,2], type = "l", col = "black", lwd = 2,
           xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
      )
      
      #ADD SAFRAN REANALYSIS ANNUAL VALUES
      query = paste0("
                     select a.season, wc * 0.1
                     from viability.wc a
                     where a.snowtype = '",snowtype,"' and a.envid = ", envid,"
                     and a.scenario = 'reanalysis' and a.model = 'safran'
                     order by season"
      )
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      data <- dbGetQuery(con, query)
      dbDisconnect(con)
      par(new = T)
      plot(data[,1], data[,2], type = "l", col = "#878787", lwd = 1, lty = 2, pch = 1,
           xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
      )
      
      par(new = T)
      plot(data[,1], data[,2], col = "#878787", pch = 1, cex = .6,
           xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
      )
      
      #ADD AXIS AND AXIS LABELS
      axis(side = 1, line = -.5, tck = -.01, labels = NA)
      axis(side = 1, line = -.5, at=xlim, labels=c("",""), lwd.ticks=0)
      axis(side=1, line = -1.2, lwd = 0, cex.axis = 0.8, font.lab=2, las = 0)
      mtext(side = 1, line = 1.5, "Saisons", cex = 1)
      
      axis(side = 2, line = -.8, tck = -.01, labels = NA)
      axis(side =2, line = -.8, at=ylim, labels=c("",""), lwd.ticks=0)
      axis(side=2, line = -1.4, lwd = 0, at = pretty(ylim), labels = pretty(ylim) / 100000, cex.axis = .7, font.lab=2, las = 2)
      mtext(side = 2, line = 0.5, expression(atop("",atop("Consommation d'eau","pour la production de neige ("*10^5*" "*m^3*")"))), cex = 1.4) 
      
      dev.off()
    }
}

plot_wc_quantiles = function (dst_dir,envid, name, snowtype, period){
  #####GRAPH PARAMETERS
  scenarios = c("historical", "rcp26", "rcp45", "rcp85")
  cols = c("#878787", "#08519c","#80cdc1", "#cb181d")
  types = c("poly", "lines")
  delta = (period - 1) / 2
  
  #Test if snowtype exists
  query = paste0("
  select distinct snowtype from viability.wc
    where envid = ",envid," and snowtype = '", snowtype,"' 
  ")
  con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
  snowtest <- dbGetQuery(con, query)
  dbDisconnect(con)
  snowtest = nrow(snowtest)
  if (snowtest == 0){
    print("nothing to plot")
    print("SNOWTYPE DOES NOT EXISTS (NO INTERSECTION ENVELOPES / NC)")
  } else {
  
      #XLIM and YLIM
      
      # Get ski slopes envelopes parts
      query = paste0("select st_area(st_collectionextract(st_intersection(st_union(st_makevalid(a.geom)), b.geom),3))/st_area(b.geom)
        from crosscut.tracks a, crosscut.envelopes b
        where envid = ", envid," and st_intersects(a.geom, b.geom)
        group by b.geom"
      )
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      envpart <- dbGetQuery(con, query)
      dbDisconnect(con)
      envpart = envpart[1,1]
      
      query = paste0("
      select min(season) + ",delta," , max(season) - ",delta,"
      from viability.idx
      ")
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      xlim <- dbGetQuery(con, query)
      dbDisconnect(con)
      xlim = as.numeric(xlim)
      
      query = paste0("
      with a as(
        select season, scenario, avg(wc) wc from viability.wc
        where envid = ",envid," and model != 'safran'
        --and snowtype = '", snowtype,"'
        group by 1,2
        union
        select season, scenario, wc from viability.wc
        where envid = ",envid," and model = 'safran'
        --and snowtype = '", snowtype,"'
        )
        select coalesce(max(wc),0) * ",envpart," from a;")
      con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
      ymax <- dbGetQuery(con, query)
      dbDisconnect(con)
      ymax = as.numeric(ymax)
      ylim = c(0,ymax)
      
      if (ymax == 0){
        print("nothing to plot")
      } else {
          prettyx = pretty(xlim)
          
          #GRAPH OUTPUT
          png(width = 800, height = 600, paste0(dst_dir,tolower(gsub(" ", "_", paste0("wc_quantiles_",envid,"_",snowtype,"_",xlim[1],"_",xlim[2],".png")))), res = 150)
          par(mar=c(3,3,2,0))
          
          ######PLOTTING
          #SETUP DEVICE
         
          typesm = intToUtf8(rev(utf8ToInt(strsplit(intToUtf8(rev(utf8ToInt(snowtype))),'_')[[1]][1])))
          envsm = gsub(paste0("_",typesm),"",snowtype)
          if ( typesm == "lance") {
            st = paste0("neige de culture (",envsm, " - bi-fluide)")
          } else {
            st = paste0("neige de culture (",envsm, " - mono-fluide)")
          }
            
          main = paste0(name," :\n",st)
          plot(0,0, type = 'n', main = main, cex.main = .8, xlim = xlim, ylim = ylim, , axes = F, xlab = NA, ylab =NA)
          
          #PLOT BACKGROUND
          for (i in 1:(length(prettyx) - 1)){
            if (i / 2 == round(i / 2)){
              col = "#F0F0F0"
            } else {
              col = "#FFFFFF"
            }
            print(col)
            polygon(c(prettyx[i],prettyx[i],prettyx[i+1],prettyx[i+1]), c(ylim,rev(ylim)), col = col, border = NA)
          }
          for (j in pretty(ylim)[2:length(pretty(ylim))]){
            lines(xlim, c(j,j), col = 'lightgrey', lty = 2, lwd = 1)
          }
          
          #add actual WC if exists      
          query = paste0(
            "select season, wc from crosscut.actual_wc
            where envid = ",envid,"
            order by season;"
          )
          con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
          wc <- dbGetQuery(con, query)
          dbDisconnect(con)
          
          if (nrow(wc) > 0){
            for (i in 1:nrow(wc)){
                polygon(c(wc[i,1]-.5,wc[i,1]-.5,wc[i,1]+.5,wc[i,1]+.5), c(0,wc[i,2],wc[i,2], 0), col = "#006d2c", border = "black", lwd = .5)
            }
          }
          
          
          
          #PLOT EXPERIMENT TIME SERIES
          for (t in types){
            i = 0
            for (scenario in scenarios){
              print(t)
              print(scenario)
              i = i + 1
              query = paste0("
                             with a as (
                             select a.season,
                             min(min(a.season)) over() smin, max(max(a.season)) over() smax,
                             percentile_cont(0.2) within group (order by b.wc) q20,
                             percentile_cont(0.5) within group (order by b.wc) q50,
                             percentile_cont(0.8) within group (order by b.wc) q80
                             from viability.wc a
                             join viability.wc b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
                             and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
                             where a.snowtype = '",snowtype,"' and a.scenario = '",scenario,"'
                             and a.envid = ", envid,"
                             group by a.season
                             )
                             
                             select season, q20 * ",envpart," q20, q50 * ",envpart," q50, q80 * ",envpart," q80
                             from a 
                             where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
                             order by season"
              )
              con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
              data <- dbGetQuery(con, query)
              dbDisconnect(con)
              print(snowtype)
              
              if (scenario == "historical"){
                query = paste0("
                               with a as (
                               select max(season) - ",delta," hmax from viability.idx
                               where snowtype = '",snowtype,"' and scenario = '",scenario,"'
                               and envid = ", envid,"
                               ), b as (
                               select min(season) + ",delta," emin from viability.idx
                               where snowtype = '",snowtype,"' and scenario != '",scenario,"' and scenario != 'reanalysis'
                               and envid = ", envid,"
                               )
                               
                               select c.season,
                               percentile_cont(0.2) within group (order by d.wc) * ",envpart," q20,
                               percentile_cont(0.5) within group (order by d.wc) * ",envpart," q50,
                               percentile_cont(0.8) within group (order by d.wc) * ",envpart," q80
                               from viability.wc c
                               join viability.wc d on  d.season >= c.season - ",delta," and d.season <= c.season + ",delta,"
                               and d.snowtype = c.snowtype and d.model = c.model and d.scenario = c.scenario and d.envid = c.envid
                               where c.snowtype = '",snowtype,"' and c.envid = ", envid,"
                               and c.season > (select hmax from a) and c.season < (select emin from b)
                               group by c.season
                               order by c.season
                               "
                )
                con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
                hist <- dbGetQuery(con, query)
                dbDisconnect(con)
                data = rbind(data,hist)
              }
              
              #save to csv
              # write.csv(data, paste0(dst_dir,"wc_quantiles_",envid,"_",snowtype,"_",scenario,"_",xlim[1],"_",xlim[2],".csv"), row.names=FALSE)
              
              if (nrow(data) != 0){
                  if (t == "poly"){
                    polygon(c(data[,1], rev(data[,1])), c(sapply(data[,2],no_more_no_less,ylim), rev(sapply(data[,4],no_more_no_less,ylim))),
                            border = paste0(cols[i],alphaborder), col = paste0(cols[i], alphafill))
                  } else {
                    par(new = T)
                    plot(data[,1], data[,3], type = "l", col = cols[i], lwd = 2,
                         xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
                    )
                  }
              } else {
                print("nothing to plot")
              }
            }
          }
          
          #ADD SAFRAN REANALYSIS ROLLING MEAN
          query = paste0("
                         with a as (
                         select a.season, min(min(a.season)) over() smin, max(max(a.season)) over() smax,
                         percentile_cont(0.2) within group (order by b.wc) q20,
                         percentile_cont(0.5) within group (order by b.wc) q50,
                         percentile_cont(0.8) within group (order by b.wc) q80
                         from viability.wc a
                         join viability.wc b on b.season >= a.season - ",delta," and b.season <= a.season + ",delta,"
                         and a.snowtype = b.snowtype and a.model = b.model and a.scenario = b.scenario and a.envid = b.envid
                         where a.snowtype = '",snowtype,"' and a.envid = ", envid,"
                         and a.scenario = 'reanalysis' and a.model = 'safran'
                         group by a.season
                         )
                         
                         
                         select season, q20 * ",envpart," q20, q50 * ",envpart," q50, q80 * ",envpart," q80
                         from a 
                         where a.season >= smin + ",delta," and a.season <= smax - ",delta,"
                         order by season"
          )
          con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
          data <- dbGetQuery(con, query)
          dbDisconnect(con)
          par(new = T)
          plot(data[,1], data[,3], type = "l", col = "black", lwd = 2,
               xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
          )
          
          #ADD SAFRAN REANALYSIS ANNUAL VALUES
          query = paste0("
                         select a.season, wc * ",envpart,"
                         from viability.wc a
                         where a.snowtype = '",snowtype,"' and a.envid = ", envid,"
                         and a.scenario = 'reanalysis' and a.model = 'safran'
                         order by season"
          )
          con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
          data <- dbGetQuery(con, query)
          dbDisconnect(con)
          par(new = T)
          plot(data[,1], data[,2], type = "l", col = "#878787", lwd = 1, lty = 2, pch = 1,
               xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
          )
          
          par(new = T)
          plot(data[,1], data[,2], col = "#878787", pch = 1, cex = .6,
               xlim = xlim, ylim = ylim, axes = F, xlab = NA, ylab =NA
          )
          
          #ADD AXIS AND AXIS LABELS
          axis(side = 1, line = -.5, tck = -.01, labels = NA)
          axis(side = 1, line = -.5, at=xlim, labels=c("",""), lwd.ticks=0)
          axis(side=1, line = -1.2, lwd = 0, cex.axis = 0.8, font.lab=2, las = 0)
          mtext(side = 1, line = 1.5, "Saisons", cex = 1)
          
          axis(side = 2, line = -.8, tck = -.01, labels = NA)
          axis(side =2, line = -.8, at=ylim, labels=c("",""), lwd.ticks=0)
          axis(side=2, line = -1.4, lwd = 0, at = pretty(ylim), labels = pretty(ylim) / 100000, cex.axis = .7, font.lab=2, las = 2)
          mtext(side = 2, line = 0.5, expression(atop("",atop("Consommation d'eau","pour la production de neige ("*10^5*" "*m^3*")"))), cex = 1.4) 
          
          dev.off()
        }
    }
}

###############PLOT IDX, FREQIDX GRAPH FOR ALL ENVELOPES IN IND

inds = c('7331A')
period = 15

for (i in 1:length(inds)){
    ind = inds[i]
    query = paste0("select * from crosscut.resorts where ind = '",ind,"';")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    rname <- dbGetQuery(con, query)
    dbDisconnect(con)
    rname = rname[2]
    dst_dir_base = paste0("/home/francois/data/",rname,"/results/")

    query = paste0("select distinct envid, name from crosscut.envelopes
    where ind = '",ind,"'
    order by 1")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    listenv <- dbGetQuery(con, query)
    dbDisconnect(con)

    query = paste0("select distinct status from crosscut.sm_resort
    where ind = '",ind,"'
    order by 1")
    con = dbConnect(drv, host=host, user=user, password=pwd, dbname=db)
    liststatus <- dbGetQuery(con, query)
    dbDisconnect(con)

    nntypes = c("gro", "nn")
    nntypes = paste0("ns&gro_", nntypes)
    if (nrow(liststatus) > 0){
        # smtypes = c("lance", "fan")
        smtypes = c("lance")
        smtypes2 = c()
        for (i in 1:nrow(liststatus)){
            smtypes2 = c(smtypes2, paste(liststatus[i,1], "_", smtypes, sep = ""))
        }
        snowtypes = c(nntypes, smtypes2)
    } else {
        snowtypes = nntypes
    }
    # print(snowtypes)
    # readline(prompt="Press [enter] to continue")

    q20 = NULL

    for (env in 1:nrow(listenv)){
      envid = listenv[env,1]
      name = listenv[env,2]
      dst_dir = paste0(dst_dir_base,envid,"/plots/")
      dir.create(dst_dir)
      for (snowtype in snowtypes){
        q20type = snowtype
        # print(paste("idx_mean",dst_dir,envid,name,snowtype, period, q20type, q20))
        # plot_idx(dst_dir,envid,name,snowtype, period, q20type, q20)
        print(paste("idx_quantiles", dst_dir,envid,name,snowtype, period, q20type, q20))
        plot_idx_quantiles(dst_dir,envid,name,snowtype, period, q20type, q20)
        # print(paste("freq_mean",dst_dir,envid,name,snowtype, period, q20type, q20))
        # plot_freq(dst_dir,envid,name,snowtype, period, q20type, q20)
        print(paste("freq_quantiles",dst_dir,envid,name,snowtype, period, q20type, q20))
        plot_freq_quantiles(dst_dir,envid,name,snowtype, period, q20type, q20)
        
        typesm = intToUtf8(rev(utf8ToInt(strsplit(intToUtf8(rev(utf8ToInt(snowtype))),'_')[[1]][1])))
        if (typesm == 'lance'){
          print("plot WC now")
          # print(paste("wc_mean", dst_dir,envid,name,snowtype, period))
          # plot_wc(dst_dir,envid,name,snowtype, period)
          print(paste("wc_quantiles", dst_dir,envid,name,snowtype, period))
          plot_wc_quantiles(dst_dir,envid,name,snowtype, period)
        }
      }
    }
}