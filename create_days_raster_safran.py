from osgeo import gdal
import psycopg2
import numpy as np
import os
from subprocess import call
import conn_param
import multiprocessing

def create_geotiff (dst_file, xsize, ysize, src_trans, src_proj, ndv):
    format = "GTiff"
    driver = gdal.GetDriverByName(format)
    if os.path.isfile(dst_file):
        os.remove(dst_file)
    dst_ds = driver.Create(dst_file, xsize, ysize, 1, gdal.GDT_Int16, [ 'TILED=YES', 'COMPRESS=LZW' ])
    dst_ds.SetGeoTransform(src_trans)
    dst_ds.SetProjection(src_proj)
    dst_ds.GetRasterBand(1).SetNoDataValue(ndv)
    return dst_ds
    
def fill_arrays(locarr,daysarr, season, snowtype, ndv):
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    srctab = f"nbdays_safran_reanalysis"
    uloc = np.unique(locarr[locarr != ndv])
    uu = []
    for u in uloc:
        uu.append(u.tolist())
    uloc = tuple(uu)
    
    if len(uloc) > 0: #uloc could be of length 0 when there is no snowmaking intersecting the concerned envelope
        query = f"""
            select loc, nbdays
            from viability.{srctab}
            where season = %s and snowtype = %s
            and loc in %s
        """
        cur.execute(query,(season,snowtype,uloc,))
        
        # loop on loc to fill arrays
        for loc in cur:
            daysarr[locarr == loc[0]] = loc[1]
    return daysarr
        
def season_nbdays_raster_launcher(srcdir, envid):
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    # count number of snowmaking areas
    query = """
        select distinct status from crosscut.sm_resort
        where ind = %s"""
    cur.execute(query,(ind,))
    nbsm = cur.rowcount
    
    if nbsm > 0:
        query = f"""
            select distinct snowtype, season
            from viability.nbdays_available_simulations
            where model = 'safran'
        """
    else:
        query = f"""
            select distinct snowtype, season
            from viability.nbdays_available_simulations
            where model = 'safran'
            and (snowtype = 'nn' or snowtype = 'gro')
        """
    cur.execute(query)
    
    query = f"""
        select distinct snowtype, season
        from viability.nbdays_available_simulations
        where model = 'safran'
    """
    cur.execute(query)
    args = []
    for sim in cur:
        snowtype = sim[0]
        season = sim[1]
        
        dstdir = os.path.join(srcdir,str(envid),"seasonal_rasters")
        if os.path.isdir(dstdir) == False:
            os.mkdir(dstdir)
        dstdir = os.path.join(dstdir,snowtype)
        if os.path.isdir(dstdir) == False:
            os.mkdir(dstdir)
        dstdir = os.path.join(dstdir,'safran')
        if os.path.isdir(dstdir) == False:
            os.mkdir(dstdir)
        
        args.append((srcdir, envid, snowtype, season))
    print("args done")
    pool = multiprocessing.Pool(processes = 10)
    pool.map(season_nbdays_raster, args)
    pool.close()
    
def season_nbdays_raster(args):
    srcdir, envid, snowtype, season = args
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()
    
    locfile = os.path.join(srcdir,str(envid),"loc.tif")
    locrast = gdal.Open(locfile)
    loctrans = locrast.GetGeoTransform()
    locproj = locrast.GetProjection()
    locband = locrast.GetRasterBand(1)
    xsize = locband.XSize
    ysize = locband.YSize
    ndv = locband.GetNoDataValue()
    locarr = locband.ReadAsArray()
    
    # Create output arrays
    dstdir = os.path.join(srcdir,str(envid),"seasonal_rasters", snowtype, "safran")
    daysarr = np.zeros((ysize, xsize), np.float32)
    daysarr[daysarr == 0] = ndv

    if snowtype == 'gro' or snowtype == 'nn':
        # retrieve nbdays for the snowtype, season, and period
        daysarr = fill_arrays(locarr,daysarr, season, snowtype, ndv)

        daysfile = os.path.join(dstdir, f"len_{season}.tif")
        daysrast = create_geotiff (daysfile, xsize, ysize, loctrans, locproj, ndv)
        
        daysrast.GetRasterBand(1).WriteArray(daysarr)
        daysrast = None

    else:
        #Loop on sm status
        query = """
            select distinct status from crosscut.sm_resort
            where ind = %s"""
        cursm = myconn.cursor()
        cursm.execute(query,(ind,))

        for status in cursm:
            status = status[0]
            
            # With snowmaking pixels
            smlocfile = os.path.join(srcdir, str(envid),f'{status}_sm.tif')
            smrast = gdal.Open(smlocfile)
            smarr = smrast.GetRasterBand(1).ReadAsArray()
            daysarr = fill_arrays(smarr, daysarr, season, snowtype, ndv)
            
            # Without snowmaking pixels
            nosmlocfile = os.path.join(srcdir, str(envid),f'{status}_nosm.tif')
            nosmrast = gdal.Open(nosmlocfile)
            nosmarr = nosmrast.GetRasterBand(1).ReadAsArray()
            daysarr = fill_arrays(nosmarr, daysarr, season, 'gro', ndv)
            
            daysfile = os.path.join(dstdir, f"{status}_len_{season}.tif")
            daysrast = create_geotiff (daysfile, xsize, ysize, loctrans, locproj, ndv)
            
            daysrast.GetRasterBand(1).WriteArray(daysarr)
            daysrast = None
    print(f"rast season {season} for {envid} ({snowtype}) done")

# psycopg2 connection to DB
myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()

inds = ['0537']

for ind in inds:
    query = """
        select name from crosscut.resorts
        where ind = %s
    """
    cur.execute(query,(ind,))
    name = cur.fetchone()[0]
    srcdir = os.path.join("/home/francois/data/", name, "results/")

    query = """
        select distinct envid from crosscut.envelopes
        where ind = %s
    """
    cur.execute(query,(ind,))

    for envid in cur:
        envid = envid[0]
        season_nbdays_raster_launcher(srcdir, envid)

