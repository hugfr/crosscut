import psycopg2
import numpy as np
import conn_param
import multiprocessing

####################################################################
# NBDAYS_SRL
# COMPUTATION PERIOD CHANGE (12/15 to 04/15)
# VS WHOLE SEASON FOR NBDAYS (11/01 to 30/04"
####################################################################

def nbdays_launcher():
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur = myconn.cursor()

    # Retrieve available simulations for these zones
    query = """select distinct a.typezone, a.model, a.scenario, a.snowtype, a.season
            from crocus.available_simulations a
            left join viability.nbdays_srl_available_simulations b on
	            a.typezone = b.typezone
	            and a.model = b.model
	            and a.scenario = b.scenario
	            and a.snowtype = b.snowtype
	            and a.season::integer = b.season
            where b.typezone is null
        order by a.typezone, a.model, a.scenario, a.snowtype, a.season"""
    cur.execute(query)
    print(cur.rowcount)
    args = []
    for sim in cur:
        typezone = sim[0]
        model = sim[1]
        scenario = sim[2]
        snowtype = sim[3]
        season = sim[4]
        args.append((typezone, model, scenario, snowtype, season))
    print("args done")
    pool = multiprocessing.Pool(processes = 10)
    pool.map(nbdays, args)
    pool.close()

def nbdays(args):
    typezone, model, scenario, snowtype, season = args
    # psycopg2 connection to DB
    myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
    cur2 = myconn.cursor()
    
    # Create destination table if not exists
    table = f"nbdays_srl_{model}_{scenario}"
    query = f"""
    create table if not exists viability.{table}(
        CONSTRAINT {table}_check
            CHECK (model = '{model}' and scenario = '{scenario}')
    )
    inherits(viability.nbdays_srl);
    create index if not exists
        nbdays_srl_{table}_typezone on viability.{table}(typezone);
    create index if not exists
        nbdays_srl_{table}_season on viability.{table}(season);
    create index if not exists
        nbdays_srl_{table}_snowtype on viability.{table}(snowtype);
    """
    cur2.execute(query)
    myconn.commit()
    print(f'{table} done')
    
    # Insert nbdays_srl for each loc  in the table
    srctab = f'snow_{typezone}_{model}_{scenario}_{snowtype}_{season}'
    query = f"""
        delete from viability.{table}
            where typezone = %s and snowtype = %s and season = %s;
        insert into viability.{table}
        select loc, typezone, season, snowtype, model, scenario, sum(case when swe >= 100 then 1 else 0 end) nbdays
        from crocus.{srctab}
        where ddate between (season||'-12-15')::date and ((season + 1)||'-04-15')::date
        group by loc, typezone, season, model, scenario, snowtype;
    """
    cur2.execute(query,(typezone, snowtype, season,))
    myconn.commit()
    print(f'data {srctab} inserted')

#############################################

myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()

query = """
    create table if not exists viability.nbdays_srl(
        loc integer,
        typezone character varying(50),
        season integer,
        snowtype character varying(50),
        model character varying(50),
        scenario character varying(50),
        nbdays double precision
    );
    
    create index if not exists idx_nbdays_srl_loc on viability.nbdays_srl(loc);
    create index if not exists idx_nbdays_srl_typezone on viability.nbdays_srl(typezone);
    create index if not exists idx_nbdays_srl_season on viability.nbdays_srl(season);
    create index if not exists idx_nbdays_srl_snowtype on viability.nbdays_srl(snowtype);
    create index if not exists idx_nbdays_srl_model on viability.nbdays_srl(model);
    create index if not exists idx_nbdays_srl_scenario on viability.nbdays_srl(scenario);
    
    CREATE MATERIALIZED VIEW IF NOT EXISTS viability.nbdays_srl_available_simulations AS
        SELECT DISTINCT typezone, model, scenario, snowtype, season
            FROM viability.nbdays_srl
        ORDER BY typezone, model, scenario, snowtype, season;
"""
cur.execute(query)
myconn.commit()

nbdays_launcher()

cur.execute("refresh materialized view viability.nbdays_srl_available_simulations")
myconn.commit()
