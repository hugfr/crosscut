#require python 3.7

import psycopg2
import pandas as pd
import os
import conn_param

myconn = psycopg2.connect(f"host={conn_param.host} dbname={conn_param.dbname} user={conn_param.user} password={conn_param.password}")
cur = myconn.cursor()

def crocus_to_csv(zonetype, snowtype, model, scenario):
    #Retrieve avalaible tables

    cur.execute(f"""
        select relname from pg_class
        where relname ilike '%{zonetype}%' and relname ilike '%{snowtype}%'
        and relname ilike '%{model}%' and relname ilike '%{scenario}%' and relname ilike 'snow%'
        order by relname
    """)

    #retrieve data in tables and create or append to pandas df
    cur2 = myconn.cursor()
    i = 0
    for t in cur:
        t = t[0]
        query = f"""
            select ddate, scenario, sd from crocus.{t}
            where loc = 2091
            """
        cur2.execute(query)
        if i == 0:
            data = pd.DataFrame(cur2.fetchall(), columns=['day', 'scenario', 'snowdepth'])
        else:
            data2 = pd.DataFrame(cur2.fetchall(), columns=['day', 'scenario', 'snowdepth'])
            data = data.append(data2, ignore_index = True)
        i = i + 1

        print(f'size: {data.size}')

    #create csv
    dstdir = "/home/francois/pour_sam/"
    if os.path.isdir(dstdir) == False:
        os.mkdir(dstdir)
    file = f"snow_{model}_{scenario}_sd.csv"
    file = os.path.join(dstdir, file)
    data.to_csv(file, index = False)
    
zonetype = 'alp'
snowtype = 'nn'
# model = 'c_a63_c_c_c_c'
# scenarios = ['historical', 'rcp26', 'rcp85']
model = 'safran'
scenarios = ['reanalysis']

for scenario in scenarios:
    print(zonetype, snowtype, model, scenario)
    crocus_to_csv(zonetype, snowtype, model, scenario)
